/*******************************************************************************************/
/********************************************************************************************
This file (config.js) has the list of all required modules imported and assigned to 
an variable ( == > wrapperRequiredModules)

********************************************************************************************/
/*******************************************************************************************/

var wrapperRequiredModules = {};
/* Additional variable declared that are required to the process*/
var iniParserModule = require('parse-ini');
var yamlConfig = require('js-yaml');
var fs = require('fs');
var arrayDiff = require('arr-diff');
var log4js = require('log4js');
var fsExtra = require('fs-extra')
var httpRequest = require('request');
var getSubscriptionObj = require('./api/helpers/Live/Subscription/GetSubscription.js');
var alerterObj = require('./api/helpers/Alerter/EmailTrigger.js');
/* Logger file configuration */
log4js.configure({
    appenders: {
        Wrapper: {
            type: 'file',
            filename: './api/logs/EsWrapper.log'
        }
    },
    categories: {
        default: {
            appenders: ['Wrapper'],
            level: 'error'
        }
    },
    categories: {
        default: {
            appenders: ['Wrapper'],
            level: 'info'
        }
    }
});
/* Logger object is assigned into wrapperRequiredModules */
var logger = log4js.getLogger('Wrapper');
var MongoClient = require('mongodb').MongoClient;
var moment = require('moment');
wrapperRequiredModules.loggerObj = logger;
var sessObj;
/* Modules from NPM */
wrapperRequiredModules.fsExtraObj = fsExtra;
wrapperRequiredModules.parserObj = require('json-parser');
wrapperRequiredModules.jsforceObj = require('jsforce');
wrapperRequiredModules.shObj = require("shorthash");
wrapperRequiredModules.uuidObj = require("uuid/v4");
wrapperRequiredModules.appObj = require('express')();
wrapperRequiredModules.sessionObj = require('express-session');
wrapperRequiredModules.sessObj = sessObj;
wrapperRequiredModules.elasticsearchObj = require('elasticsearch');
wrapperRequiredModules.urlObj = require('url');
wrapperRequiredModules.bodybuilderObj = require('bodybuilder');
wrapperRequiredModules.expressObj = require('express');
wrapperRequiredModules.bodyParserObj = require('body-parser');
wrapperRequiredModules.fsObj = require('fs');
wrapperRequiredModules._ = require('underscore');
wrapperRequiredModules.SalesforceConnectionObj = require('node-salesforce-connection');
wrapperRequiredModules.dateTime = require('node-datetime');
wrapperRequiredModules.dateFormat = require('dateformat');
wrapperRequiredModules.arraySubtract = require('array-subtract');
wrapperRequiredModules.arrayDiff = arrayDiff;
wrapperRequiredModules.moment = moment;
wrapperRequiredModules.request = require('request');
wrapperRequiredModules.MongoClient = MongoClient;
wrapperRequiredModules.unique = require('array-unique').immutable;
wrapperRequiredModules.nodeMailerObj = require('nodemailer');

/* Read configuration files*/

var credentialsObj = yamlConfig.safeLoad(fs.readFileSync('./config/Credentials.yml', 'utf8'));
var sourceIncludeMapping = yamlConfig.safeLoad(fs.readFileSync('./config/SourceIncludeMapping.yml', 'utf8'));
var datePeriodOptions = yamlConfig.safeLoad(fs.readFileSync('./config/DatePeriodOptions.yml', 'utf8'));
var parameterMappingObj = yamlConfig.safeLoad(fs.readFileSync('./config/ParameterMapping.yml', 'utf8'));
var fieldMappingObj = yamlConfig.safeLoad(fs.readFileSync('./config/FieldMapping.yml', 'utf8'));
var errorCode = yamlConfig.safeLoad(fs.readFileSync('./config/ErrorCode.yml', 'utf8'));
var avatarLinks = yamlConfig.safeLoad(fs.readFileSync('./config/Avatar.yml', 'utf8'));

wrapperRequiredModules.getSubscriptionObj = getSubscriptionObj;
wrapperRequiredModules.credentialsObj = credentialsObj;
wrapperRequiredModules.datePeriodOptionsObj = datePeriodOptions;
wrapperRequiredModules.iniParserModuleObj = iniParserModule;
wrapperRequiredModules.sourceIncludeMappingObj = sourceIncludeMapping;
wrapperRequiredModules.avatarLinksObj = avatarLinks;
wrapperRequiredModules.errorCodeObj = errorCode.ResponseCode;
var mongoDBConnectionString = "mongodb://"+credentialsObj.MongoDB.host+":"+credentialsObj.MongoDB.port+"/";
wrapperRequiredModules.mongoDBConnectionString = mongoDBConnectionString;
/************************************************************
 Customized Modules - Helper script are imported as module
 ************************************************************/
/* Live Index helper scripts*/
wrapperRequiredModules.getMetadataObj = require('./api/helpers/Live/Metadata/GetMetaData.js');
wrapperRequiredModules.subscriptionValidatorObj = require('./api/helpers/Live/Validation/SubscriptionValidator.js');
wrapperRequiredModules.beSpokeQueryValidationObj = require('./api/helpers/Live/Validation/BeSpokeQueryValidation.js');
wrapperRequiredModules.getLocationsQueryObj = require('./api/helpers/Live/Query/GetLocationsQuery.js');
wrapperRequiredModules.getSortbyQueryObj = require('./api/helpers/Live/Query/GetSortbyQuery.js');
wrapperRequiredModules.getTimeRangeQueryObj = require('./api/helpers/Live/Query/GetTimeRangeQuery.js');
wrapperRequiredModules.getSectorsQueryObj = require('./api/helpers/Live/Query/GetSectorsQuery.js');
wrapperRequiredModules.getSubscriptionQueryObj = require('./api/helpers/Live/Query/GetSubscriptionQuery.js');
wrapperRequiredModules.getStageQueryObj = require('./api/helpers/Live/Query/GetStageQuery.js');
wrapperRequiredModules.getSizesQueryObj = require('./api/helpers/Live/Query/GetSizesQuery.js');
wrapperRequiredModules.getValuesQueryObj = require('./api/helpers/Live/Query/GetValuesQuery.js');
wrapperRequiredModules.getMaterialsQueryObj = require('./api/helpers/Live/Query/GetMaterialsQuery.js');
wrapperRequiredModules.getContractTypeQueryObj = require('./api/helpers/Live/Query/GetContractTypeQuery.js');
wrapperRequiredModules.getDevelopmentTypeQueryObj = require('./api/helpers/Live/Query/GetDevelopmentTypeQuery.js');
wrapperRequiredModules.getContractDevTypeQueryObj = require('./api/helpers/Live/Query/GetContractDevTypeQuery.js');
wrapperRequiredModules.getPlanningApplicationQueryObj = require('./api/helpers/Live/Query/GetPlanningApplicationQuery.js');
wrapperRequiredModules.getStatusQueryObj = require('./api/helpers/Live/Query/GetStatusQuery.js');
wrapperRequiredModules.getProjectDateQueryObj = require('./api/helpers/Live/Query/GetProjectDateQuery.js');
wrapperRequiredModules.getRoleQueryObj = require('./api/helpers/Live/Query/GetRoleQuery.js');
wrapperRequiredModules.getDimensionsQueryObj = require('./api/helpers/Live/Query/GetDimensionsQuery.js');
wrapperRequiredModules.getProjectQueryObj = require('./api/helpers/Live/Query/GetProjectQuery.js');
wrapperRequiredModules.getCompanyContactQueryObj = require('./api/helpers/Live/Query/GetCompanyContactQuery.js');

/* HE Index helper scripts*/
wrapperRequiredModules.getMetadataHEObj = require('./api/helpers/HE/Metadata/GetMetaData.js');
wrapperRequiredModules.subscriptionValidatorHEObj = require('./api/helpers/HE/Validation/SubscriptionValidator.js');
wrapperRequiredModules.beSpokeQueryValidationHEObj = require('./api/helpers/HE/Validation/BeSpokeQueryValidation.js');
wrapperRequiredModules.getLocationsQueryHEObj = require('./api/helpers/HE/Query/GetLocationsQuery.js');
wrapperRequiredModules.getSortbyQueryHEObj = require('./api/helpers/HE/Query/GetSortbyQuery.js');
wrapperRequiredModules.getTimeRangeQueryHEObj = require('./api/helpers/HE/Query/GetTimeRangeQuery.js');
wrapperRequiredModules.getSectorsQueryHEObj = require('./api/helpers/HE/Query/GetSectorsQuery.js');
wrapperRequiredModules.getSubscriptionQueryHEObj = require('./api/helpers/HE/Query/GetSubscriptionQuery.js');
wrapperRequiredModules.getStageQueryHEObj = require('./api/helpers/HE/Query/GetStageQuery.js');
wrapperRequiredModules.getSizesQueryHEObj = require('./api/helpers/HE/Query/GetSizesQuery.js');
wrapperRequiredModules.getValuesQueryHEObj = require('./api/helpers/HE/Query/GetValuesQuery.js');
wrapperRequiredModules.getDevelopmentTypeQueryHEObj = require('./api/helpers/HE/Query/GetDevelopmentTypeQuery.js');
wrapperRequiredModules.getPlanningApplicationQueryHEObj = require('./api/helpers/HE/Query/GetPlanningApplicationQuery.js');
wrapperRequiredModules.getStatusQueryHEObj = require('./api/helpers/HE/Query/GetStatusQuery.js');
wrapperRequiredModules.getRoleQueryHEObj = require('./api/helpers/HE/Query/GetRoleQuery.js');
wrapperRequiredModules.getProjectQueryHEObj = require('./api/helpers/HE/Query/GetProjectQuery.js');


/* V2 helper scripts for Notes, Tag and Follow*/
wrapperRequiredModules.getOfficeFollowQueryObj = require('./api/helpers/V2/NotesTagFollow/Query/GetOfficeFollowQuery.js');
wrapperRequiredModules.getOfficeNotesQueryObj = require('./api/helpers/V2/NotesTagFollow/Query/GetOfficeNotesQuery.js');
wrapperRequiredModules.getOfficeTagsLinkQueryObj = require('./api/helpers/V2/NotesTagFollow/Query/GetOfficeTagsLinkQuery.js');
wrapperRequiredModules.getOfficeTagsQueryObj = require('./api/helpers/V2/NotesTagFollow/Query/GetOfficeTagsQuery.js');
wrapperRequiredModules.getProjectFollowQueryObj = require('./api/helpers/V2/NotesTagFollow/Query/GetProjectFollowQuery.js');
wrapperRequiredModules.getProjectNotesQueryObj = require('./api/helpers/V2/NotesTagFollow/Query/GetProjectNotesQuery.js');
wrapperRequiredModules.getContactNotesQueryObj = require('./api/helpers/V2/NotesTagFollow/Query/GetContactNotesQuery.js');
wrapperRequiredModules.getProjectTagsLinkQueryObj = require('./api/helpers/V2/NotesTagFollow/Query/GetProjectTagsLinkQuery.js');
wrapperRequiredModules.getContactTagsLinkQueryObj = require('./api/helpers/V2/NotesTagFollow/Query/GetContactTagsLinkQuery.js');
wrapperRequiredModules.getProjectTagsQueryObj = require('./api/helpers/V2/NotesTagFollow/Query/GetProjectTagsQuery.js');
wrapperRequiredModules.getContactTagsQueryObj = require('./api/helpers/V2/NotesTagFollow/Query/GetContactTagsQuery.js');
wrapperRequiredModules.getSalesforceQueryObj = require('./api/helpers/V2/NotesTagFollow/Query/GetSalesforceData.js');
wrapperRequiredModules.myDayTaskObj = require('./api/helpers/V2/MyDay/MyDayTask.js');
wrapperRequiredModules.getMyDayEventObj = require('./api/helpers/V2/MyDay/GetMyDayEvent.js');
wrapperRequiredModules.taskQueueObj = require('./api/helpers/V2/TaskApi/TaskQueue.js');
wrapperRequiredModules.followAlertObj = require('./api/helpers/V2/FollowAlert/GetUpcomingFollows.js');
wrapperRequiredModules.getLastTransactionDataObj = require('./api/helpers/V2/Tracking/GetLastTransactionData.js');

wrapperRequiredModules.ProjectEnquireRequestObj = require('./api/helpers/V2/Projects/ProjectEnquiryRequest.js');

/* Elastic Index Host reading form Credentials.yml assigned in a variable elasticIndexHost */
var dataObject = credentialsObj.ElasticIndex
if (dataObject.http) {
    wrapperRequiredModules.elasticIndexHost = 'http://' + dataObject.http.host + ':' + dataObject.http.port;
} else if (dataObject.https) {
    wrapperRequiredModules.elasticIndexHost = 'https://' + dataObject.username + ':' + dataObject.password + '@' + dataObject.https.host + ':' + dataObject.https.port;
}
wrapperRequiredModules.elasticIndexName = dataObject.index;
wrapperRequiredModules.elasticTypeName = dataObject.type;
wrapperRequiredModules.elasticContactTypeName = dataObject.contact_type;
wrapperRequiredModules.elasticCompanyTypeName = dataObject.company_type;
wrapperRequiredModules.elasticHistoricIndexName = dataObject.historic_index;
wrapperRequiredModules.elasticHeIndexName = dataObject.he_index;
wrapperRequiredModules.elasticTestIndexName = dataObject.test_index;
wrapperRequiredModules.elasticTestHeIndexName = dataObject.test_he_index;
wrapperRequiredModules.elasticTestHistoricalIndexName = dataObject.test_historic_index;
wrapperRequiredModules.elasticProjectNotesTypeName = dataObject.type_project_note;
wrapperRequiredModules.elasticOfficeNotesTypeName = dataObject.type_office_note;
wrapperRequiredModules.elasticContactNotesTypeName = dataObject.type_contact_note;
wrapperRequiredModules.elasticProjectTagsTypeName = dataObject.type_project_tag;
wrapperRequiredModules.elasticOfficeTagsTypeName = dataObject.type_office_tag;
wrapperRequiredModules.elasticContactTagsTypeName = dataObject.type_contact_tag;
wrapperRequiredModules.elasticProjectFollowTypeName = dataObject.type_project_follow;
wrapperRequiredModules.elasticOfficeFollowTypeName = dataObject.type_office_follow;
wrapperRequiredModules.elasticProjectTagsLinkTypeName = dataObject.type_project_tag_link;
wrapperRequiredModules.elasticOfficeTagsLinkTypeName = dataObject.type_office_tag_link;
wrapperRequiredModules.elasticContactTagsLinkTypeName = dataObject.type_contact_tag_link;
wrapperRequiredModules.elasticMyDayTypeName = dataObject.myDay_type;
wrapperRequiredModules.elasticPageSize = dataObject.size;
wrapperRequiredModules.mobileSize = dataObject.mobileSize;
wrapperRequiredModules.elasticFollowAlertsTypeName = dataObject.type_follow_alert;
wrapperRequiredModules.elasticTrackingIndexName = dataObject.tracking_index;
wrapperRequiredModules.elasticTrackingTypeName = dataObject.tracking_type;

wrapperRequiredModules.convertComma2Array = convertCommaValues2Array; /* Calling convertCommaValues2Array function */
wrapperRequiredModules.getUniqueValues = getUniqueValues; /* Calling getUniqueValues function */
wrapperRequiredModules.paginationObj = pagination; /* Calling pagination function */
wrapperRequiredModules.getIncludeFieldListObj = getIncludeFieldList; /* Calling getIncludeFieldList function */
wrapperRequiredModules.paramsValidationAndGetSubscriptionObj = paramsValidation_GetSubscription; /* Calling paramsValidation_GetSubscription function */
wrapperRequiredModules.paramsValidationGetSubscriptionWOTokenObj = paramsValidation_GetSubscription_WOToken; /* Calling paramsValidation_GetSubscription function */
wrapperRequiredModules.getResultObj = getResult; /* Calling getResult function */
wrapperRequiredModules.getMResultObj = getMResult; /* Calling getResult function */
wrapperRequiredModules.getBeSpokeResultObj = getBeSpokeResult; /* Calling getBeSpokeResult function */
wrapperRequiredModules.getResultHEObj = getHEResult; /* Calling getHEResult function */
wrapperRequiredModules.getBeSpokeResultHEObj = getBeSpokeHEResult; /* Calling getBeSpokeHEResult function */
wrapperRequiredModules.removeUnwantedFieldsObj = removeUnwantedFields; /* Calling removeUnwantedFields function */
wrapperRequiredModules.getResultNotesTagFollow = getResultNotesTagFollow; /* Calling Result function */

wrapperRequiredModules.updateDocumentES = updateDocumentES; /* Calling Update function */
wrapperRequiredModules.updateMultipleDocumentES = updateMultipleDocumentES; /* Calling Update function */
wrapperRequiredModules.createDocumentES = createDocumentES; /* Calling Create function */
wrapperRequiredModules.deleteDocumentES = deleteDocumentES; /* Calling Delete function */
wrapperRequiredModules.checkExistance = checkExistance;

wrapperRequiredModules.updateDocumentESWithParent = updateDocumentESWithParent; /* Calling Update function */
wrapperRequiredModules.createDocumentESWithParent = createDocumentESWithParent; /* Calling Create function */
wrapperRequiredModules.deleteDocumentESWithParent = deleteDocumentESWithParent; /* Calling Delete function */
wrapperRequiredModules.checkExistanceWithParent = checkExistanceWithParent; /* Calling getBeSpokeHEResult function */

wrapperRequiredModules.getRecordCount = getRecordCount; /* Calling getRecordCount function */
wrapperRequiredModules.getMyDayEvents = getMyDayEvents; /* Calling getMyDayEvents function */
wrapperRequiredModules.taskQueueRequest = taskQueueRequest; /* Calling getMyDayEvents function */
wrapperRequiredModules.taskActionRequest = taskActionRequest; /* Calling taskActionRequest function */
wrapperRequiredModules.getUpcomingFollowAlerts = getUpcomingFollowAlerts;
wrapperRequiredModules.getTrackingData = getTrackingData;
wrapperRequiredModules.SFConnection = "";

exports.requiredModulesExport = wrapperRequiredModules; /* Exporting all wrapper required modules into the variable requiredModulesExport */


/******************************************************************************
Function: convertCommaValues2Array
Argument: comma separated string
Return: array
Usage:
    1. To parse comma (,) separated values and store the values into an list 
        (array).
*******************************************************************************/
function convertCommaValues2Array(commaSeparatedStr) {

    var commaValues = [].slice.call(arguments, 1),
        i = 0;

    return commaSeparatedStr.replace(/%s/g, function () {
        return commaValues[i++];
    });
}

/******************************************************************************
Function: Pagination
Argument: user request
Return: Start offset and Size
Usage:
    1. To get from and to values which is used to display result from elastic index 
        using the requested parameters `Page` and `Size`.
        -- default value is from is 0 and Size is 50.
    2. from ==> to get the offset from the first result you want to fetch
       size ==> to get maximum amount of hits to be returned
*******************************************************************************/
function pagination(request) {
    var page = request.query.Page
    var size = request.query.Size
    var startNumber = 0;
    if (size == undefined) {
        size = dataObject.size;
    }
    try {
        if (page == 1) {
            startNumber = 0;
        } else if (page > 1) {
            startNumber = (page - 1) * size + 1;
        }
    } catch (err) {
        startNumber = 0;
    }
    return [startNumber, size];
}

/******************************************************************************
Function: getUniqueValues
Argument: Array
Return: Array
Usage:
    1. To get unique values from an array values
*******************************************************************************/
function getUniqueValues(arrayValue) {

    var uniqueRegions = arrayValue.filter(function (elem, index, self) {
        return index === self.indexOf(elem);
    });
    return uniqueRegions;
}

/******************************************************************************
Function: getIncludeFieldList
Argument: accountID
Return: fieldsList
Usage:
    1. To get the Source Include fields from `SourceIncludeMapping.yml` based on 
        AccountID and assigned into `fieldsList` object
*******************************************************************************/
function getIncludeFieldList(accountID) {
    var fieldsList;
    var idCheck = 0;
    if ((accountID == undefined) || (accountID == "default")) {
        fieldsList = sourceIncludeMapping.SourceFields.Default;
    } else {
        for (var categoryName in sourceIncludeMapping.AccountToMapping) {
            if (sourceIncludeMapping.AccountToMapping[categoryName].includes(accountID) == true) {
                idCheck = 1;
                fieldsList = sourceIncludeMapping.SourceFields[categoryName];
            }
        }
    }
    if ((fieldsList == undefined) && (idCheck == 0)) {
        return sourceIncludeMapping.SourceFields.Default;
    } else if (fieldsList == undefined) {
        return {
            Error: "AccountToMapping field is not found in SourceFields Section"
        };
    } else {
        return fieldsList;
    }
}

/******************************************************************************
Function: getIncludeFieldListHE
Argument: accountID
Return: fieldsList
Usage:
    1. To get the Source Include fields in Hi Index from `SourceIncludeMapping.yml` based on 
        AccountID and assigned into `fieldsList` object
*******************************************************************************/
function getIncludeFieldListHE(accountID) {
    var fieldsList;
    var idCheck = 0;
    if ((accountID == undefined) || (accountID == "default")) {
        fieldsList = sourceIncludeMapping.HISourceFields.Default;
    } else {
        for (var categoryName in sourceIncludeMapping.HIAccountToMapping) {
            if (sourceIncludeMapping.HIAccountToMapping[categoryName].includes(accountID) == true) {
                idCheck = 1;
                fieldsList = sourceIncludeMapping.HISourceFields[categoryName];
            }
        }
    }
    if ((fieldsList == undefined) && (idCheck == 0)) {
        return sourceIncludeMapping.HISourceFields.Default;
    } else if (fieldsList == undefined) {
        return {
            Error: "AccountToMapping field is not found in SourceFields Section"
        };
    } else {
        return fieldsList;
    }
}

/******************************************************************************
Function: paramsValidation_GetSubscription
Argument: user request
Return: subscriptionJSON, fieldsList, limits, request
Usage:
    1. Validate the request parameters
    2. To get the result offset and size from calling the Pagination function
        and pushed into `limits` array.
    3. To get the Subscription from MongoDB using key and assigned it into 
        `subscriptionJSON` object.
    4. To get the Source Include fields from `SourceIncludeMapping.yml` based on 
        AccountID and assigned into `fieldsList` object
*******************************************************************************/
function paramsValidation_GetSubscription(request, callback) {
    try {
        
        sessObj=request.session;
        var appToken = request.query["app-token"];

        MongoClient.connect(mongoDBConnectionString, function(err, db) {
            if(err){
              console.log('Failed - MongoDB connection Failed');
            }else{
              var appTokenDetails = credentialsObj.AppToken;
              var dbo = db.db(appTokenDetails.DBName);
              // console.log(req.query.UserId)
              dbo.collection(appTokenDetails.collectionName).find({"UserId": request.query.UserId}).toArray(function (err, result) {
                // console.log(result)
                if (result.length > 0 ){
                  var expireDate = moment(result[0].expireDateTime,'YYYY-MM-DDTHH:mm:ss.SSS[Z]').toISOString();
                  var currentDate = moment().toISOString();
                  if(expireDate < currentDate){
                    dbo.collection(appTokenDetails.collectionName).deleteOne({"UserId": request.query.UserId}, function(err, results) {
                          db.close();
                          callback({
                            "Error": "App Token is expired"
                        }, null);
                      });
                  }else if(result[0].Token != appToken){
                    db.close();
                    callback({
                      "Error": "App Token is not matched"
                  }, null);
                  }else{
                    
                    var dupCheck = 0;
                    var emptyCheck = 0;
                    var keyValues = Object.values(request.query);
                    var curlyBracketCheck = JSON.stringify(request.query).toString().slice(1, -1);
                    var userID = request.query.UserId;
                    // console.log("userID",userID)
                    var size = request.query.Size
                    if (userID == undefined) {
                        try {
                            userID = request.headers.UserId;
                        } catch (err) {
                            userID = undefined
                        }
                    }
                    var limits = pagination(request);
                    keyValues.forEach(function (listData) {
                        if ((typeof listData) == "object") {
                            dupCheck = 1;
                        }
                        if (listData == '') {
                            emptyCheck = 1;
                        }
                    });
                    var basePathRegex = /^\/([\w\W]*?)(?:\?|$)/g;
                    var basePath = basePathRegex.exec(request.url);
                    var mappedParameters = parameterMappingObj.ParameterData[basePath[1]].split(',').map(function (x) {
                        return x.trim()
                    });
                    var invalidParamsList = arrayDiff(Object.keys(request.query), mappedParameters);
                    if ((request.query[credentialsObj.InHouseCredential.parameter] != undefined) && (request.query[credentialsObj.InHouseCredential.parameter] != credentialsObj.InHouseCredential.value)) {
                        callback({
                            Error: 'Parameter "gleniganKey" value is invalid.'
                        }, null);
                    } else if (curlyBracketCheck.indexOf('}') > -1) {

                        callback({
                            Error: "Curly brackets are not allowed in the Parameters"
                        }, null);
                    } else if (invalidParamsList.length > 0) {
                        var errorData = "Invalid parameter - " + invalidParamsList;
                        callback({
                            Error: errorData
                        }, null);
                    } else if (dupCheck == 1) {
                        callback({
                            Error: "Duplicate parameter not allowed"
                        }, null);
                    } else if (emptyCheck == 1) {
                        callback({
                            Error: "Parameter value should not be empty"
                        }, null);
                    } else if (userID == undefined) {
                        callback({
                            Unauthorized: "Key Missing"
                        }, null);
                    } else if (size > dataObject.size) {
                        callback({
                            Error: "Size limit should not be greater than "+dataObject.size+"."
                        }, null);
                    } else {
                        
                        getSubscriptionObj.getSubscriptionJSON(userID, function (error, subscriptionList) {
                            
                            if (error) {
                                var appTokenDetails = credentialsObj.AppToken;
                                var dbo = db.db(appTokenDetails.DBName);
                                dbo.collection(appTokenDetails.collectionName).find({"UserId": request.query.UserId}).toArray(function (err, result) {
                                    if (result.length > 0 ){
                                        dbo.collection(appTokenDetails.collectionName).deleteOne({"UserId": request.query.UserId}, function(err, results) {
                                            db.close();
                                            callback(error, null);
                                        });
                                    }else{
                                        db.close();
                                        callback(error, null);
                                    }
                                    
                                  })
                            }else if(subscriptionList.length == 0){
                                callback({"Error" : "Invalid User ID."}, null);
                            } 
                            else {
                                db.close();
                                var subscriptionJSON = subscriptionList[0].Subscription_JSON__c;
                                var accountId = subscriptionList[0].Id;
                                var noteSharing = subscriptionList[0].GP_NoteSharingEnabled__c;
                                var fieldsList;
                                if (request.url.includes('_hi/') > 0) {
                                    fieldsList = getIncludeFieldListHE(accountId);
                                    if (fieldsList.hasOwnProperty('Error') == true) {
                                        callback(fieldsList, null);
                                    } else {
                                        request.AccountId = accountId;
                                        request.NoteSharing = noteSharing;
                                        var jsonObject = JSON.parse(subscriptionJSON);
                                        request.HasSmall = jsonObject.Subscription.HasSmall;
                                        request.AccountName = jsonObject.Name;
                                        request.HasHistoricAPI = jsonObject.HasHistoricalAPI;
                                        var maxPage = jsonObject.PageLimit;
                                        if (maxPage < request.query.Page) {
                                            alerterObj.callEmailAlert(jsonObject.Name, request.query.Page, function (err, res) {
                                                console.log(err);
                                            });
                                            callback({ Error: "Request page size is out of range" }, null);
                                        } else {
                                            callback(null, subscriptionJSON, fieldsList, limits, request);
                                        }
                                    }
                                } else {
                                    fieldsList = getIncludeFieldList(accountId);
                                    if (fieldsList.hasOwnProperty('Error') == true) {
                                        callback(fieldsList, null);
                                    } else {
                                        request.AccountId = accountId;
                                        request.NoteSharing = noteSharing;
                                        var jsonObject = JSON.parse(subscriptionJSON);
                                        request.HasSmall = jsonObject.Subscription.HasSmall;
                                        request.AccountName = jsonObject.Name;
                                        request.HasHistoricAPI = jsonObject.HasHistoricalAPI;
                                        var maxPage = jsonObject.PageLimit;
                                        if ((maxPage == undefined) && (100 < request.query.Page)) {
                                            alerterObj.callEmailAlert(jsonObject.Name, request.query.Page, function (err, res) {
                                                console.log(err);
                                            });
                                            callback({ Error: "Request page size is out of range" }, null);
                                        }
                                        else if (maxPage < request.query.Page) {
                                            alerterObj.callEmailAlert(jsonObject.Name, request.query.Page, function (err, res) {
                                                console.log(err);
                                            });
                                            callback({ Error: "Request page size is out of range" }, null);
                                        } else {
                                            callback(null, subscriptionJSON, fieldsList, limits, request);
                                        }
                                    }
                                }
                            }
                        });
                    }
                  }
                }else{
                    db.close();
                   callback({
                        "Error": "Token is not generated"
                    }, null);
                }
                
              })
            }
          })
    } catch (err) {
        // console.log("err", err)
        callback({
            "Error": "Parameters are not mapped into this Endpoint"
        }, null);
    }
}

/******************************************************************************
Function: paramsValidation_GetSubscription_WOToken
Argument: user request
Return: subscriptionJSON, fieldsList, limits, request
Usage:
    1. Validate the request parameters without app-token
    2. To get the result offset and size from calling the Pagination function
        and pushed into `limits` array.
    3. To get the Subscription from MongoDB using key and assigned it into 
        `subscriptionJSON` object.
    4. To get the Source Include fields from `SourceIncludeMapping.yml` based on 
        AccountID and assigned into `fieldsList` object
*******************************************************************************/
function paramsValidation_GetSubscription_WOToken(request, callback) {
    try {
        var dupCheck = 0;
        var emptyCheck = 0;
        var keyValues = Object.values(request.query);
        var curlyBracketCheck = JSON.stringify(request.query).toString().slice(1, -1);
        var userID = request.query.UserId;
        // console.log("userID",userID)
        var size = request.query.Size
        if (userID == undefined) {
            try {
                userID = request.headers.UserId;
            } catch (err) {
                userID = undefined
            }
        }
        var limits = pagination(request);
        keyValues.forEach(function (listData) {
            if ((typeof listData) == "object") {
                dupCheck = 1;
            }
            if (listData == '') {
                emptyCheck = 1;
            }
        });
        var basePathRegex = /^\/([\w\W]*?)(?:\?|$)/g;
        var basePath = basePathRegex.exec(request.url);
        var mappedParameters = parameterMappingObj.ParameterData[basePath[1]].split(',').map(function (x) {
            return x.trim()
        });
        var invalidParamsList = arrayDiff(Object.keys(request.query), mappedParameters);
        if ((request.query[credentialsObj.InHouseCredential.parameter] != undefined) && (request.query[credentialsObj.InHouseCredential.parameter] != credentialsObj.InHouseCredential.value)) {
            callback({
                Error: 'Parameter "gleniganKey" value is invalid.'
            }, null);
        } else if (curlyBracketCheck.indexOf('}') > -1) {

            callback({
                Error: "Curly brackets are not allowed in the Parameters"
            }, null);
        } else if (invalidParamsList.length > 0) {
            var errorData = "Invalid parameter - " + invalidParamsList;
            callback({
                Error: errorData
            }, null);
        } else if (dupCheck == 1) {
            callback({
                Error: "Duplicate parameter not allowed"
            }, null);
        } else if (emptyCheck == 1) {
            callback({
                Error: "Parameter value should not be empty"
            }, null);
        } else if (userID == undefined) {
            callback({
                Unauthorized: "Key Missing"
            }, null);
        } else if (size > dataObject.size) {
            callback({
                Error: "Size limit should not be greater than "+dataObject.size+"."
            }, null);
        } else {
            var dev_users = credentialsObj.InternalUsers.dev;
            if(dev_users.includes(userID) > 0){
                callback(null, [], [], limits, request);
            }else{
                getSubscriptionObj.getSubscriptionJSON(userID, function (error, subscriptionList) {
                    
                    if (error) {
                        callback(error, null);
                    }else if(subscriptionList.length == 0){
                        callback({"Error" : "Invalid User ID."}, null);
                    } 
                    else {
                        var subscriptionJSON = subscriptionList[0].Subscription_JSON__c;
                        var accountId = subscriptionList[0].Id;
                        var noteSharing = subscriptionList[0].GP_NoteSharingEnabled__c;
                        var fieldsList;
                        if (request.url.includes('_hi/') > 0) {
                            fieldsList = getIncludeFieldListHE(accountId);
                            if (fieldsList.hasOwnProperty('Error') == true) {
                                callback(fieldsList, null);
                            } else {
                                request.AccountId = accountId;
                                request.NoteSharing = noteSharing;
                                var jsonObject = JSON.parse(subscriptionJSON);
                                request.HasSmall = jsonObject.Subscription.HasSmall;
                                request.AccountName = jsonObject.Name;
                                request.HasHistoricAPI = jsonObject.HasHistoricalAPI;
                                var maxPage = jsonObject.PageLimit;
                                if (maxPage < request.query.Page) {
                                    alerterObj.callEmailAlert(jsonObject.Name, request.query.Page, function (err, res) {
                                        console.log(err);
                                    });
                                    callback({ Error: "Request page size is out of range" }, null);
                                } else {
                                    callback(null, subscriptionJSON, fieldsList, limits, request);
                                }
                            }
                        } else {
                            fieldsList = getIncludeFieldList(accountId);
                            if (fieldsList.hasOwnProperty('Error') == true) {
                                callback(fieldsList, null);
                            } else {
                                request.AccountId = accountId;
                                request.NoteSharing = noteSharing;
                                var jsonObject = JSON.parse(subscriptionJSON);
                                request.HasSmall = jsonObject.Subscription.HasSmall;
                                request.AccountName = jsonObject.Name;
                                request.HasHistoricAPI = jsonObject.HasHistoricalAPI;
                                var maxPage = jsonObject.PageLimit;
                                if ((maxPage == undefined) && (100 < request.query.Page)) {
                                    alerterObj.callEmailAlert(jsonObject.Name, request.query.Page, function (err, res) {
                                        console.log(err);
                                    });
                                    callback({ Error: "Request page size is out of range" }, null);
                                }
                                else if (maxPage < request.query.Page) {
                                    alerterObj.callEmailAlert(jsonObject.Name, request.query.Page, function (err, res) {
                                        console.log(err);
                                    });
                                    callback({ Error: "Request page size is out of range" }, null);
                                } else {
                                    callback(null, subscriptionJSON, fieldsList, limits, request);
                                }
                            }
                        }
                    }
                });
            }
        }

    } catch (err) {
        // console.log("err", err)
        callback({
            "Error": "Parameters are not mapped into this Endpoint"
        }, null);
    }
}


/******************************************************************************
Function: removeUnwantedFields
Argument: response from elastic index
Return: modified response 
Usage:
    1. Used to remove unwanted fields from elastic result
*******************************************************************************/

function removeUnwantedFields(response) {
    response = response.hits;
    delete response.max_score;
    response.results = response.hits
    delete response.hits;
    var sourceData = JSON.stringify(response);
    sourceData = sourceData.replace(/\"_index\":\s*\"[^\"]*?\"\,/ig, '');
    sourceData = sourceData.replace(/\"_type\":\s*\"[^\"]*?\"\,/ig, '');
    sourceData = sourceData.replace(/\"_score\":\s*(?:\"[^\"]*?\"|\s*[\d\.]+\s*|null)\,/ig, '');
    sourceData = sourceData.replace(/_id/g, 'id');
    sourceData = sourceData.replace(/_source/g, 'source');
    // sourceData = sourceData.replace(/\,\s*\"sort\"\:\s*\[[^\]]*?\]/g, '');

    return JSON.parse(sourceData);
}

/******************************************************************************
Function: removeUnwantedFieldsFollowAlert
Argument: response from elastic index
Return: modified response 
Usage:
    1. Used to remove unwanted fields from elastic result
*******************************************************************************/

function removeUnwantedFieldsFollowAlert(response) {
    response.hits.aggregations = response.aggregations;
    response = response.hits;
    delete response.max_score;
    try{
        delete response.aggregations.by_type.doc_count_error_upper_bound;
        delete response.aggregations.by_type.sum_other_doc_count;
    }catch(error){
        
    }
    
    response.results = response.hits
    // response.aggregations = response.aggregations
    delete response.hits;
    var sourceData = JSON.stringify(response);
    sourceData = sourceData.replace(/\"_index\":\s*\"[^\"]*?\"\,/ig, '');
    sourceData = sourceData.replace(/\"_type\":\s*\"[^\"]*?\"\,/ig, '');
    sourceData = sourceData.replace(/\"userId\"\:/ig , '"UserId":');
    sourceData = sourceData.replace(/\"dateSent\"\:/ig , '"DateSent":');
    sourceData = sourceData.replace(/\"type\"\:/ig , '"Type":');
    sourceData = sourceData.replace(/\"alertType\"\:/ig , '"AlertType":');
    sourceData = sourceData.replace(/\"createdDateTime\"\:/ig , '"CreatedDateTime":');
    sourceData = sourceData.replace(/\"HasImage\"\:/ig , '"hasImage":');
    sourceData = sourceData.replace(/\"secondaryHeading\"\:/ig , '"SecondaryHeading":');
    sourceData = sourceData.replace(/\"primaryHeading\"\:/ig , '"PrimaryHeading":');
    sourceData = sourceData.replace(/\"otherChanges\"\:/ig , '"OtherChanges":');
    sourceData = sourceData.replace(/\"updates\"\:/ig , '"Updates":');
    sourceData = sourceData.replace(/\"projectTown\"\:/ig , '"ProjectTown":');
    sourceData = sourceData.replace(/\"projectLatestInformation\"\:/ig , '"ProjectLatestInformation":');
    sourceData = sourceData.replace(/\"projectName\"\:/ig , '"ProjectName":');
    sourceData = sourceData.replace(/\"projectId\"\:/ig , '"ProjectId":');
    sourceData = sourceData.replace(/\"officeId\"\:/ig , '"OfficeId":');
    sourceData = sourceData.replace(/\"accountId\"\:/ig , '"AccountId":');
    sourceData = sourceData.replace(/\"officeName\"\:/ig , '"OfficeName":');
    sourceData = sourceData.replace(/\"relationshipType\"\:/ig , '"RelationshipType":');
    sourceData = sourceData.replace(/\"key\":\s*[0-9]+\s*\,/ig, '');
    sourceData = sourceData.replace(/\"_score\":\s*(?:\"[^\"]*?\"|\s*[\d\.]+\s*|null)\,/ig, '');
    sourceData = sourceData.replace(/_id/g, 'id');
    sourceData = sourceData.replace(/_source/g, 'source');
    sourceData = sourceData.replace(/\,\s*\"results\"\:\s*\[\]/g, '');

    return JSON.parse(sourceData);
}

/******************************************************************************
Function: getResult
Argument: exportedRequiredModules, elasticServerClient, fieldsList, limits, 
            queryValue, request
Return: output response
Usage:
    1. Added Historical index name with live index if the account has 
        `HasHistoricalAPI` field is true.
    2. Search the query into live elastic index.
    3. Record the query into log file if the `QueryLogs` is enable in 
        `Credentials.yml`
    4. return the live elastic index response.
    
*******************************************************************************/
function getResult(exportedRequiredModules, elasticServerClient, fieldsList, limits, queryValue, request, callback) {
    // console.log("queryValue",queryValue)

    var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    var indexName = '';

    var accountName = exportedRequiredModules.credentialsObj.TestAccount;

    if (((request.AccountName == accountName.AccountNameLive) || (request.AccountName == accountName.AccountNameLiveAndHI)) && (request.HasHistoricAPI == "true")) {
        indexName = exportedRequiredModules.elasticTestIndexName + "," + exportedRequiredModules.elasticTestHistoricalIndexName
    } else if ((request.AccountName == accountName.AccountNameLive) || (request.AccountName == accountName.AccountNameLiveAndHI)) {
        indexName = exportedRequiredModules.elasticTestIndexName
    } else if (request.HasHistoricAPI == "true") {
        indexName = exportedRequiredModules.elasticIndexName + "," + exportedRequiredModules.elasticHistoricIndexName
    } else {
        indexName = exportedRequiredModules.elasticIndexName
    }
    // console.log("indexName"+indexName);
    elasticServerClient.search({
        index: indexName,
        type: exportedRequiredModules.elasticTypeName,
        from: limits[0],
        size: limits[1],
        body: finalQuery
    }).then(function (resp) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/Live/Endpoints/')) {
                fsExtra.ensureDirSync('./api/logs/Live/Endpoints/');
            }
            fs.appendFileSync('./api/logs/Live/Endpoints/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback(null, removeUnwantedFields(resp));
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/Live/Endpoints/')) {
                fsExtra.ensureDirSync('./api/logs/Live/Endpoints/');
            }
            fs.appendFileSync('./api/logs/Live/Endpoints/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback({
            "Error": "Unable to fetch data from source"
        }, null);
    });
}
/******************************************************************************
Function: getMResult
Argument: exportedRequiredModules, elasticServerClient, fieldsList, limits, 
            queryValue, request
Return: output response
Usage:
    1. Added Historical index name with live index if the account has 
        `HasHistoricalAPI` field is true.
    2. Search the query into live elastic index.
    3. Record the query into log file if the `QueryLogs` is enable in 
        `Credentials.yml`
    4. return the live elastic index response.
    
*******************************************************************************/
function getMResult(exportedRequiredModules, elasticServerClient, fieldsList, limits, queryValue, request, callback) {
    // console.log("queryValue",queryValue)
    var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    var indexName = '';

    var accountName = exportedRequiredModules.credentialsObj.TestAccount;

    if (((request.AccountName == accountName.AccountNameLive) || (request.AccountName == accountName.AccountNameLiveAndHI)) && (request.HasHistoricAPI == "true")) {
        indexName = exportedRequiredModules.elasticTestIndexName + "," + exportedRequiredModules.elasticTestHistoricalIndexName
    } else if ((request.AccountName == accountName.AccountNameLive) || (request.AccountName == accountName.AccountNameLiveAndHI)) {
        indexName = exportedRequiredModules.elasticTestIndexName
    } else if (request.HasHistoricAPI == "true") {
        indexName = exportedRequiredModules.elasticIndexName + "," + exportedRequiredModules.elasticHistoricIndexName
    } else {
        indexName = exportedRequiredModules.elasticIndexName
    }
    // console.log("fieldsList"+fieldsList);
    elasticServerClient.search({
        _sourceInclude: fieldsList,
        index: indexName,
        type: exportedRequiredModules.elasticTypeName,
        from: limits[0],
        size: limits[1],
        body: finalQuery
    }).then(function (resp) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/Live/Endpoints/')) {
                fsExtra.ensureDirSync('./api/logs/Live/Endpoints/');
            }
            fs.appendFileSync('./api/logs/Live/Endpoints/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback(null, removeUnwantedFields(resp));
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/Live/Endpoints/')) {
                fsExtra.ensureDirSync('./api/logs/Live/Endpoints/');
            }
            fs.appendFileSync('./api/logs/Live/Endpoints/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback({
            "Error": "Unable to fetch data from source"
        }, null);
    });
}

/******************************************************************************
Function: getBeSpokeResult
Argument: exportedRequiredModules, elasticServerClient, fieldsList, limits, 
            queryValue, request
Return: output response
Usage:
    1. Added Historical index name with live index if the account has 
        `HasHistoricalAPI` field is true.
    2. Search the bespoke query into live elastic index.
    3. Record the bespoke query into log file if the `QueryLogs` is enable in 
        `Credentials.yml`
    4. return the live elastic index response.
*******************************************************************************/
function getBeSpokeResult(exportedRequiredModules, elasticServerClient, fieldsList, limits, queryValue, request, callback) {

    var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    var indexName = '';
    var accountName = exportedRequiredModules.credentialsObj.TestAccount;
    // console.log("accountName",accountName);
    if (((request.AccountName == accountName.AccountNameLive) || (request.AccountName == accountName.AccountNameLiveAndHI)) && (request.HasHistoricAPI == "true")) {
        indexName = exportedRequiredModules.elasticTestIndexName + "," + exportedRequiredModules.elasticTestHistoricalIndexName
    } else if ((request.AccountName == accountName.AccountNameLive) || (request.AccountName == accountName.AccountNameLiveAndHI)) {
        indexName = exportedRequiredModules.elasticTestIndexName
    } else if (request.HasHistoricAPI == "true") {
        indexName = exportedRequiredModules.elasticIndexName + "," + exportedRequiredModules.elasticHistoricIndexName
    } else {
        indexName = exportedRequiredModules.elasticIndexName
    }
    // console.log("finalQuery",finalQuery);
    if (finalQuery.from == undefined) {
        finalQuery.from = limits[0];
    }
    if (finalQuery.size == undefined) {
        finalQuery.size = limits[1];
    }
    if (finalQuery._source == undefined) {
        finalQuery._source = fieldsList;
    }
    elasticServerClient.search({
        index: indexName,
        type: exportedRequiredModules.elasticTypeName,
        body: finalQuery
    }).then(function (resp) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/Live/BeSpokeQuery/')) {
                fsExtra.ensureDirSync('./api/logs/Live/BeSpokeQuery/');
            }
            fs.appendFileSync('./api/logs/Live/BeSpokeQuery/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + JSON.stringify(JSON.parse(request.body)) + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback(null, removeUnwantedFields(resp));
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/Live/BeSpokeQuery/')) {
                fsExtra.ensureDirSync('./api/logs/Live/BeSpokeQuery/');
            }
            fs.appendFileSync('./api/logs/Live/BeSpokeQuery/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(JSON.parse(request.body)) + "\t" +JSON.stringify(finalQuery) + '\n\n');
        }
        callback({
            "Error": "Unable to fetch data from source"
        }, null);
    });
}

/******************************************************************************
Function: getHEResult
Argument: exportedRequiredModules, elasticServerClient, fieldsList, limits, 
            queryValue, request
Return: output response
Usage:
    1. Search the query into HE elastic index.
    2. Record the query into log file if the `QueryLogs` is enable in 
        `Credentials.yml`
    3. return the HE elastic index response.
    elasticTestIndexName,elasticTestHeIndexName,elasticTestHistoricalIndexName
*******************************************************************************/
function getHEResult(exportedRequiredModules, elasticServerClient, fieldsList, limits, queryValue, request, callback) {
    var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    var indexName = '';
    var accountName = exportedRequiredModules.credentialsObj.TestAccount;

    if ((request.AccountName == accountName.AccountNameHI) || (request.AccountName == accountName.AccountNameLiveAndHI)) {
        indexName = exportedRequiredModules.elasticTestHeIndexName
    } else {
        indexName = exportedRequiredModules.elasticHeIndexName
    }
    // console.log("indexName"+indexName);
    elasticServerClient.search({
        index: indexName,
        type: exportedRequiredModules.elasticTypeName,
        from: limits[0],
        size: limits[1],
        body: finalQuery
    }).then(function (resp) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/HE/Endpoints/')) {
                fsExtra.ensureDirSync('./api/logs/HE/Endpoints/');
            }
            fs.appendFileSync('./api/logs/HE/Endpoints/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback(null, removeUnwantedFields(resp));
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/HE/Endpoints/')) {
                fsExtra.ensureDirSync('./api/logs/HE/Endpoints/');
            }
            fs.appendFileSync('./api/logs/HE/Endpoints/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" +JSON.stringify(finalQuery) + '\n\n');
        }
        callback({
            "Error": "Unable to fetch data from source"
        }, null);
    });
}

/******************************************************************************
Function: getBeSpokeHEResult
Argument: exportedRequiredModules, elasticServerClient, fieldsList, limits, 
            queryValue, request
Return: output response
Usage:
    1. Search the bespoke query into HE elastic index.
    2. Record the bespoke query into log file if the `QueryLogs` is enable in 
        `Credentials.yml`
    3. return the HE elastic index response.
*******************************************************************************/
function getBeSpokeHEResult(exportedRequiredModules, elasticServerClient, fieldsList, limits, queryValue, request, callback) {
    var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    var indexName = '';
    var accountName = exportedRequiredModules.credentialsObj.TestAccount;

    if ((request.AccountName == accountName.AccountNameHI) || (request.AccountName == accountName.AccountNameLiveAndHI)) {
        indexName = exportedRequiredModules.elasticTestHeIndexName
    } else {
        indexName = exportedRequiredModules.elasticHeIndexName
    }
    // console.log("indexName"+indexName);
    if (finalQuery.from == undefined) {
        finalQuery.from = limits[0];
    }
    if (finalQuery.size == undefined) {
        finalQuery.size = limits[1];
    }
    elasticServerClient.search({
        index: indexName,
        type: exportedRequiredModules.elasticTypeName,
        body: finalQuery
    }).then(function (resp) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/HE/BeSpokeQuery/')) {
                fsExtra.ensureDirSync('./api/logs/HE/BeSpokeQuery/');
            }
            fs.appendFileSync('./api/logs/HE/BeSpokeQuery/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" +  JSON.stringify(JSON.parse(request.body)) + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback(null, removeUnwantedFields(resp));
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/HE/BeSpokeQuery/')) {
                fsExtra.ensureDirSync('./api/logs/HE/BeSpokeQuery/');
            }
            fs.appendFileSync('./api/logs/HE/BeSpokeQuery/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(JSON.parse(request.body)) + "\t" +JSON.stringify(finalQuery) + '\n\n');
        }
        callback({
            "Error": "Unable to fetch data from source"
        }, null);
    });
}
/******************************************************************************
Function: getResultNotesTagFollow
Argument: exportedRequiredModules, elasticServerClient, queryValue
Return: output response
Usage:

    1. Search the query into live elastic index.
    2. Record the query into log file if the `QueryLogs` is enable in 
        `Credentials.yml`
    3. return the live elastic index response.
    
*******************************************************************************/
function getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType, request, callback) {
    var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    
    var indexName = credentialsObj.ElasticIndex.index;

    elasticServerClient.search({
        index: indexName,
        type: elasticType,
        body: finalQuery
    }).then(function (resp) {
        resp = removeUnwantedFields(resp);
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
            }
          //  console.log(request.query.UserId);
            fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss')+ "\t" +  request.query.UserId + "\t" + request.url + "\t" +  + JSON.stringify(finalQuery) + '\n\n');
        }
        callback(null, resp);
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
            }
            fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback({
            "Error": "Unable to fetch data from source"
        }, null);
    });
}

/******************************************************************************
Function: getTrackingData
Argument: exportedRequiredModules, elasticServerClient, queryValue
Return: output response
Usage:

    1. Search the query into live elastic index.
    2. Record the query into log file if the `QueryLogs` is enable in 
        `Credentials.yml`
    3. return the live elastic index response.
    
*******************************************************************************/
function getTrackingData(exportedRequiredModules, elasticServerClient, queryValue,elasticIndex, elasticType,request, callback) {
    var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    elasticServerClient.search({
        index: elasticIndex,
        type: elasticType,
        body: finalQuery
    }).then(function (resp) {
        var response = {};
        response.total = resp.hits.total;
        response.results = resp.aggregations.by_type.buckets;
        // resp = removeUnwantedFieldsTracking(resp);
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/Tracking/')) {
                fsExtra.ensureDirSync('./api/logs/V2/Tracking/');
            }
            //console.log(request.query.UserId);
            fs.appendFileSync('./api/logs/V2/Tracking/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" + request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback(null, response);
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/Tracking/')) {
                fsExtra.ensureDirSync('./api/logs/V2/Tracking/');
            }
            fs.appendFileSync('./api/logs/V2/Tracking/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback({
            "Error": "Unable to fetch data from source"
        }, null);
    });
}


/******************************************************************************
Function: createDocumentES
Argument: exportedRequiredModules, elasticServerClient, elasticTypeName, Id, queryValue,
Return: output response
Usage:

    1. insert an document into live elastic index.
    2. Record the query into log file if the `QueryLogs` is enable in 'Credentials.yml`
    3. return the response.
    
*******************************************************************************/
function createDocumentES(exportedRequiredModules, elasticServerClient, elasticTypeName, Id, queryValue, callback) {
    // console.log(queryValue);
    var finalQuery;
    try {
        finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    } catch (err) {
        finalQuery = queryValue;
    }

    var indexName = credentialsObj.ElasticIndex.index;
    elasticServerClient.index({
        index: indexName,
        type: elasticTypeName,
        id: Id,

        body: finalQuery
    }).then(function (resp) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
            }
            fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + '\t' + Id + '\t' + JSON.stringify(finalQuery) + '\n\n');
        }
        callback(null, { "Message": "Created Successfully" });
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
            }
            fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" + Id + "\t" +JSON.stringify(finalQuery) + '\n\n');
        }
        callback({

            "Error": "Unable to Insert data to source"
        }, null);
    });
}

/******************************************************************************
Function: updateDocumentES
Argument: exportedRequiredModules, elasticServerClient, elasticTypeName, Id, queryValue,
Return: output response
Usage:

    1. Update an document into live elastic index.
    2. Record the query into log file if the `QueryLogs` is enable in 'Credentials.yml`
    3. return the response.
    
*******************************************************************************/
function updateDocumentES(exportedRequiredModules, elasticServerClient, elasticTypeName, Id, queryValue, request,callback) {
    var finalQuery;
    try {
        finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    } catch (err) {
        finalQuery = queryValue;
    }

    var indexName = credentialsObj.ElasticIndex.index;
    elasticServerClient.update({
        index: indexName,
        type: elasticTypeName,
        id: Id,
        body: finalQuery
    }).then(function (resp) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
            }
            fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" +JSON.stringify(finalQuery) + '\n\n');
        }
        callback(null, { "Message": "Updated Successfully" });
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
            }
            fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss')+ "\t" +  request.query.UserId + "\t" + request.url + "\t" +JSON.stringify(finalQuery) + '\n\n');
        }
        callback({
            "Error": "Unable to Update data to source"
        }, null);
    });
}


/******************************************************************************
Function: updateMultipleDocumentES
Argument: exportedRequiredModules, elasticServerClient, elasticTypeName, Id, queryValue,
Return: output response
Usage:

    1. Update an document into live elastic index.
    2. Record the query into log file if the `QueryLogs` is enable in 'Credentials.yml`
    3. return the response.
    
*******************************************************************************/
function updateMultipleDocumentES(exportedRequiredModules, elasticServerClient, elasticTypeName, Id, queryValue,request, callback) {
    var finalQuery;
    try {
        finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    } catch (err) {
        finalQuery = queryValue;
    }

    var indexName = credentialsObj.ElasticIndex.index;
    var idList = Id.split(',')
    var success = 0;
    var failed = 0;
    idList.forEach(function (id, index, arr){
       
        elasticServerClient.update({
            index: indexName,
            type: elasticTypeName,
            id: id,
            body: finalQuery
        }).then(function (resp) {
            // console.log("resp",resp)
            if (credentialsObj.QueryLogs.Enable == true) {
                if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                    fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
                }
                fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss')+ "\t" +  request.query.UserId + "\t" + request.url + "\t" +JSON.stringify(finalQuery) + '\n\n');
            }
            success = success + 1;
            if(arr.length == index+1){
           
                if((success > 0) && (failed > 0)){
                    callback(null, { "Message": "MyDayOutComeClear - "+success+" successfull and "+failed+" failed" });
                }else if(success > 0){
                    callback(null, { "Message": "MyDayOutComeClear - "+success+" successfull" });
                }else if(failed > 0){
                    callback(null, { "Message": "MyDayOutComeClear - "+failed+" failed" });
                }
                
            }
        }, function (err) {
            // console.log("err",err)
            if (credentialsObj.QueryLogs.Enable == true) {
                if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                    fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
                }
                fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss')+ "\t" +  request.query.UserId + "\t" + request.url + "\t" +JSON.stringify(finalQuery) + '\n\n');
            }
            failed = failed + 1;
            if(arr.length == index+1){
           
                if((success > 0) && (failed > 0)){
                    callback(null, { "Message": "MyDayOutComeClear - "+success+" successfull and "+failed+" failed" });
                }else if(success > 0){
                    callback(null, { "Message": "MyDayOutComeClear - "+success+" successfull" });
                }else if(failed > 0){
                    callback(null, { "Message": "MyDayOutComeClear - "+failed+" failed" });
                }
                
            }
        });
        
    });
    
}

/******************************************************************************
Function: checkExistance
Argument: elasticServerClient, elasticTypeName, Id
Return: output response
Usage:

    1. Check the existing record in live elastic index.
    2. Record the query into log file if the `QueryLogs` is enable in 'Credentials.yml`
    3. return the response of true or false.
    
*******************************************************************************/
function checkExistance(elasticServerClient, elasticTypeName, Id, request,callback) {

    var indexName = credentialsObj.ElasticIndex.index;

    elasticServerClient.exists({
        index: indexName,
        type: elasticTypeName,
        id: Id
    }).then(function (resp) {

        //        console.log(resp);
        callback(null, resp);
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/checkExistance/')) {
                fsExtra.ensureDirSync('./api/logs/V2/checkExistance/');
            }
            fs.appendFileSync('./api/logs/V2/checkExistance/' +"Error_" + moment().format('DDMMYYYY') + '.log', moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + '\n\n');
        }
        callback({
            "Error": "Unable to find to source"
        }, null);
    });
}


/******************************************************************************
Function: deleteDocumentES
Argument: elasticServerClient, elasticTypeName, Id
Return: output response
Usage:

    1. Delete an document into live elastic index.
    2. Record the query into log file if the `QueryLogs` is enable in 'Credentials.yml`
    3. return the response of true or false.
    
*******************************************************************************/
function deleteDocumentES(elasticServerClient, elasticTypeName, Id,request, callback) {

    var indexName = credentialsObj.ElasticIndex.index;

    elasticServerClient.delete({
        index: indexName,
        type: elasticTypeName,
        id: Id
    }).then(function (resp) {

        //      console.log(resp);
        callback(null, resp);
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/Delete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/Delete/');
            }
            fs.appendFileSync('./api/logs/V2/Delete/' +"Error_" + moment().format('DDMMYYYY') + '.log', moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" +'\n\n');
        }
        callback({
            "Error": "Unable to find to source"
        }, null);
    });
}


/******************************************************************************
Function: createDocumentESWithParent
Argument: exportedRequiredModules, elasticServerClient, elasticTypeName, Id,ParentId, queryValue,
Return: output response
Usage:

    1. insert an document into live elastic index with parent child relationship.
    2. Record the query into log file if the `QueryLogs` is enable in 'Credentials.yml`
    3. return the response.
    
*******************************************************************************/
function createDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticTypeName, Id, ParentId, queryValue,request, callback) {


    var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    var indexName = credentialsObj.ElasticIndex.index;
    elasticServerClient.index({
        index: indexName,
        type: elasticTypeName,
        id: Id,
        parent: ParentId,
        body: finalQuery
    }).then(function (resp) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
            }
            fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        // console.log(resp)
        callback(null, { "Message": "Created Successfully" });
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
            }
            fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss')+ "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback({

            "Error": "Unable to Insert data to source"
        }, null);
    });
}

/******************************************************************************
Function: updateDocumentESWithParent
Argument: exportedRequiredModules, elasticServerClient, elasticTypeName, Id,ParentId, queryValue
Return: output response
Usage:

    1. Update an document into live elastic index with parent child relationship.
    2. Record the query into log file if the `QueryLogs` is enable in 'Credentials.yml`
    3. return the response.
    
*******************************************************************************/
function updateDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticTypeName, Id, ParentId, queryValue,request, callback) {
    var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    var indexName = credentialsObj.ElasticIndex.index;
    elasticServerClient.update({
        index: indexName,
        type: elasticTypeName,
        id: Id,
        parent: ParentId,
        body: finalQuery
    }).then(function (resp) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
            }
            fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss')+ "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        // console.log(resp)
        callback(null, { "Message": "Updated Successfully" });
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
            }
            fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback({
            "Error": "Unable to Update data to source"
        }, null);
    });
}

/******************************************************************************
Function: checkExistanceWithParent
Argument: elasticServerClient, elasticTypeName, Id
Return: output response
Usage:

    1. Check the existing record in live elastic index with parent Id.
    2. Record the query into log file if the `QueryLogs` is enable in 'Credentials.yml`
    3. return the response of true or false.
    
*******************************************************************************/
function checkExistanceWithParent(elasticServerClient, elasticTypeName, Id, ParentId, request, callback) {

    var indexName = credentialsObj.ElasticIndex.index;
    // console.log(elasticTypeName)
    // console.log(ParentId)
    // console.log(Id)
    elasticServerClient.exists({
        index: indexName,
        type: elasticTypeName,
        id: Id,
        parent: ParentId
    }).then(function (resp) {
        // console.log(resp)
          if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
            }
            fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss')+ "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback(null, resp);
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
            }
            fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback({
            "Error": "Unable to find to source"
        }, null);
    });
}


/******************************************************************************
Function: deleteDocumentESWithParent
Argument: elasticServerClient, elasticTypeName, Id,ParentId
Return: output response
Usage:

    1. Delete an document into live elastic index with parent chld relationship.
    2. Record the query into log file if the `QueryLogs` is enable in 'Credentials.yml`
    3. return the response of true or false.
    
*******************************************************************************/

function deleteDocumentESWithParent(elasticServerClient, elasticTypeName, Id, ParentId, request,callback) {

    var indexName = credentialsObj.ElasticIndex.index;
    // console.log(elasticTypeName)
    // console.log(ParentId)
    // console.log(Id)
    elasticServerClient.delete({
        index: indexName,
        type: elasticTypeName,
        id: Id,
        parent: ParentId
    }).then(function (resp) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/CreateOrUpdateorDelete/')) {
                fsExtra.ensureDirSync('./api/logs/V2/CreateOrUpdateorDelete/');
            }
            fs.appendFileSync('./api/logs/V2/CreateOrUpdateorDelete/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss')+ "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback(null, resp);
    }, function (err) {

        callback({
            "Error": "Unable to find to source"
        }, null);
    });
}


/******************************************************************************
Function: getRecordCount
Argument: exportedRequiredModules, elasticServerClient, elasticTypeName, queryValue,
Return: output response
Usage:

    1. get count from live elastic index
    
*******************************************************************************/
function getRecordCount(exportedRequiredModules, elasticServerClient, elasticTypeName, queryValue, request, callback) {


    // var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    var Id = request.query.UserId
    var finalQuery = queryValue;
    var indexName = credentialsObj.ElasticIndex.index;
    elasticServerClient.count({
        index: indexName,
        type: elasticTypeName,
        body: finalQuery
    }).then(function (resp) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/createMyDayTask/')) {
                fsExtra.ensureDirSync('./api/logs/V2/createMyDayTask/');
            }
            fs.appendFileSync('./api/logs/V2/createMyDayTask/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        var response = resp.count;
        callback(null, { 'count': response });
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/createMyDayTask/')) {
                fsExtra.ensureDirSync('./api/logs/V2/createMyDayTask/');
            }
            fs.appendFileSync('./api/logs/V2/createMyDayTask/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.UserId + "\t" + request.url + "\t" +JSON.stringify(finalQuery) + '\n\n');
        }
        callback({
            "Error": "Unable to Insert data to source"
        }, null);
    });
}

/******************************************************************************
Function: getUpcomingEvents
Argument: exportedRequiredModules, elasticServerClient, elasticTypeName, queryValue,
Return: output response
Usage:

    1. insert my day task into live elastic index
    
*******************************************************************************/
function getMyDayEvents(exportedRequiredModules, elasticServerClient, elasticTypeName, queryValue, request, callback) {

    var Id = request.UserId
    // var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
    var finalQuery = queryValue;
    // console.log("finalQuery",finalQuery)
    var indexName = credentialsObj.ElasticIndex.index;
    elasticServerClient.search({
        index: indexName,
        type: elasticTypeName,
        body: finalQuery
    }).then(function (resp) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/createMyDayTask/')) {
                fsExtra.ensureDirSync('./api/logs/V2/createMyDayTask/');
            }
            fs.appendFileSync('./api/logs/V2/createMyDayTask/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        // console.log(resp)
        callback(null, removeUnwantedFields(resp));
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/createMyDayTask/')) {
                fsExtra.ensureDirSync('./api/logs/V2/createMyDayTask/');
            }
            fs.appendFileSync('./api/logs/V2/createMyDayTask/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss')+ "\t" +  request.UserId + "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
        }
        callback({

            "Error": "Unable to Insert data to source"
        }, null);
    });
}
/******************************************************************************
Function: getUpcomingFollowAlerts
Argument: exportedRequiredModules, elasticServerClient, elasticTypeName, queryValue,
Return: output response
Usage:

    1. searches follow alerts in live elastic index
    
*******************************************************************************/
function getUpcomingFollowAlerts(exportedRequiredModules, elasticServerClient, elasticTypeName, queryValue, request, callback) {
    var Id = request.UserId
    var finalQuery = queryValue;
    var indexName = credentialsObj.ElasticIndex.follow_alert_index;
    elasticServerClient.search({
        index: indexName,
        type: elasticTypeName,
        body: finalQuery
    }).then(function (resp) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/FollowAlert/')) {
                fsExtra.ensureDirSync('./api/logs/V2/FollowAlert/');
            }
            fs.appendFileSync('./api/logs/V2/FollowAlert/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss')+ "\t" +  request.UserId + "\t" + request.url + "\t" +JSON.stringify(finalQuery) + '\n\n');
        }
        callback(null, removeUnwantedFieldsFollowAlert(resp));
        // callback(null, resp);
    }, function (err) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/FollowAlert/')) {
                fsExtra.ensureDirSync('./api/logs/V2/FollowAlert/');
            }
            fs.appendFileSync('./api/logs/V2/FollowAlert/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.UserId + "\t" + request.url + "\t" +JSON.stringify(finalQuery) + '\n\n');
        }
        callback({
            "Error": "Unable to Insert data to source"
        }, null);
    });
}

/******************************************************************************
Function: taskQueueRequest
Argument: exportedRequiredModules, elasticServerClient, elasticTypeName, queryValue,
Return: output response
Usage:

    1. insert my day task into live elastic index
    
*******************************************************************************/
function taskQueueRequest(exportedRequiredModules, elasticServerClient, queryValue, request, callback) {


    var url = credentialsObj.TaskAPI.QueueUrl
    var key = credentialsObj.TaskAPI.QueueKey
    var queueActionUrl = credentialsObj.TaskAPI.QueueActionUrl;
    var taskActionQuery = credentialsObj.TaskAPI.QueueActionQuery;
    var postQueueActionObj = JSON.parse(taskActionQuery);
    // console.log("response.statusCode", JSON.stringify(queryValue))
    httpRequest({
        url: url, method: "POST",
        json: true,
        headers: { 'Content-Type': 'application/json', 'Authorization': 'Basic ' + new Buffer(key).toString('base64') + "Og==" }
        , body: queryValue
    }, function (error, response, body) {
        // console.log("response.statusCode", response.statusCode)
        if (response.statusCode == 200) {
            httpRequest.post({
                url: queueActionUrl, 
                headers: {'Authorization': 'Basic ' + new Buffer(key).toString('base64')+"Og=="},
                form: postQueueActionObj
            }, function(error, res, body){
                if (res.statusCode == 200) {
                    if (credentialsObj.QueryLogs.Enable == true) {
                        if (!fs.existsSync('./api/logs/V2/FollowAlert/')) {
                            fsExtra.ensureDirSync('./api/logs/V2/FollowAlert/');
                        }
                        fs.appendFileSync('./api/logs/V2/FollowAlert/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss')+ "\t" +  request.query.UserId + "\t" + request.url + "\t" +JSON.stringify(finalQuery) + '\n\n');
                    }
                  
                    callback(null, { "Message": "Request sent successfully. Id is "+queryValue[0].id })
                } else {
                    if (credentialsObj.QueryLogs.Enable == true) {
                        if (!fs.existsSync('./api/logs/V2/FollowAlert/')) {
                            fsExtra.ensureDirSync('./api/logs/V2/FollowAlert/');
                        }
                        fs.appendFileSync('./api/logs/V2/FollowAlert/' +"Error_" +exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +  request.query.UserId + "\t" + request.url + "\t" +JSON.stringify(finalQuery) + '\n\n');
                    }
                    callback({ "Error": "Request sent failed" }, null)
                }
            });
        } else {
            callback({ "Error": "Request sent failed" }, null)
        }

    });

}

/******************************************************************************
Function: taskActionRequest
Argument: queryValue,
Return: output response
Usage:

    1. insert my day task into live elastic index
    
*******************************************************************************/
function taskActionRequest(queryValue, request, callback) {

    var url = credentialsObj.TaskAPI.QueueActionUrl
    var key = credentialsObj.TaskAPI.QueueActionKey

    // console.log(url)
    // console.log(key)
    //console.log(JSON.stringify(queryValue))
    httpRequest({
        url: url, method: "POST",
        json: true,
        headers: { 'Content-Type': 'application/json', 'Authorization': 'Basic ' + new Buffer(key).toString('base64') + "Og==" }
        , body: queryValue
    }, function (error, response, body) {
        if (credentialsObj.QueryLogs.Enable == true) {
            if (!fs.existsSync('./api/logs/V2/FollowAlert/')) {
                fsExtra.ensureDirSync('./api/logs/V2/FollowAlert/');
            }
            fs.appendFileSync('./api/logs/V2/FollowAlert/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss')+ "\t" +  request.query.UserId + "\t" + request.url + "\t" +JSON.stringify(finalQuery) + '\n\n');
        }
      
        // console.log("response.statusCode", response.statusCode)
        if (response.statusCode == 200) {
            callback(null, response)
        } else {
            callback({ "Error": "Request sent failed" }, null)
        }
    });
}