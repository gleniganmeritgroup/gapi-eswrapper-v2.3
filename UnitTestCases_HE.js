'use strict';
/********************************************************************************************
 ********************************************************************************************
 * This is unit test cases for Glenigan API Application - HE endpoints (unitTestCaseHE.js) 
 ********************************************************************************************
 *******************************************************************************************/

/* Requiring the npm modules for unit testing */
var expect = require('chai').expect;
var request = require('request');
var mongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var elasticsearch = require('elasticsearch');
var yamlConfig = require('js-yaml');
var iniParserModule = require('parse-ini');
var moment = require('moment');
var fs = require('fs');
var async = require('async');
var salesforceConnection = require("node-salesforce-connection");
var credentialsObject = yamlConfig.safeLoad(fs.readFileSync('./config/Credentials.yml', 'utf8'));

var subscriptionJSON = '{"SubscriptionExists": true,"Subscription": {"SubscriptionStartDate": "2014-09-30","SubscriptionEndDate": "2019-02-15","ProjectSectors": [{"Sector": "Private Housing"},{"Sector": "Industrial"},{"Sector": "Offices"},{"Sector": "Retail"},{"Sector": "Hotel & Leisure"},{"Sector": "Education"},{"Sector": "Community & Amenity"},{"Sector": "Health"},{"Sector": "Utilities"},{"Sector": "Infrastructure"}],"ProjectRegions": [{"Region": "London","Counties": []},{"Region": "East Midlands","Counties": ["Lincolnshire"]},{"Region": "Yorkshire and the Humber","Counties": ["Humberside"]}],"InsightSectors": [],"InsightRegions": [],"HasSmall": false,"HasReporting": false,"HasAPI": true,"HasInsight": false,"HasOpportunities": false,"HasCompanyProfiles": false}}';

var inputDataForExceptions = [
    //radius
    {
        "controller": "getLocationsQueryHEObj",
        "helper": "getQueryByRadius",
        "query": {
            "Radius": "5mm",
            "Lat": "50.7235471",
            "Lon": "-1.8664622"
        },
        "error": "Invalid radius unit is given"
    },
    //postcode
    {
        "controller": "getLocationsQueryHEObj",
        "helper": "getQueryByPostcode",
        "query": {
            "PostCodeDistricts": "DN38112"
        },
        "error": "Invalid PostCodeDistricts value is given"
    },
    //town
    {
        "controller": "getLocationsQueryHEObj",
        "helper": "getQueryByTown",
        "query": {
            "TownNames": "Barnetby11"
        },
        "error": "Invalid TownNames value is given"
    },
    //county
    {
        "controller": "getLocationsQueryHEObj",
        "helper": "getQueryByCounty",
        "query": {
            "CountyNames": "Lincolnshire11"
        },
        "error": "Invalid CountyNames value is given"
    },
    //region
    {
        "controller": "getLocationsQueryHEObj",
        "helper": "getQueryByRegions",
        "query": {
            "RegionNames": "East Midlands11"
        },
        "error": "Invalid RegionNames value is given"
    },
    // Status
    {
        "controller": "getStatusQueryHEObj",
        "helper": "getQueryByStatus",
        "query": {
            "StatusNames": "Cancelled1"
        },
        "error": "Invalid StatusNames value is given"
    },
    // Size
    {
        "controller": "getSizesQueryHEObj",
        "helper": "getQueryBySizes",
        "query": {
            "Names": "Large1"
        },
        "error": "Invalid Names value is given"
    },
    // Stages
    {
        "controller": "getStageQueryHEObj",
        "helper": "getQueryByStages",
        "query": {
            "Stages": "Early Planning1",
            "Level2": "Outline Grant after Appeal"
        },
        "error": "Invalid Stages value is given"
    },
    {
        "controller": "getStageQueryHEObj",
        "helper": "getQueryByStagesBoth",
        "query": {
            "PlanningStages": "Early Planning",
            "PlanningStageLevel2": "Outline Grant after Appeal1",
            "ContractStages": "Pre-Tender1"
        },
        "error": "Invalid PlanningStageLevel2 value is given"
    },
    // Sector
    {
        "controller": "getSectorsQueryHEObj",
        "helper": "getQueryBySector",
        "query": {
            "Sectors": "Health",
            "Categories": "Colleges1"
        },
        "error": "Invalid Categories value is given"
    },
    // Values
    {
        "controller": "getValuesQueryHEObj",
        "helper": "getQueryByValues",
        "query": {
            "From": "2.5ma",
            "To": "5m"
        },
        "error": "Invalid From value unit is given"
    },
    // Development Type
    {
        "controller": "getDevelopmentTypeQueryHEObj",
        "helper": "getQueryByDevelopmentType",
        "query": {
            "Types": "New1"
        },
        "error": "Invalid Types value is given"
    },
    // Planning Application
    //PlanningAuthority
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByPlanningAuthority",
        "query": {
            "CouncilNames": "Aberdeen1"
        },
        "error": "Invalid CouncilNames value is given"
    },
    //planningType
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByPlanningType",
        "query": {
            "PlanningNames": "Pre-Planning1"
        },
        "error": "Invalid PlanningNames value is given"
    },
    //planningApplication
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByPlanningApplication",
        "query": {
            "CouncilNames": undefined,
            "PlanningNames": "Pre-Planning,Detailed Planning"
        },
        "error": "parameter CouncilNames is missing"
    },
    //applicationDate
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByApplicationDate",
        "query": {
            "From": "04-1-2018",
            "To": "02-03-2018"
        },
        "error": "Invalid From value is given"
    },
    //permissionDate
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByPermissionDate",
        "query": {
            "From": "04-1-2018",
            "To": "02-03-2018"
        },
        "error": "Invalid From value is given"
    },
    //refusedDate
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByRefusedDate",
        "query": {
            "From": "04-01-2018",
            "To": "02-3-2018"
        },
        "error": "Invalid To value is given"
    },
    //withdrawnDate
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByWithdrawnDate",
        "query": {
            "From": "04-01-2018",
            "To": "02-3-2018"
        },
        "error": "Invalid To value is given"
    },
    //Role
    {
        "controller": "getRoleQueryHEObj",
        "helper": "getQueryByRoles",
        "query": {
            "RoleLocationRegions": "East Midlands,London",
            "Roles": "Client1,Fit Out Contractor1"
        },
        "error": "Invalid Roles value is given"
    },
];

var inputDataForQueryParsing = [
    //radius
    {
        "controller": "getLocationsQueryHEObj",
        "helper": "getQueryByRadius",
        "query": {
            "Radius": "5mi",
            "Lat": "50.7235471",
            "Lon": "-1.8664622"
        }
    },
    //postcode
    {
        "controller": "getLocationsQueryHEObj",
        "helper": "getQueryByPostcode",
        "query": {
            "PostCodeDistricts": "DN38"
        }
    },
    //town
    {
        "controller": "getLocationsQueryHEObj",
        "helper": "getQueryByTown",
        "query": {
            "TownNames": "Barnetby"
        }
    },
    //county
    {
        "controller": "getLocationsQueryHEObj",
        "helper": "getQueryByCounty",
        "query": {
            "CountyNames": "Lincolnshire"
        }
    },
    //region
    {
        "controller": "getLocationsQueryHEObj",
        "helper": "getQueryByRegions",
        "query": {
            "RegionNames": "East Midlands"
        }
    },
    // Status
    {
        "controller": "getStatusQueryHEObj",
        "helper": "getQueryByStatus",
        "query": {
            "StatusNames": "Cancelled"
        }
    },
    // Size
    {
        "controller": "getSizesQueryHEObj",
        "helper": "getQueryBySizes",
        "query": {
            "Names": "Large"
        }
    },
    // Stages
    {
        "controller": "getStageQueryHEObj",
        "helper": "getQueryByStages",
        "query": {
            "Stages": "Tenders",
            "Level2": "bills called"
        }
    },
    {
        "controller": "getStageQueryHEObj",
        "helper": "getQueryByStagesBoth",
        "query": {
            "PlanningStages": "Early planning",
            "PlanningStageLevel2": "public enquiry",
            "ContractStages": "Tenders"
        }
    },
    // Primary Sector
    {
        "controller": "getSectorsQueryHEObj",
        "helper": "getQueryBySector",
        "query": {
            "Sectors": "Private Housing",
            "Categories": "Houses"
        }
    },

    // Values
    {
        "controller": "getValuesQueryHEObj",
        "helper": "getQueryByValues",
        "query": {
            "From": "2.5m",
            "To": "5m"
        }
    },
    // Development Type
    {
        "controller": "getDevelopmentTypeQueryHEObj",
        "helper": "getQueryByDevelopmentType",
        "query": {
            "Types": "Extension,New"
        }
    },

    // Planning Application
    //PlanningAuthority
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByPlanningAuthority",
        "query": {
            "CouncilNames": "Aberdeen,Angus"
        }
    },
    //planningType
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByPlanningType",
        "query": {
            "PlanningNames": "Pre-Planning,Detailed Planning"
        }
    },
    //planningApplication
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByPlanningApplication",
        "query": {
            "CouncilNames": "Aberdeen,Angus",
            "PlanningNames": "Pre-Planning,Detailed Planning"
        }
    },
    //applicationId
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByApplicationId",
        "query": {
            "Ids": "07/18/0083/F, 17/01100/FUL"
        }
    },
    //applicationDate
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByApplicationDate",
        "query": {
            "From": "04-01-2018",
            "To": "02-03-2018"
        }
    },
    //permissionDate
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByPermissionDate",
        "query": {
            "From": "04-01-2018",
            "To": "02-03-2018"
        }
    },
    //refusedDate
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByRefusedDate",
        "query": {
            "From": "04-01-2018",
            "To": "02-03-2018"
        }
    },
    //withdrawnDate
    {
        "controller": "getPlanningApplicationQueryHEObj",
        "helper": "getQueryByWithdrawnDate",
        "query": {
            "From": "04-01-2018",
            "To": "02-03-2018"
        }
    },
    //Role
    {
        "controller": "getRoleQueryHEObj",
        "helper": "getQueryByRoles",
        "query": {
            "RoleLocationRegions": "East Midlands,London",
            "Roles": "Client,Fit Out Contractor"
        }
    },
];

/* Endpoints List*/
var endpointsArray = [
    "/glenigan_he/project", "/glenigan_he/project/sector", "/glenigan_he/project/Stage", "/glenigan_he/project/stages/bothStage", "/glenigan_he/project/size", "/glenigan_he/project/value", "/glenigan_he/project/developmentType", "/glenigan_he/project/status", "/glenigan_he/project/developmentType", "/glenigan_he/project/planningApplication/withdrawnDate", "/glenigan_he/project/planningApplication/planningAuthority", "/glenigan_he/project/planningApplication/applicationDate", "/glenigan_he/project/planningApplication/planningauthority-planningtype", "/glenigan_he/project/planningApplication/applicationId", "/glenigan_he/project/planningApplication/applicationDate", "/glenigan_he/project/planningApplication/permissionDate", "/glenigan_he/project/planningApplication/refusedDate", "/glenigan_he/project/role", "/glenigan_he/project/location", "/glenigan_he/project/region", "/glenigan_he/project/county", "/glenigan_he/project/town", "/glenigan_he/project/postCode", "/glenigan_he/project/radius"
];

/* Getting the port*/

var port = ''
try {
    var configObject = iniParserModule.parse('./config/LocalConfig.ini');
    port = process.env.PORT || configObject.Local.port;
} catch (error) {
    port = process.env.PORT || 80;
}

/**
 * After completing all test cases exit the script (unitTestCase_HE.js) 
 */
after(function (done) {
    done();
    process.exit(0);
});

/**
 * Glenigan API unit test cases for HE Index
 */
describe('Glenigan API Unit Test Cases for HE index', function () {

    /*******************************************************************************************
     * Unit test case for Checking server  (app.js)
     *******************************************************************************************/

    describe('Checking the port', function () {
        this.timeout(5000);
        it('port ' + port + ' is working fine', function (done) {
            try {
                var appTest = require('./app.js');
                done();
            } catch (e) {}
        });
    });
    /*******************************************************************************************
     * Checking the Meta data end-points
     *******************************************************************************************/

    /* "/glenigan/metadata/_search" Checking the end-point */
    describe('"/glenigan_he/metadata/locations" Checking the end-point', function () {
        it('"/glenigan_he/metadata/locations" should return 200', function (done) {
            var exportedRequiredModules = require('./config.js').requiredModulesExport;
            var host = exportedRequiredModules.elasticIndexHost;
            request(host + '/glenigan_he/metadata/locations', function (error, response, body) {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
    });

    /*******************************************************************************************
     *  Checking the end-points of the application
     * ******************************************************************************************/

    describe('Checking that all the Endpoints are defined', function () {
        var exportedRequiredModules = require('./Config.js').requiredModulesExport;

        function endpoints(object, callback) {
            describe('"' + object + '" Checking the end-point', function () {
                this.timeout(20000);
                it('"' + object + '" - endpoint should return a message that Parameter (key) is required', function (done) {
                    request('http://localhost:' + port + object, function (error, response, body) {
                        var data = JSON.parse(body);
                        expect("REQUIRED").to.equal(data.code);
                        done();
                        callback(null, 1);
                    });
                });
            });
        }
        async.mapSeries(endpointsArray, endpoints, function (error, result) {
            // console.log(result);
        });
    });

    describe('Checking HE Index query formation', function () {
        var exportedRequiredModules = require('./Config.js').requiredModulesExport;

        function helperQueryParsing(object, callback) {
            describe('Checking the HE Index helper function - ' + object.controller, function () {
                this.timeout(20000);
                it('Should given a valid query from ' + object.helper + ' function', function (done) {
                    if (object.controller != "getCompanyContactQueryObj")
                        exportedRequiredModules[object.controller][object.helper](subscriptionJSON, object, function (error, queryValue) {
                            assert.equal(null, error);
                            expect((typeof (queryValue) == 'string') ? JSON.parse(queryValue) : queryValue).to.be.an('object');
                            done();
                            callback(null, 1);
                        });
                    else {
                        exportedRequiredModules[object.controller][object.helper](object, function (error, queryValue) {
                            assert.equal(null, error);
                            expect((typeof (queryValue) == 'string') ? JSON.parse(queryValue) : queryValue).to.be.an('object');
                            done();
                            callback(null, 1);
                        });
                    }
                });
            });
        }
        async.mapSeries(inputDataForQueryParsing, helperQueryParsing, function (error, result) {
            //console.log(result);
        });
    });
    describe('Checking HE index exception handling on helper functions', function () {
        var exportedRequiredModules = require('./Config.js').requiredModulesExport;

        function helperCheckExceptions(object, callback) {
            // console.log(object);
            describe('Checking HE index exception on helper function - ' + object.controller, function () {
                this.timeout(20000);
                it('Should throw an exception - ' + object.error, function (done) {
                    if (object.controller != "getCompanyContactQueryObj")
                        exportedRequiredModules[object.controller][object.helper](subscriptionJSON, object, function (error, queryValue) {
                            assert.equal(null, queryValue);
                            done();
                            callback(null, 1);
                        });
                    else {
                        exportedRequiredModules[object.controller][object.helper](object, function (error, queryValue) {
                            assert.equal(null, queryValue);
                            done();
                            callback(null, 1);
                        });
                    }
                });
            });
        }
        async.mapSeries(inputDataForExceptions, helperCheckExceptions, function (error, result) {
            //console.log(result);
        });
    });

    describe('Checking exception handling for invalid parameter', function () {
        var exportedRequiredModules = require('./Config.js').requiredModulesExport;
        var testCase1 = {
            url: '/glenigan_he/project/radius',
            query: {
                TimeRange: '',
                key: '03f0994b-2cef-451b-938b-b84cda2ee2e4',
                Radius: '10mi',
                Lat: '51.5',
                Lon: '-0.13'
            }
        };
        describe('Checking with empty parameter', function () {
            this.timeout(20000);
            it('Should throw an exception - Parameter value should not be empty', function (done) {
                exportedRequiredModules.paramsValidationAndGetSubscriptionObj(testCase1, function (error, value) {
                    assert.equal(null, value);
                    done();
                });
            });
        });
        var testCase2 = {
            url: '/glenigan_he/project/location',
            query: {
                TimeRange: '',
                key: '03f0994b-2cef-451b-938b-b84cda2ee2e4',
                Radius: '10mi',
                Lat: '51.5',
                Lon: '-0.13'
            }
        };
        describe('Checking with invalid parameter', function () {
            this.timeout(20000);
            it('Should throw an exception - Invalid parameter - Radius,Lat,Lon', function (done) {
                exportedRequiredModules.paramsValidationAndGetSubscriptionObj(testCase2, function (error, value) {
                    assert.equal(null, value);
                    done();
                });
            });
        });
        var testCase3 = {
            url: '/glenigan_he/project/radius',
            query: {
                TimeRange: ['2y', ''],
                key: '03f0994b-2cef-451b-938b-b84cda2ee2e4',
                Radius: '10mi',
                Lat: '51.5',
                Lon: '-0.13'
            }
        };
        describe('Checking with duplicate parameter', function () {
            this.timeout(20000);
            it('Should throw an exception - Duplicate parameter not allowed', function (done) {
                exportedRequiredModules.paramsValidationAndGetSubscriptionObj(testCase3, function (error, value) {
                    assert.equal(null, value);
                    done();
                });
            });
        });
        var testCase4 = {
            url: '/glenigan_he/project/radius',
            query: {
                TimeRange: '{2y}',
                key: '03f0994b-2cef-451b-938b-b84cda2ee2e4',
                Radius: '10mi',
                Lat: '51.5',
                Lon: '-0.13'
            }
        };
        describe('Checking with Curly brackets in the parameter', function () {
            this.timeout(20000);
            it('Should throw an exception - Curly brackets are not allowed in the Parameters', function (done) {
                exportedRequiredModules.paramsValidationAndGetSubscriptionObj(testCase4, function (error, value) {
                    assert.equal(null, value);
                    done();
                });
            });
        });
        var testCase4 = {
            url: '/glenigan_he/project/radius',
            query: {
                TimeRange: '2y',
                key: undefined,
                Radius: '10mi',
                Lat: '51.5',
                Lon: '-0.13'
            }
        };
        describe('Checking with missing key', function () {
            this.timeout(20000);
            it('Should throw an exception - Key Missing', function (done) {
                exportedRequiredModules.paramsValidationAndGetSubscriptionObj(testCase4, function (error, value) {
                    assert.equal(null, value);
                    done();
                });
            });
        });
    });
});