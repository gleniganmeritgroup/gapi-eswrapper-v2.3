'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (TagLink.js) is used for fetching the response from
endpoint /glenigan/project/taglink

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
    host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
    getProjectTagLink: getProjectTagLink,
    getOfficeTagLink: getOfficeTagLink,
    getContactTagLink: getContactTagLink,
    getProjectTagLinkAndTags: getProjectTagLinkAndTags,
    getOfficeTagLinkAndTags: getOfficeTagLinkAndTags,
    getContactTagLinkAndTags: getContactTagLinkAndTags,
    createOrUpdateProjectTagLink: createOrUpdateProjectTagLink,
    createOrUpdateOfficeTagLink: createOrUpdateOfficeTagLink,
    createOrUpdateContactTagLink: createOrUpdateContactTagLink,
    deleteProjectTagLink: deleteProjectTagLink,
    deleteOfficeTagLink: deleteOfficeTagLink,
    deleteContactTagLink: deleteContactTagLink
};


/******************************************************************************
"ErrorResponse" function writes error occurring in the taglink endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response, error, errorCode) {
    exportedRequiredModules.loggerObj.error(error);
    response.status(errorCode);
    response.setHeader('Content-Type', 'application/json');
    response.send(JSON.stringify(error, null, 3));
}

/******************************************************************************
Function: getProjectTagLink
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getProjectTagsLinkQueryObj - used to form a elastic query based on user request.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/

function getProjectTagLink(request, response) {


    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
                exportedRequiredModules.getProjectTagsQueryObj.getQueryByProjectTags(request, function (err, queryValue) {
                var elasticType = exportedRequiredModules.elasticProjectTagsTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType,request, function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        exportedRequiredModules.getProjectTagsLinkQueryObj.getQueryByProjectTagsLink(request, result,elasticServerClient, function (error, result) {
                            if (error) {
                                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                            }
                            else {
                                //	console.log(res);
                                response.status(200);
                                response.setHeader('Content-Type', 'application/json');
                                response.send(JSON.stringify(result, null, 3));
                            }
                        });
                    }
                });

            });

        }

    });
}
/******************************************************************************
Function: getOfficeTagLink
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getOfficeTagsLinkQueryObj - used to form a elastic query based on user request.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/

function getOfficeTagLink(request, response) {


    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
                exportedRequiredModules.getOfficeTagsQueryObj.getQueryByofficeTags(request, function (err, queryValue) {
                var elasticType = exportedRequiredModules.elasticOfficeTagsTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType, request,function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        exportedRequiredModules.getOfficeTagsLinkQueryObj.getQueryByOfficeTagsLink(request, result,elasticServerClient, function (error, result) {
                            if (error) {
                                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                            }
                            else {
                                //	console.log(res);
                                response.status(200);
                                response.setHeader('Content-Type', 'application/json');
                                response.send(JSON.stringify(result, null, 3));
                            }
                        });
                    }
                });

            });

        }

    });
}

/******************************************************************************
Function: getContactTagLink
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getContactTagsLinkQueryObj - used to form a elastic query based on user request.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/

function getContactTagLink(request, response) {


    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
                exportedRequiredModules.getContactTagsQueryObj.getQueryByContactTags(request, function (err, queryValue) {
                var elasticType = exportedRequiredModules.elasticContactTagsTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType, request,function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        exportedRequiredModules.getContactTagsLinkQueryObj.getQueryByContactTagsLink(request, result,elasticServerClient, function (error, result) {
                            if (error) {
                                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                            }
                            else {
                                //	console.log(res);
                                response.status(200);
                                response.setHeader('Content-Type', 'application/json');
                                response.send(JSON.stringify(result, null, 3));
                            }
                        });
                    }
                });

            });

        }

    });
}
/******************************************************************************
Function: createOrUpdateContactTagLink
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if contact id is present in elastic index.
    3. postQueryByContactTagLink - Used to create a query for posting it to elastic index.
    4. createDocumentESWithParent - Create a new taglink document for contact id passed.
    5. updateDocumentESWithParent - Updates the taglink document of contact id, if taglink document is present.
	6. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function createOrUpdateContactTagLink(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            var RequestParam = JSON.parse(request.body);
            var ContactIds = RequestParam.ContactId;
            var TagIds = RequestParam.TagId;
            var failureCount = 0;
            var successCount = 0;
            TagIds.split(',').forEach(function (TagId, Tagindex, Tagarray) {
                //console.log(TagId);

                ContactIds.split(',').forEach(function (ContactId, index, array) {

                    var ContactTagLinkId = TagId + '#' + ContactId;

                    var checkExistance;
                    var elasticType = exportedRequiredModules.elasticContactTagsLinkTypeName;
                    exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, ContactTagLinkId, ContactId, request, function (error, result) {
                        if (error) {
                            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                        }
                        else {
                            checkExistance = result;

                            exportedRequiredModules.getContactTagsLinkQueryObj.postQueryByContactTagLink(request, TagId, checkExistance, function (error, queryValue) {

                                if (checkExistance == true) {
                                    exportedRequiredModules.updateDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, ContactTagLinkId, ContactId, queryValue,request, function (error, result) {
                                        if (error) {
                                            failureCount++;
                                        }
                                        else {
                                            successCount++;
                                        }
                                    });
                                }
                                else if (checkExistance == false) {
                                    exportedRequiredModules.createDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, ContactTagLinkId, ContactId, queryValue, request, function (error, result) {
                                        if (error) {
                                            failureCount++;
                                        }
                                        else {
                                            successCount++;

                                        }

                                    });

                                }

                            });
                        }
                    });

                });
                if (Tagindex === Tagarray.length - 1) {
                    response.status(200);
                    response.setHeader('Content-Type', 'application/json');
                    response.send(JSON.stringify({ "Message": "Success" }, null, 3));
                }
            });

        }

    });

}
/******************************************************************************
Function: createOrUpdateProjectTagLink
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if project id is present in elastic index.
    3. postQueryByProjectTagLink - Used to create a query for posting it to elastic index.
    4. createDocumentESWithParent - Create a new taglink document for project id passed.
    5. updateDocumentESWithParent - Updates the taglink document of project id, if taglink document is present.
	6. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function createOrUpdateProjectTagLink(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {

            var RequestParam = JSON.parse(request.body);
            var ProjectIds = RequestParam.ProjectId;
            var failureCount = 0;
            var successCount = 0;
            var TagIds = RequestParam.TagId;
            TagIds.split(',').forEach(function (TagId, Tagindex, Tagarray) {
                ProjectIds.split(',').forEach(function (ProjectId, index, array) {

                    var ProjectTagLinkId = TagId + '#' + ProjectId;

                    var checkExistance;
                    var elasticType = exportedRequiredModules.elasticProjectTagsLinkTypeName;
                    exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, ProjectTagLinkId, ProjectId, request, function (error, result) {
                        if (error) {
                            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                        }
                        else {
                            checkExistance = result;

                            exportedRequiredModules.getProjectTagsLinkQueryObj.postQueryByProjectTagLink(request, TagId, checkExistance, function (error, queryValue) {

                                if (checkExistance == true) {
                                    exportedRequiredModules.updateDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, ProjectTagLinkId, ProjectId, queryValue, request,function (error, result) {
                                        if (error) {
                                            failureCount++;
                                        }
                                        else {
                                            successCount++;
                                        }
                                    });
                                }
                                else if (checkExistance == false) {
                                    exportedRequiredModules.createDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, ProjectTagLinkId, ProjectId, queryValue,  request,function (error, result) {
                                        if (error) {
                                            failureCount++;
                                        }
                                        else {
                                            successCount++;

                                        }

                                    });

                                }

                            });
                        }
                    });

                });
                if (Tagindex === Tagarray.length - 1) {
                    response.status(200);
                    response.setHeader('Content-Type', 'application/json');
                    response.send(JSON.stringify({ "Message": "Success" }, null, 3));
                }
            });

        }

    });

}
/******************************************************************************
Function: deleteContactTagLink
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if contact id is present in elastic index.
    3. deleteDocumentESWithParent - Deletes taglink document for contact id passed.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function deleteContactTagLink(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {

            var ContactId = request.query.ContactId;
            var failureCount = 0;
            var successCount = 0;
            var TagIds = request.query.TagId;
            TagIds.split(',').forEach(function (TagId, Tagindex, Tagarray) {
                ContactId.split(',').forEach(function (ContactId, index, array) {
                    var ContactTagLinkId = TagId + '#' + ContactId;
                    var checkExistance;
                    var elasticType = exportedRequiredModules.elasticContactTagsLinkTypeName;
                    exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, ContactTagLinkId, ContactId,  request,function (error, result) {
                        if (error) {
                            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                        }
                        else {
                            checkExistance = result;

                            if (checkExistance == true) {

                                exportedRequiredModules.deleteDocumentESWithParent(elasticServerClient, elasticType, ContactTagLinkId, ContactId,request, function (error, result) {
                                    if (error) {
                                        failureCount++;
                                        // ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                    }
                                    else {
                                        successCount++;
                                        // response.status(200);
                                        // response.setHeader('Content-Type', 'application/json');
                                        // response.send(JSON.stringify(result, null, 3));
                                    }
                                });
                            }
                            else {
                                failureCount++;
                                // response.status(422);
                                // response.setHeader('Content-Type', 'application/json');
                                // response.send(JSON.stringify(" No Such Tags to Delete", null, 3));

                            }
 
                        }
                    });
                });
                if (Tagindex === Tagarray.length - 1) {
                    response.status(200);
                    response.setHeader('Content-Type', 'application/json');
                    response.send(JSON.stringify({ "Message": "Success" }, null, 3));
                }
            });
        }

    });

}
/******************************************************************************
Function: deleteProjectTagLink
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if project id is present in elastic index.
    3. deleteDocumentESWithParent - Deletes taglink document for project id passed.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function deleteProjectTagLink(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {

            var ProjectId = request.query.ProjectId;

            var failureCount = 0;
            var successCount = 0;
            var TagIds = request.query.TagId;
            TagIds.split(',').forEach(function (TagId, Tagindex, Tagarray) {
                ProjectId.split(',').forEach(function (ProjectId, index, array) {
                    var ProjectTagLinkId = TagId + '#' + ProjectId;

                    var checkExistance;
                    var elasticType = exportedRequiredModules.elasticProjectTagsLinkTypeName;
                    exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, ProjectTagLinkId, ProjectId, request,function (error, result) {
                        if (error) {
                            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                        }
                        else {
                            checkExistance = result;

                            if (checkExistance == true) {
                                exportedRequiredModules.deleteDocumentESWithParent(elasticServerClient, elasticType, ProjectTagLinkId, ProjectId,request, function (error, result) {
                                    if (error) {
                                        failureCount++;
                                        // ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                    }
                                    else {
                                        successCount++;
                                        // response.status(200);
                                        // response.setHeader('Content-Type', 'application/json');
                                        // response.send(JSON.stringify(result, null, 3));
                                    }
                                });
                            }
                            else {
                                failureCount++;
                                // response.status(422);
                                // response.setHeader('Content-Type', 'application/json');
                                // response.send(JSON.stringify(" No Such Tags to Delete", null, 3));

                            }


                        }
                    });
                });
                if (Tagindex === Tagarray.length - 1) {
                    response.status(200);
                    response.setHeader('Content-Type', 'application/json');
                    response.send(JSON.stringify({ "Message": "Success" }, null, 3));
                }
            });
        }

    });

}
/******************************************************************************
Function: createOrUpdateOfficeTagLink
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if office id is present in elastic index.
    3. postQueryByOfficeTagsLink - Used to create a query for posting it to elastic index.
    4. createDocumentESWithParent - Create a new taglink document for office id passed.
    5. updateDocumentESWithParent - Updates the taglink document of office id, if taglink document is present.
	6. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function createOrUpdateOfficeTagLink(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            var RequestParam = JSON.parse(request.body);
            var OfficeIds = RequestParam.OfficeId;

            var failureCount = 0;
            var successCount = 0;
            var TagIds = RequestParam.TagId;
            TagIds.split(',').forEach(function (TagId, Tagindex, Tagarray) {
                OfficeIds.split(',').forEach(function (OfficeId, index, array) {
                    var OfficeTagLinkId = TagId + '#' + OfficeId;
                    //console.log(OfficeTagLinkId);

                    var checkExistance;
                    var elasticType = exportedRequiredModules.elasticOfficeTagsLinkTypeName;
                    exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, OfficeTagLinkId, OfficeId,  request,function (error, result) {
                        if (error) {
                            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                        }
                        else {
                            checkExistance = result;
                            exportedRequiredModules.getOfficeTagsLinkQueryObj.postQueryByOfficeTagsLink(request, TagId, checkExistance, function (err, queryValue) {
                                if (checkExistance == true) {
                                    exportedRequiredModules.updateDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, OfficeTagLinkId, OfficeId, queryValue,request, function (error, result) {
                                        if (error) {
                                            failureCount++;
                                            // ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                        }
                                        else {
                                            successCount++;
                                            
                                        }
                                    });
                                }
                                else if (checkExistance == false) {
                                    exportedRequiredModules.createDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, OfficeTagLinkId, OfficeId, queryValue, request, function (error, result) {
                                        if (error) {
                                            failureCount++;
                                            // ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                        }
                                        else {
                                            successCount++;
                                            // response.status(200);
                                            // response.setHeader('Content-Type', 'application/json');
                                            // response.send(JSON.stringify(result, null, 3));
                                        }

                                    });

                                }

                            });
                        }
                    });
                });
                if (Tagindex === Tagarray.length - 1) {
                    response.status(200);
                    response.setHeader('Content-Type', 'application/json');
                    response.send(JSON.stringify({ "Message": "Success" }, null, 3));
                }
            });
        }

    });


}


/******************************************************************************
Function: deleteOfficeTagLink
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if office id is present in elastic index.
    3. deleteDocumentESWithParent - Deletes taglink document for office id passed.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function deleteOfficeTagLink(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {

            var OfficeIds = request.query.OfficeId;

            var failureCount = 0;
            var successCount = 0;
            var TagIds = request.query.TagId;
            TagIds.split(',').forEach(function (TagId, Tagindex, Tagarray) {
                OfficeIds.split(',').forEach(function (OfficeId, index, array) {
                    var OfficeTagLinkId = request.query.TagId + '#' + OfficeId;

                    var checkExistance;
                    var elasticType = exportedRequiredModules.elasticOfficeTagsLinkTypeName;
                    exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, OfficeTagLinkId, OfficeId, request, function (error, result) {
                        if (error) {
                            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                        }
                        else {
                            checkExistance = result;

                            if (checkExistance == true) {
                                exportedRequiredModules.deleteDocumentESWithParent(elasticServerClient, elasticType, OfficeTagLinkId, OfficeId, request,function (error, result) {
                                    if (error) {
                                        failureCount++;
                                        // ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                    }
                                    else {
                                        successCount++;
                                        // response.status(200);
                                        // response.setHeader('Content-Type', 'application/json');
                                        // response.send(JSON.stringify(result, null, 3));
                                    }
                                });
                            }
                            else {
                                failureCount++;
                                // ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);

                            }
                            if (index === array.length - 1) {
                                response.status(200);
                                response.setHeader('Content-Type', 'application/json');
                                response.send(JSON.stringify({ "Message": "Success" }, null, 3));
                            }


                        }
                    });
                });
                if (Tagindex === Tagarray.length - 1) {
                    response.status(200);
                    response.setHeader('Content-Type', 'application/json');
                    response.send(JSON.stringify({ "Message": "Success" }, null, 3));
                }
            });
        }

    });

}

/******************************************************************************
Function: getProjectTagLinkAndTags
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getProjectTagsLinkQueryObj - used to form a elastic query based on user request.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function getProjectTagLinkAndTags(request, response) {


    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
                exportedRequiredModules.getProjectTagsQueryObj.getQueryByProjectTags(request, function (err, queryValue) {
                var elasticType = exportedRequiredModules.elasticProjectTagsTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType,request, function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        exportedRequiredModules.getProjectTagsLinkQueryObj.mergeTagLinksAndTags(request, result,elasticServerClient, function (error, result) {
                            if (error) {
                                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                            }
                            else {
                                //	console.log(res);
                                response.status(200);
                                response.setHeader('Content-Type', 'application/json');
                                response.send(JSON.stringify(result, null, 3));
                            }
                        });
                    }
                });

            });

        }

    });
}

/******************************************************************************
Function: getOfficeTagLinkAndTags
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getcompanyTagsLinkQueryObj - used to form a elastic query based on user request.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function getOfficeTagLinkAndTags(request, response) {


    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
                exportedRequiredModules.getOfficeTagsQueryObj.getQueryByofficeTags(request, function (err, queryValue) {
                var elasticType = exportedRequiredModules.elasticOfficeTagsTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType,request, function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        exportedRequiredModules.getOfficeTagsLinkQueryObj.mergeTagLinksAndTags(request, result,elasticServerClient, function (error, result) {
                            if (error) {
                                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                            }
                            else {
                                //	console.log(res);
                                response.status(200);
                                response.setHeader('Content-Type', 'application/json');
                                response.send(JSON.stringify(result, null, 3));
                            }
                        });
                    }
                });

            });

        }

    });
}



/******************************************************************************
Function: getContactTagLinkAndTags
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getContactTagsLinkQueryObj - used to form a elastic query based on user request.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function getContactTagLinkAndTags(request, response) {


    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
                exportedRequiredModules.getContactTagsQueryObj.getQueryByContactTags(request, function (err, queryValue) {
                var elasticType = exportedRequiredModules.elasticContactTagsTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType,request, function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        exportedRequiredModules.getContactTagsLinkQueryObj.mergeTagLinksAndTags(request, result,elasticServerClient, function (error, result) {
                            if (error) {
                                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                            }
                            else {
                                //	console.log(res);
                                response.status(200);
                                response.setHeader('Content-Type', 'application/json');
                                response.send(JSON.stringify(result, null, 3));
                            }
                        });
                    }
                });

            });

        }

    });
}