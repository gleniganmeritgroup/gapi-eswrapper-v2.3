'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (MetadataHE.js) is used for fetching the response from
endpoint /glenigan_hi/metadata

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
  host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
	metadataHE: metadata
};

/******************************************************************************
"ErrorResponse" function writes error occurring in the metadata endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response,error, errorCode){
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}

/******************************************************************************
Function: metadata
Argument: request, response
Return: response
Usage:
	1. getMetadataHEObj - returns metadata source based on metadata id request.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function metadata (request, response) {
	
	// console.log("request"+JSON.stringify(request.swagger.params['id'].value));
	exportedRequiredModules.getMetadataHEObj.getMetadataSource(request, function(error, metadataSource){
		
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
		}
		else{
			response.status(200);
			response.setHeader('Content-Type', 'application/json');
			response.send(JSON.stringify(metadataSource, null, 3));
		}
	});
}