'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (Follow.js) is used for fetching the response from
endpoint /glenigan/project/follow

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
  host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
    getProjectFollow : getProjectFollow,
    getOfficeFollow : getOfficeFollow,
    createOrUpdateProjectFollow : createOrUpdateProjectFollow,
    createOrUpdateOfficeFollow : createOrUpdateOfficeFollow,
    deleteProjectFollow : deleteProjectFollow,
    deleteOfficeFollow : deleteOfficeFollow,
    getProjectFollowByProjectId : getProjectFollowByProjectId,
    getOfficeFollowByOfficeId : getOfficeFollowByOfficeId
};

/******************************************************************************
"ErrorResponse" function writes error occurring in the follow endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response,error, errorCode){
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}

/******************************************************************************
Function: getProjectFollow
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getProjectFollowQueryObj - used to form a elastic query based on user request.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function getProjectFollow (request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
		
		  
            exportedRequiredModules.getProjectFollowQueryObj.getQueryByProjectFollows(request, function (err, queryValue) {

                var elasticType = exportedRequiredModules.elasticProjectFollowTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType,request, function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        //	console.log(res);
                        response.status(200);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));
                    }
                });
        
            });
		}
		
		});
  
    

}
/******************************************************************************
Function: getProjectFollowByProjectId
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getProjectFollowQueryObj - used to form a elastic query based on user request and project id.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function getProjectFollowByProjectId (request, response) {
   exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
            exportedRequiredModules.getProjectFollowQueryObj.getQueryByProjectFollowsByProjectId(request, function (err, queryValue) {
                var elasticType = exportedRequiredModules.elasticProjectFollowTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType, request,function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        response.status(200);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));
                    }
                });                
            });            
        }        
   });  
}

/******************************************************************************
Function: getOfficeFollow
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getOfficeFollowQueryObj - used to form a elastic query based on user request.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function getOfficeFollow(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
            exportedRequiredModules.getOfficeFollowQueryObj.getQueryByOfficeFollow(request, function (error, queryValue) {

                var elasticType = exportedRequiredModules.elasticOfficeFollowTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType,request, function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        //	console.log(res);
                        response.status(200);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));
                    }
                });
        
            });
		
		}
		
		});
  
}
/******************************************************************************
Function: getOfficeFollowByOfficeId
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getOfficeFollowQueryObj - used to form a elastic query based on user request and office id.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function getOfficeFollowByOfficeId (request, response) {
   exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
            exportedRequiredModules.getOfficeFollowQueryObj.getQueryByOfficeFollowsByOfficeId(request, function (error, queryValue) {
                var elasticType = exportedRequiredModules.elasticOfficeFollowTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType,request, function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        response.status(200);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));
                    }
                });                
            });            
        }        
   });  
}
/******************************************************************************
Function: createOrUpdateProjectFollow
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if project id is present in elastic index.
    3. postQueryByProjectFollow - Used to create a query for posting it to elastic index.
    4. createDocumentESWithParent - Create a new follow document for project id passed.
    5. updateDocumentESWithParent - Updates the follow document of project id, if follow document is present.
	6. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function createOrUpdateProjectFollow(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
		
            var RequestParam = JSON.parse(request.body);
            var ProjectIds = RequestParam.ProjectId;
            var failureCount = 0 ;
            var successCount = 0 ;
            ProjectIds.split(',').forEach(function (ProjectId, index, array) {
                var ProjectFollowId = request.query.UserId + '#' + ProjectId;
                
            
                var checkExistance;
                var elasticType = exportedRequiredModules.elasticProjectFollowTypeName;
                exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, ProjectFollowId, ProjectId, request,function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        checkExistance = result;
                        exportedRequiredModules.getProjectFollowQueryObj.postQueryByProjectFollow(request, checkExistance, function (err, queryValue) {
                            if (checkExistance == true) {
                                exportedRequiredModules.updateDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, ProjectFollowId, ProjectId, queryValue, request,function (error, result) {
                                    if (error) {
                                        failureCount++;
                                        // ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                    }
                                    else {
                                        successCount++;
                                        // response.status(200);
                                        // response.setHeader('Content-Type', 'application/json');
                                        // response.send(JSON.stringify(result, null, 3));
                                    }
                                });
                            }
                            else if (checkExistance == false) {
                                exportedRequiredModules.createDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, ProjectFollowId, ProjectId, queryValue,request, function (error, result) {
                                    if (error) {
                                        failureCount++;
                                        // ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                    }
                                    else {
                                        successCount++;
                                        // response.status(200);
                                        // response.setHeader('Content-Type', 'application/json');
                                        // response.send(JSON.stringify(result, null, 3));
                                    }
    
                                });
    
                            }
                            if (index === array.length - 1){
                                response.status(200);
                               response.setHeader('Content-Type', 'application/json');
                               response.send(JSON.stringify({"Message" : "Success"}, null, 3));
                           }
                        });
                    }
                });
            });	
		}
		
		});
	
   
}


/******************************************************************************
Function: deleteProjectFollow
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if project id is present in elastic index.
    3. deleteDocumentESWithParent - Deletes follow document for project id passed.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function deleteProjectFollow(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
            
            var ProjectIds = request.query.ProjectId;
            var failureCount = 0 ;
            var successCount = 0 ;
            ProjectIds.split(',').forEach(function (ProjectId, index, array) {
                
                var ProjectFollowId = request.query.UserId + '#' + ProjectId;
        
                var checkExistance;
                var elasticType = exportedRequiredModules.elasticProjectFollowTypeName;
                exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, ProjectFollowId, ProjectId, request,function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);    
                    }
                    else {
                        checkExistance = result;
        
                        if (checkExistance == true) {
                            exportedRequiredModules.deleteDocumentESWithParent(elasticServerClient, elasticType, ProjectFollowId, ProjectId,request, function (error, result) {
                                if (error) {
                                    failureCount++;
                                    // ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    successCount++;
                                    // response.status(200);
                                    // response.setHeader('Content-Type', 'application/json');
                                    // response.send(JSON.stringify(result, null, 3));
                                }
                            });
                        }
                        else {
                            failureCount++;
        
                        }
                        if (index === array.length - 1){
                            response.status(200);
                           response.setHeader('Content-Type', 'application/json');
                           response.send(JSON.stringify({"Message" : "Success"}, null, 3));
                       }
        
                    }
                });
            });
		
		}
		
		});
   
}
/******************************************************************************
Function: createOrUpdateOfficeFollow
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if office id is present in elastic index.
    3. postQueryByOfficeFollow - Used to create a query for posting it to elastic index.
    4. createDocumentESWithParent - Create a new follow document for office id passed.
    5. updateDocumentESWithParent - Updates the follow document of office id, if follow document is present.
	6. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function createOrUpdateOfficeFollow(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
		
            var RequestParam = JSON.parse(request.body);
            var OfficeIds = RequestParam.OfficeId;
            var failureCount = 0 ;
            var successCount = 0 ;
            OfficeIds.split(',').forEach(function (OfficeId, index, array) {
                var OfficeFollowId = request.query.UserId + '#' + OfficeId;
        
                
                var checkExistance;
                var elasticType = exportedRequiredModules.elasticOfficeFollowTypeName;
                exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, OfficeFollowId, OfficeId,  request,function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        checkExistance = result;
                        exportedRequiredModules.getOfficeFollowQueryObj.postQueryByOfficeFollow(request, checkExistance, function (error, queryValue) {
                            if (checkExistance == true) {
                                exportedRequiredModules.updateDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, OfficeFollowId, OfficeId, queryValue,request, function (error, result) {
                                    if (error) {
                                        failureCount++;
                                        // ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                    }
                                    else {
                                        successCount++;
                                        // response.status(200);
                                        // response.setHeader('Content-Type', 'application/json');
                                        // response.send(JSON.stringify(result, null, 3));
                                    }
                                });
                            }
                            else if (checkExistance == false) {
                                exportedRequiredModules.createDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, OfficeFollowId, OfficeId, queryValue,request, function (error, result) {
                                    if (error) {
                                        failureCount++;
                                        // ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                    }
                                    else {
                                        successCount++;
                                        // response.status(200);
                                        // response.setHeader('Content-Type', 'application/json');
                                        // response.send(JSON.stringify(result, null, 3));
                                    }
        
                                });
        
                            }
                            if (index === array.length - 1){
                                response.status(200);
                               response.setHeader('Content-Type', 'application/json');
                               response.send(JSON.stringify({"Message" : "Success"}, null, 3));
                           }
                        });
                    }
                });
            });
		}
		
		});
  
   

}


/******************************************************************************
Function: deleteOfficeFollow
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if office id is present in elastic index.
    3. deleteDocumentESWithParent - Deletes follow document for office id passed.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function deleteOfficeFollow(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
		
            
            var OfficeIds =  request.query.OfficeId;
            var failureCount = 0 ;
            var successCount = 0 ;
            OfficeIds.split(',').forEach(function (OfficeId, index, array) {
                var OfficeFollowId = request.query.UserId + '#' + OfficeId;
                var checkExistance;
                var elasticType = exportedRequiredModules.elasticOfficeFollowTypeName;
                exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, OfficeFollowId, OfficeId,  request,function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        checkExistance = result;
        
                        if (checkExistance == true) {
                            exportedRequiredModules.deleteDocumentESWithParent(elasticServerClient, elasticType, OfficeFollowId, OfficeId, request,function (error, result) {
                                if (error) {
                                    failureCount++;
                                    // ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    successCount++;
                                    // response.status(200);
                                    // response.setHeader('Content-Type', 'application/json');
                                    // response.send(JSON.stringify(result, null, 3));
                                }
                            });
                        }
                        else {
                            failureCount++;
        
                        }
                        if (index === array.length - 1){
                            response.status(200);
                           response.setHeader('Content-Type', 'application/json');
                           response.send(JSON.stringify({"Message" : "Success"}, null, 3));
                       }
        
                    }
                });
            });	
		}
		
		});
  
}