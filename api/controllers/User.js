'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (User.js) is used for fetching the response from
endpoint  /glenigan/user

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/

var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
	host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
	userprofile: userprofile
};

/******************************************************************************
"ErrorResponse" function writes error occurring in the user endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response, error, errorCode) {
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}


/******************************************************************************
Function: User
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getLocationsQueryObj - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function userprofile(request, response) {

	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
if(error)
        {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
}else{
        exportedRequiredModules.getSalesforceQueryObj.userProfileDataList(request, function (error, result) {
            if (error) {
                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
            }
            else {
    
                response.status(200);
                response.setHeader('Content-Type', 'application/json');
                //	resut=JSON.parse(result);
                // console.log(result);
                response.send(result, null, 3);
            }
    
    
        });
    }
    });


}