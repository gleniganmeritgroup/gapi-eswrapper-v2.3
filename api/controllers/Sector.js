'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (Sector.js) is used for fetching the response from
endpoint /glenigan/project/sector

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
  host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
	primarySector: primarySector,
	secondarySector: secondarySector,
	primaryOrSecondary: primaryOrSecondary
  };

/******************************************************************************
"ErrorResponse" function writes error occurring in the sector endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response,error, errorCode){
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}


/******************************************************************************
Function: primarySector
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getSectorsQueryObj.getQueryByPrimarySector - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response for primary sectors only.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function primarySector (request, response) {
	// res.setHeader('Content-Type', 'application/json');
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
			exportedRequiredModules.getSectorsQueryObj.getQueryByPrimarySector(subscriptionJSON,request, function(error, queryValue){
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}
				else
				{
					exportedRequiredModules.getResultObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}
/******************************************************************************
Function: secondarySector
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getSectorsQueryObj.getQueryBySecondarySector - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response for secondary sectors only.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function secondarySector (request, response) {
	// res.setHeader('Content-Type', 'application/json');
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
			exportedRequiredModules.getSectorsQueryObj.getQueryBySecondarySector(subscriptionJSON,request, function(error, queryValue){
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}
				else
				{
					exportedRequiredModules.getResultObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}
/******************************************************************************
Function: primaryOrSecondary
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getSectorsQueryObj.getQueryByPrimaryAndSecondarySector - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response for both primary and secondary sectors.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function primaryOrSecondary (request, response) {
	// res.setHeader('Content-Type', 'application/json');
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
			exportedRequiredModules.getSectorsQueryObj.getQueryByPrimaryAndSecondarySector(subscriptionJSON,request, function(error, queryValue){
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}
				else
				{
					exportedRequiredModules.getResultObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}