'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (Metadata.js) is used for fetching the response from
endpoint /glenigan/metadata

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
	host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
	metadataSearch: metadataSearch,
	metadatalocations: metadatalocations,
	metadataSectors: metadataSectors,
	metadataTowns: metadataTowns,
	metadataPostCodeDistrict: metadataPostCodeDistrict
};

/******************************************************************************
"ErrorResponse" function writes error occurring in the metadata endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response, error, errorCode) {
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}

/******************************************************************************
Function: metadataSearch
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. getMetadataSource - returns metadata source based on metadata id request.
	3. getMetadataSearchCompare - Get metadata source from metadata url
Exception function:	
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function metadataSearch(request, response) {
	var metaDataId = '_search';
	var size = parseInt(exportedRequiredModules.credentialsObj.ElasticIndex.metadata_size);
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else {
			exportedRequiredModules.getMetadataObj.getMetadataSource(request, metaDataId, size, function (error, metadataSource) {

				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {
					exportedRequiredModules.getMetadataObj.getMetadataSearchCompare(request, subscriptionJSON, metadataSource, function (error, metadataResult) {

						if (error) {
							ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else {
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(metadataResult, null, 3);
						}
					});

				}
			});
		}
	});
}
/******************************************************************************
Function: metadatalocations
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. getMetadataSource - returns metadata source based on metadata id request.
	3. getMetadataLocationCompare - Get metadata location source from metadata url
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function metadatalocations(request, response) {
	var metaDataId = 'locations';
	var size = parseInt(exportedRequiredModules.credentialsObj.ElasticIndex.metadata_size);
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else {
			exportedRequiredModules.getMetadataObj.getMetadataSource(request, metaDataId, size, function (error, metadataSource) {

				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {

					var SubscritionJSONObj = JSON.parse(subscriptionJSON);

					exportedRequiredModules.getMetadataObj.getMetadataLocationCompare(request, metadataSource, SubscritionJSONObj, function (error, metadataResult) {

						if (error) {
							ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else {

							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(metadataResult, null, 3);
						}
					});

				}

			});
		}
	});
}
/******************************************************************************
Function: metadataSectors
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. getMetadataSource - returns metadata source based on metadata id request.
	3. getMetadataSectorCompare - Get metadata sector source from metadata url
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function metadataSectors(request, response) {
	var metaDataId = 'sectors';
	var size = parseInt(exportedRequiredModules.credentialsObj.ElasticIndex.metadata_size);
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else {
			exportedRequiredModules.getMetadataObj.getMetadataSource(request, metaDataId, size, function (error, metadataSource) {

				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {

					var SubscritionJSONObj = JSON.parse(subscriptionJSON);

					exportedRequiredModules.getMetadataObj.getMetadataSectorCompare(metadataSource, SubscritionJSONObj, function (error, metadataResult) {

						if (error) {
							ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else {

							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(metadataResult, null, 3);
						}
					});

				}

			});
		}
	});
}
/******************************************************************************
Function: metadataTowns
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. getSubscribedCountyWithSalesForce - returns metadata source based on subscribed county list.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function metadataTowns(request, response) {

	var metaDataId = 'towns';
	var size = parseInt(exportedRequiredModules.credentialsObj.ElasticIndex.metadata_size);
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else {

			exportedRequiredModules.getMetadataObj.getSubscribedCountyWithSalesForce(request, size, subscriptionJSON, function (error, metadataResult) {

				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {
					response.status(200);
					response.setHeader('Content-Type', 'application/json');
					response.send(metadataResult, null, 3);
				}
			});

		}
	});

}
/******************************************************************************
Function: metadataPostCodeDistrict
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. getMetadataPostcodes - Get metadata postcode source from metadata url
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function metadataPostCodeDistrict(request, response) {
	var metaDataId = 'postcode_district';
	var requestedSize=request.query.Size;
	
	if(requestedSize !=undefined){
		var size = requestedSize;
	}
	// else{
	// var size = parseInt(exportedRequiredModules.credentialsObj.ElasticIndex.metadata_size);
	// }
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else {
			exportedRequiredModules.getMetadataObj.getMetadataPostcodes(request, metaDataId, size, subscriptionJSON, function (error, metadataSource) {

				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {

					response.status(200);
					response.setHeader('Content-Type', 'application/json');
					response.send(metadataSource, null, 3);


				}
			});
		}
	});
}