'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (FollowAlert.js) is used for fetching the response from
endpoint /glenigan/project/followalert

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
	host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
    FollowAlertCount: FollowAlertCount,
    getFollowAlertByDate: getFollowAlertByDate
};

/******************************************************************************
"ErrorResponse" function writes error occurring in the followalert endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response, error, errorCode) {
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}

/******************************************************************************
Function: FollowAlertCount
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. getCount - Creates query for getting count based on user id.
    3. getUpcomingFollowAlerts -  used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function FollowAlertCount(request, response) {
	var elasticType = exportedRequiredModules.elasticFollowAlertsTypeName;
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else {
			exportedRequiredModules.followAlertObj.getCount(request, function (error, queryValue) {
                if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {                   
                    exportedRequiredModules.getUpcomingFollowAlerts(exportedRequiredModules, elasticServerClient, elasticType, queryValue, request, function (error, result){ 
                        if (error) {
							ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else {
                            response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(result, null, 3);
						}
                    });
                }
            });
		}
	});
}
/******************************************************************************
Function: getFollowAlertByDate
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. getFollowAlertTypeDate - Creates query for getting follow alert based on type and user id.
    3. getUpcomingFollowAlerts -  used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function getFollowAlertByDate(request, response) {
	var elasticType = exportedRequiredModules.elasticFollowAlertsTypeName;
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else {
			exportedRequiredModules.followAlertObj.getFollowAlertTypeDate(request, function (error, queryValue) {
                if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {  
                    exportedRequiredModules.getUpcomingFollowAlerts(exportedRequiredModules, elasticServerClient, elasticType, queryValue, request, function (error, result){ 
                        if (error) {
							ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else {
                            response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(result, null, 3);
						}
                    });
                }
            });
		}
	});
}