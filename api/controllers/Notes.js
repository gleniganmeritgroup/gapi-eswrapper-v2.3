'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (Notes.js) is used for fetching the response from
endpoint /glenigan/project/notes

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
    host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
    getProjectNotes: getProjectNotes,
    getOfficeNotes: getOfficeNotes,
    getContactNotes: getContactNotes,
    createOrUpdateProjectNotes: createOrUpdateProjectNotes,
    createOrUpdateOfficeNotes: createOrUpdateOfficeNotes,
    createOrUpdateContactNotes: createOrUpdateContactNotes,
    deleteProjectNotes: deleteProjectNotes,
    deleteOfficeNotes: deleteOfficeNotes,
    deleteContactNotes: deleteContactNotes
};

/******************************************************************************
"ErrorResponse" function writes error occurring in the notes endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response, error, errorCode) {
    exportedRequiredModules.loggerObj.error(error);
    response.status(errorCode);
    response.setHeader('Content-Type', 'application/json');
    response.send(JSON.stringify(error, null, 3));
}
/******************************************************************************
Function: getProjectNotes
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getQueryByProjectNotes - used to form a elastic query based on user request.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function getProjectNotes(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            exportedRequiredModules.getProjectNotesQueryObj.getQueryByProjectNotes(request, function (error, queryValue) {
                if (error) {
                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                    var elasticType = exportedRequiredModules.elasticProjectNotesTypeName;
                    exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType, request,function (error, result) {
                        if (error) {
                            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                        }
                        else {
                            //	console.log(res);
                            response.status(200);
                            response.setHeader('Content-Type', 'application/json');
                            response.send(JSON.stringify(result, null, 3));
                        }
                    });
                }

            });

        }

    });




}
/******************************************************************************
Function: getOfficeNotes
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getQueryByOfficeNotes - used to form a elastic query based on user request.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function getOfficeNotes(request, response) {

    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {

            exportedRequiredModules.getOfficeNotesQueryObj.getQueryByOfficeNotes(request, function (error, queryValue) {
                if (error) {
                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                  
                    var elasticType = exportedRequiredModules.elasticOfficeNotesTypeName;
                    exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType, request,function (error, result) {
                        if (error) {
                            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                        }
                        else {
                            //	console.log(res);
                            response.status(200);
                            response.setHeader('Content-Type', 'application/json');
                            response.send(JSON.stringify(result, null, 3));
                        }
                    });
                }
            });
        }

    });


}
/******************************************************************************
Function: getContactNotes
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getQueryByContactNotes - used to form a elastic query based on user request.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function getContactNotes(request, response) {

    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            exportedRequiredModules.getContactNotesQueryObj.getQueryByContactNotes(request, function (error, queryValue) {
                if (error) {
                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                var elasticType = exportedRequiredModules.elasticContactNotesTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType, request,function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        //	console.log(res);
                        response.status(200);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));
                    }
                });
            }
            });

        }

    });


}
/******************************************************************************
Function: createOrUpdateProjectNotes
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if project id is present in elastic index.
    3. postQueryByProjectNotes - Used to create a query for posting it to elastic index.
    4. createDocumentESWithParent - Create a new note document for project id passed.
    5. updateDocumentESWithParent - Updates the note document of project id, if note document is present.
	6. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function createOrUpdateProjectNotes(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
           
            var RequestParam = JSON.parse(request.body);
            var ProjectId = RequestParam.ProjectId;
            var ProjectNoteId = RequestParam.NoteId;
            if (ProjectNoteId == undefined) {
                ProjectNoteId = exportedRequiredModules.uuidObj(request.query.UserId + exportedRequiredModules.moment().toISOString());

            }
            var checkExistance;
            var elasticType = exportedRequiredModules.elasticProjectNotesTypeName;
            exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, ProjectNoteId, ProjectId, request, function (error, result) {
                if (error) {
                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                    checkExistance = result;
                    
                    exportedRequiredModules.getProjectNotesQueryObj.postQueryByProjectNotes(request, checkExistance, function (err, queryValue) {

                        if (checkExistance == true) {
                            exportedRequiredModules.updateDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, ProjectNoteId, ProjectId, queryValue,request ,function (error, result) {
                                if (error) {
                                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    response.status(200);
                                    response.setHeader('Content-Type', 'application/json');
                                    response.send(JSON.stringify(result, null, 3));
                                }
                            });
                        }
                        else if (checkExistance == false) {
                            exportedRequiredModules.createDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, ProjectNoteId, ProjectId, queryValue,  request,function (error, result) {
                                if (error) {
                                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    response.status(200);
                                    response.setHeader('Content-Type', 'application/json');
                                    response.send(JSON.stringify(result, null, 3));
                                }

                            });

                        }
                    });
                }
            });


        }

    });


}


/******************************************************************************
Function: deleteProjectNotes
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if project id is present in elastic index.
    3. deleteDocumentESWithParent - Deletes note document for project id passed.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function deleteProjectNotes(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            var ProjectNoteId = request.query.NoteId;
        
            var ProjectId = request.query.ProjectId;

            var checkExistance;
            var elasticType = exportedRequiredModules.elasticProjectNotesTypeName;
            exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, ProjectNoteId, ProjectId, request, function (error, result) {
                if (error) {
                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                    checkExistance = result;

                    if (checkExistance == true) {
                        exportedRequiredModules.deleteDocumentESWithParent(elasticServerClient, elasticType, ProjectNoteId, ProjectId,request, function (error, result) {
                            if (error) {
                                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                            }
                            else {
                                response.status(200);
                                response.setHeader('Content-Type', 'application/json');
                                response.send(JSON.stringify(result, null, 3));
                            }
                        });
                    }
                    else {
                        response.status(422);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));

                    }

                }
            });

        }

    });


}
/******************************************************************************
Function: createOrUpdateContactNotes
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if contact id is present in elastic index.
    3. postQueryByProjectNotes - Used to create a query for posting it to elastic index.
    4. createDocumentESWithParent - Create a new note document for contact id passed.
    5. updateDocumentESWithParent - Updates the note document of contact id, if note document is present.
	6. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function createOrUpdateContactNotes(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            var RequestParam = JSON.parse(request.body);
            var ContactNoteId = RequestParam.NoteId;
            if (ContactNoteId == undefined) {
                ContactNoteId = exportedRequiredModules.uuidObj(request.query.UserId + exportedRequiredModules.moment().toISOString());

            }
         
            var ContactId = RequestParam.ContactId;

            var checkExistance;
            var elasticType = exportedRequiredModules.elasticContactNotesTypeName;
            exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, ContactNoteId, ContactId, request, function (error, result) {
                if (error) {
                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                    checkExistance = result;

                    exportedRequiredModules.getContactNotesQueryObj.postQueryByContactNotes(request, checkExistance, function (err, queryValue) {

                        if (checkExistance == true) {
                            exportedRequiredModules.updateDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, ContactNoteId, ContactId, queryValue,request, function (error, result) {
                                if (error) {
                                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    response.status(200);
                                    response.setHeader('Content-Type', 'application/json');
                                    response.send(JSON.stringify(result, null, 3));
                                }
                            });
                        }
                        else if (checkExistance == false) {
                            exportedRequiredModules.createDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, ContactNoteId, ContactId, queryValue, request, function (error, result) {
                                if (error) {
                                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    response.status(200);
                                    response.setHeader('Content-Type', 'application/json');
                                    response.send(JSON.stringify(result, null, 3));
                                }

                            });

                        }
                    });
                }
            });

        }

    });



}
/******************************************************************************
Function: deleteContactNotes
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if contact id is present in elastic index.
    3. deleteDocumentESWithParent - Deletes note document for contact id passed.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function deleteContactNotes(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            var ContactNoteId = request.query.NoteId;
            
            var ContactId = request.query.ContactId;

            var checkExistance;
            var elasticType = exportedRequiredModules.elasticContactNotesTypeName;
            exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, ContactNoteId, ContactId, request, function (error, result) {
                if (error) {
                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                    checkExistance = result;

                    if (checkExistance == true) {
                        exportedRequiredModules.deleteDocumentESWithParent(elasticServerClient, elasticType, ContactNoteId, ContactId,request, function (error, result) {
                            if (error) {
                                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                            }
                            else {
                                response.status(200);
                                response.setHeader('Content-Type', 'application/json');
                                response.send(JSON.stringify(result, null, 3));
                            }
                        });
                    }
                    else {
                        response.status(422);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));

                    }

                }
            });

        }

    });

}

/******************************************************************************
Function: createOrUpdateOfficeNotes
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if office id is present in elastic index.
    3. postQueryByProjectNotes - Used to create a query for posting it to elastic index.
    4. createDocumentESWithParent - Create a new note document for office id passed.
    5. updateDocumentESWithParent - Updates the note document of office id, if note document is present.
	6. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function createOrUpdateOfficeNotes(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            var RequestParam = JSON.parse(request.body);
            var OfficeNoteId = RequestParam.NoteId;
            if (OfficeNoteId == undefined) {
                OfficeNoteId = exportedRequiredModules.uuidObj(request.query.UserId + exportedRequiredModules.moment().toISOString());

            }
            var OfficeId = RequestParam.OfficeId;


            var checkExistance;
            var elasticType = exportedRequiredModules.elasticOfficeNotesTypeName;
            exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, OfficeNoteId, OfficeId,  request,function (error, result) {
                if (error) {
                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                    checkExistance = result;
                    //			console.log(result);
                    exportedRequiredModules.getOfficeNotesQueryObj.postQueryByOfficeNotes(request, checkExistance, function (err, queryValue) {

                        if (checkExistance == true) {
                            exportedRequiredModules.updateDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, OfficeNoteId, OfficeId, queryValue,request, function (error, result) {
                                if (error) {
                                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    response.status(200);
                                    response.setHeader('Content-Type', 'application/json');
                                    response.send(JSON.stringify(result, null, 3));
                                }
                            });
                        }
                        else if (checkExistance == false) {
                            exportedRequiredModules.createDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, OfficeNoteId, OfficeId, queryValue, request, function (error, result) {
                                if (error) {
                                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    response.status(200);
                                    response.setHeader('Content-Type', 'application/json');
                                    response.send(JSON.stringify(result, null, 3));
                                }

                            });

                        }
                    });
                }
            });

        }

    });

}


/******************************************************************************
Function: deleteOfficeNotes
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - Used to validate requested
		parameters and get the subscription JSON.
	2. checkExistanceWithParent - Check if office id is present in elastic index.
    3. deleteDocumentESWithParent - Deletes note document for office id passed.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function deleteOfficeNotes(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            var OfficeNoteId = request.query.NoteId;
            var OfficeId = request.query.OfficeId;

            //var postOfficeNotes = require('../helpers/OfficeNotesQuery.js');
            var checkExistance;
            var elasticType = exportedRequiredModules.elasticOfficeNotesTypeName;
            exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, OfficeNoteId, OfficeId, request, function (error, result) {
                if (error) {
                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                    checkExistance = result;

                    if (checkExistance == true) {
                        exportedRequiredModules.deleteDocumentESWithParent(elasticServerClient, elasticType, OfficeNoteId, OfficeId,request, function (error, result) {
                            if (error) {
                                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                            }
                            else {
                                response.status(200);
                                response.setHeader('Content-Type', 'application/json');
                                response.send(JSON.stringify(result, null, 3));
                            }
                        });
                    }
                    else {
                        response.status(400);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(" No Such Notes to Delete", null, 3));

                    }

                }
            });

        }

    });

}