'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (TaskApi.js) is used for fetching the response from
endpoints such as /glenigan/taskapi/taskqueue

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/

var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({

	host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"ErrorResponse" function writes error occurring in the taskapi endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response, error, errorCode) {
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
	taskQueue: taskQueue
};

/******************************************************************************
Function: taskQueue
Argument: request, response
Return: response

Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception.
*******************************************************************************/

function taskQueue(request, response) {

	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
	// exportedRequiredModules.paramsValidationObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		} else {
			exportedRequiredModules.taskQueueObj.exportTaskQueue(request, function (error, queryValue) {
				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				} else {
					
					exportedRequiredModules.taskQueueRequest(exportedRequiredModules,elasticServerClient,queryValue,request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}