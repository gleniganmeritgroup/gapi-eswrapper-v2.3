'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (Contact.js) is used for fetching the response from
endpoints such as /glenigan/contact and /glenigan/contact/_search

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/

var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({

	host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"ErrorResponse" function writes error occurring in the contact endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response, error, errorCode) {
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
	contact: contact,
	contactSearch: contactSearch,
	contactRadius: contactRadius,
	contactMRadius: contactMRadius
};

/******************************************************************************
Function: contact
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getCompanyContactQueryObj - used to form a elastic query based on user request.
	3. Search the query into live elastic index in Contact type.
	4. return the response
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception.
*******************************************************************************/

function contact(request, response) {

	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		} else {
			exportedRequiredModules.getCompanyContactQueryObj.getQueryByContactID(request, function (error, queryValue) {
				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				} else {
					var limits = exportedRequiredModules.paginationObj(request);
					var start = limits[0];
					var size = limits[1];

					var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
					// console.log(queryValue);
					elasticServerClient.search({
						index: exportedRequiredModules.elasticIndexName,
						type: exportedRequiredModules.elasticContactTypeName,
						from: start,
						size: size,
						body: finalQuery
					}).then(function (resp) {
						resp = exportedRequiredModules.removeUnwantedFieldsObj(resp);
						if (exportedRequiredModules.credentialsObj.QueryLogs.Enable == true) {
							if (!exportedRequiredModules.fsObj.existsSync('./api/logs/Live/Endpoints/')) {
								exportedRequiredModules.fsExtraObj.ensureDirSync('./api/logs/Live/Endpoints/');
							}
							exportedRequiredModules.fsObj.appendFileSync('./api/logs/Live/Endpoints/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +    request.query.UserId+ "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
						}
						response.status(200);
						response.setHeader('Content-Type', 'application/json');
						response.send(JSON.stringify(resp, null, 3));
					}, function (err) {
						// console.log("err"+err);
						ErrorResponse(response, {
							"Error": "Unable to fetch data from source"
						}, exportedRequiredModules.errorCodeObj.codeForResponseException);
					});
				}
			});
		}
	});
}

/******************************************************************************
Function: contactSearch
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getCompanyContactQueryObj - used to check the bespoke query formation 
		based on user request.
	3. Search the bespoke query into live elastic index in Contact type.
	4. return the response
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception.
*******************************************************************************/
function contactSearch(request, response) {

	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		} else {
			exportedRequiredModules.getCompanyContactQueryObj.contactQueryValidation(request, function (error, queryValue) {

				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				} else {
					var limits = exportedRequiredModules.paginationObj(request);
					var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
					if (typeof finalQuery != 'object'){
						finalQuery = JSON.parse(finalQuery);
					}
					if (finalQuery.from == undefined) {
						finalQuery.from = limits[0];
					}
					if (finalQuery.size == undefined) {
						finalQuery.size = limits[1];
					}
					elasticServerClient.search({

						index: exportedRequiredModules.elasticIndexName,
						type: exportedRequiredModules.elasticContactTypeName,
						body: finalQuery
					}).then(function (resp) {
						resp = exportedRequiredModules.removeUnwantedFieldsObj(resp);
						if (exportedRequiredModules.credentialsObj.QueryLogs.Enable == true) {
							if (!exportedRequiredModules.fsObj.existsSync('./api/logs/Live/BeSpokeQuery/')) {
								exportedRequiredModules.fsExtraObj.ensureDirSync('./api/logs/Live/BeSpokeQuery/');
							}
							exportedRequiredModules.fsObj.appendFileSync('./api/logs/Live/BeSpokeQuery/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +    request.query.UserId+ "\t" + request.url + "\t" +  JSON.stringify(request.body) + "\t" + JSON.stringify(finalQuery) + '\n\n');
						}
						response.status(200);
						response.setHeader('Content-Type', 'application/json');
						response.send(JSON.stringify(resp, null, 3));
					}, function (err) {
						if (exportedRequiredModules.credentialsObj.QueryLogs.Enable == true) {
							if (!exportedRequiredModules.fsObj.existsSync('./api/logs/Live/Endpoints/')) {
								exportedRequiredModules.fsExtraObj.ensureDirSync('./api/logs/Live/Endpoints/');
							}
							exportedRequiredModules.fsObj.appendFileSync('./api/logs/Live/Endpoints/Error_' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +    request.query.UserId+ "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
						}
						ErrorResponse(response, {
							"Error": "Unable to fetch data from source"
						}, exportedRequiredModules.errorCodeObj.codeForResponseException);
					});
				}
			});
		}
	});
}

/******************************************************************************
Function: contactRadius
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getCompanyContactQueryObj - used to form a radius query 
		based on user request.
	3. Search the radius query into live elastic index in contact type.
	4. return the response
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception.
*******************************************************************************/
function contactRadius(request, response) {

	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		} else {
			exportedRequiredModules.getCompanyContactQueryObj.getContactQueryByRadius(request, function (error, queryValue) {
				// console.log("queryValue",queryValue)
				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				} else {
					var limits = exportedRequiredModules.paginationObj(request);
					var start = limits[0];
					var size = limits[1];
					var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
					// console.log("finalQuery",finalQuery)
					elasticServerClient.search({

						index: exportedRequiredModules.elasticIndexName,
						type: exportedRequiredModules.elasticContactTypeName,
						from: start,
						size: size,
						body: finalQuery
					}).then(function (resp) {
						
						resp = exportedRequiredModules.removeUnwantedFieldsObj(resp);
						if (exportedRequiredModules.credentialsObj.QueryLogs.Enable == true) {
							if (!exportedRequiredModules.fsObj.existsSync('./api/logs/Live/Endpoints/')) {
								exportedRequiredModules.fsExtraObj.ensureDirSync('./api/logs/Live/Endpoints/');
							}
							// exportedRequiredModules.fsObj.appendFileSync('./api/logs/Live/BeSpokeQuery/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +    request.query.UserId+ "\t" + JSON.stringify(JSON.parse(request.body)) + "\t" + JSON.stringify(JSON.parse(finalQuery)) + '\n\n');
							exportedRequiredModules.fsObj.appendFileSync('./api/logs/Live/Endpoints/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +    request.query.UserId+ "\t" + request.url + "\t" + JSON.stringify(finalQuery) +  '\n\n');
						}
						
						response.status(200);
						response.setHeader('Content-Type', 'application/json');
						response.send(JSON.stringify(resp, null, 3));
					}, function (err) {
						if (exportedRequiredModules.credentialsObj.QueryLogs.Enable == true) {
							if (!exportedRequiredModules.fsObj.existsSync('./api/logs/Live/Endpoints/')) {
								exportedRequiredModules.fsExtraObj.ensureDirSync('./api/logs/Live/Endpoints/');
							}
							exportedRequiredModules.fsObj.appendFileSync('./api/logs/Live/Endpoints/Error_' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +    request.query.UserId+ "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
						}
						ErrorResponse(response, {
							"Error": "Unable to fetch data from source"
						}, exportedRequiredModules.errorCodeObj.codeForResponseException);
					});
				}
			});
		}
	});
}
/******************************************************************************
Function: contactMRadius
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getCompanyContactQueryObj - used to form a mradius query 
		based on user request.
	3. Search the radius query into live elastic index in contact type.
	4. return the response
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception.
*******************************************************************************/
function contactMRadius(request, response) {
	var mLimit = exportedRequiredModules.mobileSize;
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		} else {
			exportedRequiredModules.getCompanyContactQueryObj.getContactQueryByRadius(request, function (error, queryValue) {
				// console.log("queryValue",queryValue)
				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				} else {
					var limits = exportedRequiredModules.paginationObj(request);
					var start = limits[0];
					var size = mLimit;
					var finalQuery = exportedRequiredModules.parserObj.parse(queryValue);
					var fieldsList = exportedRequiredModules.sourceIncludeMappingObj.MobileSourceFields.Contact;
					// console.log("finalQuery",finalQuery)
					elasticServerClient.search({
						_sourceInclude: fieldsList,
						index: exportedRequiredModules.elasticIndexName,
						type: exportedRequiredModules.elasticContactTypeName,
						from: start,
						size: size,
						body: finalQuery
					}).then(function (resp) {
						
						resp = exportedRequiredModules.removeUnwantedFieldsObj(resp);
						if (exportedRequiredModules.credentialsObj.QueryLogs.Enable == true) {
							if (!exportedRequiredModules.fsObj.existsSync('./api/logs/Live/Endpoints/')) {
								exportedRequiredModules.fsExtraObj.ensureDirSync('./api/logs/Live/Endpoints/');
							}
							// exportedRequiredModules.fsObj.appendFileSync('./api/logs/Live/BeSpokeQuery/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +    request.query.UserId+ "\t" + JSON.stringify(JSON.parse(request.body)) + "\t" + JSON.stringify(JSON.parse(finalQuery)) + '\n\n');
							exportedRequiredModules.fsObj.appendFileSync('./api/logs/Live/Endpoints/' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +    request.query.UserId+ "\t" + request.url + "\t" + JSON.stringify(finalQuery) +  '\n\n');
						}
						
						response.status(200);
						response.setHeader('Content-Type', 'application/json');
						response.send(JSON.stringify(resp, null, 3));
					}, function (err) {
						if (exportedRequiredModules.credentialsObj.QueryLogs.Enable == true) {
							if (!exportedRequiredModules.fsObj.existsSync('./api/logs/Live/Endpoints/')) {
								exportedRequiredModules.fsExtraObj.ensureDirSync('./api/logs/Live/Endpoints/');
							}
							exportedRequiredModules.fsObj.appendFileSync('./api/logs/Live/Endpoints/Error_' + exportedRequiredModules.moment().format('DDMMYYYY') + '.log', exportedRequiredModules.moment().format('DD-MM-YYYYTHH:mm:ss') + "\t" +    request.query.UserId+ "\t" + request.url + "\t" + JSON.stringify(finalQuery) + '\n\n');
						}
						ErrorResponse(response, {
							"Error": "Unable to fetch data from source"
						}, exportedRequiredModules.errorCodeObj.codeForResponseException);
					});
				}
			});
		}
	});
}