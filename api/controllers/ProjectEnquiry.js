'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (ProjectEnquiry.js) is used for fetching the response from


********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
	host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
	requestSend: requestSend
	
  };

/******************************************************************************
"ErrorResponse" function writes error occurring in the enquiry endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response, error, errorCode) {
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}


/******************************************************************************
Function: requestSend
Argument: request, response
Return: response

Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function requestSend (request, response) {
	// res.setHeader('Content-Type', 'application/json');
	var elasticType = exportedRequiredModules.elasticMyDayTypeName;
	// console.log("elasticType",elasticType)
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
			exportedRequiredModules.ProjectEnquireRequestObj.requestSend(request, function(error, queryValue){
				
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}
				else
				{
					response.status(200);
                    response.setHeader('Content-Type', 'application/json');
                    response.send(JSON.stringify(queryValue, null, 3))
				}
			});
		}
	});
}
