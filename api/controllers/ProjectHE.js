'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (ProjectHE.js) is used for fetching the response from
endpoint /glenigan_hi/project/postcode

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
	
  host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"ErrorResponse" function writes error occurring in the postcode endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response,error, errorCode){
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
	projectHE: project,
	newProjectHE: newProject,
	updatedProjectHE: updatedProject
  };
  
/******************************************************************************
Function: project
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getProjectQueryHEObj.getQueryByProject - used to form a elastic query based on user request.
	3. getResultHEObj - used to search the query into live elastic index.
	4. return the response for all projects.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function project (request, response) {
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}else{
		exportedRequiredModules.getProjectQueryHEObj.getQueryByProject(request, function(error, queryValue){
			// console.log("queryValue"+queryValue);
			if(error){
				ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
			}
			else
			{
				exportedRequiredModules.getResultHEObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
					if(error){
						ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
					}
					else{
						response.status(200);
						response.setHeader('Content-Type', 'application/json');
						response.send(JSON.stringify(result, null, 3));
					}
				});
			}
		});
	}
});
}
/******************************************************************************
Function: newProject
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getProjectQueryHEObj.getQueryByNewProject - used to form a elastic query based on user request.
	3. getResultHEObj - used to search the query into live elastic index.
	4. return the response for new projects.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function newProject (request, response) {
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
			exportedRequiredModules.getProjectQueryHEObj.getQueryByNewProject(subscriptionJSON,request, function(error, queryValue){
				// console.log("queryValue"+queryValue);
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}
				else
				{
					exportedRequiredModules.getResultHEObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}
/******************************************************************************
Function: updatedProject
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getProjectQueryHEObj.getQueryByUpdatedProject - used to form a elastic query based on user request.
	3. getResultHEObj - used to search the query into live elastic index.
	4. return the response for updated projects.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function updatedProject (request, response) {
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
			exportedRequiredModules.getProjectQueryHEObj.getQueryByUpdatedProject(subscriptionJSON,request, function(error, queryValue){
				// console.log("error"+error);
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}
				else
				{
					exportedRequiredModules.getResultHEObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}