'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (Tracking.js) is used for fetching the response from
endpoint /glenigan/tracking

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
    host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
    getLastTransactions: getLastTransactions
};


/******************************************************************************
"ErrorResponse" function writes error occurring in the taglink endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response, error, errorCode) {
    exportedRequiredModules.loggerObj.error(error);
    response.status(errorCode);
    response.setHeader('Content-Type', 'application/json');
    response.send(JSON.stringify(error, null, 3));
}

/******************************************************************************
Function: getProjectTagLink
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getProjectTagsLinkQueryObj - used to form a elastic query based on user request.
	3. getResultNotesTagFollow - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/

function getLastTransactions(request, response) {

    var param =  request.query["app-token"]
    if(param == undefined){
      
        exportedRequiredModules.getLastTransactionDataObj.getTransactionData(request,elasticServerClient, function (error, queryValue, typeIds) {
            if (error) {
                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
            }else {
                var type = request.query.Type
                var elasticType = '';
                if(type == "Project"){
                    elasticType = exportedRequiredModules.elasticTypeName;
                }else if(type == "Office"){
                    elasticType = exportedRequiredModules.elasticCompanyTypeName;
                }else if(type == "Contact"){
                    elasticType = exportedRequiredModules.elasticContactTypeName;
                }
            
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType, request,function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }else {
                        if(result.total == 0){
                            response.status(200);
                            response.setHeader('Content-Type', 'application/json');
                            response.send(JSON.stringify(result, null, 3));
                        }else{
                            var newSource = [];
                            var newResult = {};
                            newResult.total = result.total
                            typeIds.forEach(function(typeID, index, array){
                            // var testArray = [ '20055094','20055219' ]
                            // testArray.forEach(function(typeID, index, array){
                                var endList = 0;
                                result.results.forEach(function(record, index1, array1){
                                    if(typeID == record.id){
                                        newSource.push(result.results[index1])
                                    }
                                    if(index1+1 == array1.length){
                                        newResult.results = newSource;
                                        endList = 1;
                                    }
                                })
                                if(index+1 == array.length && endList == 1){
                                    response.status(200);
                                    response.setHeader('Content-Type', 'application/json');
                                    response.send(JSON.stringify(newResult, null, 3));
                                }
                            })
                        }	
                    }
                });
            }

        });
    
           
    }else{
        exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
            if (error) {
                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
            }
            else {
                    exportedRequiredModules.getLastTransactionDataObj.getTransactionData(request,subscriptionJSON,elasticServerClient, function (error,  queryValue, typeIds) {
                        if (error) {
                            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                        }else {
                            var type = request.query.Type
                            var elasticType = '';
                            if(type == "Project"){
                                elasticType = exportedRequiredModules.elasticTypeName;
                            }else if(type == "Office"){
                                elasticType = exportedRequiredModules.elasticCompanyTypeName;
                            }else if(type == "Contact"){
                                elasticType = exportedRequiredModules.elasticContactTypeName;
                            }

                            exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType,request, function (error, result) {
                            
							if (error) {
                                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                            }else {
                                if(result.total == 0){
									response.status(200);
									response.setHeader('Content-Type', 'application/json');
									response.send(JSON.stringify(result, null, 3));
								}else{
									var newSource = [];
									var newResult = {};
									newResult.total = result.total
									typeIds.forEach(function(typeID, index, array){
									// var testArray = [ '20055094','20055219' ]
									// testArray.forEach(function(typeID, index, array){
										var endList = 0;
										result.results.forEach(function(record, index1, array1){
											if(typeID == record.id){
												newSource.push(result.results[index1])
											}
											if(index1+1 == array1.length){
												newResult.results = newSource;
												endList = 1;
											}
										})
										if(index+1 == array.length && endList == 1){
											response.status(200);
											response.setHeader('Content-Type', 'application/json');
											response.send(JSON.stringify(newResult, null, 3));
										}
									})
								}	
                            }
                        });
                    }
    
                });
    
            }
    
        });
    }
    
}
