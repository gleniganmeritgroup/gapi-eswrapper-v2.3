'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (PlanningApplication.js) is used for fetching the response from
endpoint /glenigan/project/planningapplication

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
  host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
	planningAuthority: planningAuthority,
	planningType: planningType,
	applicationType: applicationType,
	planningApplication: planningApplication,
	applicationId: applicationId,
	applicationDate: applicationDate,
	permissionDate: permissionDate,
	refusedDate: refusedDate,
	withdrawnDate: withdrawnDate
  };

/******************************************************************************
"ErrorResponse" function writes error occurring in the planningapplication endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response,error, errorCode){
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}


/******************************************************************************
Function: planningAuthority
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getPlanningApplicationQueryObj.getQueryByPlanningAuthority - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response for planning authority.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function planningAuthority (request, response) {
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
			exportedRequiredModules.getPlanningApplicationQueryObj.getQueryByPlanningAuthority(subscriptionJSON,request, function(error, queryValue){
				
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}
				else
				{
					exportedRequiredModules.getResultObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}

/******************************************************************************
Function: planningType
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getPlanningApplicationQueryObj.getQueryByPlanningType - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response for planning type.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function planningType (request, response) {
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}else{
			exportedRequiredModules.getPlanningApplicationQueryObj.getQueryByPlanningType(subscriptionJSON,request, function(error, queryValue){
				
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}
				else
				{
					exportedRequiredModules.getResultObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}
/******************************************************************************
Function: applicationType
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getPlanningApplicationQueryObj.getQueryByApplicationType - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response for application type.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function applicationType (request, response) {
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}else{
			exportedRequiredModules.getPlanningApplicationQueryObj.getQueryByApplicationType(subscriptionJSON,request, function(error, queryValue){
				
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}else{
					exportedRequiredModules.getResultObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}
/******************************************************************************
Function: planningApplication
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getPlanningApplicationQueryObj.getQueryByPlanningApplication - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response for planning application.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function planningApplication (request, response) {
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}else{
			exportedRequiredModules.getPlanningApplicationQueryObj.getQueryByPlanningApplication(subscriptionJSON,request, function(error, queryValue){
				
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}else{
					exportedRequiredModules.getResultObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}
/******************************************************************************
Function: applicationId
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getPlanningApplicationQueryObj.getQueryByApplicationId - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response for application id.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function applicationId (request, response) {
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}else{
			exportedRequiredModules.getPlanningApplicationQueryObj.getQueryByApplicationId(subscriptionJSON,request, function(error, queryValue){
				// console.log("queryValue"+queryValue);
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}else{
					exportedRequiredModules.getResultObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}
/******************************************************************************
Function: applicationDate
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getPlanningApplicationQueryObj.getQueryByApplicationDate - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response for application date.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function applicationDate (request, response) {
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}else{
			exportedRequiredModules.getPlanningApplicationQueryObj.getQueryByApplicationDate(subscriptionJSON,request, function(error, queryValue){
				// console.log("queryValue"+queryValue);
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}else{
					exportedRequiredModules.getResultObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}
/******************************************************************************
Function: permissionDate
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getPlanningApplicationQueryObj.getQueryByPermissionDate - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response for permission date.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function permissionDate (request, response) {
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}else{
			exportedRequiredModules.getPlanningApplicationQueryObj.getQueryByPermissionDate(subscriptionJSON,request, function(error, queryValue){
				// console.log("queryValue"+queryValue);
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}else{
					exportedRequiredModules.getResultObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}
/******************************************************************************
Function: refusedDate
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getPlanningApplicationQueryObj.getQueryByRefusedDate - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response for refused date.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function refusedDate (request, response) {
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}else{
			exportedRequiredModules.getPlanningApplicationQueryObj.getQueryByRefusedDate(subscriptionJSON,request, function(error, queryValue){
				// console.log("queryValue"+queryValue);
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}else{
					exportedRequiredModules.getResultObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}
/******************************************************************************
Function: withdrawnDate
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getPlanningApplicationQueryObj.getQueryByWithdrawnDate - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response for withdrawn date.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function withdrawnDate (request, response) {
	
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}else{
			exportedRequiredModules.getPlanningApplicationQueryObj.getQueryByWithdrawnDate(subscriptionJSON,request, function(error, queryValue){
				// console.log("queryValue"+queryValue);
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}else{
					exportedRequiredModules.getResultObj(exportedRequiredModules,elasticServerClient,fieldsList,limits,queryValue, request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}