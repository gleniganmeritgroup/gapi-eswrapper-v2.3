'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetSizesQuery.js) is used form a query for given Sizes .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryBySizes
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Sizes. 
	2. 'getSubscriptionQueryObj' returns location and sector based on subscription json.
	3. 'getSizeMetadata' returns metadata of size.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryBySizes = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var sizeValues = requireQuery.query.Names;
	
	if ((sizeValues == undefined)) {
		callback({Error:"Parameter Size Values is Missing"},null);
	}
	else{
		
		var typeProjectSize = jsonObject.Subscription.HasSmall.toString();
		
		var sizeList = [];
		if (sizeValues != undefined){
				sizeValues.split(',').forEach(function (size){
					sizeList.push(size);
			});}
		/*******************************************************************************************/
		/********************************************************************************************
		This Condition will validate the queried Size and Subscribed size
		********************************************************************************************/
		/*******************************************************************************************/
		
		if((typeProjectSize == false) && (sizeList.includes("small") > -1)){
			callback({Error: 'Your are not authorized for small projects'},null);
		}
		else
		{
			exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
				if(err){
					callback(err, null);
				}else{
					try{
						var queryBuildProjectSize={};
						queryBuildProjectSize.ProjectSize=sizeList;
						var queryProjectSizeField='{"terms": %s}';
							
						var ProjectSize = '{"bool": {"should": [ '+exportedRequiredModules.convertComma2Array(queryProjectSizeField, JSON.stringify(queryBuildProjectSize))+' ]}}'; 
					
						var projectHistories =exportedRequiredModules.getTimeRangeQueryHEObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryHEObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						if (ProjectSize != '' ){ queryConcat = queryConcat +', '+ProjectSize; }
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);
					}catch (err){
						callback(err, null);
					}
				}
			});
		}
	}
}