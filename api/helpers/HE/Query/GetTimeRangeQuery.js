'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetTimeRangeQuery.js) is used to form an elastic query for TimeRange parameter.
********************************************************************************************/
/*******************************************************************************************/ 

/******************************************************************************
Function: byTimeRange
Argument:exportedRequiredModules,require
Return: return a elastic search query 
Usage:
	1. Forms a elastic query time range from and to. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.byTimeRange = function (exportedRequiredModules,require)
{
	// var projectHistories = '{ "bool": { "must_not": [ { "term": { "IsArchived": "false" } } ] } }'
	// return projectHistories;
	var daysValues = require.query.TimeRange;
	
	if (daysValues != undefined) {
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		
		var days = '';
		
		if (daysValues.includes('From') != true ){ 
			
			try{
				var daysRegex = /^([\d]+)([\w])$/g;
				var daysCountPattern = daysRegex.exec(daysValues);
				var daysCount = daysCountPattern[1];
				var daysPattern = daysCountPattern[2];
			}catch(err)
			{ throw ({Error : 'Parameter "TimeRange" value is Invalid.'}) }
			
			if (daysPattern.toLocaleLowerCase() == 'm'){
				daysCount = daysCount * 30;
			}
			else if (daysPattern.toLocaleLowerCase() == 'y') {
				daysCount = daysCount * 365;
			}
			else if (daysPattern.toLocaleLowerCase() == 'd') {
				daysCount = daysCount
			}
			else{
				throw ({Error : 'Parameter "TimeRange" value is Invalid.'})
			}
			
			var previousDate = exportedRequiredModules.moment().subtract(daysCount, 'days').format("YYYY-MM-DDT00:00:00");
			var projectHistories ='{ "nested": { "path": "Histories", "query": { "bool": { "filter": [ { "range": { "Histories.Modified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
			return projectHistories;
		}
		else{
			try{
				var fromToRegex = /From\s*\:\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*and\s*To\s*\:\s*([\d]{2}\-[\d]{2}\-[\d]{4})/g;

				var fromToDate = fromToRegex.exec(daysValues);
				
				if(fromToDate == null){
					throw ({Error : 'Parameter "TimeRange" value is Invalid. Date format should be \"DD-MM-YYYY\"'});
				}else{
					var currentDate = exportedRequiredModules.moment(fromToDate[2],'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59");
					var previousDate = exportedRequiredModules.moment(fromToDate[1],'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00");
					var projectHistories ='{ "nested": { "path": "Histories", "query": { "bool": { "filter": [ { "range": { "Histories.Modified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
					return projectHistories;
				}
			}catch(err)
			{ 
				throw ({Error : 'Parameter "TimeRange" value is Invalid. Date format should be \"DD-MM-YYYY\"'});
			}
		}
	}
	else{
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		var previousDate = exportedRequiredModules.moment().subtract(1, 'days').format("YYYY-MM-DDT00:00:00");
		var projectHistories ='{ "nested": { "path": "Histories", "query": { "bool": { "filter": [ { "range": { "Histories.Modified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
		return projectHistories;
	}
}