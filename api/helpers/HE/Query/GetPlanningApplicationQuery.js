'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetPlanningApplicationQuery.js) is used form a query for given Sizes .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryByWithdrawnDate
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for withdrawn date of planning application. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/ 
exports.getQueryByWithdrawnDate = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var WithdrawnDateFrom = requireQuery.query.From;
	var WithdrawnDateTo = requireQuery.query.To;
	
	if ((WithdrawnDateFrom == undefined) && (WithdrawnDateTo == undefined)){
		callback({Error:"Parameter From/To is Missing"},null);
	}
	else if((WithdrawnDateFrom != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(WithdrawnDateFrom) == null)){
		callback({Error:"Parameter From value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
	else if((WithdrawnDateTo != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(WithdrawnDateTo) == null)){
		callback({Error:"Parameter To value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
	else{
		var gte,lte;
		if (WithdrawnDateFrom != undefined) {
			gte = exportedRequiredModules.moment(WithdrawnDateFrom,'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00")+'.000Z';
		}
		if (WithdrawnDateTo != undefined) {
			lte = exportedRequiredModules.moment(WithdrawnDateTo,'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59")+'.000Z';
		}
		if((gte != undefined) && (lte != undefined) && (exportedRequiredModules.moment(WithdrawnDateFrom,'DD-MM-YYYY') > exportedRequiredModules.moment(WithdrawnDateTo,'DD-MM-YYYY'))){
			callback({Error:"From date is greater than To date."},null);
		}else{
			exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
				if(err){
					callback(err, null);
				}else{
					try{					
						var dateConcat=''
						if((gte != undefined) && (lte != undefined)){
							dateConcat = '"gte": "'+gte+'","lte": "'+lte+'"';
						}
						else if(gte != undefined){
							dateConcat = '"gte": "'+gte+'"';
						}
						else if(lte != undefined){
							dateConcat = '"lte": "'+lte+'"';
						}

						var withdrawnDateQueryFormation = '{"nested":{"path":"Applications","query":{"bool":{"should":[{"range":{"Applications.WithdrawnDate":{'+dateConcat+'}}}]}}}}';
						
						// console.log("applicationIdQueryFormation"+applicationIdQueryFormation);
					
						var projectHistories =exportedRequiredModules.getTimeRangeQueryHEObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryHEObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						if (withdrawnDateQueryFormation != ''){ queryConcat = queryConcat +', '+withdrawnDateQueryFormation; }
						
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);
					}catch (err){
						callback(err, null);
					}
				}
			});
		}
	}
}
/******************************************************************************
Function: getQueryByRefusedDate
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for refused date of planning application. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByRefusedDate = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var refusedDateFrom = requireQuery.query.From;
	var refusedDateTo = requireQuery.query.To;
	
	if ((refusedDateFrom == undefined) && (refusedDateTo == undefined)){
		callback({Error:"Parameter From/To is Missing"}, null);
	}
	else if((refusedDateFrom != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(refusedDateFrom) == null)){
		callback({Error:"Parameter From value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
	else if((refusedDateTo != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(refusedDateTo) == null)){
		callback({Error:"Parameter To value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
	else{
		
		var gte,lte;
		if (refusedDateFrom != undefined) {
			gte = exportedRequiredModules.moment(refusedDateFrom,'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00")+'.000Z';
		}
		if (refusedDateTo != undefined) {
			lte = exportedRequiredModules.moment(refusedDateTo,'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59")+'.000Z';
		}
		if((gte != undefined) && (lte != undefined) && (exportedRequiredModules.moment(refusedDateFrom,'DD-MM-YYYY') > exportedRequiredModules.moment(refusedDateTo,'DD-MM-YYYY'))){
			callback({Error:"From date is greater than To date."},null);
		}else{
			exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
				if(err){
					callback(err, null);
				}else{
					try{	
						var dateConcat=''
						if((gte != undefined) && (lte != undefined)){
							dateConcat = '"gte": "'+gte+'","lte": "'+lte+'"';
						}
						else if(gte != undefined){
							dateConcat = '"gte": "'+gte+'"';
						}
						else if(lte != undefined){
							dateConcat = '"lte": "'+lte+'"';
						}

						var refusedDateQueryFormation = '{"nested":{"path":"Applications","query":{"bool":{"should":[{"range":{"Applications.RefusedDate":{'+dateConcat+'}}}]}}}}';
						
						// console.log("applicationIdQueryFormation"+applicationIdQueryFormation);
						
						var projectHistories =exportedRequiredModules.getTimeRangeQueryHEObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryHEObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						if (refusedDateQueryFormation != ''){ queryConcat = queryConcat +', '+refusedDateQueryFormation; }
						
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);
					
					}catch (err){
						callback(err, null);
					}
				}
			});
		}
	}
}
/******************************************************************************
Function: getQueryByPermissionDate
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for permission date of planning application. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByPermissionDate = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var permissionDateFrom = requireQuery.query.From;
	var permissionDateTo = requireQuery.query.To;
	
	if ((permissionDateFrom == undefined) && (permissionDateTo == undefined)){
		callback({Error:"Parameter From/To is Missing"}, null);
	}
	else if((permissionDateFrom != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(permissionDateFrom) == null)){
		callback({Error:"Parameter From value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
	else if((permissionDateTo != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(permissionDateTo) == null)){
		callback({Error:"Parameter To value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
	else{
		var gte,lte;
		if (permissionDateFrom != undefined) {
			gte = exportedRequiredModules.moment(permissionDateFrom,'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00")+'.000Z';
		}
		if (permissionDateTo != undefined) {
			lte = exportedRequiredModules.moment(permissionDateTo,'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59")+'.000Z';
		}
		if((gte != undefined) && (lte != undefined) && (exportedRequiredModules.moment(permissionDateFrom,'DD-MM-YYYY') > exportedRequiredModules.moment(permissionDateTo,'DD-MM-YYYY'))){
			callback({Error:"From date is greater than To date."},null);
		}else{
			exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
				if(err){
					callback(err, null);
				}else{
					try{	
						var dateConcat=''
						if((gte != undefined) && (lte != undefined)){
							dateConcat = '"gte": "'+gte+'","lte": "'+lte+'"';
						}
						else if(gte != undefined){
							dateConcat = '"gte": "'+gte+'"';
						}
						else if(lte != undefined){
							dateConcat = '"lte": "'+lte+'"';
						}

						var permissionDateQueryFormation = '{"nested":{"path":"Applications","query":{"bool":{"should":[{"range":{"Applications.PermissionDate":{'+dateConcat+'}}}]}}}}';
						
						// console.log("applicationIdQueryFormation"+applicationIdQueryFormation);
						
						var projectHistories =exportedRequiredModules.getTimeRangeQueryHEObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryHEObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						if (permissionDateQueryFormation != ''){ queryConcat = queryConcat +', '+permissionDateQueryFormation; }
						
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);
						
					}catch (err){
						callback(err, null);
					}
				}
			});
		}
	}
}
/******************************************************************************
Function: getQueryByApplicationDate
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for application date of planning application. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByApplicationDate = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var applicationDateFrom = requireQuery.query.From;
	var applicationDateTo = requireQuery.query.To;
	
	if ((applicationDateFrom == undefined) && (applicationDateTo == undefined)){
		callback({Error:"Parameter From/To is Missing"}, null);
	}
	else if((applicationDateFrom != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(applicationDateFrom) == null)){
		callback({Error:"Parameter From value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
	else if((applicationDateTo != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(applicationDateTo) == null)){
		callback({Error:"Parameter To value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
	else{
		var gte,lte;
		if (applicationDateFrom != undefined) {
			gte = exportedRequiredModules.moment(applicationDateFrom,'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00")+'.000Z';
		}
		if (applicationDateTo != undefined) {
			lte = exportedRequiredModules.moment(applicationDateTo,'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59")+'.000Z';
		}
		
		if((gte != undefined) && (lte != undefined) && (exportedRequiredModules.moment(applicationDateFrom,'DD-MM-YYYY') > exportedRequiredModules.moment(applicationDateTo,'DD-MM-YYYY'))){
			callback({Error:"From date is greater than To date."},null);
		}else{
			exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
				if(err){
					callback(err, null);
				}else{
					try{	
						var dateConcat=''
						if((gte != undefined) && (lte != undefined)){
							dateConcat = '"gte": "'+gte+'","lte": "'+lte+'"';
						}
						else if(gte != undefined){
							dateConcat = '"gte": "'+gte+'"';
						}
						else if(lte != undefined){
							dateConcat = '"lte": "'+lte+'"';
						}

						var applicationDateQueryFormation = '{"nested":{"path":"Applications","query":{"bool":{"should":[{"range":{"Applications.ApplicationDate":{'+dateConcat+'}}}]}}}}';
						
						// console.log("applicationIdQueryFormation"+applicationIdQueryFormation);
					
						var projectHistories =exportedRequiredModules.getTimeRangeQueryHEObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryHEObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						if (applicationDateQueryFormation != ''){ queryConcat = queryConcat +', '+applicationDateQueryFormation; }
						
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);
					
					}catch (err){
						callback(err, null);
					}
				}
			});
		}
	}
}
/******************************************************************************
Function: getQueryByApplicationId
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for application id. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByApplicationId = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var applicationIds = requireQuery.query.Ids;
	
	var applicationIdList = [];
	if(applicationIds == undefined){
		callback({Error:"Parameter Ids is Missing"}, null);
	}
	else{
		if (applicationIds != undefined){
			applicationIds.split(',').forEach(function (id){
				applicationIdList.push(id);
		});}
		// console.log("applicationIdList"+applicationIdList);
		exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			if(err){
				callback(err, null);
			}else{
				try{	
					var queryBuildApplicationId = {};
					
					var queryFieldApplicationId = '{"terms": %s}';
					// var applicationIdField = 'Applications.ApplicationNumber';
					queryBuildApplicationId["Applications.ApplicationNumber"] = applicationIdList;
					
					var applicationIdQueryFormation = '';
				
					if (applicationIdList.length > 0 ){ applicationIdQueryFormation = '{"nested":{"path":"Applications","query":{"bool":{"must":['+exportedRequiredModules.convertComma2Array(queryFieldApplicationId, JSON.stringify(queryBuildApplicationId))+']}}}}'; }
					
					// console.log("applicationIdQueryFormation"+applicationIdQueryFormation);
				
					var projectHistories =exportedRequiredModules.getTimeRangeQueryHEObj.byTimeRange(exportedRequiredModules,requireQuery);
					var sortBy = exportedRequiredModules.getSortbyQueryHEObj.SortByCondition(exportedRequiredModules,requireQuery);
					
					var queryConcat=projectHistories
					if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
					if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
					if (applicationIdQueryFormation != ''){ queryConcat = queryConcat +', '+applicationIdQueryFormation; }
					
					var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
					// console.log("FinalQuery"+FinalQuery);
					callback(null, FinalQuery);

				}catch (err){
					callback(err, null);
				}
			}
		});
	}
}
/******************************************************************************
Function: getQueryByPlanningApplication
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for planning application. 
	2. 'getPlanningApplicationMetadata' returns authorized application type,planning type,council names from metadata.
	3. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByPlanningApplication = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var councilValues = requireQuery.query.CouncilNames;
	var planningValues = requireQuery.query.PlanningNames;
	
	if ((councilValues == undefined) || ((planningValues == undefined))){
		callback({Error:"Parameter CouncilNames & PlanningNames is Missing"}, null);
	}else{
		var councilValuesList = [];
		var planningValuesList = [];
		var applicationTypeList = [];
		
		if (councilValues != undefined){
			councilValues.split(',').forEach(function (council){
				councilValuesList.push(council.toLowerCase());
		});}
		if (planningValues != undefined){
			planningValues.split(',').forEach(function (planning){
				planningValuesList.push(planning.toLowerCase());
		});}
		

		exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			if(err){
				callback(err, null);
			}else{
					
				getPlanningApplicationMetadata(exportedRequiredModules, councilValuesList, planningValuesList, function(err, councilDataList, planningTypeDataList){
					
				if (err){
					callback(err, null);
				}
				else if((councilDataList.length != councilValuesList.length) && (councilValuesList.length > 0)){
					callback({Error: 'Invalid Council Names given'}, null);
				}
				else if((planningTypeDataList.length != planningValuesList.length) && (planningValuesList.length > 0)){
					callback({Error: 'Invalid Planning Names given'}, null);
				}
				else{
					try{
						var queryBuildAuthority = {};
						var queryBuildPlanningType = {};
						
						var queryFieldPlanningType = '{"terms": %s}';
						var queryFieldAuthority = '{"terms": %s}';
						
						queryBuildAuthority.PlanningAuthorities = councilDataList;
						queryBuildPlanningType.PlanningTypes = planningTypeDataList;
						
						var authorityQueryFormation ='';
						var planningTypeQueryFormation = '';
						
						if (councilDataList.length > 0 ){ authorityQueryFormation = '{"bool": {"should": [ '+exportedRequiredModules.convertComma2Array(queryFieldAuthority, JSON.stringify(queryBuildAuthority))+' ]}}'; }

						if (planningTypeDataList.length > 0 ){ planningTypeQueryFormation = '{"bool": {"should": [ '+exportedRequiredModules.convertComma2Array(queryFieldPlanningType, JSON.stringify(queryBuildPlanningType))+' ]}}'; }

						
						var projectHistories =exportedRequiredModules.getTimeRangeQueryHEObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryHEObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						if (authorityQueryFormation != ''){ queryConcat = queryConcat +', '+authorityQueryFormation; }
						if (planningTypeQueryFormation != ''){ queryConcat = queryConcat +', '+planningTypeQueryFormation; }
						
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);
					}catch (err){
						callback(err, null);
					}
					}
				});
				
			}
		});
	}
}

/******************************************************************************
Function: getQueryByPlanningType
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for planning type. 
	2. 'getPlanningTypeMetadata' returns authorized planning type from metadata.
	3. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByPlanningType = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var planningValues = requireQuery.query.PlanningNames;
	
	var planningList = [];
	
	if (planningValues != undefined){
		planningValues.split(',').forEach(function (planning){
			planningList.push(planning.toLowerCase());
	});}

	exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
		if(err){
			callback(err, null);
		}else{
				
				getPlanningTypeMetadata(exportedRequiredModules, planningList, function(err, planningDataList){
					// console.log("planningDataList"+planningDataList);
				if (err){
					callback(err, null);
				}
				else if((planningDataList.length != planningList.length) && (planningList.length > 0)){
					callback({Error: 'Invalid Planning Names given'}, null);
				}else{
					try{
						var queryBuildPlanningType = {};
						
						var queryFieldPlanningType = '{"terms": %s}';
						
						queryBuildPlanningType.PlanningTypes = planningDataList;
						
						var planningTypeQueryFormation = '';
					
						if (planningDataList.length > 0 ){ planningTypeQueryFormation = '{"bool": {"should": [ '+exportedRequiredModules.convertComma2Array(queryFieldPlanningType, JSON.stringify(queryBuildPlanningType))+' ]}}'; }
						var projectHistories =exportedRequiredModules.getTimeRangeQueryHEObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryHEObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						if (planningTypeQueryFormation != ''){ queryConcat = queryConcat +', '+planningTypeQueryFormation; }
						
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);
					}catch (err){
						callback(err, null);
					}
				}
			});
			
		}
	});
}
/******************************************************************************
Function: getQueryByPlanningAuthority
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for planning authority. 
	2. 'getPlanningAuthorityMetadata' returns authorized council names from metadata.
	3. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByPlanningAuthority = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var councilsValues = requireQuery.query.CouncilNames;
	
	var councilsList = [];
	
	if (councilsValues != undefined){
		councilsValues.split(',').forEach(function (council){
			councilsList.push(council.toLowerCase());
	});}

	exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
		if(err){
			callback(err, null);
		}else{
				getPlanningAuthorityMetadata(exportedRequiredModules, councilsList, function(err, councilDataList){
				
				if (err){
					callback(err, null);
				}else if((councilDataList.length != councilsList.length) && (councilsList.length > 0)){
					callback({Error: 'Invalid Council Names given'}, null);
				}else{
					try{
						var queryBuildAuthority = {};
						
						var queryFieldAuthority = '{"terms": %s}';
						
						queryBuildAuthority.PlanningAuthorities = councilDataList;
						
						var authorityQueryFormation = '';
					
						if (councilDataList.length > 0 ){ authorityQueryFormation = '{"bool": {"should": [ '+exportedRequiredModules.convertComma2Array(queryFieldAuthority, JSON.stringify(queryBuildAuthority))+' ]}}'; }
						var projectHistories =exportedRequiredModules.getTimeRangeQueryHEObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryHEObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						if (authorityQueryFormation != ''){ queryConcat = queryConcat +', '+authorityQueryFormation; }
						
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);	
					}catch (err){
						callback(err, null);
					}
				}
			});
			
		}
	});
}
/******************************************************************************
Function: getPlanningAuthorityMetadata
Argument:exportedRequiredModules, councilValuesList
Return:  get council name list.
Usage:
	1. Extracts council name list from metadata url.
*******************************************************************************/
function getPlanningAuthorityMetadata(exportedRequiredModules, councilValuesList, callback){
	
	var councilDataList = [];
	
	try{
	 	var urlPlanningAuthority = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticHeIndexName+'/metadata/projectplanningauthorities';
		
		exportedRequiredModules.request(urlPlanningAuthority, function (error, response,planningAuthoritiesContent) {
			if( planningAuthoritiesContent != undefined){
				var planningAuthoritiesData = JSON.parse(planningAuthoritiesContent);
				planningAuthoritiesData._source.ProjectPlanningAuthorities.forEach(function(councils) {
					if (councilValuesList.length > 0) {
						if (councilValuesList.includes(councils.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							councilDataList.push(councils.toLowerCase());
						}
					}
					else{
						councilDataList.push(councils.toLowerCase());
					}
				});
				callback(null, councilDataList);
			}
			else{
				callback({Error: 'Planning Authority metadata URL response failed.'}, null);
			}
		});
	}catch(err){
		callback({Error: 'Planning Authority metadata URL response failed.'}, null);
	}
}
/******************************************************************************
Function: getPlanningTypeMetadata
Argument:exportedRequiredModules, planningTypeList
Return:  get planning type.
Usage:
	1. Extracts planning type from metadata url.
*******************************************************************************/
function getPlanningTypeMetadata(exportedRequiredModules, planningTypeList, callback){
	
	var planningTypeDataList = [];
	try{
	 	var urlPlanningType = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticHeIndexName+'/metadata/planningapplicationtypes';
		
		exportedRequiredModules.request(urlPlanningType, function (error, response,planningTypeContent) {
			if( planningTypeContent != undefined){
				var planningTypeData = JSON.parse(planningTypeContent);
				planningTypeData._source.PlanningApplicationTypes.forEach(function(planningType) {
					if (planningTypeList.length > 0) {
						if (planningTypeList.includes(planningType.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							planningTypeDataList.push(planningType.toLowerCase());
						}
					}
					else{
						planningTypeDataList.push(planningType.toLowerCase());
					}
				});
				callback(null, planningTypeDataList);
			}
			else{
				callback({Error: 'Planning Type metadata URL response failed.'}, null);
			}
		});
	}catch(err){
		callback({Error: 'Planning Type metadata URL response failed.'}, null);
	}
}

/******************************************************************************
Function: getPlanningApplicationMetadata
Argument:exportedRequiredModules, councilsValuesList, planningTypeList, applicationTypeList
Return:  get application type,planning type,council.
Usage:
	1. Extracts application type,planning type,council from metadata url.
*******************************************************************************/
function getPlanningApplicationMetadata(exportedRequiredModules, councilsValuesList, planningTypeList, callback){
	var CouncilDataList = [];
	var planningTypeDataList = [];
	
	var urlPlanningType = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticHeIndexName+'/metadata/planningapplicationtypes';
	var urlPlanningAuthority = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticHeIndexName+'/metadata/projectplanningauthorities';
	try{
		exportedRequiredModules.request(urlPlanningAuthority, function (error, response,planningAuthoritiesContent) {
			if( planningAuthoritiesContent != undefined){
				var planningAuthoritiesData = JSON.parse(planningAuthoritiesContent);
				planningAuthoritiesData._source.ProjectPlanningAuthorities.forEach(function(councils) {
					if (councilsValuesList.length > 0) {
						if (councilsValuesList.includes(councils.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							CouncilDataList.push(councils.toLowerCase());
						}
					}
				});
				
				exportedRequiredModules.request(urlPlanningType, function (error, response,planningTypeContent) {
					if( planningTypeContent != undefined){
						var planningTypeData = JSON.parse(planningTypeContent);
						planningTypeData._source.PlanningApplicationTypes.forEach(function(planningType) {
							if (planningTypeList.length > 0) {
								if (planningTypeList.includes(planningType.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
									planningTypeDataList.push(planningType.toLowerCase());
								}
							}
						});
						callback(null, CouncilDataList, planningTypeDataList);
					}
					else{
						callback({Error: 'Planning Application metadata URL response failed.'}, null);
					}
				});
			}
			else{
				callback({Error: 'Planning Application metadata URL response failed.'}, null);
			}
		});
   }catch(err){
		callback({Error: 'Planning Application metadata URL response failed.'}, null);
   }
}