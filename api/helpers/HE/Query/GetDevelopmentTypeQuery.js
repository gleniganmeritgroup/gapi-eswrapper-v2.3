'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetDevelopmentTypeQuery.js) is used form a query for given Sizes .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryByDevelopmentType
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching development type. 
	2. 'getSubscriptionQueryHEObj' returns location and sector based on subscription json.
	3. 'getDevelopmentTypeMetadata' returns metadata of development type.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByDevelopmentType = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var typesValues = requireQuery.query.Types;
	
	var typesValuesList = [];
	
	if (typesValues != undefined){
		typesValues.split(',').forEach(function (developmentType){
			typesValuesList.push(developmentType.toLowerCase());
	});}
	exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(error, locationQuery,sectorQuery){
		if(error){
			callback(error, null);
		}
		else{
			try{
				getDevelopmentTypeMetadata(exportedRequiredModules, typesValuesList, function(err, developmentTypeDataList){
				
				if (err){
					callback(err, null);
				}else if ((developmentTypeDataList.length != typesValuesList.length) && (typesValuesList.length > 0)){
					callback({Error: 'Invalid DevelopmentTypes value is given.'}, null);
				}
				else{
					var queryBuildDevelopmentType={};
					var queryFieldDevelopmentType='{"terms": %s}';
					
					queryBuildDevelopmentType.DevelopmentType=developmentTypeDataList;
					
					var developmentTypeQueryFormation, developmentTypeQueryConcat;
				
					if (developmentTypeDataList.length > 0 ){ developmentTypeQueryConcat = exportedRequiredModules.convertComma2Array(queryFieldDevelopmentType, JSON.stringify(queryBuildDevelopmentType)); }
					
					developmentTypeQueryConcat = developmentTypeQueryConcat.replace(/^\s*undefined\s*\,/, "");
					developmentTypeQueryConcat = developmentTypeQueryConcat.replace(/^\s*\,/, "");
					developmentTypeQueryConcat = developmentTypeQueryConcat.replace(/\,\s*$/, "");
					developmentTypeQueryFormation= '{"bool": {"should": [ '+developmentTypeQueryConcat+' ]}}'; 

					try{
						var projectHistories =exportedRequiredModules.getTimeRangeQueryHEObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryHEObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						if (developmentTypeQueryFormation != '' ){ queryConcat = queryConcat +', '+developmentTypeQueryFormation; }
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);
					}catch (err){
						callback(err, null);
					}
				}
			});
			
			}catch (err){
				callback(err, null);
			}
		}
	});
}
/******************************************************************************
Function: getDevelopmentTypeMetadata
Argument:exportedRequiredModules, typesValuesList
Return: get development type.
Usage:
	1. Extracts development type from metadata
*******************************************************************************/
function getDevelopmentTypeMetadata(exportedRequiredModules, typesValuesList, callback){
	
	var developmentTypeDataList = [];
	
	try{
	 	var urlRequestDevelopmentType = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticHeIndexName+'/metadata/projectdevelopmenttypes';
		exportedRequiredModules.request(urlRequestDevelopmentType, function (error, response,developmentTypeContent) {
			if( developmentTypeContent != undefined){
				var developmentTypeData = JSON.parse(developmentTypeContent);
				developmentTypeData._source.ProjectDevelopmentTypes.forEach(function(developmentType) {
					if (typesValuesList.length > 0) {
						if (typesValuesList.includes(developmentType.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							developmentTypeDataList.push(developmentType);
						}
					}
					else{
						developmentTypeDataList.push(developmentType);
					}
				});
				
				callback(null, developmentTypeDataList);
			}
			else{
				callback({Error: 'Development Type metadata URL response failed.'}, null);
			}
		});
	}catch(err){
		throw({Error: 'Development Type metadata URL response failed.'});
	}
}