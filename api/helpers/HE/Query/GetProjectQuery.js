'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetProjectQuery.js) is used form a query for given Project .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryByProject
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for project. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByProject = function ( requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	try{
		// var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
		var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
		
		// var queryConcat=projectHistories
		
		var FinalQuery='{'+sortBy+'}';
		callback(null, FinalQuery);

	}catch (err){
		callback(err, null);
	}

}
/******************************************************************************
Function: getQueryByUpdatedProject
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for updated project. 
	2. 'getSubscriptionQueryObj' returns location and sector based on subscription json.
	3. 'getProjectHistoryMetadata' returns metadata of project event types.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByUpdatedProject = function (subscriptionJSON, requireQuery, callback) {
	// console.log("historyEventValues");
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	var historyEventValues = requireQuery.query.ProjectHistoryEvent;
	var historyEventList = [];
	// console.log("historyEventValues"+historyEventValues);
	if (historyEventValues != undefined){
		historyEventValues.split(',').forEach(function(event){
			historyEventList.push(event.toLowerCase());
		});  
	}
	var jsonObject = JSON.parse(subscriptionJSON);
	exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
		if(err){
			callback(err, null);
		}else{
			getProjectHistoryMetadata(exportedRequiredModules, historyEventList, function(err, historyEventDataList){
				if (err){
					callback({Error: 'Exception on Project History Event Metadata'}, null);
				}
				else{
					try{
						var projectHistories = getUpdatedProjectQuery(exportedRequiredModules,requireQuery, historyEventDataList);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						callback(null, FinalQuery);
					}catch (err){
						callback(err, null);
					}
				}
			});
		}
	});
}
/******************************************************************************
Function: getQueryByNewProject
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for new project. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByNewProject = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	var jsonObject = JSON.parse(subscriptionJSON);
	exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
		if(err){
			callback(err, null);
		}else{
			try{
				var projectHistories = getNewProjectQuery(exportedRequiredModules,requireQuery);
				var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
				var queryConcat=projectHistories
				if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
				if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
				
				var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
				callback(null, FinalQuery);

			}catch (err){
				callback(err, null);
			}
		}
	});
}
/******************************************************************************
Function: getNewProjectQuery
Argument:exportedRequiredModules, require
Return: return a elastic search query .
Usage:
	1. Forms a elastic query for new project for parameters range From and To. 
*******************************************************************************/
function getNewProjectQuery (exportedRequiredModules,require)
{
	var daysValues = require.query.TimeRange;
	
	if (daysValues != undefined) {
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		
		var days = '';
		
		if (daysValues.includes('From') != true ){ 
			
			try{
				var daysRegex = /^([\d]+)([\w])$/g;
				var daysCountPattern = daysRegex.exec(daysValues);
				var daysCount = daysCountPattern[1];
				var daysPattern = daysCountPattern[2];
			}catch(err)
			{ throw ({Error : 'Parameter "TimeRange" value is Invalid.'}) }
			
			if (daysPattern.toLocaleLowerCase() == 'm'){
				daysCount = daysCount * 30;
			}
			else if (daysPattern.toLocaleLowerCase() == 'y') {
				daysCount = daysCount * 365;
			}
			else if (daysPattern.toLocaleLowerCase() == 'd') {
				daysCount = daysCount
			}
			else{
				throw ({Error : 'Parameter "TimeRange" value is Invalid.'})
			}
			
			var previousDate = exportedRequiredModules.moment().subtract(daysCount, 'days').format("YYYY-MM-DDT00:00:00");
			var projectHistories ='{ "nested": { "path": "Histories", "query": { "bool": { "filter": [ { "terms": { "Histories.Event": [ "NewProject" ] } },{ "range": { "Histories.Modified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
			return projectHistories;
		}
		else{
			try{
			var fromToRegex = /From\:([0-9\-]+)\s*and\s*To\:([0-9\-]+)/g;

			var fromToDate = fromToRegex.exec(daysValues);
			
			var currentDate = exportedRequiredModules.moment(fromToDate[2],'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59");
			var previousDate = exportedRequiredModules.moment(fromToDate[1],'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00");
			}catch(err)
			{ throw ({Error : 'Parameter "TimeRange" value is Invalid.'}) }

			var projectHistories ='{ "nested": { "path": "Histories", "query": { "bool": { "filter": [ { "terms": { "Histories.Event": [ "NewProject" ] } }, { "range": { "Histories.Modified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
			return projectHistories;
		}
	}
	else{
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		var previousDate = exportedRequiredModules.moment().subtract(1, 'days').format("YYYY-MM-DDT00:00:00");
		var projectHistories ='{ "nested": { "path": "Histories", "query": { "bool": { "filter": [ { "terms": { "Histories.Event": [ "NewProject" ] } }, { "range": { "Histories.Modified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
		return projectHistories;
	}
	
}
/******************************************************************************
Function: getUpdatedProjectQuery
Argument:exportedRequiredModules, require,historyEventDataList
Return: return a elastic search query .
Usage:
	1. forms a elastic query for new project from range From and To. 
*******************************************************************************/
function getUpdatedProjectQuery (exportedRequiredModules, require, historyEventDataList)
{
	
	var daysValues = require.query.TimeRange;
	var queryFieldHistoryEvent = '{"terms": %s}';
	var queryBuildHistoryEvent = {};
	queryBuildHistoryEvent["Histories.Event"]=historyEventDataList;
	var historyEventQuery = exportedRequiredModules.convertComma2Array(queryFieldHistoryEvent, JSON.stringify(queryBuildHistoryEvent));
	
	if (daysValues != undefined) {
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		
		var days = '';
		
		if (daysValues.includes('From') != true ){ 
			
			try{
				var daysRegex = /^([\d]+)([\w])$/g;
				var daysCountPattern = daysRegex.exec(daysValues);
				var daysCount = daysCountPattern[1];
				var daysPattern = daysCountPattern[2];
			}catch(err)
			{ throw ({Error : 'Parameter "TimeRange" value is Invalid.'}) }
			
			if (daysPattern.toLocaleLowerCase() == 'm'){
				daysCount = daysCount * 30;
			}
			else if (daysPattern.toLocaleLowerCase() == 'y') {
				daysCount = daysCount * 365;
			}
			else if (daysPattern.toLocaleLowerCase() == 'd') {
				daysCount = daysCount
			}
			else{
				throw ({Error : 'Parameter "TimeRange" value is Invalid.'})
			}
			
			var previousDate = exportedRequiredModules.moment().subtract(daysCount, 'days').format("YYYY-MM-DDT00:00:00");
			var projectHistories ='{ "nested": { "path": "Histories", "query": { "bool": { "filter": [ '+historyEventQuery+',{ "range": { "Histories.Modified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
			return projectHistories;
		}
		else{
			try{
			var fromToRegex = /From\:([0-9\-]+)\s*and\s*To\:([0-9\-]+)/g;

			var fromToDate = fromToRegex.exec(daysValues);
			
			var currentDate = exportedRequiredModules.moment(fromToDate[2],'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59");
			var previousDate = exportedRequiredModules.moment(fromToDate[1],'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00");
			}catch(err)
			{ throw ({Error : 'Parameter "TimeRange" value is Invalid.'}) }

			var projectHistories ='{ "nested": { "path": "Histories", "query": { "bool": { "filter": [ '+historyEventQuery+', { "range": { "Histories.Modified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
			return projectHistories;
		}
	}
	else{
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		var previousDate = exportedRequiredModules.moment().subtract(1, 'days').format("YYYY-MM-DDT00:00:00");
		var projectHistories ='{ "nested": { "path": "Histories", "query": { "bool": { "filter": [ '+historyEventQuery+', { "range": { "Histories.Modified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
		return projectHistories;
	}
	
}
/******************************************************************************
Function: getProjectHistoryMetadata
Argument:exportedRequiredModules, projectHistoryEventList
Return:  get project event types.
Usage:
	1. Extracts project event types from metadata url.
*******************************************************************************/
function getProjectHistoryMetadata(exportedRequiredModules, projectHistoryEventList, callback){
	
	var projectHistoryEventDataList = [];
	
	try{
	 	var urlProjectHistory = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticHeIndexName+'/metadata/projecteventtypes';
		
		exportedRequiredModules.request(urlProjectHistory, function (error, response,historyContent) {
			if( historyContent != undefined){
				var historyEvent = JSON.parse(historyContent);
				historyEvent._source.ProjectEventTypes.forEach(function(event) {
					if (projectHistoryEventList.length > 0) {
						if ((projectHistoryEventList.includes(event.EventDescription.toLowerCase().replace('&','and')) == true) || (projectHistoryEventList.includes(event.EventCode.toLowerCase().replace('&','and')) == true)){
							projectHistoryEventDataList.push(event.EventCode);
						}
					}else{
						projectHistoryEventDataList.push(event.EventCode);
					}

				});
				
				callback(null, projectHistoryEventDataList);
			}
			else{
				callback({Error: 'Project History metadata URL response failed.'}, null);
			}
		});
	}catch(err){
		callback({Error: 'Project History metadata URL response failed.'}, null);
	}
}