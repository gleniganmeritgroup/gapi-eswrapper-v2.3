'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetMetaData.js) is used to collect all regions, counties, towns and
postcode, which is come under the each subscription.
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: townMetadataExtraction
Argument:townList, locationContent, townContent
Return: list of towns from metadata
Usage:
	1. Extracts town list from metadata
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.townMetadataExtraction =  function (townList, locationContent, townContent, callback){
	try{
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
		
		townList = townList.map(function(x){ return x.toLowerCase() }) 
		
		var sourceTowns = [];
		// console.log("locationList"+locationList); 
		var locationList = JSON.parse(locationContent);
		var towns = JSON.parse(townContent);
		var errorMessage;
		locationList._source.Locations.forEach(function (locations){
			try{
				locations.Counties.forEach(function(countyValue){
					towns._source.CountiesAndTowns.forEach(function (countiesAndTowns){
						if (countyValue.toLowerCase() == countiesAndTowns.County.toLowerCase()){
							countiesAndTowns.Towns.forEach(function (town){
								if (townList.includes(town.toLowerCase().replace(', ',' and ').replace('&','and')) == true)
								{
									sourceTowns.push(locations.Region.toLowerCase()+"#"+countyValue.toLowerCase()+"#"+town.toLowerCase());
								}
							});
						}
					});
				});	
			}
			catch(err){
				errorMessage=err;
			}
		});
		callback(null, sourceTowns);
	}catch(err){
	//	console.log("err"+err);
		callback({Error : "Exception on Town Metadata extraction"}, null);
	}
}

/******************************************************************************
Function: locationMetadataExtraction
Argument:CountiesList, locationContent
Return: list of counties from metadata
Usage:
	1. Extracts county list from metadata
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.locationMetadataExtraction =  function (CountiesList, locationContent, callback){
	try{
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
		
		CountiesList = CountiesList.map(function(x){ return x.toLowerCase() })
		
		var sourceCounties = [];
		var locationList = JSON.parse(locationContent);
		locationList._source.Locations.forEach(function (locations){
			try{
				locations.Counties.forEach(function(countyValue){
					
					if (CountiesList.includes(countyValue.toLowerCase().replace(', ',' and ').replace('&','and')) == true)
					{
						sourceCounties.push(locations.Region.toLowerCase()+"#"+countyValue.toLowerCase());
					}
				});	
			}
			catch(err){
				locations.Boroughs.forEach(function(countyValue){
					
					if (CountiesList.includes(countyValue.toLowerCase().replace(', ',' and ').replace('&','and')) == true)
					{
						sourceCounties.push(locations.Region.toLowerCase()+"#"+countyValue.toLowerCase());
					}
				});	
			}
		});
		callback(null, sourceCounties);
	}catch(err){
		callback({Error : "Exception on Location Metadata extraction"}, null);
	}
}

/******************************************************************************
Function: metadataExtraction
Argument:exportedRequiredModules,subscriptionJSONObj,urlRequestLocations,urlRequestTowns,urlRequestPostcode,returnValue
Return: list of region,counties,town,postcode from metadata
Usage:
	1. Extracts region,counties,town,postcode list from metadata
*******************************************************************************/
function metadataExtraction(exportedRequiredModules,subscriptionJSONObj,urlRequestLocations,urlRequestTowns,urlRequestPostcode,returnValue, callback){
	var subscriberRegionList = [];
	var subscriberCountyList = [];
	var subscriberTownList = [];
	var subscriberPostcodeList = [];
	var locationSource, townSource;
	exportedRequiredModules.request(urlRequestLocations, function (error, response, locationContent) {
		
		subscriptionJSONObj.Subscription.ProjectRegions.forEach(function(locationLevel1){
			var region =  locationLevel1.Region;
			var countyList =  locationLevel1.Counties;
			var locationList = JSON.parse(locationContent);
			locationSource = locationContent;
			locationList._source.Locations.forEach(function (locations){
			if (region.toLowerCase() == locations.Region.toLowerCase()){
				subscriberRegionList.push(locations.Region.toLowerCase().replace(', ',' and ').replace('&','and'));
				try{
					if (countyList != ""){
						
						countyList = countyList.map(function(x){ return x.toLowerCase() })
						locations.Counties.forEach(function (countyData){
							
							if ((countyList.includes(countyData.toLowerCase()) == true))
							{
								subscriberCountyList.push(countyData.toLowerCase().replace(', ',' and ').replace('&','and'));
							}
						});
					}
					else{
						locations.Counties.forEach(function (county){
							subscriberCountyList.push(county.toLowerCase().replace(', ',' and ').replace('&','and'));
						});
					}
				}catch (err) {
					
					if (countyList != ""){
						countyList = countyList.map(function(x){ return x.toLowerCase() })
						locations.Boroughs.forEach(function (borough){
							
							if ((countyList.includes(borough.toLowerCase()) == true))
							{
								subscriberCountyList.push(borough.toLowerCase().replace(', ',' and ').replace('&','and'));
							}
						});
					}
					else{
						locations.Boroughs.forEach(function (borough){
							subscriberCountyList.push(borough.toLowerCase().replace(', ',' and ').replace('&','and'));
						});
					}
				}
			}
			});
		});
		// console.log("urlRequestTowns"+urlRequestTowns);
		exportedRequiredModules.request(urlRequestTowns, function (error, response, townContent) {
			townSource = townContent;
			var towns = JSON.parse(townContent);
			towns._source.CountiesAndTowns.forEach(function (countiesAndTowns){
				if (subscriberCountyList.includes(countiesAndTowns.County.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
					countiesAndTowns.Towns.forEach(function (town){
						subscriberTownList.push(town.toLowerCase().replace(', ',' and ').replace('&','and'));
					});
				}
			});
			exportedRequiredModules.request(urlRequestPostcode, function (error, response, postcodeContent) {
				var postcodeSource = JSON.parse(postcodeContent);
				postcodeSource.hits.hits.forEach(function (postcodeData){
					// console.log(subscriberCountyList);
					if (subscriberCountyList.includes(postcodeData._source.County.toLowerCase()) == true){
						subscriberPostcodeList.push(postcodeData._source.PostcodeDistrict.toLowerCase());
					}
				});
				if (returnValue == "Town"){callback(null, subscriberTownList, locationSource, townSource);}
				else if (returnValue == "Region"){callback(null, subscriberRegionList, locationSource, townSource);}
				else if (returnValue == "County"){callback(null, subscriberCountyList, locationSource, townSource);}
				else if (returnValue == "Postcode"){callback(null, subscriberPostcodeList, locationSource, townSource);}
				else if (returnValue == "search"){callback(null, subscriberRegionList, subscriberCountyList, subscriberTownList, subscriberPostcodeList);}
				
			});
		});
	});
}
/******************************************************************************
Function: toGetPostcodeDistrictSize
Argument:exportedRequiredModules,urlRequestPostcode
Return: get postcode content
Usage:
	1. Extracts postcode list from metadata
*******************************************************************************/
function toGetPostcodeDistrictSize (exportedRequiredModules,urlRequestPostcode, callback){
	exportedRequiredModules.request(urlRequestPostcode, function (error, response, postcodeContent) {
		var postcodeContent = JSON.parse(postcodeContent);
		callback(null, postcodeContent.hits.total);
	});
}
/******************************************************************************
Function: getLocationsTownsPostcodes
Argument:subscriptionJSONObj, returnValue
Return: get location,town,postcode content
Usage:
	1. Get Authorized location,town,postcode list from metadata.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getLocationsTownsPostcodes = function (subscriptionJSONObj, returnValue, callback){
	try{
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
		
		var urlRequestLocations = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticHeIndexName+'/metadata/locations';
		var urlRequestTowns = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticHeIndexName+'/metadata/towns';
		var urlRequestPostcode =exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/postcode_district/_search';
		toGetPostcodeDistrictSize(exportedRequiredModules,urlRequestPostcode, function(err, result){
			urlRequestPostcode = urlRequestPostcode +"?size="+result
			metadataExtraction(exportedRequiredModules,subscriptionJSONObj,urlRequestLocations,urlRequestTowns,urlRequestPostcode,returnValue, function(err, subscriberRegionList,subscriberCountyList,subscriberTownList,subscriberPostcodeList){
				callback(null, subscriberRegionList,subscriberCountyList,subscriberTownList,subscriberPostcodeList);
			});
		});
	}catch(err){
		callback({Error: "Exception on Location Metadata"}, null);
	}
}
/******************************************************************************
Function: getSectorsCatagories
Argument:subscriptionSectors
Return: get sectors,categories content
Usage:
	1. Get Authorized sectors,categories list from metadata.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getSectorsCatagories = function (subscriptionSectors, callback){
	try{
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
		var authorizedSectorList = [];
		var authorizedSubSectorList = [];
		
		var subscriptionSubSectors = []; /* Subscription having subSector/category this will come as second argument */ 
		var subscriptionSectors = []; /* Subscription having live index sector only, we need to change this after getting HE index subscription */ 

		// subscriptionSectors = subscriptionSectors.map(function(x){ return x.toLowerCase() })
		// subscriptionSubSectors = subscriptionSubSectors.map(function(x){ return x.toLowerCase() })

		var urlRequestSectors = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticHeIndexName+'/metadata/sectors';

		exportedRequiredModules.request(urlRequestSectors, function (error, response, sectorsContent) {
			
			var sectorList = JSON.parse(sectorsContent);
			sectorList._source.Sectors.forEach(function(sectorGroup){
				if ((subscriptionSectors.includes(sectorGroup.SectorGroupName.toLowerCase()) == true) || (subscriptionSectors.length == 0)){
					authorizedSectorList.push(sectorGroup.SectorGroupName.toLowerCase());
					
					sectorGroup.SectorNames.forEach(function(sector){
						if ((subscriptionSubSectors.includes(sectorGroup.SectorGroupName.toLowerCase()) == true) || (subscriptionSubSectors.length == 0)){
							authorizedSubSectorList.push(sector.toLowerCase());
						}
					});
				}
			});
			callback(null, authorizedSectorList, authorizedSubSectorList,sectorsContent);
		});
	}catch(err){
		callback({Error: "Exception on Sector Metadata"}, null);
	}
}	

/******************************************************************************
Function: getMetadataSource
Argument:request
Return: get metadata source
Usage:
	1. Get metadata source from metadata url.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getMetadataSource = function (request, callback){
	try{
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
		var metadataID = request.swagger.params['id'].value
		var size = request.query.size;
		// console.log("size"+size);
		var url;
		if (metadataID == '_search'){
			callback({"_id": "_search", "found" : false}, null);
		}
		else 
		{
			if (metadataID == 'postcode_district'){
			url =exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticHeIndexName+'/postcode_district/_search';
			}
			else{
				url = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticHeIndexName+'/metadata/'+metadataID;
			}
			if ((size != parseInt(size)) && (size != undefined)){
				callback({Error: "Parameter size is invalid"}, null);
			}
			else if (size != undefined)
			{
				url = url+"?size="+size
			}
			exportedRequiredModules.request(url, function (error, response, metaSource) {
				metaSource = JSON.parse(metaSource);
				var sourceData = JSON.stringify(metaSource);
				sourceData = sourceData.replace(/\"_index\":\s*\"[^\"]*?\"\,/ig, ''); 
				sourceData = sourceData.replace(/\"_type\":\s*\"[^\"]*?\"\,/ig, ''); 
				sourceData = sourceData.replace(/\"_version\":\s*(?:\"[^\"]*?\"\,|[\d]+\,)/ig, ''); 
				sourceData = sourceData.replace(/\"_score\":\s*(?:\"[^\"]*?\"|null)\,/ig, ''); 
				sourceData = sourceData.replace(/_id/g, 'id'); 
				sourceData = sourceData.replace(/_source/g, 'source'); 
				callback(null, JSON.parse(sourceData));
				
			});
		}
	}catch(err){
		callback({Error: "Exception on Metadata source extraction"}, null);
	}
}