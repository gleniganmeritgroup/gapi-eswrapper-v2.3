'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (ProjectEnquiryRequest.js) is used to send a enquiry to customer.
********************************************************************************************/
/*******************************************************************************************/


// updateEvent function Used to update the existing task 
/******************************************************************************
Function: requestSend
Argument: requireQuery, callback
Usage:
	1.Used to update enquiry request.
*******************************************************************************/ 
exports.requestSend = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
    // console.log("beSpokeQuery",requireQuery)
    var OpusCredentials = exportedRequiredModules.credentialsObj.Opus;
    var SalesforceCredentials = exportedRequiredModules.credentialsObj.Salesforce;
    var OpusConnection = new exportedRequiredModules.jsforceObj.Connection({ loginUrl: 'https://' + OpusCredentials.host });
    var SFConnection = new exportedRequiredModules.jsforceObj.Connection({ loginUrl: 'https://' + SalesforceCredentials.host });
    var userIDValue = requireQuery.query.UserId;
    var username = OpusCredentials.username;
    var password = OpusCredentials.password+OpusCredentials.token;
    var SFusername = SalesforceCredentials.username;
    var SFpassword = SalesforceCredentials.password+SalesforceCredentials.token;
    var beSpokeQuery='';
    try{
        beSpokeQuery = JSON.parse(requireQuery.body);
    }catch(err){ 
        beSpokeQuery= ''; 
    }
    SFConnection.login(SFusername, SFpassword, function(err, userInfo) {
        SFConnection.sobject("User").select('Full_Name__c,Contact.Account.Name,Contact.Phone,Email,UserName,Contact.Account.Owner_Name__c,Contact.Account.Owner.Email').where("Id = '"+userIDValue+"'").execute(function (error, records) {
        var G2ContactName__c = records[0].Full_Name__c;
        var G2AccountName__c = records[0].Contact.Account.Name;
        var G2Phone__c = records[0].Contact.Phone;
        var G2Email__c = records[0].Email;
        var G2UserName__c = records[0].Username;
        var G2AccountManager__c = records[0].Contact.Account.Owner_Name__c;
        var G2AccountManagerEmail__c = records[0].Contact.Account.Owner.Email;
        var reason = beSpokeQuery.Reason;
        var subject = beSpokeQuery.subject;
        var description = beSpokeQuery.description;
        var origin = beSpokeQuery.origin;    
        
        try{
            var finalQuery = {}
            if(reason != undefined) {finalQuery.Reason = reason;}
            if(subject != undefined) {finalQuery.subject = subject;}
            if(description != undefined) {finalQuery.description = description;}
            if(G2Email__c != undefined) {finalQuery.G2Email__c = G2Email__c;}
            if(G2UserName__c != undefined) {finalQuery.G2UserName__c = G2UserName__c;}
            if(origin != undefined) {finalQuery.origin = origin;}
            if(G2AccountName__c != undefined) {finalQuery.G2AccountName__c = G2AccountName__c;}
            if(G2ContactName__c != undefined) {finalQuery.G2ContactName__c = G2ContactName__c;}
            if(G2Phone__c != undefined) {finalQuery.G2Phone__c = G2Phone__c;}
            if(G2AccountManager__c != undefined) {finalQuery.G2AccountManager__c = G2AccountManager__c;}
            // if(G2AccountManagerEmail__c != undefined) {finalQuery.G2AccountManagerEmail__c = G2AccountManagerEmail__c;}
            
            OpusConnection.login(username, password, function(err, userInfo) {
                if (err) { callback({"Error":"Opus login failed"},null) }
                else{
                    // console.log("finalQuery",finalQuery)
                    OpusConnection.sobject("Case").create(finalQuery, function(err, ret) {
                        if (err) { callback({"Error":"Enquiry update is Failed"},null) }
                        else{
                            callback(null, {"Message":"Enquiry updated successfully"})
                        }
                        
                    }); 
                }
            });
        }catch(err){
            callback(err, null)
        }
    });
});
}
