'use strict';
/******************************************************************************
Function: getTransactionData
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Tags for Project. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getTransactionData = function (requireQuery, elasticServerClient, callback) {

	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;
	// var userIDValues = '00520000003EX4zAAG';
	var type = requireQuery.query.Type;
	var FinalQuery = '';
	if (type == undefined) {
		callback({ Error: "Parameter type  is Missing" }, null);
	}
	else if (type != "Project" && type != "Office" && type != "Contact") {
		callback({ Error: "Parameter type is invalid" }, null);
	}
	else {
		// elasticTrackingIndexName, elasticTrackingTypeName, getTrackingData
		var trackingQuery = '{"query":{"bool":{"must":[{"term":{"UserId":"'+userIDValues+'"}},{"term":{"Type":"'+type+'"}}]}},"sort":[{"ViewedDateTime":{"order":"desc"}}],"aggs":{"by_type":{"terms":{"field":"TypeId","size":50,"order":{"ViewedDateTime":"desc"}},"aggs":{"ViewedDateTime":{"avg":{"field":"ViewedDateTime"}}}}},"size":0}'
		// console.log("trackingQuery",trackingQuery)
		try {
			var elasticIndex = exportedRequiredModules.elasticTrackingIndexName;
			var elasticType = exportedRequiredModules.elasticTrackingTypeName;
			// var elasticTrackingServerClient = new exportedRequiredModules.elasticsearchObj.Client({
				// host: 'https://testuser:1234567890@es-es246-staging.glenigan.com'
			// });
			// elasticServerClient
			exportedRequiredModules.getTrackingData(exportedRequiredModules, elasticServerClient, trackingQuery, elasticIndex, elasticType,requireQuery, function (error, trackingResult) {
				
				if (error) {
					callback(error, null)
				}
				else {
					// trackingResult
					var resultData = JSON.parse(JSON.stringify(trackingResult));
					// console.log("resultData",resultData)
					if(trackingResult.total == 0){
						callback(resultData, null);
					} else {
						var typeIds = []
						resultData.results.forEach(function (tracking) {
							var typeId = tracking.key;
							typeIds.push(typeId);
						})
						// console.log("typeIds",typeIds)
						var queryBuildTypeIdsField = '{"terms": %s}';
						var queryBuildTypeIds = {};
						//sourceIncludeMappingObj.TrackingSourceFields
					
						if (type == "Project") {
							queryBuildTypeIds.ProjectId = typeIds;
							var TypeIdsQuery = exportedRequiredModules.convertComma2Array(queryBuildTypeIdsField, JSON.stringify(queryBuildTypeIds));

							var typeQuery = '{"bool": {"should":[' + TypeIdsQuery + ' ] }}';

							/* Enable the below for live */

							// exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
							// 	var queryConcat=typeQuery
							// 	if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
							// 	if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
							// 	var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}'+',"_source": '+exportedRequiredModules.sourceIncludeMappingObj.TrackingSourceFields.Project+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
							// 	callback(null, FinalQuery, typeIds);
							// });

							/* Comment the below for live */
							
							var queryConcat=typeQuery
							var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}'+',"_source": '+exportedRequiredModules.sourceIncludeMappingObj.TrackingSourceFields.Project+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
							callback(null, FinalQuery,typeIds);


						} else if (type == "Office") {
							queryBuildTypeIds.OfficeId = typeIds;
							var TypeIdsQuery =  exportedRequiredModules.convertComma2Array(queryBuildTypeIdsField, JSON.stringify(queryBuildTypeIds));
							var typeQuery = '{"bool": {"should":['+TypeIdsQuery+' ] }}';
							var FinalQuery='{"query": {"bool": {"filter": ['+typeQuery+']}}'+',"_source": '+exportedRequiredModules.sourceIncludeMappingObj.TrackingSourceFields.Company+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
							callback(null, FinalQuery, typeIds);
						}else if(type == "Contact"){
							queryBuildTypeIds.ContactId = typeIds;
							var TypeIdsQuery =  exportedRequiredModules.convertComma2Array(queryBuildTypeIdsField, JSON.stringify(queryBuildTypeIds));
							var typeQuery = '{"bool": {"should":['+TypeIdsQuery+' ] }}';
							var FinalQuery='{"query": {"bool": {"filter": ['+typeQuery+']}}'+',"_source": '+exportedRequiredModules.sourceIncludeMappingObj.TrackingSourceFields.Contact+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
							// console.log(FinalQuery);
							callback(null, FinalQuery, typeIds);
						}						
					}
				}
			});

		} catch (err) {
			callback(err, null);
		}
	}
}
