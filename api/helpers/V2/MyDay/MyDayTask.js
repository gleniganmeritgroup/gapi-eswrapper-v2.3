'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (MyDayTask.js) is used form a query for given UserId.
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: createEvent
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for creating events. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.createEvent = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
    
    
    var userIDValue = requireQuery.query.UserId;
    var myDayCompanyID = requireQuery.AccountId;
    var beSpokeQuery='';
    try{
        beSpokeQuery = JSON.parse(requireQuery.body);
    }catch(err){ 
        beSpokeQuery= ''; 
    }
    
    var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
        host: exportedRequiredModules.elasticIndexHost
      });
    var elasticType = exportedRequiredModules.elasticMyDayTypeName;
    var queryValue ='{"query":{"bool":{"filter":[{"range" : {"MyDayStartTime" : {"gte" : "now/d","lt" : "now+1d/d"}}},{"bool":{"should":[{"terms":{"MyDayUserId":["'+userIDValue+'"]}}]}}]}}}';
    exportedRequiredModules.getRecordCount(exportedRequiredModules,elasticServerClient,elasticType,queryValue,requireQuery, function(error, result){
        if(error){
            callback({Error : 'Exception on get MyDay count.'}, null)
        }
        else{
            var countValues = result.count;
            var startTime = beSpokeQuery.MyDayStartTime;
            var duration = beSpokeQuery.Duration;
            var myDayType = beSpokeQuery.MyDayType;
            var myDayTypeId = beSpokeQuery.MyDayTypeId;
            var myDayOutCome = beSpokeQuery.MyDayOutCome;
            var currentDate = exportedRequiredModules.moment().toISOString();
            var startDate = exportedRequiredModules.moment(startTime, 'YYYY-MM-DDTHH:mm:ss.SSS[Z]');
            var dayDuration = exportedRequiredModules.moment.duration(startDate.diff(currentDate));
            var days = dayDuration.asDays();
            if(duration == undefined){
                callback({"Error" : "Duration is missing."}, null);
            }
            else if (days < 0){
                callback({"Error" : "MyDayStartTime should be in future date"}, null);
            }
            else if (myDayType == undefined){
                callback({"Error" : "Filed MyDayType is missing"}, null);
            }
            else if(myDayType != undefined && myDayType != 'Project' && myDayType != 'Office'){
                callback({"Error" : "Incorrect MyDayType is given. It should be Project OR Office"}, null);
            }
            else if(countValues < 21){
                var time,durations,endDateTime,myDayOutComeAdded;
                try{
                    if(myDayOutCome == undefined){
                        myDayOutComeAdded = false;
                        myDayOutCome=''
                    }else if(myDayOutCome == 'Successful' || myDayOutCome == 'Unsuccessful'){
                        myDayOutComeAdded = true;
                    }
                   
                    var hoursRegex = /^([\d]+)([\w])$/g;
                
                    try{
                        var durationPattern = hoursRegex.exec(duration);
                        time = durationPattern[1];
                        durations = durationPattern[2];
                    }catch(err)
                    { throw ({Error : 'Parameter "Duration" value is Invalid.'}) }
                    if (durations.toLocaleLowerCase() == 'h'){
                        endDateTime = exportedRequiredModules.moment(startTime,'YYYY-MM-DDTHH:mm:ss.SSS[Z]').add(time, 'hours').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
                    }
                    else if (durations.toLocaleLowerCase() == 'm') {
                        endDateTime = exportedRequiredModules.moment(startTime,'YYYY-MM-DDTHH:mm:ss.SSS[Z]').add(time, 'minutes').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
                    }
                    var finalQuery = {}
                    finalQuery.MyDayCompanyId = myDayCompanyID;
                    finalQuery.MyDayUserId = userIDValue;
                    finalQuery.MyDayStartTime = startTime
                    finalQuery.MyDayEndTime = endDateTime;
                    finalQuery.MyDayType = myDayType;
                    finalQuery.MyDayTypeId = myDayTypeId;
                    finalQuery.MyDayOutComeAdded = myDayOutComeAdded;
                    finalQuery.MyDayOutCome = myDayOutCome;
                    finalQuery.MyDayDuration = duration;
                    finalQuery.MyDayCreatedDate=exportedRequiredModules.moment().toISOString();
                    finalQuery.MyDayUpdatedDate=exportedRequiredModules.moment().toISOString();
                    // var hashKey = exportedRequiredModules.shObj.unique(userIDValue+startTime+myDayType+exportedRequiredModules.moment().toISOString());
                    var hashKey = exportedRequiredModules.uuidObj(userIDValue+startTime+myDayTypeId+exportedRequiredModules.moment().toISOString());
                    // var currentDateTime = exportedRequiredModules.moment(exportedRequiredModules.moment().toISOString(),'YYYY-MM-DDTHH:mm:ss.SSS[Z]').format('HHYYmmMMssDD')
                    // var myDayID = hashKey+currentDateTime;
                    callback(null, finalQuery, hashKey)
                }catch(err){
                    callback(err, null)
                }
            }else{
                callback({Error : 'MyDay task limit exceeded'}, null)
            }
            
        }
    });
}


/******************************************************************************
Function: updateEvent
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for updating events. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/ 
exports.updateEvent = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
    // console.log("beSpokeQuery",requireQuery)
    var userIDValue = requireQuery.query.UserId;
    var beSpokeQuery='';
    try{
        beSpokeQuery = JSON.parse(requireQuery.body);
    }catch(err){ 
        beSpokeQuery= ''; 
    }
    var myDayId = beSpokeQuery.MyDayID;
    var myDayCompanyID = beSpokeQuery.MyDayCompanyId;
    var startTime = beSpokeQuery.MyDayStartTime;
    var duration = beSpokeQuery.Duration;
    var myDayType = beSpokeQuery.MyDayType;
    var myDayTypeId = beSpokeQuery.MyDayTypeId;
    var myDayOutCome = beSpokeQuery.MyDayOutCome;
    var myDayNoteAdded = beSpokeQuery.MyDayNoteAdded;
    var time,durations,endDateTime,myDayOutComeAdded;
    try{
        if(myDayOutCome == 'Successful' || myDayOutCome == 'Unsuccessful'){
            myDayOutComeAdded = true;
        }
        var days;
        if(startTime != undefined){
            var currentDate = exportedRequiredModules.moment().toISOString();
            var startDate = exportedRequiredModules.moment(startTime, 'YYYY-MM-DDTHH:mm:ss.SSS[Z]');
            var dayDuration = exportedRequiredModules.moment.duration(startDate.diff(currentDate));
            days = dayDuration.asDays();
        }
        if(myDayOutCome != undefined && myDayOutCome != 'Successful' && myDayOutCome != 'Unsuccessful'){
            callback({"Error" : "MyDayOutCome should be Successful OR Unsuccessful"}, null);
        }
        else if (days < 0 && days !=undefined){
            callback({"Error" : "MyDayStartTime should be in future date"}, null);
        }
        else if(myDayType != undefined && myDayType != 'Project' && myDayType != 'Office'){
            callback({"Error" : "Incorrect MyDayType is given"}, null);
        }else{
            var hoursRegex = /^([\d]+)([\w])$/g;
            if(duration != undefined){
                try{
                    var durationPattern = hoursRegex.exec(duration);
                    time = durationPattern[1];
                    durations = durationPattern[2];
                }catch(err)
                { throw ({Error : 'Parameter "Duration" value is Invalid.'}) }
                if (durations.toLocaleLowerCase() == 'h'){
                    // endDateTime = exportedRequiredModules.moment(startTime,'YYYY-MM-DDTHH:mm:ss').add(time, 'hours').format("YYYY-MM-DDTHH:mm:ss");
                    endDateTime = exportedRequiredModules.moment(startTime,'YYYY-MM-DDTHH:mm:ss.SSS[Z]').add(time, 'hours').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
                }
                else if (durations.toLocaleLowerCase() == 'm') {
                    // endDateTime = exportedRequiredModules.moment(startTime,'YYYY-MM-DDTHH:mm:ss').add(time, 'minutes').format("YYYY-MM-DDTHH:mm:ss");
                    endDateTime = exportedRequiredModules.moment(startTime,'YYYY-MM-DDTHH:mm:ss.SSS[Z]').add(time, 'minutes').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
                }
            }
            var finalQuery = {}
            if(myDayCompanyID != undefined) {finalQuery.MyDayCompanyId = myDayCompanyID;}
            if(userIDValue != undefined) {finalQuery.MyDayUserId = userIDValue;}
            if(startTime != undefined) {finalQuery.MyDayStartTime = startTime;}
            if(endDateTime != undefined) {finalQuery.MyDayEndTime = endDateTime;}
            if(myDayType != undefined) {finalQuery.MyDayType = myDayType;}
            if(myDayTypeId != undefined) {finalQuery.MyDayTypeId = myDayTypeId;}
            if(myDayOutComeAdded != undefined) {finalQuery.MyDayOutComeAdded = myDayOutComeAdded;}
            if(myDayOutCome != undefined) {finalQuery.MyDayOutCome = myDayOutCome;}
            if(myDayNoteAdded != undefined) {finalQuery.MyDayNoteAdded = myDayNoteAdded;}
            if(duration != undefined) {finalQuery.MyDayDuration = duration;}
            // console.log("finalQuery",finalQuery)
            finalQuery.MyDayUpdatedDate=exportedRequiredModules.moment().toISOString();
            var document = {}
            document.doc=finalQuery
            callback(null, document, myDayId)
        }
    }catch(err){
        callback(err, null)
    }
}

/******************************************************************************
Function: getCount
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for getting count of total events. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/ 
exports.getCount = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
    // console.log("beSpokeQuery",requireQuery)
	var userIDValue = requireQuery.query.UserId;
	var myDayDate = requireQuery.query.Date;
    if(myDayDate == undefined){
		callback({Error:"Parameter Date value is Missing."},null);
	}
    else if((myDayDate != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(myDayDate) == null)){
		callback({Error:"Parameter Date value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}else{
        var myDayFromDate = exportedRequiredModules.moment(myDayDate, 'DD-MM-YYYY').format('YYYY-MM-DDT00:00:00.000Z');
        var myDayToDate = exportedRequiredModules.moment(myDayDate, 'DD-MM-YYYY').format('YYYY-MM-DDT23:59:59.999Z');
        var myDayQuery ='{"query":{"bool":{"filter":[{"range" : {"MyDayStartTime" : {"gte" : "'+myDayFromDate+'","lte" : "'+myDayToDate+'"}}},{"bool":{"should":[{"terms":{"MyDayUserId":["'+userIDValue+'"]}}]}}]}}}';
        callback(null, myDayQuery);
    }
    
}


/******************************************************************************
Function: updateMyDayOutCome
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for updating events. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/ 
exports.updateMyDayOutCome = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
    // console.log("beSpokeQuery",requireQuery)
    var userIDValue = requireQuery.query.UserId;
    var beSpokeQuery='';
    try{
        beSpokeQuery = JSON.parse(requireQuery.body);
    }catch(err){ 
        beSpokeQuery= ''; 
    }
    var myDayId = beSpokeQuery.MyDayID;
    
    try{
        var finalQuery = {}
        finalQuery.MyDayOutComeAdded = false;
        finalQuery.MyDayOutCome = '';
        finalQuery.MyDayUpdatedDate=exportedRequiredModules.moment().toISOString();
        var document = {}
        document.doc=finalQuery
        callback(null, document, myDayId)
    }catch(err){
        callback(err, null)
    }
}