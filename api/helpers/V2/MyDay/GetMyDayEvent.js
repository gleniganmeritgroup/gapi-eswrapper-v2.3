'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetMyDayEvent.js) is used form a query for given UserId.
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: upcomingEvent
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching upcoming events. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.upcomingEvent = function (requireQuery, callback) {

    var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
    // console.log("beSpokeQuery",requireQuery)
    var userID = requireQuery.query.UserId;
    var typeValue = requireQuery.query.Type;
    var typeIdValue = requireQuery.query.TypeId;

    if (typeValue == undefined) {
        callback({ "Error": "Parameter Type is missing" }, null)
    }
    else if (typeValue != undefined && typeValue != 'Project' && typeValue != 'Office') {
        callback({ "Error": "Incorrect MyDayType is given. It should be Project OR Office" }, null);
    }
    else {

        var currentDateTime = exportedRequiredModules.moment().toISOString();
        var Typequery = '';
        var TypeIdquery = '';
        var userIdQuery = '';
        if (typeValue != undefined) {
            Typequery = ',{"bool":{"should":[{"terms":{"MyDayType":["' + typeValue + '"]}}]}}';

        }
        if (typeIdValue != undefined) {
            TypeIdquery = ',{"bool":{"must":[{"terms":{"MyDayTypeId":["' + typeIdValue + '"]}}]}}';
        }
        if (userID != undefined) {
            userIdQuery = '{"bool":{"should":[{"terms":{"MyDayUserId":["' + userID + '"]}}]}}'
        }
        var upcomingDateQuery = ',{"range" : {"MyDayStartTime" : {"gte" : "' + currentDateTime + '"}}}';

        var FinalQuery = '{"query":{"bool":{"filter":[' + userIdQuery + Typequery + TypeIdquery + upcomingDateQuery + ']}},"sort": { "MyDayStartTime": "asc"},"size" : 1 }';
      //  console.log(FinalQuery);
        // var query = '{"bool":{"filter":[,{"bool":{"should":[{"terms":{"MyDayType":["'+typeValue+'"]}},{"bool":{"should":[{"terms":{"MyDayUserId":["'+userID+'"]}}]}}]}},"sort": { "MyDayStartTime": "asc"},"size" : 1 '
        //      var FinalQuery = '{"query": ' + AllEventsQuery + '}';
        callback(null, FinalQuery)
    }
}
/******************************************************************************
Function: getMydayAllEventsQuery
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching all events. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getMydayAllEventsQuery = function (requireQuery, callback) {

    var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

    var userID = requireQuery.query.UserId;
    var DateSelected = requireQuery.query.Date;
    var Type = requireQuery.query.Type;

    if ((userID == undefined)) {
        callback({ Error: "Parameter UserId is Missing" }, null);
    }
    else if (DateSelected == undefined) {
        callback({ Error: "Parameter Date is Missing" }, null);
    }
    else if((DateSelected != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(DateSelected) == null)){
		callback({Error:"Parameter Date value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
    else if (Type != undefined && Type != 'Project' && Type != 'Office') {
        callback({ "Error": "Incorrect MyDayType is given. It should be Project OR Office" }, null);
    }
    else if ((userID != undefined && DateSelected != undefined)) {
        
        try {
            var FromDateTime = exportedRequiredModules.moment(DateSelected, 'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00");
            var ToDateTime = exportedRequiredModules.moment(DateSelected, 'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59");
            var Typequery = '';
            if (Type != undefined) {
                Typequery = ',{ "terms": { "MyDayType": [ "' + Type + '" ] } }';
            }
            var AllEventsQuery = '{ "bool": { "filter": [ { "terms": { "MyDayUserId": [ "' + userID + '" ] } }' + Typequery + ',{"range": {"MyDayStartTime": { "gte": "' + FromDateTime + '", "lte": "' + ToDateTime + '"}}}]}},"sort": {"MyDayStartTime": "asc"},"size": 20';
            //var fQuery = Typequery + AllEventsQuery
            var FinalQuery = '{"query": ' + AllEventsQuery + '}';
            // console.log("FinalQuery "+FinalQuery);
            callback(null, FinalQuery);
        } catch (err) {
            callback(err, null);
        }
    }
}
/******************************************************************************
Function: getCalendarEventsQuery
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching events based on two dates. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getCalendarEventsQuery = function (requireQuery, callback) {

    var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

    var userID = requireQuery.query.UserId;
    var FromDateSelected = requireQuery.query.From;
    var ToDateSelected = requireQuery.query.To;
    var Type = requireQuery.query.Type;

    if ((userID == undefined)) {
        callback({ Error: "Parameter UserId is Missing" }, null);
    }
    else if (FromDateSelected == undefined || ToDateSelected == undefined) {
        callback({ Error: "Parameter From/To is Missing" }, null);
    }
    else if((FromDateSelected != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(FromDateSelected) == null)){
		callback({Error:"Parameter From date is invalid. Date format should be \"DD-MM-YYYY\""},null);
    }
    else if((ToDateSelected != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(ToDateSelected) == null)){
		callback({Error:"Parameter To date is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
    else if (Type != undefined && Type != 'Project' && Type != 'Office') {
        callback({ "Error": "Incorrect MyDayType is given. It should be Project OR Office" }, null);
    }
    else {
        try {
            var FromDateTime = exportedRequiredModules.moment(FromDateSelected, 'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00");
            var ToDateTime = exportedRequiredModules.moment(ToDateSelected, 'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59");
            var Typequery = '';
            if (Type != undefined) {
                Typequery = ',{ "terms": { "MyDayType": [ "' + Type + '" ] } }';
            }
            var AllEventsQuery = '{ "bool": { "filter": [ { "terms": { "MyDayUserId": [ "' + userID + '" ] } }' + Typequery + ',{"range": {"MyDayStartTime": { "gte": "' + FromDateTime + '", "lte": "' + ToDateTime + '"}}}]}},"sort": {"MyDayStartTime": "asc"},"size": 20';
            //var fQuery = Typequery + AllEventsQuery
            var FinalQuery = '{"query": ' + AllEventsQuery + '}';
            // console.log("FinalQuery "+FinalQuery);
            callback(null, FinalQuery);
        } catch (err) {
            callback(err, null);
        }

    }
}