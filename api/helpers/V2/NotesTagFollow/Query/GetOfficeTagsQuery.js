'use strict';
/******************************************************************************
Function: getQueryByofficeTags
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Tags for Office. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByofficeTags = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;
	var size = requireQuery.query.Size;
	var officeTagId = requireQuery.query.TagId;
	var accountID = requireQuery.AccountId;
	if ((userIDValues == undefined)) {
		callback({ Error: "Parameter userID Values  is Missing" }, null);
	}
	else {

		try {
			
			var OfficeTag = ''
			if(officeTagId == undefined){
				var officeTagIdQuery = '{"term":{ "OfficeTagUserId" : "' + userIDValues + '"}}';
				OfficeTag = '{"bool": {"should": [{"bool": {"filter": [{"term": {"OfficeTagCompanyId": "'+accountID+'"}},{"term": {"OfficeTagType": "Shared"}}]}},'+officeTagIdQuery+']}}'
			}else{
				var officeTagIdQuery = '{"term":{ "OfficeTagUserId" : "' + userIDValues + '"}},{"term":{ "_id" : "' + officeTagId + '"}}';
				OfficeTag = '{"bool": {"must": [{"bool": {"filter": [{"term": {"OfficeTagCompanyId": "'+accountID+'"}},{"term": {"OfficeTagType": "Shared"}}]}},'+officeTagIdQuery+']}}'
			}
			if(size != undefined && exportedRequiredModules.elasticPageSize >= size){
				var FinalQuery = '{"query": ' + OfficeTag +',"size": '+ size +'}';
				callback(null, FinalQuery);
			}
			else if(size != undefined && exportedRequiredModules.elasticPageSize < size){
				callback({Error : "Page size is exceeded the limit."}, null);
			}else{
				var FinalQuery = '{"query": ' + OfficeTag +',"size": '+ exportedRequiredModules.elasticPageSize +'}';
				// console.log("FinalQuery",FinalQuery)
				callback(null, FinalQuery);
			}
		} catch (err) {
			callback(err, null);
		}
	}
}
/******************************************************************************
Function: postQueryByOfficeTags
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for creating and updating office tags. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.postQueryByOfficeTags = function (requireQuery, checkExistance, callback) {


	
	var datetime = new Date();

	var req = JSON.parse(requireQuery.body);
	var OfficeTagUserId = requireQuery.query.UserId;
	var OfficeTagType = req.TagType;
	var OfficeTagName = req.TagName;
	var OfficeTagCompanyId =requireQuery.AccountId;
	var OfficeTagColour = req.TagColour;

	var OfficeTagUserIdQuery = '';
	var OfficeTagTypeQuery = '';
	var OfficeTagNameQuery = '';
	var OfficeTagCompanyIdQuery = '';
	var OfficeTagColourQuery = '';
	try {


		if (OfficeTagType != undefined) { OfficeTagTypeQuery = ',"OfficeTagType": "' + OfficeTagType + '"'; }
		if (OfficeTagName != undefined) { OfficeTagNameQuery = ',"OfficeTagName": "' + OfficeTagName + '"'; }
		if (OfficeTagCompanyId != undefined) { OfficeTagCompanyIdQuery = ',"OfficeTagCompanyId": "' + OfficeTagCompanyId + '"'; }
		if (OfficeTagColour != undefined) { OfficeTagColourQuery = ',"OfficeTagColour": "' + OfficeTagColour + '"'; }

		if (checkExistance != false) {
			if (OfficeTagUserId != undefined) { OfficeTagUserIdQuery = '"OfficeTagModifiedByUserId": "' + OfficeTagUserId + '"'; }
			var OfficeTagUpdatedDate = datetime.toISOString();
			var DateQuery = ',"OfficeTagUpdatedDate": "' + OfficeTagUpdatedDate + '"';


		} else {
			if (OfficeTagUserId != undefined) { OfficeTagUserIdQuery = '"OfficeTagUserId": "' + OfficeTagUserId + '"'; }
			var OfficeTagUpdatedDate = datetime.toISOString();
			var OfficeTagCreatedDate = datetime.toISOString();
			var DateQuery = ',"OfficeTagCreatedDate": "' + OfficeTagCreatedDate + '","OfficeTagUpdatedDate": "' + OfficeTagUpdatedDate + '"';
		}

		var FQuery = OfficeTagUserIdQuery + OfficeTagTypeQuery + OfficeTagNameQuery + OfficeTagCompanyIdQuery + OfficeTagColourQuery + DateQuery;
		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}

		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
