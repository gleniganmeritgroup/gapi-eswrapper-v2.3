'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetContactNotesQuery.js) is used form a query for given UserId.
********************************************************************************************/
/*******************************************************************************************/
/******************************************************************************
Function: getQueryByContactNotes
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Contact Notes by UserId,ContactId. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByContactNotes = function (requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;
	var contactId=requireQuery.query.ContactId;
	var size = requireQuery.query.Size;
	var notesSharingEnabled = requireQuery.query.NotesSharingEnabled
	var accountId = requireQuery.AccountId;
	var noteSharing = requireQuery.NoteSharing;
	
	if(contactId == undefined) {
		callback({Error:"Parameter ContactId is Missing"},null);
	}
	else{
		
		try{
			
			var ContactNotes = '';
			if (noteSharing == true || notesSharingEnabled == true) {
				var idQuery = '{"term":{ "ContactNoteCompanyId" : "'+accountId+'"}}'
				ContactNotes = '{ "bool": {"filter": [{"has_parent": { "parent_type": "contact", "query": { "terms": { "ContactId": [ "'+contactId+'" ] } } } }, '+idQuery+' ] } }'; 
				
			}else{
				var idQuery = '{"term":{ "ContactNoteCreatedByUserId" : "'+userIDValues+'"}}'
				ContactNotes = '{ "bool": {"filter": [{"has_parent": { "parent_type": "contact", "query": { "terms": { "ContactId": [ "'+contactId+'" ] } } } }, '+idQuery+' ] } }'; 
			}

			if(size != undefined && exportedRequiredModules.elasticPageSize >= size){
				var FinalQuery = '{"query": ' + ContactNotes +',"size": '+ size +'}';
				callback(null, FinalQuery);
			}
			else if(size != undefined && exportedRequiredModules.elasticPageSize < size){
				callback({Error : "Page size is exceeded the limit."}, null);
			}else{
				var FinalQuery = '{"query": ' + ContactNotes +',"size": '+ exportedRequiredModules.elasticPageSize +'}';
				callback(null, FinalQuery);
			}
			
		}catch (err){
			callback(err, null);
		}
	}
}
/******************************************************************************
Function: postQueryByContactNotes
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for creating and updating contact notes. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.postQueryByContactNotes = function (requireQuery, checkExistance, callback) {


	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var ContactNoteUserId = requireQuery.query.UserId;
	var ContactNoteText = req.NoteText;
	var ContactNoteContactId = req.ContactId;
	var ContactNoteCompanyId = requireQuery.AccountId;
	
	var DateQuery = '';
	var ContactNoteTextQuery = '';
	var ContactNoteContactIdQuery = '';
	var ContactNoteCompanyIdQuery = '';
	
	try {

		//if (ContactNoteUserId != undefined) { ContactNoteUserIdQuery = '"ContactNoteCreatedByUserId": "' + ContactNoteUserId + '"'; }
		if (ContactNoteText != undefined) { ContactNoteTextQuery = ',"ContactNoteText": "' + ContactNoteText + '"'; }
		if (ContactNoteContactId != undefined) { ContactNoteContactIdQuery = ',"ContactNoteContactId": "' + ContactNoteContactId + '"'; }
		if (ContactNoteCompanyId != undefined) { ContactNoteCompanyIdQuery = ',"ContactNoteCompanyId": "' + ContactNoteCompanyId + '"'; }
	

		if (checkExistance != false) {
			var ContactNoteModifiedDateTime = datetime.toISOString();
			var ContactNoteModifiedByUserId= ',"ContactNoteModifiedByUserId": "' + ContactNoteUserId + '"';
			DateQuery = ContactNoteModifiedByUserId+',"ContactNoteModifiedDateTime": "' + ContactNoteModifiedDateTime + '"';
		
		} else {
			var ContactNoteCreatedDateTime = datetime.toISOString();
			var ContactNoteCreatedByUserId= ',"ContactNoteCreatedByUserId": "' + ContactNoteUserId + '"';
			DateQuery = ContactNoteCreatedByUserId+',"ContactNoteCreatedDateTime": "' + ContactNoteCreatedDateTime + '","ContactNoteModifiedByUserId": null,"ContactNoteModifiedDateTime": null';
		}

		var FQuery = ContactNoteTextQuery + ContactNoteContactIdQuery + DateQuery+ContactNoteCompanyIdQuery ;
		var FinalQuery =''
		if (FQuery.substring(0, 1) == ',') {
			FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			FinalQuery = FinalQuery;
		}
		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
