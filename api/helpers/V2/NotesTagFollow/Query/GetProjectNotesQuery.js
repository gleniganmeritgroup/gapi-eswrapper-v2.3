'use strict';
/******************************************************************************
Function: getQueryByProjectNotes
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Project by UserId,projectId. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByProjectNotes = function (requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;
	var projectId= requireQuery.query.ProjectId
	var size = requireQuery.query.Size;
	var notesSharingEnabled = requireQuery.query.NotesSharingEnabled
	var accountId = requireQuery.AccountId;
	var noteSharing = requireQuery.NoteSharing;
	if ((projectId == undefined)) {
		callback({Error:"Parameter ProjectId is Missing"},null);
	}
	else{
		
		try{
			var ProjectNotes = '';
			if (noteSharing == true || notesSharingEnabled == true) {
				var idQuery = '{"term":{ "ProjectNoteCompanyId" : "'+accountId+'"}}'
				ProjectNotes = '{ "bool": {"filter": [{"has_parent": { "parent_type": "project", "query": { "terms": { "ProjectId": [ "'+projectId+'" ] } } } }, '+idQuery+' ] } }'; 
				
			}else{
				var idQuery = '{"term":{ "ProjectNoteCreatedByUserId" : "'+userIDValues+'"}}'
				ProjectNotes = '{ "bool": {"filter": [{"has_parent": { "parent_type": "project", "query": { "terms": { "ProjectId": [ "'+projectId+'" ] } } } }, '+idQuery+' ] } }'; 
			}

			if(size != undefined && exportedRequiredModules.elasticPageSize >= size){
				var FinalQuery = '{"query": ' + ProjectNotes +',"size": '+ size +'}';
				callback(null, FinalQuery);
			}
			else if(size != undefined && exportedRequiredModules.elasticPageSize < size){
				callback({Error : "Page size is exceeded the limit."}, null);
			}else{
				var FinalQuery = '{"query": ' + ProjectNotes +',"size": '+ exportedRequiredModules.elasticPageSize +'}';
				// console.log("FinalQuery",FinalQuery)
				callback(null, FinalQuery);
			}
			
		}catch (err){
			callback(err, null);
		}
	}
}
/******************************************************************************
Function: postQueryByProjectNotes
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for creating and updating project notes. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/

exports.postQueryByProjectNotes = function (requireQuery, checkExistance, callback) {


	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var ProjectNoteUserId = requireQuery.query.UserId;
	var ProjectNoteText = req.NoteText;
	var ProjectNoteProjectId = req.ProjectId;
	var ProjectNoteCompanyId =  requireQuery.AccountId;
	
	var DateQuery = '';
	var ProjectNoteTextQuery = '';
	var ProjectNoteProjectIdQuery = '';
	var ProjectNoteCompanyIdQuery = '';
	
	try {
		if (ProjectNoteText != undefined) { ProjectNoteTextQuery = ',"ProjectNoteText": "' + ProjectNoteText + '"'; }
		if (ProjectNoteProjectId != undefined) { ProjectNoteProjectIdQuery = ',"ProjectNoteProjectId": "' + ProjectNoteProjectId + '"'; }
		if (ProjectNoteCompanyId != undefined) { ProjectNoteCompanyIdQuery = ',"ProjectNoteCompanyId": "' + ProjectNoteCompanyId + '"'; }
	
		if (checkExistance != false) {
			var ProjectNoteModifiedDateTime = datetime.toISOString();
			var ProjectNoteModifiedByUserId= ',"ProjectNoteModifiedByUserId": "' + ProjectNoteUserId + '"';
			DateQuery = ProjectNoteModifiedByUserId+',"ProjectNoteModifiedDateTime": "' + ProjectNoteModifiedDateTime + '"';
		
		} else {
			var ProjectNoteCreatedDateTime = datetime.toISOString();
			var ProjectNoteCreatedByUserId= ',"ProjectNoteCreatedByUserId": "' + ProjectNoteUserId + '"';
			DateQuery = ProjectNoteCreatedByUserId+',"ProjectNoteCreatedDateTime": "' + ProjectNoteCreatedDateTime + '","ProjectNoteModifiedByUserId": null,"ProjectNoteModifiedDateTime": null';
		}
		var FQuery = ProjectNoteTextQuery + ProjectNoteProjectIdQuery + DateQuery+ProjectNoteCompanyIdQuery ;
		var FinalQuery='';
		if (FQuery.substring(0, 1) == ',') {
			FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			FinalQuery = FinalQuery;
		}
		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}