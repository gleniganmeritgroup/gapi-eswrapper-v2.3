'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetContactTagsLinkQuery.js) is used form a query for given UserId.
********************************************************************************************/
/*******************************************************************************************/
/******************************************************************************
Function: getQueryByContactTagsLink
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Contact TagLink by UserId,ContactTagIdQuery. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByContactTagsLink = function (requireQuery, tagResult,elasticServerClient, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
	var resultData = JSON.parse(JSON.stringify(tagResult));
	var ContactId = requireQuery.query.ContactId;
	if(tagResult.total == 0){
		callback(null, resultData);
	}else{
		var tagID = []
		resultData.results.forEach(function(tagData){
			tagID.push(tagData.id)
		})
		var queryBuildContactTagIds = {};
		queryBuildContactTagIds.ContactTagId = tagID;
		var queryBuildContactTagIdsField = '{"terms": %s}';
		var ContactTagIdQuery =  exportedRequiredModules.convertComma2Array(queryBuildContactTagIdsField, JSON.stringify(queryBuildContactTagIds));
		var ContactTagsQuery = '';
		if(ContactId == undefined){
			ContactTagsQuery= '{"query": {"bool": {"filter": ['+ContactTagIdQuery+']}}'+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
		}else{
			ContactTagsQuery= '{"query": {"bool": {"filter": ['+ContactTagIdQuery+',{"has_parent": {"parent_type": "contact","filter": [{"terms": {"ContactId": ["'+ContactId+'"]}}]}}]}}'+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
		}
		
		var elasticType = exportedRequiredModules.elasticContactTagsLinkTypeName;
		exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, ContactTagsQuery, elasticType,requireQuery, function (error, tagLinkResult) {
			if (error) {
				callback(error, null)
			}
			else {
				callback(null, tagLinkResult);
			}
		});	
	}
}
/******************************************************************************
Function: postQueryByContactTagLink
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for creating and updating taglinks for contact. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.postQueryByContactTagLink = function (requireQuery,TagId, checkExistance, callback) {

	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var ContactTagLinkTaggedByUserId = requireQuery.query.UserId;
	var ContactTagId = TagId;
	var ContactTagLinkName = req.ContactId;
	var ContactTagLinkUserIdQuery = '';
	var ContactTagLinkTypeTagId = '';
	try {

		if (ContactTagLinkTaggedByUserId != undefined) { ContactTagLinkUserIdQuery = '"ContactTagLinkTaggedByUserId": "' + ContactTagLinkTaggedByUserId + '"'; }
		if (ContactTagId != undefined) { ContactTagLinkTypeTagId = ',"ContactTagId": "' + ContactTagId + '"'; }
		var ContactTagLinkCreatedDate = datetime.toISOString();
		var DateQuery = ',"ContactTagLinkCreatedDate": "' + ContactTagLinkCreatedDate + '"';

		var FQuery = ContactTagLinkUserIdQuery + ContactTagLinkTypeTagId + DateQuery;
		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}

		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}

/******************************************************************************
Function: mergeTagLinksAndTags
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Project TagLink by UserId,ProjectTagIdQuery. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.mergeTagLinksAndTags = function (requireQuery, tagResult,elasticServerClient, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
	var resultData = JSON.parse(JSON.stringify(tagResult));
	var ContactId = requireQuery.query.ContactId;
	if(tagResult.total == 0){
		callback(null, resultData);
	}else{
		var tagID = []
		resultData.results.forEach(function(tagData){
			tagID.push(tagData.id)
		})
		var queryBuildContactTagIds = {};
		queryBuildContactTagIds.ContactTagId = tagID;
		var queryBuildContactTagIdsField = '{"terms": %s}';
		var ContactTagIdQuery =  exportedRequiredModules.convertComma2Array(queryBuildContactTagIdsField, JSON.stringify(queryBuildContactTagIds));
		var ContactTagsQuery = '';
		if(ContactId == undefined){
			ContactTagsQuery= '{"query": {"bool": {"filter": ['+ContactTagIdQuery+']}}'+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
		}else{
			ContactTagsQuery= '{"query": {"bool": {"filter": ['+ContactTagIdQuery+',{"has_parent": {"parent_type": "contact","filter": [{"terms": {"ContactId": ["'+ContactId+'"]}}]}}]}}'+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
		}
		
		var elasticType = exportedRequiredModules.elasticContactTagsLinkTypeName;
		exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, ContactTagsQuery, elasticType,requireQuery, function (error, tagLinkResult) {
			if (error) {
				callback(error, null)
			}
			else {
				if(tagLinkResult.total == 0){
					callback(null, tagLinkResult);
				}else{
					tagLinkResult.results.forEach(function(tagLinkData, index, array){
						var cTagLinkId = tagLinkData.source.ContactTagId
						resultData.results.forEach(function(tagData,tagIndex,tagArray){
							var cTagId = tagData.id
							if(cTagLinkId == cTagId){
								tagLinkResult.results[index].source.ContactTagType = tagData.source.ContactTagType;
								tagLinkResult.results[index].source.ContactTagName = tagData.source.ContactTagName;
								tagLinkResult.results[index].source.ContactTagColour = tagData.source.ContactTagColour;
							}
							if(index+1 == array.length && tagIndex+1 == tagArray.length){
								callback(null, tagLinkResult);
							}

						})
					})
				}
			}
		});	
	}
}