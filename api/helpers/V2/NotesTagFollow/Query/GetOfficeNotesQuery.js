'use strict';
/******************************************************************************
Function: getQueryByOfficeNotes
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Office Notes by UserId,officeId. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/

exports.getQueryByOfficeNotes = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;
	var officeId= requireQuery.query.OfficeId;
	var size = requireQuery.query.Size;
	var notesSharingEnabled = requireQuery.query.NotesSharingEnabled
	var accountId = requireQuery.AccountId;
	var noteSharing = requireQuery.NoteSharing;
	if ((officeId == undefined)) {
		callback({ Error: "Parameter OfficeId is Missing" }, null);
	}
	else {

		try {
			var OfficeNote = '';
			if (noteSharing == true || notesSharingEnabled == true) {
				var idQuery = '{"term":{ "OfficeNoteCompanyId" : "'+accountId+'"}}'
				OfficeNote = '{ "bool": {"filter": [{"has_parent": { "parent_type": "company", "query": { "terms": { "OfficeId": [ "'+officeId+'" ] } } } }, '+idQuery+' ] } }'; 
				
			}else{
				var idQuery = '{"term":{ "OfficeNoteCreatedByUserId" : "'+userIDValues+'"}}'
				OfficeNote = '{ "bool": {"filter": [{"has_parent": { "parent_type": "company", "query": { "terms": { "OfficeId": [ "'+officeId+'" ] } } } }, '+idQuery+' ] } }'; 
			}

			if(size != undefined && exportedRequiredModules.elasticPageSize >= size){
				var FinalQuery = '{"query": ' + OfficeNote +',"size": '+ size +'}';
				callback(null, FinalQuery);
			}
			else if(size != undefined && exportedRequiredModules.elasticPageSize < size){
				callback({Error : "Page size is exceeded the limit."}, null);
			}else{
				var FinalQuery = '{"query": ' + OfficeNote +',"size": '+ exportedRequiredModules.elasticPageSize +'}';
				callback(null, FinalQuery);
			}
		} catch (err) {
			callback(err, null);
		}
	}
}
/******************************************************************************
Function: postQueryByOfficeNotes
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for creating and updating office notes. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.postQueryByOfficeNotes = function (requireQuery, checkExistance, callback) {


	
	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var OfficeNoteUserId = requireQuery.query.UserId;
	var OfficeNoteText = req.NoteText;
	var OfficeNoteOfficeId = req.OfficeId;
	var OfficeNoteCompanyId = requireQuery.AccountId;

	var DateQuery = '';
	var OfficeNoteTextQuery = '';
	var OfficeNoteOfficeIdQuery = '';
	var OfficeNoteCompanyIdQuery = '';
	var parent = '';
	try {

		//if (OfficeNoteUserId != undefined) { OfficeNoteUserIdQuery = '"OfficeNoteCreatedByUserId": "' + OfficeNoteUserId + '"'; }
		if (OfficeNoteText != undefined) { OfficeNoteTextQuery = ',"OfficeNoteText": "' + OfficeNoteText + '"'; }
		if (OfficeNoteOfficeId != undefined) { OfficeNoteOfficeIdQuery = ',"OfficeNoteOfficeId": "' + OfficeNoteOfficeId + '"'; }
		if (OfficeNoteCompanyId != undefined) { OfficeNoteCompanyIdQuery = ',"OfficeNoteCompanyId": "' + OfficeNoteCompanyId + '"'; }
		if (parent != undefined) { parent = ',"parent": "' + OfficeNoteOfficeId + '"'; }
		if (checkExistance != false) {
			var OfficeNoteModifiedDateTime = datetime.toISOString();
			var OfficeNoteModifiedByUserId = ',"OfficeNoteModifiedByUserId": "' + OfficeNoteUserId + '"';
			DateQuery = OfficeNoteModifiedByUserId + ',"OfficeNoteModifiedDateTime": "' + OfficeNoteModifiedDateTime + '"';

		} else {
			
			var OfficeNoteCreatedDateTime = datetime.toISOString();
			var OfficeNoteCreatedByUserId = ',"OfficeNoteCreatedByUserId": "' + OfficeNoteUserId + '"';
			DateQuery = OfficeNoteCreatedByUserId + ',"OfficeNoteCreatedDateTime": "' + OfficeNoteCreatedDateTime + '","OfficeNoteModifiedByUserId": null,"OfficeNoteModifiedDateTime": null';


		}
		var FQuery = OfficeNoteTextQuery + OfficeNoteOfficeIdQuery + DateQuery + OfficeNoteCompanyIdQuery;
		var FinalQuery = '';
		if (FQuery.substring(0, 1) == ',') {
			FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			FinalQuery = FinalQuery;
		}
		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
