'use strict';
/******************************************************************************
Function: getQueryByProjectTagsLink
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Project TagLink by UserId,ProjectTagIdQuery. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByProjectTagsLink = function (requireQuery, tagResult,elasticServerClient, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
	var resultData = JSON.parse(JSON.stringify(tagResult));
	var ProjectId = requireQuery.query.ProjectId;
	var size = requireQuery.query.Size;
	if(tagResult.total == 0){
		callback(null, resultData);
	}else{
		var tagID = []
		resultData.results.forEach(function(tagLinkData){
			var pTagLinkId = tagLinkData.id
			tagID.push(pTagLinkId)
			
		})
		var queryBuildProjectTagIds = {};
		queryBuildProjectTagIds.ProjectTagId = tagID;
		var queryBuildProjectTagIdsField = '{"terms": %s}';
		var ProjectTagIdQuery =  exportedRequiredModules.convertComma2Array(queryBuildProjectTagIdsField, JSON.stringify(queryBuildProjectTagIds));
		var ProjectTagsQuery = '';
		if(ProjectId == undefined){
			ProjectTagsQuery= '{"query": {"bool": {"filter": ['+ProjectTagIdQuery+']}}'+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
		}else{
			ProjectTagsQuery= '{"query": {"bool": {"filter": ['+ProjectTagIdQuery+',{"has_parent": {"parent_type": "project","filter": [{"terms": {"ProjectId": ["'+ProjectId+'"]}}]}}]}}'+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
		}
		
		var elasticType = exportedRequiredModules.elasticProjectTagsLinkTypeName;
		exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, ProjectTagsQuery, elasticType,requireQuery, function (error, tagLinkResult) {
			if (error) {
				callback(error, null)
			}
			else {
				callback(null, tagLinkResult);
			}
		});	
	}
}

/******************************************************************************
Function: postQueryByProjectTagLink
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for creating and updating taglinks for project. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.postQueryByProjectTagLink = function (requireQuery,TagId, checkExistance, callback) {

	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var ProjectTagLinkTaggedByUserId =  requireQuery.query.UserId;
	var ProjectTagId = TagId;
	var ProjectTagLinkName = req.ProjectId;


	var ProjectTagLinkUserIdQuery = '';
	var ProjectTagLinkTypeTagId = '';
	try {

		if (ProjectTagLinkTaggedByUserId != undefined) { ProjectTagLinkUserIdQuery = '"ProjectTagLinkTaggedByUserId": "' + ProjectTagLinkTaggedByUserId + '"'; }
		if (ProjectTagId != undefined) { ProjectTagLinkTypeTagId = ',"ProjectTagId": "' + ProjectTagId + '"'; }
		var ProjectTagLinkCreatedDate = datetime.toISOString();
		var DateQuery = ',"ProjectTagLinkCreatedDate": "' + ProjectTagLinkCreatedDate + '"';

		var FQuery = ProjectTagLinkUserIdQuery + ProjectTagLinkTypeTagId + DateQuery;
		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';
		} else {
			var FinalQuery = FinalQuery;
		}

		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}

/******************************************************************************
Function: mergeTagLinksAndTags
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Project TagLink by UserId,ProjectTagIdQuery. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.mergeTagLinksAndTags = function (requireQuery, tagResult,elasticServerClient, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
	var resultData = JSON.parse(JSON.stringify(tagResult));
	var ProjectId = requireQuery.query.ProjectId;
	if(tagResult.total == 0){
		callback(null, resultData);
	}else{
		var tagID = []
		resultData.results.forEach(function(tagLinkData){
			var pTagLinkId = tagLinkData.id
			tagID.push(pTagLinkId)
			
		})
		var queryBuildProjectTagIds = {};
		queryBuildProjectTagIds.ProjectTagId = tagID;
		var queryBuildProjectTagIdsField = '{"terms": %s}';
		var ProjectTagIdQuery =  exportedRequiredModules.convertComma2Array(queryBuildProjectTagIdsField, JSON.stringify(queryBuildProjectTagIds));
		var ProjectTagsQuery = '';
		if(ProjectId == undefined){
			ProjectTagsQuery= '{"query": {"bool": {"filter": ['+ProjectTagIdQuery+']}}'+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
		}else{
			ProjectTagsQuery= '{"query": {"bool": {"filter": ['+ProjectTagIdQuery+',{"has_parent": {"parent_type": "project","filter": [{"terms": {"ProjectId": ["'+ProjectId+'"]}}]}}]}}'+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
		}
		
		var elasticType = exportedRequiredModules.elasticProjectTagsLinkTypeName;
		exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, ProjectTagsQuery, elasticType, requireQuery,function (error, tagLinkResult) {
			if (error) {
				callback(error, null)
			}
			else {
				if(tagLinkResult.total == 0){
					callback(null, tagLinkResult);
				}else{
					tagLinkResult.results.forEach(function(tagLinkData, index, array){
						var pTagLinkId = tagLinkData.source.ProjectTagId
						resultData.results.forEach(function(tagData,tagIndex,tagArray){
							var pTagId = tagData.id
							if(pTagLinkId == pTagId){
								tagLinkResult.results[index].source.ProjectTagType = tagData.source.ProjectTagType;
								tagLinkResult.results[index].source.ProjectTagName = tagData.source.ProjectTagName;
								tagLinkResult.results[index].source.ProjectTagColour = tagData.source.ProjectTagColour;
							}
							if(index+1 == array.length && tagIndex+1 == tagArray.length){
								callback(null, tagLinkResult);
							}

						})
					})
				}
			}
		});	
	}
}