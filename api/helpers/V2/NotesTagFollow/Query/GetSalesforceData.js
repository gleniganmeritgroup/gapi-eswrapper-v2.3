
/********************************************************************************************
This file (GetMetadata.js) is getting data from salesforce which is updated from past hour.

********************************************************************************************/
/*******************************************************************************************/

let SalesforceConnection = require("node-salesforce-connection");
let moment = require('moment');
var jsSalesForceforce = require('jsforce');
var yamlConfig = require('js-yaml');
var fs = require('fs');
var sizeof = require('object-sizeof');
var credentialsObj = yamlConfig.safeLoad(fs.readFileSync('./config/Credentials.yml', 'utf8'));
let username = credentialsObj.Salesforce.username;
let password = credentialsObj.Salesforce.password;
let token = credentialsObj.Salesforce.token;
let hostname = credentialsObj.Salesforce.host;
let apiVersion = credentialsObj.Salesforce.version;

var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
/******************************************************************************
Function: handler
Argument:query
Usage:
	1. Function handles request by passing query to salesforce connection and returning the result.
*******************************************************************************/
async function handler(query) {

    let sfConn = new SalesforceConnection();


    let queryPath = "/services/data/v" + apiVersion + "/query/?q=";

    await sfConn.soapLogin({
        hostname: hostname,
        apiVersion: apiVersion,
        username: username,
        password: password + token,
    });
    var offset = new Date().getTimezoneOffset();

    //console.log("query " + query);

    let sfInfo = await sfConn.rest(queryPath + encodeURIComponent(query), api = "bulk");
    //  console.log(sfInfo);
    return sfInfo;
}


/******************************************************************************
Function: salesforceData
Argument:query
Usage:
	1. Returns records based on the query from salesforce.
*******************************************************************************/
function salesforceData(query, callback) {

    var initializePromise = handler(query);
    initializePromise.then(function (result) {
        sfDetails = result;
        callback(null, sfDetails);
    }, function (err) {
        callback("Error", null);
    })
}
/******************************************************************************
Function: projectSavedSearchDataList
Argument:request
Return: return project saved search list.
Usage:
	1. Retreives project saved search from salesforce. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.projectSavedSearchDataList = function (request, callback) {
    var userId = request.query.UserId;
    
    var Size = request.query.Size;
    if (Size == undefined) {
        Size = parseInt(credentialsObj.Salesforce.DefaultSavedSearchSize);
    }
    var SavedSearchResult = [];
    var query = credentialsObj.Salesforce.mobileProjectQuerySavedSearch.replace('user_Id', "'" + userId + "'");
    salesforceData(query, function (err, SavedSearches) {
        if (err) {
            callback(err, null);
        } else {
            delete SavedSearches.totalSize;
            delete SavedSearches.done;
            SavedSearches.records.forEach(function (SavedSearchRecords, indexNumber) {

                SavedSearchRecords.JSONCriteriaArrayString = JSON.parse(SavedSearchRecords.Field2__c);
                SavedSearchRecords.ElasticSearchQueryString = JSON.parse(SavedSearchRecords.Field3__c);
                delete SavedSearchRecords.Field2__c;
                delete SavedSearchRecords.Field3__c;
                delete SavedSearchRecords.attributes;
                var type = SavedSearchRecords.Type__c;
                if(type == "ProjectMobileSavedSearch"){
                    SavedSearchRecords.Type = "Mobile";
                    delete SavedSearchRecords.Type__c;
                }else if(type == "ProjectSavedSearch" || type == "SavedSearch"){
                    SavedSearchRecords.Type = "Desktop";
                    delete SavedSearchRecords.Type__c;
                }
                
                if (SavedSearchRecords.LastRunDate != null) {
                    SavedSearchRecords.LastRunDate = new Date(SavedSearchRecords.LastRunDate);
                }

            });
            SavedSearches.records.sort(LastUpdateSort);
            SavedSearches.records.forEach(function (SavedSearchFinalRecords, indexNumber) {
                if (indexNumber < Size) {
                    SavedSearchResult.push(SavedSearchFinalRecords);

                }

            });
            var mobileSearchLength = SavedSearchResult.length;
            if(SavedSearchResult.length < Size){
                var query = credentialsObj.Salesforce.desktopProjectQuerySavedSearch.replace('user_Id', "'" + userId + "'");
                salesforceData(query, function (err, SavedSearches) {
                    if (err) {
                        callback(err, null);
                    } else {
                        delete SavedSearches.totalSize;
                        delete SavedSearches.done;
                        SavedSearches.records.forEach(function (SavedSearchRecords, indexNumber) {
                            SavedSearchRecords.JSONCriteriaArrayString = JSON.parse(SavedSearchRecords.Field2__c);
                            SavedSearchRecords.ElasticSearchQueryString = JSON.parse(SavedSearchRecords.Field3__c);
                            delete SavedSearchRecords.Field2__c;
                            delete SavedSearchRecords.Field3__c;
                            delete SavedSearchRecords.attributes;
                            var type = SavedSearchRecords.Type__c;
                            if(type == "ProjectMobileSavedSearch"){
                                SavedSearchRecords.Type = "Mobile";
                                delete SavedSearchRecords.Type__c;
                            }else if(type == "ProjectSavedSearch" || type == "SavedSearch"){
                                SavedSearchRecords.Type = "Desktop";
                                delete SavedSearchRecords.Type__c;
                            }
                            
                            if (SavedSearchRecords.LastRunDate != null) {
                                SavedSearchRecords.LastRunDate = new Date(SavedSearchRecords.LastRunDate);
                            }
            
                        });
                        SavedSearches.records.sort(LastUpdateSort);
                        SavedSearches.records.forEach(function (SavedSearchFinalRecords, indexNumber) {
                            if ((mobileSearchLength + indexNumber) < Size) {
                                SavedSearchResult.push(SavedSearchFinalRecords);
                            }
                        });
                        SavedSearches.records = SavedSearchResult;
                        callback(null, SavedSearches);
                    }
                });
            }else{
                SavedSearches.records = SavedSearchResult;
                callback(null, SavedSearches);
            }
        }
    });
}
/******************************************************************************
Function: officeSavedSearchDataList
Argument:request
Return: return office saved search list.
Usage:
	1. Retreives office saved search from salesforce. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.officeSavedSearchDataList = function (request, callback) {
    var userId = request.query.UserId;
    
    var Size = request.query.Size;
    if (Size == undefined) {
        Size = parseInt(credentialsObj.Salesforce.DefaultSavedSearchSize);
    }
    var SavedSearchResult = [];
    var query = credentialsObj.Salesforce.mobileOfficeQuerySavedSearch.replace('user_Id', "'" + userId + "'");
    salesforceData(query, function (err, SavedSearches) {
        if (err) {
            callback(err, null);
        } else {
            delete SavedSearches.totalSize;
            delete SavedSearches.done;
            SavedSearches.records.forEach(function (SavedSearchRecords, indexNumber) {

                SavedSearchRecords.JSONCriteriaArrayString = JSON.parse(SavedSearchRecords.Field2__c);
                SavedSearchRecords.ElasticSearchQueryString = JSON.parse(SavedSearchRecords.Field3__c);
                delete SavedSearchRecords.Field2__c;
                delete SavedSearchRecords.Field3__c;
                delete SavedSearchRecords.attributes;
                var type = SavedSearchRecords.Type__c;
                if(type == "OfficeMobileSavedSearch"){
                    SavedSearchRecords.Type = "Mobile";
                    delete SavedSearchRecords.Type__c;
                }else if(type == "OfficeSavedSearch"){
                    SavedSearchRecords.Type = "Desktop";
                    delete SavedSearchRecords.Type__c;
                }
                if (SavedSearchRecords.LastRunDate != null) {
                    SavedSearchRecords.LastRunDate = new Date(SavedSearchRecords.LastRunDate);
                }

            });
            SavedSearches.records.sort(LastUpdateSort);
            SavedSearches.records.forEach(function (SavedSearchFinalRecords, indexNumber) {
                if (indexNumber < Size) {
                    SavedSearchResult.push(SavedSearchFinalRecords);

                }

            });
            var mobileSearchLength = SavedSearchResult.length;
            if(SavedSearchResult.length < Size){
                var query = credentialsObj.Salesforce.desktopOfficeQuerySavedSearch.replace('user_Id', "'" + userId + "'");
                salesforceData(query, function (err, SavedSearches) {
                    if (err) {
                        callback(err, null);
                    } else {
                        delete SavedSearches.totalSize;
                        delete SavedSearches.done;
                        SavedSearches.records.forEach(function (SavedSearchRecords, indexNumber) {

                            SavedSearchRecords.JSONCriteriaArrayString = JSON.parse(SavedSearchRecords.Field2__c);
                            SavedSearchRecords.ElasticSearchQueryString = JSON.parse(SavedSearchRecords.Field3__c);
                            delete SavedSearchRecords.Field2__c;
                            delete SavedSearchRecords.Field3__c;
                            delete SavedSearchRecords.attributes;
                            var type = SavedSearchRecords.Type__c;
                            if(type == "OfficeMobileSavedSearch"){
                                SavedSearchRecords.Type = "Mobile";
                                delete SavedSearchRecords.Type__c;
                            }else if(type == "OfficeSavedSearch"){
                                SavedSearchRecords.Type = "Desktop";
                                delete SavedSearchRecords.Type__c;
                            }
                            if (SavedSearchRecords.LastRunDate != null) {
                                SavedSearchRecords.LastRunDate = new Date(SavedSearchRecords.LastRunDate);
                            }

                        });
                        SavedSearches.records.sort(LastUpdateSort);
                        SavedSearches.records.forEach(function (SavedSearchFinalRecords, indexNumber) {
                            if ((mobileSearchLength + indexNumber) < Size) {
                                SavedSearchResult.push(SavedSearchFinalRecords);

                            }

                        });
                        SavedSearches.records = SavedSearchResult;
                        callback(null, SavedSearches);
                    }
                });
            }else{
                SavedSearches.records = SavedSearchResult;
                callback(null, SavedSearches);
            }
        }
    });
}
/******************************************************************************
Function: contactSavedSearchDataList
Argument:request
Return: return contact saved search list.
Usage:
	1. Retreives contact saved search from salesforce. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.contactSavedSearchDataList = function (request, callback) {
    var userId = request.query.UserId;
    
    var Size = request.query.Size;
    if (Size == undefined) {
        Size = parseInt(credentialsObj.Salesforce.DefaultSavedSearchSize);
    }
    var SavedSearchResult = [];
    var query = credentialsObj.Salesforce.mobileContactQuerySavedSearch.replace('user_Id', "'" + userId + "'");
    salesforceData(query, function (err, SavedSearches) {
        if (err) {
            callback(err, null);
        } else {
            delete SavedSearches.totalSize;
            delete SavedSearches.done;
            SavedSearches.records.forEach(function (SavedSearchRecords, indexNumber) {

                SavedSearchRecords.JSONCriteriaArrayString = JSON.parse(SavedSearchRecords.Field2__c);
                SavedSearchRecords.ElasticSearchQueryString = JSON.parse(SavedSearchRecords.Field3__c);
                delete SavedSearchRecords.Field2__c;
                delete SavedSearchRecords.Field3__c;
                delete SavedSearchRecords.attributes;
                var type = SavedSearchRecords.Type__c;
                if(type == "ContactMobileSavedSearch"){
                    SavedSearchRecords.Type = "Mobile";
                    delete SavedSearchRecords.Type__c;
                }else if(type == "ContactSavedSearch"){
                    SavedSearchRecords.Type = "Desktop";
                    delete SavedSearchRecords.Type__c;
                }
                if (SavedSearchRecords.LastRunDate != null) {
                    SavedSearchRecords.LastRunDate = new Date(SavedSearchRecords.LastRunDate);
                }

            });
            SavedSearches.records.sort(LastUpdateSort);
            SavedSearches.records.forEach(function (SavedSearchFinalRecords, indexNumber) {
                if (indexNumber < Size) {
                    SavedSearchResult.push(SavedSearchFinalRecords);

                }

            });
            var mobileSearchLength = SavedSearchResult.length;
            if(SavedSearchResult.length < Size){
                var query = credentialsObj.Salesforce.desktopContactQuerySavedSearch.replace('user_Id', "'" + userId + "'");
                salesforceData(query, function (err, SavedSearches) {
                    if (err) {
                        callback(err, null);
                    } else {
                        delete SavedSearches.totalSize;
                        delete SavedSearches.done;
                        SavedSearches.records.forEach(function (SavedSearchRecords, indexNumber) {

                            SavedSearchRecords.JSONCriteriaArrayString = JSON.parse(SavedSearchRecords.Field2__c);
                            SavedSearchRecords.ElasticSearchQueryString = JSON.parse(SavedSearchRecords.Field3__c);
                            delete SavedSearchRecords.Field2__c;
                            delete SavedSearchRecords.Field3__c;
                            delete SavedSearchRecords.attributes;
                            var type = SavedSearchRecords.Type__c;
                            if(type == "ContactMobileSavedSearch"){
                                SavedSearchRecords.Type = "Mobile";
                                delete SavedSearchRecords.Type__c;
                            }else if(type == "ContactSavedSearch"){
                                SavedSearchRecords.Type = "Desktop";
                                delete SavedSearchRecords.Type__c;
                            }
                            if (SavedSearchRecords.LastRunDate != null) {
                                SavedSearchRecords.LastRunDate = new Date(SavedSearchRecords.LastRunDate);
                            }

                        });
                        SavedSearches.records.sort(LastUpdateSort);
                        SavedSearches.records.forEach(function (SavedSearchFinalRecords, indexNumber) {
                            if ((mobileSearchLength + indexNumber) < Size) {
                                SavedSearchResult.push(SavedSearchFinalRecords);

                            }

                        });
                        SavedSearches.records = SavedSearchResult;
                        callback(null, SavedSearches);
                    }
                });
            }else{
                SavedSearches.records = SavedSearchResult;
                callback(null, SavedSearches);
            }
        }
    });
}

function LastUpdateSort(a, b) {
    return new Date(b.LastRunDate).getTime() - new Date(a.LastRunDate).getTime();
}
/******************************************************************************
Function: searchQuery
Argument:UserId,SearchId
Return: return a saved search list.
Usage:
	1. Retrieves saved search list from salesforce. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.searchQuery = function (UserId, SearchId, callback) {

    var query = credentialsObj.Salesforce.queryExecuteSearch.replace('user_Id', "'" + UserId + "'");
    query = query.replace('Search_Id', "'" + SearchId + "'")

    salesforceData(query, function (err, res) {

        if (err) {
            callback(err, null);
        } else {
            if (res.records.length > 0) {
                var salesforceList = res.records[0].Field3__c;
                if (salesforceList != null) {
                    var savedSearchQuery = salesforceList;
                    callback(null, savedSearchQuery);

                }
                else {
                    var SfConnection = new jsSalesForceforce.Connection({ loginUrl: 'https://' + hostname });

                    SfConnection.login(username, password + token, function (err, userInfo) {
                        if (err) {

                            callback(err, null);
                        }
                        else {
                            var elasticQueryName = 'Field3__c.json';
                            //     console.log(elasticQueryName);

                            //    SfConnection.sobject('Attachment').record('00P6E000004zGahUAE').blob('Body').on('data', function(data) {
                            SfConnection.sobject("Attachment ").select('Id').where("ParentId = '" + SearchId + "' AND Name = '" + elasticQueryName + "'").execute(function (error, AttachmentIds) {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    var AttachmentData = SfConnection.sobject('Attachment').record(AttachmentIds[0].Id).blob('Body');
                                    var bufferOutput = [];
                                    AttachmentData.on('data', function (data) {
                                        bufferOutput.push(data);
                                    });


                                    AttachmentData.on('end', function () {
                                        var AttachmentContent = Buffer.concat(bufferOutput);
                                        var savedSearchQuery = AttachmentContent.toString();
                                        // savedSearchQuery = savedSearchQuery.replace(/\n/g, ' ');
                                        // console.log("savedSearchQuery",savedSearchQuery)
                                        callback(null, savedSearchQuery);
                                    });
                                }
                            });
                        }
                    });

                }
            }
            else {
                callback({ "Message": "No Records found" }, null);
            }
        }

    });

}
/******************************************************************************
Function: userProfileDataList
Argument:request
Usage:
	1. Returns User details stored in Salesforce.
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.userProfileDataList = function (request, callback) {

    var userId = request.query.UserId;
    var sharedUserIds = request.query.SharedUserIds;
    if(sharedUserIds != undefined){
        var splitIds = sharedUserIds.split(",")
        var sharedID = '';
        splitIds.forEach(function(id, index,array){
            if(array.length == 1){
                sharedID = "'"+id+"'";
                var query = credentialsObj.Salesforce.querySharedUserProfile.replace('sharedUserIds',sharedID);
                var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
                var avatarLinks = exportedRequiredModules.avatarLinksObj.Avatar;
                salesforceData(query, function (error, UserDetails) {
                    if (error) {
                        callback(error, null);
                    } else {
                        delete UserDetails.totalSize;
                        delete UserDetails.done;
                        UserDetails.records.forEach(function(data,index){
                            delete UserDetails.records[index].attributes;
                        })
                        callback(null, UserDetails);
                    }
                });
            }else if(index == 0){
                sharedID = "'"+id+"'";
            }else if(index+1 == array.length){
                sharedID = sharedID+",'"+id+"'";
                var query = credentialsObj.Salesforce.querySharedUserProfile.replace('sharedUserIds', sharedID  );
                var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
                var avatarLinks = exportedRequiredModules.avatarLinksObj.Avatar;
                salesforceData(query, function (error, UserDetails) {
                    if (error) {
                        callback(error, null);
                    } else {
                        delete UserDetails.totalSize;
                        delete UserDetails.records[0].Contact;
                        delete UserDetails.done;
                        UserDetails.records.forEach(function(data,index){
                            delete UserDetails.records[index].attributes;
                        })
                        callback(null, UserDetails);
                    }
                });
            }else{
                sharedID = sharedID+",'"+id+"'";
            }
        })
    }else{
        var query = credentialsObj.Salesforce.queryUserProfile.replace('user_Id', "'" + userId + "'");
        var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
        var avatarLinks = exportedRequiredModules.avatarLinksObj.Avatar;
        salesforceData(query, function (error, UserDetails) {
            if (error) {
                callback(error, null);
            } else {
                
                var accountManagerName = UserDetails.records[0].Contact.Account.Owner_Name__c;
                var accountManagerEmail = UserDetails.records[0].Contact.Account.Owner.Email;
                var phoneNumber = UserDetails.records[0].Contact.Phone;
                var accountManagerPhone = UserDetails.records[0].Contact.Account.Owner.Phone;
                var companyName = UserDetails.records[0].Contact.Account.Name;
                var noteSharing = UserDetails.records[0].Contact.Account.GP_NoteSharingEnabled__c;
                var advanceUser = UserDetails.records[0].GP_IsAdvancedUser__c;
                var accountOwnerID = UserDetails.records[0].Contact.Account.Owner.Id;
                var avatarLink = avatarLinks[accountOwnerID]
                if(avatarLink == undefined){avatarLink = '';}
                delete UserDetails.totalSize;
                delete UserDetails.records[0].Contact;
                delete UserDetails.done;
                delete UserDetails.records[0].attributes;
                delete UserDetails.records[0].GP_IsAdvancedUser__c;
                UserDetails.records[0]["CompanyName"] = companyName;
                UserDetails.records[0]["Account_Manager_Name"] = accountManagerName;
                UserDetails.records[0]["Account_Manager_Email_Address"] = accountManagerEmail;
                UserDetails.records[0]["Phone_Number"] = phoneNumber;
                UserDetails.records[0]["Account_Manager_Phone_Number"] = accountManagerPhone;
                UserDetails.records[0]["Account_Manager_Avatar"] = avatarLink;
                UserDetails.records[0]["Notes_Sharing_Enabled"] = noteSharing;
                UserDetails.records[0]["IsAdvancedUser"] = advanceUser;
                callback(null, UserDetails);
            }
        });
    }
}
/******************************************************************************
Function: CreateSavedSearchQuery
Argument:request,salesforcefConn
Usage:
	1. Creates a saved search object in Salesforce.
*******************************************************************************/
function CreateSavedSearchQuery(request, salesforcefConn, callback) {
    var RequestParam = JSON.parse(request.body);
    //var userId = RequestParam.userId;
    //var organisationId = RequestParam.organisationId;
    var organisationId;
    var userId = request.query.UserId;
    var searchName = RequestParam.name;
    var elasticQueryUpdated = {}
    elasticQueryUpdated["filters"] = RequestParam.filters
    elasticQueryUpdated["switches"] = RequestParam.switches
    elasticQueryUpdated["query"] = RequestParam.query
    var elasticQuery = JSON.stringify(elasticQueryUpdated);
    var lastRunCount = RequestParam.lastRunCount;
    var searchType = RequestParam.Type;
    var datetime = new Date();
    var LastRunDateQuery = datetime.toISOString();
    var createdDateQuery = datetime.toISOString();
    var length_elasticQuery = sizeof(elasticQuery) / 1024;
    // let objJsonB64 = Buffer.from(objJsonStr).toString("base64");
    // console.log(length_elasticQuery);
    var elasticQueryName = 'Field3__c.json';
    var query = credentialsObj.Salesforce.queryUserProfile.replace('user_Id', "'" + userId + "'");    
    salesforceData(query, function (error, result) {
            if (error) {
               callback(err, null);
            }
            else {
                    organisationId = result.records[0].AccountId;
                    if (length_elasticQuery < 30) {
                        salesforcefConn.sobject("GP_CustomObject__c").create({ UserId__c: userId, OrganisationId__c: organisationId, Field1__c: searchName,  Field3__c: elasticQuery, Field6__c: lastRunCount, Field4__c: createdDateQuery, Field5__c: LastRunDateQuery, Key_Type_Organisation_User__c: searchType + "_" + organisationId + "_" + userId, Key_Type_Organisation__c: searchType + "_" + organisationId, Type__c: searchType }
                            , function (error, response) {
                                if (error || !response.success) { 
                                    // console.log("error",error)
                                    callback({ "Message": "Not Created Successfully" }, null); }
                                else {

                                    callback(null, { "Message": "Created Successfully and SearchId is " + response.id });
                                }
                            }
                        );
                    }
                    else if (length_elasticQuery >= 30) {
                        salesforcefConn.sobject("GP_CustomObject__c").create({ UserId__c: userId, OrganisationId__c: organisationId, Field1__c: searchName, Field6__c: lastRunCount, Field4__c: createdDateQuery, Field5__c: LastRunDateQuery, Key_Type_Organisation_User__c: searchType + "_" + organisationId + "_" + userId, Key_Type_Organisation__c: searchType + "_" + organisationId, Type__c: searchType }
                            , function (error, response) {
                                // console.log(error)
                                if (error || !response.success) { callback({ "Message": "Not Created Successfully" }, null); }
                                else {
                                   let elasticQueryB64 = Buffer.from(elasticQuery).toString("base64");
                                    salesforcefConn.sobject("Attachment").create({ Name: elasticQueryName, Body: elasticQueryB64, ParentId: response.id }
                                        , function (error, responseElasticquery) {
                                            if (error || !responseElasticquery.success) { callback({ "Message": "Not Created Successfully" }, null); }
                                            else {

                                                callback(null, { "Message": "Created Successfully and SearchId: " + response.id + ", ElasticQuery AttachmentID : " + responseElasticquery.id });
                                            }
                                        }
                                    );
                                }
                            }
                        );

                    }
            }
    });
}
/******************************************************************************
Function: UpdateSavedSearch
Argument:request,salesforcefConn
Usage:
	1. Updates a saved search object in Salesforce.
*******************************************************************************/
function UpdateSavedSearch(request, salesforcefConn, callback) {
    var RequestParam = JSON.parse(request.body);
    var SavedSearchId = RequestParam.SavedSearchId;
    if(SavedSearchId == undefined){
        callback({"Error" : "SavedSearchId is missed in updated query"}, null);
    }else{
        var searchName = RequestParam.name;
        var elasticQueryUpdated = {}
        elasticQueryUpdated["filters"] = RequestParam.filters
        elasticQueryUpdated["switches"] = RequestParam.switches
        elasticQueryUpdated["query"] = RequestParam.query
        var elasticQuery = JSON.stringify(elasticQueryUpdated);
        // console.log("elasticQuery",elasticQuery)
        var lastRunCount = RequestParam.lastRunCount;
        var searchType = RequestParam.Type;
        var datetime = new Date();
        var LastRunDateQuery = datetime.toISOString();
        var length_elasticQuery = sizeof(elasticQuery) / 1024;
        var elasticQueryName = 'Field3__c.json';
        // console.log(length_elasticQuery);
        if (length_elasticQuery < 30) {
            salesforcefConn.sobject("GP_CustomObject__c").update({ Id: SavedSearchId, Field1__c: searchName, Field3__c: elasticQuery, Field6__c: lastRunCount, Field5__c: LastRunDateQuery, Type__c: searchType }
                , function (error, response) {

                    // console.log("error",error)
                    if (error || !response.success) { callback({ "Message": "Not Updated Successfully" }, null); }
                    else {

                        callback(null, { "Message": "Updated Successfully and SearchId is " + response.id });
                    }
                }
            );
        }
        else if (length_elasticQuery >= 30) {

            salesforcefConn.sobject("GP_CustomObject__c").update({ Id: SavedSearchId, Field1__c: searchName, Field3__c: '', Field6__c: lastRunCount, Field5__c: LastRunDateQuery, Type__c: searchType }
                , function (error, response) {
                    if (error || !response.success) { callback({ "Message": "Not Updated Successfully" }, null); }
                    else {
                        // console.log("SavedSearchId",SavedSearchId)
                        // console.log("elasticQueryName",elasticQueryName)
                        salesforcefConn.sobject("Attachment ").select('Id').where("ParentId = '" + SavedSearchId + "' AND Name = '" + elasticQueryName + "'").execute(function (error, AttachmentIds) {
                            if (error || !response.success) { callback({ "Message": "Not Updated Successfully" }, null); }
                            else {
                                let elasticQueryB64 = Buffer.from(elasticQuery).toString("base64");
                                // console.log(AttachmentId)
                                if (AttachmentId == undefined){
                                    salesforcefConn.sobject("Attachment").create({ Name: elasticQueryName, Body: elasticQueryB64, ParentId: SavedSearchId }
                                        , function (error, responseElasticquery) {
                                            if (error || !responseElasticquery.success) { callback({ "Message": "Not Created Successfully" }, null); }
                                            else {

                                                callback(null, { "Message": "Created Successfully and SearchId: " + SavedSearchId + ", ElasticQuery AttachmentID : " + responseElasticquery.id });
                                            }
                                        }
                                    );
                                }else{
                                    var AttachmentId = AttachmentIds[0].Id;
                                    // console.log("AttachmentId",AttachmentId)
                                    salesforcefConn.sobject("Attachment").update({ Id: AttachmentId, Name: elasticQueryName, Body: elasticQueryB64 }, function (error, responseElasticquery) {
                                        if (error || !responseElasticquery.success) { callback({ "Message": "Not Updated Successfully" }, null); }
                                        else {
                                            callback(null, { "Message": "Updated Successfully and SearchId: " + response.id + ", ElasticQuery AttachmentID" + responseElasticquery.id });
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            );
        }
    }
}
/******************************************************************************
Function: DeleteSavedSearch
Argument:request,salesforcefConn
Usage:
	1. Deletes a saved search object in Salesforce.
*******************************************************************************/
function DeleteSavedSearch(request, salesforcefConn, callback) {
    var RequestParam = JSON.parse(request.body);
    var SavedSearchId = RequestParam.SavedSearchId;
    salesforcefConn.sobject("Attachment ").select('Id').where("ParentId = '" + SavedSearchId + "'").execute(function (error, AttachmentIds) {

        if (error) { callback({ "Message": "Not Deleted Successfully" }, null); }
        else {

            if (AttachmentIds.length > 0) {
                AttachmentIds.forEach(function (data, index) {
                    var AttachmentId = data.Id
                    salesforcefConn.sobject("Attachment").destroy(AttachmentId, function (error, attachmentResponse) {

                        if (error) { callback({ "Message": "Not Deleted Successfully" }, null); }
                        else {

                            if (index == ((AttachmentIds.length) - 1)) {
                                salesforcefConn.sobject("GP_CustomObject__c").destroy(SavedSearchId, function (error, response) {
                                    if (error || !response.success) { callback({ "Message": "Not Deleted Successfully" }, null); }
                                    else {

                                        callback(null, { "Message": "Deleted Successfully and SearchId is " + response.id });
                                    }
                                }
                                );
                            }
                        }
                    });
                    //   console.log("index",index)  
                })
            }
            else {
                salesforcefConn.sobject("GP_CustomObject__c").destroy(SavedSearchId, function (error, response) {
                    if (error || !response.success) { callback({ "Message": "Not Deleted Successfully" }, null); }
                    else {

                        callback(null, { "Message": "Deleted Successfully and SearchId is " + response.id });
                    }
                }
                );
            }
        }
    });
}
/******************************************************************************
Function: CUDSavedSearch
Argument:request,opType
Usage:
	1. Function determines what operations to be done based on Operation Type.
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.CUDSavedSearch = function (request, opType, callback) {


    var SfConnection = new jsSalesForceforce.Connection({ loginUrl: 'https://' + hostname });

    SfConnection.login(username, password + token, function (err, userInfo) {
        if (err) {

            callback(error, null);
        }

        if (opType == 'Create') {
            CreateSavedSearchQuery(request, SfConnection, function (error, successMessgae) {
                if (error) {
                    callback(error, null);
                }
                else {

                    callback(null, successMessgae)
                }

            });
        }
        else if (opType == "Update") {

            UpdateSavedSearch(request, SfConnection, function (error, successMessgae) {
                if (error) {
                    callback(error, null);
                }
                else {

                    callback(null, successMessgae)
                }

            });
        }
        else if (opType == "Delete") {


            DeleteSavedSearch(request, SfConnection, function (error, successMessgae) {
                if (error) {
                    callback(error, null);
                }
                else {

                    callback(null, successMessgae)
                }

            });
        }

    });
}

/******************************************************************************
Function: ExportOptionList
Argument:request
Return: return a export options from salesforce.
Usage:
	1. Forms a salesforce soql query for searching export options. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.ExportOptionList = function (request, callback) {
    var userId = request.query.UserId;
    var searchType = request.query.TypeId;
    
//	console.log(request.AccountId);
    var Size = request.query.OptionItemsSize;
    if (Size == undefined) {
        Size = parseInt(credentialsObj.Salesforce.DefaultExportOptionsSize);
    }
    var ExportOptionsResult = [];
    if (searchType != undefined) {
        var query = credentialsObj.Salesforce.queryExportoptions;
        query=query.replace("'CustomFormat_AccountId_UserID' OR Key_Type_Organisation_User__c = 'CustomFormatPdf_AccountId_UserID' OR Key_Type_Organisation__c = 'CustomFormat_Shared_AccountId' OR Key_Type_Organisation__c = 'CustomFormatPdf_Shared_AccountId' OR Type__c = 'CustomFormat_Std' OR Type__c = 'CustomFormatPdf_Std'","'"+searchType+'_'+request.AccountId+'_'+userId+"'");
    }
    else {
        var query = credentialsObj.Salesforce.queryExportoptions;
        query= query.replace(new RegExp('UserID', 'g'), userId);
        query=query.replace(new RegExp('AccountId', 'g'), request.AccountId);
    }
//   console.log(query);
    salesforceData(query, function (err, ExportOptions) {
        if (err) {
            callback(err, null);
        } else {

            delete ExportOptions.totalSize;
            delete ExportOptions.done;
            ExportOptions.records.forEach(function (ExportOptionRecords, indexNumber) {
              ExportOptionRecords.Field2 = JSON.parse(ExportOptionRecords.Field2__c);
                ExportOptionRecords.Field3 = JSON.parse(ExportOptionRecords.Field3__c);
                ExportOptionRecords.Field4 = JSON.parse(ExportOptionRecords.CreatedDate);
                ExportOptionRecords.Field5 = ExportOptionRecords.LastRunDate;
                ExportOptionRecords.Field6 = ExportOptionRecords.LastRunCount;
                ExportOptionRecords.Field7 = ExportOptionRecords.Field7__c;
                ExportOptionRecords.Field8 = ExportOptionRecords.Field8__c;
                ExportOptionRecords.Field9 = ExportOptionRecords.Field9__c;
                ExportOptionRecords.Field10 = ExportOptionRecords.Field10__c;
                ExportOptionRecords.Type = ExportOptionRecords.Type__c;
                delete ExportOptionRecords.attributes;
                delete ExportOptionRecords.LastRunCount;
                delete ExportOptionRecords.LastRunDate;
                delete ExportOptionRecords.CreatedDate;
                delete ExportOptionRecords.Type__c;
                delete ExportOptionRecords.Field2__c;
                delete ExportOptionRecords.Field3__c;
                delete ExportOptionRecords.Field7__c;
                delete ExportOptionRecords.Field8__c;
                delete ExportOptionRecords.Field9__c;
                delete ExportOptionRecords.Field10__c;
                

            });

            ExportOptions.records.forEach(function (ExportOptionFinalRecords, indexNumber) {
                if (indexNumber < Size) {
                    ExportOptionsResult.push(ExportOptionFinalRecords);

                }

                ExportOptions.records = ExportOptionsResult;

                //        console.log (ExportOptions);
            });
            callback(null, ExportOptions);
        }
    });
}