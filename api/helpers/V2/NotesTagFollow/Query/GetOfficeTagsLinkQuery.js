'use strict';
/******************************************************************************
Function: getQueryByOfficeTagsLink
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Office TagLink by UserId,officeTagIdQuery. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/

exports.getQueryByOfficeTagsLink = function (requireQuery, tagResult,elasticServerClient, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
	var resultData = JSON.parse(JSON.stringify(tagResult));
	var OfficeId = requireQuery.query.OfficeId;
	if(tagResult.total == 0){
		callback(null, resultData);
	}else{
		var tagID = []
		resultData.results.forEach(function(tagData){
			tagID.push(tagData.id)
		})
		var queryBuildOfficeTagIds = {};
		queryBuildOfficeTagIds.OfficeTagId = tagID;
		var queryBuildOfficeTagIdsField = '{"terms": %s}';
		var officeTagIdQuery =  exportedRequiredModules.convertComma2Array(queryBuildOfficeTagIdsField, JSON.stringify(queryBuildOfficeTagIds));
		var officeTagsQuery = '';
		if(OfficeId == undefined){
			officeTagsQuery= '{"query": {"bool": {"filter": ['+officeTagIdQuery+']}}'+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
		}else{
			officeTagsQuery= '{"query": {"bool": {"filter": ['+officeTagIdQuery+',{"has_parent": {"parent_type": "company","filter": [{"terms": {"OfficeId": ["'+OfficeId+'"]}}]}}]}}'+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
		}
		
		var elasticType = exportedRequiredModules.elasticOfficeTagsLinkTypeName;
		exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, officeTagsQuery, elasticType,requireQuery, function (error, tagLinkResult) {
			if (error) {
				callback(error, null)
			}
			else {
				callback(null, tagLinkResult);
			}
		});	
	}
}

/******************************************************************************
Function: postQueryByOfficeTagsLink
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for creating and updating taglinks for office. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.postQueryByOfficeTagsLink = function (requireQuery,TagId, checkExistance, callback) {

	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var OfficeTagLinkTaggedByUserId =  requireQuery.query.UserId;
	var OfficeTagId = TagId;
	
	var OfficeTagsLinkUserIdQuery = '';
	var OfficeTagsLinkTagIdQuery = '';
	try {

		if (OfficeTagLinkTaggedByUserId != undefined) { OfficeTagsLinkUserIdQuery = '"OfficeTagLinkTaggedByUserId": "' + OfficeTagLinkTaggedByUserId + '"'; }
		if (OfficeTagId != undefined) { OfficeTagsLinkTagIdQuery = ',"OfficeTagId": "' + OfficeTagId + '"'; }
		var OfficeTagLinkCreatedDate = datetime.toISOString();
		var DateQuery = ',"OfficeTagLinkCreatedDate": "' + OfficeTagLinkCreatedDate + '"';
		var FQuery = OfficeTagsLinkUserIdQuery + OfficeTagsLinkTagIdQuery + DateQuery;
		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}



		//var FinalQuery = '{"OfficeTagsLinkUserId": "' + OfficeTagsLinkUserId + '","OfficeTagsLinkType": "' + OfficeTagsLinkType + '","OfficeTagsLinkName": "' + OfficeTagsLinkName + '","OfficeTagsLinkCompanyId": "' + OfficeTagsLinkCompanyId + '","OfficeTagsLinkColour": "' + OfficeTagsLinkColour + '"' + DateQuery + '}';
		//console.log("FinalQuery " + FinalQuery);
		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}

/******************************************************************************
Function: mergeTagLinksAndTags
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Project TagLink by UserId,ProjectTagIdQuery. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.mergeTagLinksAndTags = function (requireQuery, tagResult,elasticServerClient, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
	var resultData = JSON.parse(JSON.stringify(tagResult));
	var OfficeId = requireQuery.query.OfficeId;
	if(tagResult.total == 0){
		callback(null, resultData);
	}else{
		var tagID = []
		resultData.results.forEach(function(tagData){
			tagID.push(tagData.id)
		})
		var queryBuildOfficeTagIds = {};
		queryBuildOfficeTagIds.OfficeTagId = tagID;
		var queryBuildOfficeTagIdsField = '{"terms": %s}';
		var officeTagIdQuery =  exportedRequiredModules.convertComma2Array(queryBuildOfficeTagIdsField, JSON.stringify(queryBuildOfficeTagIds));
		var officeTagsQuery = '';
		if(OfficeId == undefined){
			officeTagsQuery= '{"query": {"bool": {"filter": ['+officeTagIdQuery+']}}'+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
		}else{
			officeTagsQuery= '{"query": {"bool": {"filter": ['+officeTagIdQuery+',{"has_parent": {"parent_type": "company","filter": [{"terms": {"OfficeId": ["'+OfficeId+'"]}}]}}]}}'+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
		}
		
		var elasticType = exportedRequiredModules.elasticOfficeTagsLinkTypeName;
		exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, officeTagsQuery, elasticType,requireQuery, function (error, tagLinkResult) {
			if (error) {
				callback(error, null)
			}
			else {
				if(tagLinkResult.total == 0){
					callback(null, tagLinkResult);
				}else{
					tagLinkResult.results.forEach(function(tagLinkData, index, array){
						var oTagLinkId = tagLinkData.source.OfficeTagId
						resultData.results.forEach(function(tagData,tagIndex,tagArray){
							var oTagId = tagData.id
							if(oTagLinkId == oTagId){
								tagLinkResult.results[index].source.OfficeTagType = tagData.source.OfficeTagType;
								tagLinkResult.results[index].source.OfficeTagName = tagData.source.OfficeTagName;
								tagLinkResult.results[index].source.OfficeTagColour = tagData.source.OfficeTagColour;
							}
							if(index+1 == array.length && tagIndex+1 == tagArray.length){
								callback(null, tagLinkResult);
							}

						})
					})
				}
			}
		});	
	}
}