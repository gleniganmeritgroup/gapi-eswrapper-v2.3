'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetContactTagsQuery.js) is used form a query for given UserId.
********************************************************************************************/
/*******************************************************************************************/
/******************************************************************************
Function: getQueryByContactTags
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Tags for Contact. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByContactTags = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;
	var size = requireQuery.query.Size;
	var tagId = requireQuery.query.TagId;
	var accountID = requireQuery.AccountId;
	if ((userIDValues == undefined)) {
		callback({ Error: "Parameter userIDValues  is Missing" }, null);
	}
	else {

		try {
			
			var ContactTag = ''
			if(tagID == undefined){
				var contactTagIdQuery = '{"term":{ "ContactTagUserId" : "' + userIDValues + '"}}';
				ContactTag = '{"bool": {"should": [{"bool": {"filter": [{"term": {"ContactTagCompanyId": "'+accountID+'"}},{"term": {"ContactTagType": "Shared"}}]}},'+ContactTagUserId+']}}'
			}else{
				var contactTagIdQuery = '{"term":{ "ContactTagUserId" : "' + userIDValues + '"}},{"term":{ "_id" : "' + tagId + '"}}';
				ContactTag = '{"bool": {"must": [{"bool": {"filter": [{"term": {"ContactTagCompanyId": "'+accountID+'"}},{"term": {"ContactTagType": "Shared"}}]}},'+ContactTagUserId+']}}'
			}
			if(size != undefined && exportedRequiredModules.elasticPageSize >= size){
				var FinalQuery = '{"query": ' + ContactTag +',"size": '+ size +'}';
				callback(null, FinalQuery);
			}
			else if(size != undefined && exportedRequiredModules.elasticPageSize < size){
				callback({Error : "Page size is exceeded the limit."}, null);
			}else{
				var FinalQuery = '{"query": ' + ContactTag +',"size": '+ exportedRequiredModules.elasticPageSize +'}';
				callback(null, FinalQuery);
			}
		} catch (err) {
			callback(err, null);
		}
	}
}
/******************************************************************************
Function: postQueryByContactTags
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for creating and updating contact tags. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.postQueryByContactTags = function (requireQuery, checkExistance, callback) {

	var datetime = new Date();

	var req = JSON.parse(requireQuery.body);

	var ContactTagUserId = requireQuery.query.UserId;
	var ContactTagType = req.TagType;
	var ContactTagName = req.TagName;
	var ContactTagCompanyId = requireQuery.AccountId;
	var ContactTagColour = req.TagColour;

	var ContactTagUserIdQuery = '';
	var ContactTagTypeQuery = '';
	var ContactTagNameQuery = '';
	var ContactTagCompanyIdQuery = '';
	var ContactTagColourQuery = '';
	try {

		if (ContactTagUserId != undefined) { ContactTagUserIdQuery = '"ContactTagUserId": "' + ContactTagUserId + '"'; }
		if (ContactTagType != undefined) { ContactTagTypeQuery = ',"ContactTagType": "' + ContactTagType + '"'; }
		if (ContactTagName != undefined) { ContactTagNameQuery = ',"ContactTagName": "' + ContactTagName + '"'; }
		if (ContactTagCompanyId != undefined) { ContactTagCompanyIdQuery = ',"ContactTagCompanyId": "' + ContactTagCompanyId + '"'; }
		if (ContactTagColour != undefined) { ContactTagColourQuery = ',"ContactTagColour": "' + ContactTagColour + '"'; }

		if (checkExistance != false) {
			var ContactTagUpdatedDate = datetime.toISOString();
			var DateQuery = ',"ContactTagUpdatedDate": "' + ContactTagUpdatedDate + '"';

		} else {
			var ContactTagUpdatedDate = datetime.toISOString();
			var ContactTagCreatedDate = datetime.toISOString();
			var DateQuery = ',"ContactTagCreatedDate": "' + ContactTagCreatedDate + '","ContactTagUpdatedDate": "' + ContactTagUpdatedDate + '"';
		}

		var FQuery = ContactTagUserIdQuery + ContactTagTypeQuery + ContactTagNameQuery + ContactTagCompanyIdQuery + ContactTagColourQuery + DateQuery;
		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}

		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
