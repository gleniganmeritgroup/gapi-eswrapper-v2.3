'use strict';
/******************************************************************************
Function: getQueryByProjectTags
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Tags for Project. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByProjectTags = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;
	var size = requireQuery.query.Size;
	var tagID = requireQuery.query.TagId;
	var accountID = requireQuery.AccountId;
	
	if ((userIDValues == undefined)) {
		callback({ Error: "Parameter userIDValues  is Missing" }, null);
	}
	else {

		try {
			var ProjectTag = ''
			if(tagID == undefined){
				var ProjectTagQuery = '{"term":{ "ProjectTagUserId" : "' + userIDValues + '"}}';
				ProjectTag = '{"bool": {"should": [{"bool": {"filter": [{"term": {"ProjectTagCompanyId": "'+accountID+'"}},{"term": {"ProjectTagType": "Shared"}}]}},'+ProjectTagQuery+']}}'
			}else{
				var ProjectTagQuery = '{"term":{ "ProjectTagUserId" : "' + userIDValues + '"}},{"term":{ "_id" : "' + tagID + '"}}';
				ProjectTag = '{"bool": {"must": [{"bool": {"filter": [{"term": {"ProjectTagCompanyId": "'+accountID+'"}},{"term": {"ProjectTagType": "Shared"}}]}},'+ProjectTagQuery+']}}'
			}
			
			if(size != undefined && exportedRequiredModules.elasticPageSize >= size){
				var FinalQuery = '{"query": '+ProjectTag+',"size": '+ size +'}';
				callback(null, FinalQuery);
			}
			else if(size != undefined && exportedRequiredModules.elasticPageSize < size){
				callback({Error : "Page size is exceeded the limit."}, null);
			}else{
				var FinalQuery = '{"query": '+ProjectTag+',"size": '+  exportedRequiredModules.elasticPageSize +'}';
				callback(null, FinalQuery);
			}
		} catch (err) {
			callback(err, null);
		}
	}
}
/******************************************************************************
Function: postQueryByProjectTags
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for creating and updating project tags. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.postQueryByProjectTags = function (requireQuery, checkExistance, callback) {
	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
	var datetime = new Date();
	
	var req = JSON.parse(requireQuery.body);

	var ProjectTagUserId = requireQuery.query.UserId;
	var ProjectTagType = req.TagType;
	var ProjectTagName = req.TagName;
	var ProjectTagCompanyId = requireQuery.AccountId;
	var ProjectTagColour = req.TagColour;

	var ProjectTagUserIdQuery = '';
	var ProjectTagTypeQuery = '';
	var ProjectTagNameQuery = '';
	var ProjectTagCompanyIdQuery = '';
	var ProjectTagColourQuery = '';
	try {

		if (ProjectTagUserId != undefined) { ProjectTagUserIdQuery = '"ProjectTagUserId": "' + ProjectTagUserId + '"'; }
		if (ProjectTagType != undefined) { ProjectTagTypeQuery = ',"ProjectTagType": "' + ProjectTagType + '"'; }
		if (ProjectTagName != undefined) { ProjectTagNameQuery = ',"ProjectTagName": "' + ProjectTagName + '"'; }
		if (ProjectTagCompanyId != undefined) { ProjectTagCompanyIdQuery = ',"ProjectTagCompanyId": "' + ProjectTagCompanyId + '"'; }
		if (ProjectTagColour != undefined) { ProjectTagColourQuery = ',"ProjectTagColour": "' + ProjectTagColour + '"'; }

		if (checkExistance != false) {
			var ProjectTagUpdatedDate = datetime.toISOString();
			var DateQuery = ',"ProjectTagUpdatedDate": "' + ProjectTagUpdatedDate + '"';

		} else {
			var ProjectTagUpdatedDate = datetime.toISOString();
			var ProjectTagCreatedDate = datetime.toISOString();
			var DateQuery = ',"ProjectTagCreatedDate": "' + ProjectTagCreatedDate + '","ProjectTagUpdatedDate": "' + ProjectTagUpdatedDate + '"';
		}

		var FQuery = ProjectTagUserIdQuery + ProjectTagTypeQuery + ProjectTagNameQuery + ProjectTagCompanyIdQuery + ProjectTagColourQuery + DateQuery;
		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}



		//var FinalQuery = '{"ProjectTagUserId": "' + ProjectTagUserId + '","ProjectTagType": "' + ProjectTagType + '","ProjectTagName": "' + ProjectTagName + '","ProjectTagCompanyId": "' + ProjectTagCompanyId + '","ProjectTagColour": "' + ProjectTagColour + '"' + DateQuery + '}';
		// console.log("FinalQuery " + FinalQuery);
		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
