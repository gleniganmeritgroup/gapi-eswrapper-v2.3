'use strict';
/******************************************************************************
Function: getQueryByProjectFollows
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Project Follow. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByProjectFollows = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;

	var size = requireQuery.query.Size;
	if ((userIDValues == undefined)) {
		callback({ Error: "Parameter userIDValues  is Missing" }, null);
	}
	else {

		try {
			var ProjectFollow = '{"bool": {"must":[{"term":{ "ProjectFollowUserId" : "' + userIDValues + '"}}] }}';

			if(size != undefined && exportedRequiredModules.elasticPageSize >= size){
				var FinalQuery = '{"query": ' + ProjectFollow +',"size": '+ size +'}';
				callback(null, FinalQuery);
			}
			else if(size != undefined && exportedRequiredModules.elasticPageSize < size){
				callback({Error : "Page size is exceeded the limit."}, null);
			}else{
				var FinalQuery = '{"query": ' + ProjectFollow +',"size": '+ exportedRequiredModules.elasticPageSize +'}';
				callback(null, FinalQuery);
			}
		} catch (err) {
			callback(err, null);
		}
	}
}

/******************************************************************************
Function: getQueryByProjectFollowsByProjectId
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Project Follow by Project Id. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByProjectFollowsByProjectId = function (requireQuery, callback) {
		var userIDValues = requireQuery.query.UserId;
	
		var req = JSON.parse(requireQuery.body);
		var ProjectIds = req.ProjectId;
		var projectIdCombined='';
		ProjectIds.split(',').forEach(function (ProjectId, Idindex, Idarray) {
			
			if(Idindex !=0){

				projectIdCombined=projectIdCombined+',"'+ProjectId+'"';
			}else{
				projectIdCombined='"'+ProjectId+'"';
			}
		});
		if ((userIDValues == undefined)) {
			callback({ Error: "Parameter userIDValues  is Missing" }, null);
		}
		else {
			try {
				var ProjectFollow = '{"bool": {"filter": [{"has_parent": {"type": "project","query": {"bool":{"must" : [{"terms" : {"ProjectId" : ['+projectIdCombined+']}}]}}}}]}}';
				var FinalQuery = '{"query": ' + ProjectFollow + '}';
				//console.log(FinalQuery);
				callback(null, FinalQuery);
			} 
			catch (err) {
			//	console.log(err);
				callback(err, null);
			}			
		}
}
/******************************************************************************
Function: postQueryByProjectFollow
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for creating and updating project follows. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.postQueryByProjectFollow = function (requireQuery, checkExistance, callback) {


	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var ProjectFollowUserId = requireQuery.query.UserId;
	var ProjectFollowProjectId = req.ProjectId;
	var ProjectFollowNotificationType = req.ProjectFollowNotificationType;
	var ProjectfollowRoleTypes = req.RoleTypes;
	var ProjectFollowPlanningStages = req.ProjectFollowPlanningStages;
	var ProjectFollowContractStages = req.ProjectFollowContractStages;
	var ProjectFollowNewBidderAdded = req.NewBidderAdded;
	var ProjectFollowAlertSent = req.ProjectFollowAlertSent;
	var ProjectFollowAlertNote = req.ProjectFollowAlertNote;
	var ProjectFollowAlertDate = req.ProjectFollowAlertDate;

	var ProjectFollowUserIdQuery = ''; var ProjectFollowNotificationTypeQuery = ''; var ProjectfollowRoleTypesQuery = ''; var ProjectFollowPlanningStagesQuery = ''; var ProjectFollowContractStagesQuery = ''; var ProjectFollowNewBidderAddedQuery = '';
	var ProjectFollowAlertSentQuery = '';var ProjectFollowAlertNoteQuery = '';var ProjectFollowAlertDateQuery = '';

	try {

		if (ProjectFollowUserId != undefined) { ProjectFollowUserIdQuery = '"ProjectFollowUserId": "' + ProjectFollowUserId + '"'; }

		if (ProjectFollowNotificationType != undefined) { ProjectFollowNotificationTypeQuery = ',"ProjectFollowNotificationType": "' + ProjectFollowNotificationType + '"'; }

		if (ProjectfollowRoleTypes != undefined) { ProjectfollowRoleTypesQuery = ',"RoleTypes": ' + JSON.stringify(ProjectfollowRoleTypes); }

		if (ProjectFollowPlanningStages != undefined) { ProjectFollowPlanningStagesQuery = ',"ProjectFollowPlanningStages": ' + JSON.stringify(ProjectFollowPlanningStages); }

		if (ProjectFollowContractStages != undefined) { ProjectFollowContractStagesQuery = ',"ProjectFollowContractStages": ' + JSON.stringify(ProjectFollowContractStages); }

		if (ProjectFollowNewBidderAdded != undefined) { ProjectFollowNewBidderAddedQuery = ',"NewBidderAdded": ' + JSON.stringify(ProjectFollowNewBidderAdded); }
		if (ProjectFollowAlertSent != undefined) { ProjectFollowAlertSentQuery = ',"ProjectFollowAlertSent": ' + JSON.stringify(ProjectFollowAlertSent); }
		if (ProjectFollowAlertNote != undefined) { ProjectFollowAlertNoteQuery = ',"ProjectFollowAlertNote": ' + JSON.stringify(ProjectFollowAlertNote); }
		if (ProjectFollowAlertDate != undefined) { ProjectFollowAlertDateQuery = ',"ProjectFollowAlertDate": ' + JSON.stringify(ProjectFollowAlertDate); }
		if (checkExistance != true) {
			var ProjectFollowCreatedDate = datetime.toISOString();
			var DateQuery = ',"ProjectFollowCreatedDate": "' + ProjectFollowCreatedDate + '"';
			DateQuery = DateQuery + ',"LastProcessedDateTime": "' + ProjectFollowCreatedDate + '"';
		} else {

			var ProjectFollowUpdatedDate = datetime.toISOString();
			var DateQuery = ',"ProjectFollowUpdatedDate": "' + ProjectFollowUpdatedDate + '"';
			DateQuery = DateQuery + ',"LastProcessedDateTime": "' + ProjectFollowUpdatedDate + '"';


		}


		var FQuery = ProjectFollowUserIdQuery + ProjectFollowNotificationTypeQuery + ProjectfollowRoleTypesQuery + ProjectFollowPlanningStagesQuery + ProjectFollowContractStagesQuery + ProjectFollowNewBidderAddedQuery + ProjectFollowAlertSentQuery+ProjectFollowAlertDateQuery+ProjectFollowAlertNoteQuery + DateQuery;

		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}



		//var FinalQuery = '{"ProjectFollowUserId": "' + ProjectFollowUserId + '","ProjectFollowType": "' + ProjectFollowType + '","ProjectFollowName": "' + ProjectFollowName + '","ProjectFollowCompanyId": "' + ProjectFollowCompanyId + '","ProjectFollowColour": "' + ProjectFollowColour + '"' + DateQuery + '}';
		//		console.log("FinalQuery " + FinalQuery);
		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
