'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (TaskQueue.js) is used form a query for given UserId.
********************************************************************************************/
/*******************************************************************************************/

// addTask function Used to add new task 
/******************************************************************************
Function: exportTaskQueue
Argument: requireQuery, callback
Usage:
	1.Get Export Task Queue.
*******************************************************************************/
exports.exportTaskQueue = function (requireQuery, callback) {

    var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
    // console.log("beSpokeQuery",requireQuery)
    var userID = requireQuery.query.UserId;
    var exportType = requireQuery.query.ExportType;
    var AccountId = requireQuery.AccountId;
    var postJsonData='';
    try{
        postJsonData = JSON.parse(requireQuery.body);
        if (exportType == undefined){
            callback({"Error" : "Parameter ExportType is missing."}, null)
        }else if((exportType != undefined) && (exportType != 'Pdf' && exportType != 'Excel')){
            callback({"Error" : "Parameter ExportType is invalid."}, null)
        }else if(exportType == 'Excel' && postJsonData.pdfOptions != undefined){
            callback({"Error" : "Input and ExportType is mismatched."}, null)
        }else{
            
            
            // console.log("postQueueActionObj",postQueueActionObj)
            var currentTime = exportedRequiredModules.moment().format('YYYYMMDDHHmmss');
            var outputData = {}
            outputData.id = userID.toLowerCase()+currentTime;
            outputData.salesForceUserId = userID;
            outputData.salesForceAccountId = AccountId;
            outputData.status = "pending";
            outputData.emailStatus = "pending";
            outputData.type = postJsonData.type;
            outputData.output =  postJsonData.output;
            outputData.sortOrder = postJsonData.sortOrder;
            outputData.type = postJsonData.type;
            outputData.output =  postJsonData.output;
            outputData.sortOrder = postJsonData.sortOrder;
            if(postJsonData.exportLabel != '' && postJsonData.exportLabel != undefined){
                outputData.exportLabel = postJsonData.exportLabel;
            }else{
                outputData.exportLabel = 's';
            }

            if(exportType == 'Pdf'){
                outputData.pdfOptions = postJsonData.pdfOptions;
            }
            
            if (postJsonData.projectIds != undefined){
                outputData.projectIds = postJsonData.projectIds;
            }else if(postJsonData.companyIds != undefined){
            outputData.companyIds = postJsonData.companyIds;
            }else if(postJsonData.contactIds != undefined){
                outputData.contactIds = postJsonData.contactIds;
            }
            
            // console.log("outputData",outputData)
            // var outputDataList = []
            var outputDataList = []
            outputDataList.push(outputData)
            callback(null, outputDataList);
        }
    }catch(err){ 
        // console.log("err",err)
        callback({"Error" : "Exception on Task API post content"}, null)
    }
}
