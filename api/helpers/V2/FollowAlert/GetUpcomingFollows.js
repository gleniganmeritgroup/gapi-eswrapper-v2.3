'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetUpcomingFollows.js) is used form a query for given UserId.
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getCount
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for getting count of total follow alerts. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getCount = function (requireQuery, callback) { 
   var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
   // console.log("beSpokeQuery",requireQuery)
   var userID = requireQuery.query.UserId;    
   if ((userID == undefined)) {
		callback({Error:"Parameter UserId Values is Missing"},null);
	}
	else{
        try{
            var FinalQuery = '{"query":{"bool":{"must":[{"range":{"dateSent":{"gte":"now-2w/d","lte":"now"}}},{"terms":{"userId":["'+userID+'"]}}]}},"sort":[{"dateSent":{"order":"desc"}}],"aggs":{"by_type":{"terms":{"field":"type"},"aggs":{"by_day":{"date_histogram":{"field":"dateSent","interval":"day","format":"yyyy-MM-dd"}}}}},"size": 0 }';
            callback(null, FinalQuery);
        }
        catch (err) {
            callback(err, null);
        }         
    }    
}
/******************************************************************************
Function: getFollowAlertTypeDate
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for getting follow alerts by type and date. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getFollowAlertTypeDate = function (requireQuery, callback) {

    var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

    var userID = requireQuery.query.UserId;
    var DateSelected = requireQuery.query.Date;
    var Type = requireQuery.query.Type;
    var size = requireQuery.query.Size;
    var typearr = ["Project","Contact","Office"];

    if ((userID == undefined)) {
        callback({ Error: "Parameter UserId is Missing" }, null);
    }
    else if (DateSelected == undefined) {
        callback({ Error: "Parameter Date is Missing" }, null);
    }
    else if((DateSelected != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(DateSelected) == null)){
		callback({Error:"Parameter Date value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
    else if (Type == undefined) {
        callback({ Error: "Parameter Type is Missing" }, null);
    }
    else if (Type != undefined && typearr.indexOf(Type) == -1) {
        callback({ Error: "Parameter Type value is invalid." }, null);
    }
    else if ((userID != undefined && DateSelected != undefined && Type != undefined)) {        
        try {
            if(size != undefined && exportedRequiredModules.elasticPageSize >= size){
                var FromDateTime = exportedRequiredModules.moment(DateSelected, 'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00");
                var ToDateTime = exportedRequiredModules.moment(DateSelected, 'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59");   
                var FinalQuery = '{"query":{ "bool":{"must":[{"range":{"dateSent": {"gte":"'+FromDateTime+'", "lte": "'+ToDateTime+'"}}},{"terms":{"userId":["'+userID+'"]}},{"terms":{ "type" : ["'+Type+'"]}}]}}'+',"size": '+ size +'}';
                callback(null, FinalQuery);
            }
            else if(size != undefined && exportedRequiredModules.elasticPageSize < size){
                callback({Error : "Page size is exceeded the limit."}, null);
            }else{
                var FromDateTime = exportedRequiredModules.moment(DateSelected, 'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00");
                var ToDateTime = exportedRequiredModules.moment(DateSelected, 'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59");   
                var FinalQuery = '{"query":{ "bool":{"must":[{"range":{"dateSent": {"gte":"'+FromDateTime+'", "lte": "'+ToDateTime+'"}}},{"terms":{"userId":["'+userID+'"]}},{"terms":{ "type" : ["'+Type+'"]}}]}}'+',"size": '+ exportedRequiredModules.elasticPageSize +'}';
                callback(null, FinalQuery);
            }
        } catch (err) {
            callback(err, null);
        }        
    }
}