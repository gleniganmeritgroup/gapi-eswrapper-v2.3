'use strict';
/*******************************************************************************************/
/********************************************************************************************
This file (EmailTrigger.js) used to trigger an email to process owner
********************************************************************************************/
/*******************************************************************************************/



// Generate test SMTP service account from ethereal.email
// Only needed if you don't have a real mail account for testing
exports.callEmailAlert = function (accountName, page, callback) {
    var exportedRequiredModules = require('../../../config.js').requiredModulesExport;
   
    exportedRequiredModules.nodeMailerObj.createTestAccount((err, account) => {
        
        var html='<p>Hi,</p>\
        <p>The Account Name -&nbsp;<strong>'+accountName+'</strong> is trying to accessing the page limit of&nbsp;<strong>'+page+'.</strong></p>'
        // console.log("html"+html);
        var transporter;
        if(exportedRequiredModules.credentialsObj.Email.userName.length == 0){
            transporter = exportedRequiredModules.nodeMailerObj.createTransport({
                host: exportedRequiredModules.credentialsObj.Email.host,
                port: exportedRequiredModules.credentialsObj.Email.port,
                secure: exportedRequiredModules.credentialsObj.Email.ssl, // true for 465, false for other ports
                tls: { rejectUnauthorized: false }
            });
        }else{
            transporter = exportedRequiredModules.nodeMailerObj.createTransport({
                host: exportedRequiredModules.credentialsObj.Email.host,
                port: exportedRequiredModules.credentialsObj.Email.port,
                secure: exportedRequiredModules.credentialsObj.Email.ssl, // true for 465, false for other ports
                auth: {
                    user: exportedRequiredModules.credentialsObj.Email.userName, // generated ethereal user
                    pass: exportedRequiredModules.credentialsObj.Email.password // generated ethereal password
                },
                tls: { rejectUnauthorized: false }
            });
        }
        
        var mailOptions = {
            from: exportedRequiredModules.credentialsObj.Email.from, // sender address
            // to: 'bar@example.com, baz@example.com', // list of receivers
            to: exportedRequiredModules.credentialsObj.Email.to, // list of receivers
            subject: 'Alert for Page Limit -' + accountName, // Subject line
            html: html // html body
        };
        // send mail with defined transport object
        if(html != ''){
            
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    return console.log(error);
                }
                // console.log('Message sent: %s', info.messageId);
            });
        }
        
    });
};