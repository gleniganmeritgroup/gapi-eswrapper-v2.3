'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (SubscriptionValidator.js) is using to validate the Input query with subscription
Json. It has two level of validation, One is authentication and another
one is authorization.

Function SubscriptionAuthentication ==> Validate SubscriptionExists,HasAPI,SubscriptionDate,
HasSmall.

Function SubscriptionAuthorization ==> Validate ProjectSectors and ProjectRegions.

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: SubscriptionAuthentication
Argument:subscriptionJSON
Usage:
	1. Authenticates Subscription based on the data in Mongodb. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.SubscriptionAuthentication = function (subscriptionJson) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	
	var subscriptionJsonObj = JSON.parse(subscriptionJson);
	var currentDate = exportedRequiredModules.dateTime.create().format('Y-m-d');
	
	var currentDate = exportedRequiredModules.moment();
	currentDate = exportedRequiredModules.moment(currentDate).format("YYYY-MM-DD")
	var SubscriptionStartDate = exportedRequiredModules.moment(subscriptionJsonObj.Subscription.SubscriptionStartDate, "YYYY-MM-DD").format("YYYY-MM-DD");
	var SubscriptionEndDate = exportedRequiredModules.moment(subscriptionJsonObj.Subscription.SubscriptionEndDate, "YYYY-MM-DD").format("YYYY-MM-DD");
	if( subscriptionJsonObj.BaseURL.includes('_hi/') > 0){
		if (subscriptionJsonObj.Subscription.HasHIAPI != true)
		{
			return {"Error" : "API is not subscribed"};
		}
	}else if (subscriptionJsonObj.Subscription.HasAPI != true)
	{
		return {"Error" : "API is not subscribed"};
	}
	
	if(subscriptionJsonObj.SubscriptionExists.toString() != "true")
	{
		return {"Error" : "Not a Valid Subscription"};
	}
	else if ((currentDate < SubscriptionStartDate) || (currentDate > SubscriptionEndDate))
	{
		
		return {"Error" : "SubscriptionEndDate is expired."};
	}
	else{
		
		return {"Success" : "Valid Subscription"};
	}
	
}