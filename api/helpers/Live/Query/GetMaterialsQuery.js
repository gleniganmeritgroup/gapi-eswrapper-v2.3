'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetMaterialsQuery.js) is used form a query for given Materials .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getUniqueValues
Argument:arrayValues
Return: Array
Usage:
	1. Returns only unique values in an array.
*******************************************************************************/
function getUniqueValues(arrayValues) {
    var uniqueValues = arrayValues.filter(function(elem, index, self) {
        return index === self.indexOf(elem);
    });
    return uniqueValues;
}

/******************************************************************************
Function: getQueryByMaterials
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for materials. 
	2. 'getSubscriptionQueryObj' returns location and sector based on subscription json.
	3. 'getMaterialMetadata' returns metadata of material list.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByMaterials = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var materialParentsValues = requireQuery.query.MaterialParents;
	var materialValues = requireQuery.query.Materials;
	
	if ((materialParentsValues == undefined) && (materialValues == undefined)) {
		callback({Error:"Parameter MaterialParents/Materials Values is Missing"}, null);
	}
	else{
		
		var materialParentList = [];
		var materialList = [];
		if (materialParentsValues != undefined){
			materialParentsValues.split(',').forEach(function (material){
				materialParentList.push(material.toLowerCase());
		});}
		if (materialValues != undefined){
			materialValues.split(',').forEach(function (material){
				materialList.push(material.toLowerCase());
		});}
		
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			if(err){
				callback(err, null);
			}else{
				try{	
					getMaterialMetadata(exportedRequiredModules,materialParentList, materialList, function(err, projectMaterialParents, ProjectMaterialsList){
					
					if (err){
						callback(err,null);
					}
					else{
						try{
							
							var queryBuildMaterialParent={};
							var queryBuildMaterialNames={};
							var queryFieldMaterialParent='{"terms": %s}';
							var queryFieldMaterialNames='{"terms": %s}';

							queryBuildMaterialParent.MaterialParents=projectMaterialParents;
							queryBuildMaterialNames.ProjectMaterials=ProjectMaterialsList;
							
							var materialQueryFormation, materialQueryConcat;
						
							if (projectMaterialParents.length > 0 ){ materialQueryConcat = exportedRequiredModules.convertComma2Array(queryFieldMaterialParent, JSON.stringify(queryBuildMaterialParent)); }
							if (ProjectMaterialsList.length > 0 ){ materialQueryConcat = materialQueryConcat +', '+exportedRequiredModules.convertComma2Array(queryFieldMaterialNames, JSON.stringify(queryBuildMaterialNames)); }
							
							materialQueryConcat = materialQueryConcat.replace(/^\s*undefined\s*\,/, "");
							materialQueryConcat = materialQueryConcat.replace(/^\s*\,/, "");
							materialQueryConcat = materialQueryConcat.replace(/\,\s*$/, "");
							materialQueryFormation= '{"bool": {"should": [ '+materialQueryConcat+' ]}}'; 

							
							var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
							var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
							
							var queryConcat=projectHistories
							if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
							if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
							
							if (materialQueryFormation != '' ){ queryConcat = queryConcat +', '+materialQueryFormation; }
							var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
							// console.log("FinalQuery"+FinalQuery);
							callback(null, FinalQuery);
						}catch (err){
							callback(err, null);
						}
					}
				});
				}catch (err){
					callback(err, null);
				}
			}
		});
	}
}
/******************************************************************************
Function: getMaterialMetadata
Argument:exportedRequiredModules, materialParentList, materialList
Return:  get materials.
Usage:
	1. Extracts material from metadata url.
*******************************************************************************/
function getMaterialMetadata(exportedRequiredModules, materialParentList, materialList, callback){
	materialParentList = materialParentList.map(function(x){ return x.toLowerCase()})
	materialList = materialList.map(function(x){ return x.toLowerCase()})
	var MaterialParents = [];
	var ProjectMaterials = [];
	var CheckMaterialParentsValues = [];
	var CheckProjectMaterialsValues = [];
	try{
	 	var urlRequestMaterials = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/materials';
		
		exportedRequiredModules.request(urlRequestMaterials, function (error, response,materialContent) {
			if( materialContent != undefined){
				var projectMaterialData = JSON.parse(materialContent);
				projectMaterialData._source.Materials.forEach(function(materialData) {
					if (materialParentList.length > 0){
						if (materialParentList.includes(materialData.MaterialGroupName.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							CheckMaterialParentsValues.push(materialData.MaterialGroupName.toLowerCase());
							MaterialParents.push(materialData.MaterialGroupName.toLowerCase());
						}
					}
					if (materialList.length > 0){
						materialData.MaterialNames.forEach(function(materialName){
							if ((materialList.includes(materialName.toLowerCase().replace(', ',' and ').replace('&','and')) == true) && ((materialParentList.includes(materialData.MaterialGroupName.toLowerCase().replace(', ',' and ').replace('&','and')) == true) || materialParentList.length == 0)){
								CheckProjectMaterialsValues.push(materialName.toLowerCase());
								ProjectMaterials.push(materialData.MaterialGroupName.toLowerCase()+'#'+materialName.toLowerCase())
								if (MaterialParents.includes(materialData.MaterialGroupName.toLowerCase()) == true){ MaterialParents.splice(MaterialParents.indexOf(materialData.MaterialGroupName.toLowerCase()),1); }
							}
						});
					}
				});
				CheckProjectMaterialsValues = CheckProjectMaterialsValues.map(function(x){ return x.toLowerCase()});
				
				var missedMaterialList = exportedRequiredModules.arrayDiff(materialList, CheckProjectMaterialsValues);
				if(missedMaterialList.length > 0){
					projectMaterialData._source.Materials.forEach(function(materialData) {
						materialData.MaterialNames.forEach(function(materialName){
							if (missedMaterialList.includes(materialName.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
								CheckProjectMaterialsValues.push(materialName.toLowerCase());
								ProjectMaterials.push(materialData.MaterialGroupName.toLowerCase()+'#'+materialName.toLowerCase())
								if (MaterialParents.includes(materialData.MaterialGroupName.toLowerCase()) == true){ MaterialParents.splice(MaterialParents.indexOf(materialData.MaterialGroupName.toLowerCase()),1); }
							}
						});
					});
				}
				
				CheckMaterialParentsValues = getUniqueValues(CheckMaterialParentsValues);
				CheckProjectMaterialsValues = getUniqueValues(CheckProjectMaterialsValues);
				if((CheckMaterialParentsValues.length != materialParentList.length) && (materialParentList.length > 0)){
					callback({Error: 'Invalid MaterialParents Values given.'}, null, null);
				}
				else if((CheckProjectMaterialsValues.length != materialList.length) && (materialList.length > 0)){
					callback({Error: 'Invalid Materials Values given.'}, null, null);
				}
				else if(((CheckMaterialParentsValues.length == materialParentList.length) && (materialParentList.length > 0)) || ((CheckProjectMaterialsValues.length == materialList.length) && (materialList.length > 0))){
					callback(null, MaterialParents, ProjectMaterials);
				}
				
			}
			else{
				callback({Error: 'Material metadata URL response failed.'},null);
			}
		});
	}catch(err){
		callback({Error: 'Material metadata URL response failed.'},null);
	}
}