'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetProjectQuery.js) is used form a query for given Project .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryByProject
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for project. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByProject = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
		if(err){
			callback(err, null);
		}else{
			try{
				var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
				var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
				var queryConcat=projectHistories
				
				if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
				if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
				
				var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
				callback(null, FinalQuery);

			}catch (err){
				callback(err, null);
			}
		}
	});
}
/******************************************************************************
Function: getQueryByUpdatedProject
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for updated project. 
	2. 'getSubscriptionQueryObj' returns location and sector based on subscription json.
	3. 'getProjectHistoryMetadata' returns metadata of project event types.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByUpdatedProject = function (subscriptionJSON, requireQuery, callback) {
	// console.log("historyEventValues");
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	var historyEventValues = requireQuery.query.ProjectHistoryEvent;
	var historyEventList = [];
	// console.log("historyEventValues"+historyEventValues);
	if (historyEventValues != undefined){
		historyEventValues.split(',').forEach(function(event){
			historyEventList.push(event.toLowerCase());
		});  
	}
	var radiusValues = requireQuery.query.Radius;
	var latValues = requireQuery.query.Lat;
	var longValues = requireQuery.query.Lon;
	
	var unit,radiusSearch;
	
	if (radiusValues != undefined){
		var unitRegex = /[\d]+((?:mi|km|m))$/g;
		unit = unitRegex.exec(radiusValues.trim());
	}
	
	if ((radiusValues == undefined) && (latValues == undefined) && (longValues == undefined)) {
        radiusSearch = false;
    }
    
	if (((radiusValues == undefined) || (latValues == undefined) || (longValues == undefined)) && (radiusSearch != false)) {
		callback({Error:"Parameter is Missing, Required Parameters is Radius,Lat,Long"}, null);
	}
	else if((unit == null)  && (radiusSearch != false)){
		callback({Error:"Radius unit must be mi or km."}, null);
	}else{
	
		var jsonObject = JSON.parse(subscriptionJSON);
		
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			if(err){
				callback(err, null);
			}else{
				
					
				getProjectHistoryMetadata(exportedRequiredModules, historyEventList, function(err, historyEventDataList){
				if (err){
					callback({Error: 'Exception on Project History Event Metadata'}, null);
				}
				else{
					try{
						
						var projectHistories = getUpdatedProjectQuery(exportedRequiredModules,requireQuery, historyEventDataList);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						if (radiusSearch != false){
							var radiusQuery = '{"bool": { "should": [ { "geo_distance": { "distance": "'+radiusValues+'", "ProjectLocation": { "lat": '+latValues+', "lon": '+longValues+' } } } ] } }';	
							queryConcat = queryConcat +', '+radiusQuery
						}
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						callback(null, FinalQuery);
					}catch (err){
						callback(err, null);
					}
				}
			});
			
		}
	});
}
}
/******************************************************************************
Function: getQueryByNewProject
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for new project. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByNewProject = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var radiusValues = requireQuery.query.Radius;
	var latValues = requireQuery.query.Lat;
	var longValues = requireQuery.query.Lon;
	
	var unit,radiusSearch;
	
	if (radiusValues != undefined){
		var unitRegex = /[\d]+((?:mi|km|m))$/g;
		unit = unitRegex.exec(radiusValues.trim());
	}
	
	if ((radiusValues == undefined) && (latValues == undefined) && (longValues == undefined)) {
        radiusSearch = false;
    }
    
	if (((radiusValues == undefined) || (latValues == undefined) || (longValues == undefined)) && (radiusSearch != false)) {
		callback({Error:"Parameter is Missing, Required Parameters is Radius,Lat,Long"}, null);
	}
	else if((unit == null)  && (radiusSearch != false)){
		callback({Error:"Radius unit must be mi or km."}, null);
	}else{
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			if(err){
				callback(err, null);
			}else{
				try{
					var projectHistories = getNewProjectQuery(exportedRequiredModules,requireQuery);
					var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
					
					var queryConcat=projectHistories
					if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
					if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
					if (radiusSearch != false){
						var radiusQuery = '{"bool": { "should": [ { "geo_distance": { "distance": "'+radiusValues+'", "ProjectLocation": { "lat": '+latValues+', "lon": '+longValues+' } } } ] } }';	
						queryConcat = queryConcat +', '+radiusQuery
					}
					var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
					callback(null, FinalQuery);

				}catch (err){
					callback(err, null);
				}
			}
		});
	}
}
/******************************************************************************
Function: getNewProjectQuery
Argument:exportedRequiredModules, require
Return: return a elastic search query .
Usage:
	1. forms a elastic query for new project from range From and To. 
*******************************************************************************/
function getNewProjectQuery (exportedRequiredModules,require)
{
	var daysValues = require.query.TimeRange;
	// var addSizeQuery='';
	var hasSmall = require.HasSmall;
	// if(hasSmall == false){
	// 	addSizeQuery = '{"bool":{"must_not":[{"terms":{"ProjectSize":["Small"]}}]}}';
	// }
	var addSizeQuery = '{"bool":{"must_not":[{"terms":{"ProjectSize":["Small"]}}]}}';
	if (daysValues != undefined) {
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		
		var days = '';
		
		if (daysValues.includes('From') != true ){ 
			
			try{
				var daysRegex = /^([\d]+)([\w])$/g;
				var daysCountPattern = daysRegex.exec(daysValues);
				var daysCount = daysCountPattern[1];
				var daysPattern = daysCountPattern[2];
			}catch(err)
			{ throw ({Error : 'Parameter "TimeRange" value is Invalid.'}) }
			
			if (daysPattern.toLocaleLowerCase() == 'm'){
				daysCount = daysCount * 30;
			}
			else if (daysPattern.toLocaleLowerCase() == 'y') {
				daysCount = daysCount * 365;
			}
			else if (daysPattern.toLocaleLowerCase() == 'd') {
				daysCount = daysCount
			}
			else{
				throw ({Error : 'Parameter "TimeRange" value is Invalid.'})
			}
			
			var previousDate = exportedRequiredModules.moment().subtract(daysCount, 'days').format("YYYY-MM-DDT00:00:00");
			var projectHistories ='{ "nested": { "path": "ProjectHistories", "query": { "bool": { "filter": [ { "terms": { "ProjectHistories.ProjectHistoryEvent": [ "NewProject" ] } },{ "range": { "ProjectHistories.ProjectHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } },{ "bool": { "should": [ { "term": { "IsArchived": "false" } } ] } }';	
			if(addSizeQuery != ''){
				var projectHistories =  projectHistories +','+addSizeQuery;
			}
			return projectHistories;
		}
		else{
			try{
			var fromToRegex = /From\:([0-9\-]+)\s*and\s*To\:([0-9\-]+)/g;

			var fromToDate = fromToRegex.exec(daysValues);
			
			var currentDate = exportedRequiredModules.moment(fromToDate[2],'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59");
			var previousDate = exportedRequiredModules.moment(fromToDate[1],'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00");
			}catch(err)
			{ throw ({Error : 'Parameter "TimeRange" value is Invalid.'}) }

			var projectHistories ='{ "nested": { "path": "ProjectHistories", "query": { "bool": { "filter": [ { "terms": { "ProjectHistories.ProjectHistoryEvent": [ "NewProject" ] } }, { "range": { "ProjectHistories.ProjectHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } },{ "bool": { "should": [ { "term": { "IsArchived": "false" } } ] } }';	
			if(addSizeQuery != ''){
				var projectHistories =  projectHistories +','+addSizeQuery;
			}
			return projectHistories;
		}
	}
	else{
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		var previousDate = exportedRequiredModules.moment().subtract(1, 'days').format("YYYY-MM-DDT00:00:00");
		var projectHistories ='{ "nested": { "path": "ProjectHistories", "query": { "bool": { "filter": [ { "terms": { "ProjectHistories.ProjectHistoryEvent": [ "NewProject" ] } }, { "range": { "ProjectHistories.ProjectHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } },{ "bool": { "should": [ { "term": { "IsArchived": "false" } } ] } }';	
		if(addSizeQuery != ''){
			var projectHistories =  projectHistories +','+addSizeQuery;
		}
		return projectHistories;
	}
	
}

/******************************************************************************
Function: getUpdatedProjectQuery
Argument:exportedRequiredModules, require,historyEventDataList
Return: return a elastic search query .
Usage:
	1. Forms a elastic query for new project for parameters range From and To. 
*******************************************************************************/
function getUpdatedProjectQuery (exportedRequiredModules, require, historyEventDataList)
{
	var daysValues = require.query.TimeRange;
	var hasSmall = require.HasSmall;
	// var addSizeQuery='';
	// if(hasSmall == false){
	// 	addSizeQuery = '{"bool":{"must_not":[{"terms":{"ProjectSize":["Small"]}}]}}';
	// }
	var addSizeQuery = '{"bool":{"must_not":[{"terms":{"ProjectSize":["Small"]}}]}}';
	var queryFieldHistoryEvent = '{"terms": %s}';
	var queryBuildHistoryEvent = {};
	queryBuildHistoryEvent["ProjectHistories.ProjectHistoryEvent"]=historyEventDataList;
	var historyEventQuery = exportedRequiredModules.convertComma2Array(queryFieldHistoryEvent, JSON.stringify(queryBuildHistoryEvent));
	
	if (daysValues != undefined) {
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		
		var days = '';
		
		if (daysValues.includes('From') != true ){ 
			
			try{
				var daysRegex = /^([\d]+)([\w])$/g;
				var daysCountPattern = daysRegex.exec(daysValues);
				var daysCount = daysCountPattern[1];
				var daysPattern = daysCountPattern[2];
			}catch(err)
			{ throw ({Error : 'Parameter "TimeRange" value is Invalid.'}) }
			
			if (daysPattern.toLocaleLowerCase() == 'm'){
				daysCount = daysCount * 30;
			}
			else if (daysPattern.toLocaleLowerCase() == 'y') {
				daysCount = daysCount * 365;
			}
			else if (daysPattern.toLocaleLowerCase() == 'd') {
				daysCount = daysCount
			}
			else{
				throw ({Error : 'Parameter "TimeRange" value is Invalid.'})
			}
			
			var previousDate = exportedRequiredModules.moment().subtract(daysCount, 'days').format("YYYY-MM-DDT00:00:00");
			var projectHistories ='{ "nested": { "path": "ProjectHistories", "query": { "bool": { "filter": [ '+historyEventQuery+',{ "range": { "ProjectHistories.ProjectHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } },{ "bool": { "should": [ { "term": { "IsArchived": "false" } } ] } }';	
			if(addSizeQuery != ''){
				var projectHistories =  projectHistories +','+addSizeQuery;
			}
			return projectHistories;
		}
		else{
			try{
			var fromToRegex = /From\:([0-9\-]+)\s*and\s*To\:([0-9\-]+)/g;

			var fromToDate = fromToRegex.exec(daysValues);
			
			var currentDate = exportedRequiredModules.moment(fromToDate[2],'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59");
			var previousDate = exportedRequiredModules.moment(fromToDate[1],'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00");
			}catch(err)
			{ throw ({Error : 'Parameter "TimeRange" value is Invalid.'}) }

			var projectHistories ='{ "nested": { "path": "ProjectHistories", "query": { "bool": { "filter": [ '+historyEventQuery+', { "range": { "ProjectHistories.ProjectHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } },{ "bool": { "should": [ { "term": { "IsArchived": "false" } } ] } }';	
			if(addSizeQuery != ''){
				var projectHistories =  projectHistories +','+addSizeQuery;
			}
			return projectHistories;
		}
	}
	else{
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		var previousDate = exportedRequiredModules.moment().subtract(1, 'days').format("YYYY-MM-DDT00:00:00");
		var projectHistories ='{ "nested": { "path": "ProjectHistories", "query": { "bool": { "filter": [ '+historyEventQuery+', { "range": { "ProjectHistories.ProjectHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } },{ "bool": { "should": [ { "term": { "IsArchived": "false" } } ] } }';	
		if(addSizeQuery != ''){
			var projectHistories =  projectHistories +','+addSizeQuery;
		}
		return projectHistories;
	}
	
}
/******************************************************************************
Function: getProjectHistoryMetadata
Argument:exportedRequiredModules, projectHistoryEventList
Return:  get project event types.
Usage:
	1. Extracts project event types from metadata url.
*******************************************************************************/
function getProjectHistoryMetadata(exportedRequiredModules, projectHistoryEventList, callback){
	
	var projectHistoryEventDataList = [];
	
	try{
	 	var urlProjectHistory = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/projecteventtypes';
		
		exportedRequiredModules.request(urlProjectHistory, function (error, response,historyContent) {
			if( historyContent != undefined){
				var historyEvent = JSON.parse(historyContent);
				historyEvent._source.ProjectEventTypes.forEach(function(event) {
					if (projectHistoryEventList.length > 0) {
						if ((projectHistoryEventList.includes(event.EventDescription.toLowerCase().replace('&','and')) == true) || (projectHistoryEventList.includes(event.EventCode.toLowerCase().replace('&','and')) == true)){
							projectHistoryEventDataList.push(event.EventCode);
						}
					}
					else{
						projectHistoryEventDataList.push(event.EventCode);
					}
				});
				callback(null, projectHistoryEventDataList);
			}
			else{
				callback({Error: 'Project History metadata URL response failed.'}, null);
			}
		});
	}catch(err){
		callback({Error: 'Project History metadata URL response failed.'}, null);
	}
}