'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetTimeRangeQuery.js) is used to form an elastic query for TimeRange parameter.
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: byTimeRange
Argument:exportedRequiredModules,require
Return: return a elastic search query 
Usage:
	1. Forms a elastic query time range from and to. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.byTimeRange = function (exportedRequiredModules,require)
{
	var daysValues = require.query.TimeRange;
	var hasSmall = require.HasSmall;
	// var addSizeQuery='';
	// if(hasSmall == false){
	// 	addSizeQuery = '{"bool":{"must_not":[{"terms":{"ProjectSize":["Small"]}}]}}';
	// }
	var addSizeQuery = '{"bool":{"must_not":[{"terms":{"ProjectSize":["Small"]}}, { "term": { "IsArchived": "true" } }, { "terms" : { "RecordType" : ["Group","Framework Agreement"]}}]}}'
	if (daysValues != undefined) {
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		
		var days = '';
		
		if (daysValues.includes('From') != true ){ 
			
			try{
				var daysRegex = /^([\d]+)([\w])$/g;
				var daysCountPattern = daysRegex.exec(daysValues);
				var daysCount = daysCountPattern[1];
				var daysPattern = daysCountPattern[2];
			}catch(err)
			{ throw ({Error : 'Parameter "TimeRange" value is Invalid.'}) }
			
			if (daysPattern.toLocaleLowerCase() == 'm'){
				daysCount = daysCount * 30;
			}
			else if (daysPattern.toLocaleLowerCase() == 'y') {
				daysCount = daysCount * 365;
			}
			else if (daysPattern.toLocaleLowerCase() == 'd') {
				daysCount = daysCount
			}
			else{
				throw ({Error : 'Parameter "TimeRange" value is Invalid.'})
			}
			
			var previousDate = exportedRequiredModules.moment().subtract(daysCount, 'days').format("YYYY-MM-DDT00:00:00");
			var projectHistories ='{ "nested": { "path": "ProjectHistories", "query": { "bool": { "filter": [ { "range": { "ProjectHistories.ProjectHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
			if(addSizeQuery != ''){
				var projectHistories =  projectHistories +','+addSizeQuery;
			}
			return projectHistories;
		}
		else{
			try{
				var fromToRegex = /From\s*\:\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*and\s*To\s*\:\s*([\d]{2}\-[\d]{2}\-[\d]{4})/g;

				var fromToDate = fromToRegex.exec(daysValues);
				
				if(fromToDate == null){
					throw ({Error : 'Parameter "TimeRange" value is Invalid. Date format should be \"DD-MM-YYYY\"'});
				}else{
					var currentDate = exportedRequiredModules.moment(fromToDate[2],'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59");
					var previousDate = exportedRequiredModules.moment(fromToDate[1],'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00");
					var projectHistories ='{ "nested": { "path": "ProjectHistories", "query": { "bool": { "filter": [ { "range": { "ProjectHistories.ProjectHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
					if(addSizeQuery != ''){
						var projectHistories =  projectHistories +','+addSizeQuery;
					}
					return projectHistories;
				}
			}catch(err)
			{ 
				throw ({Error : 'Parameter "TimeRange" value is Invalid. Date format should be \"DD-MM-YYYY\"'});
			}
		}
	}
	else{
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		var previousDate = exportedRequiredModules.moment().subtract(1, 'days').format("YYYY-MM-DDT00:00:00");
		var projectHistories ='{ "nested": { "path": "ProjectHistories", "query": { "bool": { "filter": [ { "range": { "ProjectHistories.ProjectHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
		if(addSizeQuery != ''){
			var projectHistories =  projectHistories +','+addSizeQuery;
		}
		return projectHistories;
	}
	
}
/******************************************************************************
Function: byCompanyTimeRange
Argument:exportedRequiredModules,require
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for getting company by time range from and to. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.byCompanyTimeRange = function (exportedRequiredModules,require)
{
	var daysValues = require.query.TimeRange;
	var hasSmall = require.HasSmall;
	var addSizeQuery='';
	
	if (daysValues != undefined) {
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		
		var days = '';
		
		if (daysValues.includes('From') != true ){ 
			
			try{
				var daysRegex = /^([\d]+)([\w])$/g;
				var daysCountPattern = daysRegex.exec(daysValues);
				var daysCount = daysCountPattern[1];
				var daysPattern = daysCountPattern[2];
			}catch(err)
			{ throw ({Error : 'Parameter "TimeRange" value is Invalid.'}) }
			
			if (daysPattern.toLocaleLowerCase() == 'm'){
				daysCount = daysCount * 30;
			}
			else if (daysPattern.toLocaleLowerCase() == 'y') {
				daysCount = daysCount * 365;
			}
			else if (daysPattern.toLocaleLowerCase() == 'd') {
				daysCount = daysCount
			}
			else{
				throw ({Error : 'Parameter "TimeRange" value is Invalid.'})
			}
			
			var previousDate = exportedRequiredModules.moment().subtract(daysCount, 'days').format("YYYY-MM-DDT00:00:00");
			var projectHistories ='{ "nested": { "path": "OfficeHistories", "query": { "bool": { "filter": [ { "range": { "OfficeHistories.OfficeHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } },{"bool": {"must_not": [{"term": {"OfficeWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"OfficeWorkingOnSmallProjects": "true"}},{"term": {"Publish": "false"}}]}}';	
			if(addSizeQuery != ''){
				var projectHistories =  projectHistories +','+addSizeQuery;
			}
			return projectHistories;
		}
		else{
			try{
				var fromToRegex = /From\s*\:\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*and\s*To\s*\:\s*([\d]{2}\-[\d]{2}\-[\d]{4})/g;

				var fromToDate = fromToRegex.exec(daysValues);
				
				if(fromToDate == null){
					throw ({Error : 'Parameter "TimeRange" value is Invalid. Date format should be \"DD-MM-YYYY\"'});
				}else{
					var currentDate = exportedRequiredModules.moment(fromToDate[2],'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59");
					var previousDate = exportedRequiredModules.moment(fromToDate[1],'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00");
					var projectHistories ='{ "nested": { "path": "OfficeHistories", "query": { "bool": { "filter": [ { "range": { "OfficeHistories.OfficeHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } },{"bool": {"must_not": [{"term": {"OfficeWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"OfficeWorkingOnSmallProjects": "true"}},{"term": {"Publish": "false"}}]}}';
					if(addSizeQuery != ''){
						var projectHistories =  projectHistories +','+addSizeQuery;
					}
					return projectHistories;
				}
			}catch(err)
			{ 
				throw ({Error : 'Parameter "TimeRange" value is Invalid. Date format should be \"DD-MM-YYYY\"'});
			}
		}
	}
	else{
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		var previousDate = exportedRequiredModules.moment().subtract(1, 'days').format("YYYY-MM-DDT00:00:00");
		var projectHistories ='{ "nested": { "path": "OfficeHistories", "query": { "bool": { "filter": [ { "range": { "OfficeHistories.OfficeHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } },{"bool": {"must_not": [{"term": {"OfficeWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"OfficeWorkingOnSmallProjects": "true"}},{"term": {"Publish": "false"}}]}}';	
		if(addSizeQuery != ''){
			var projectHistories =  projectHistories +','+addSizeQuery;
		}
		return projectHistories;
	}
	
}

/******************************************************************************
Function: byContactTimeRange
Argument:exportedRequiredModules,require
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for getting contact by time range from and to. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.byContactTimeRange = function (exportedRequiredModules,require)
{
	var daysValues = require.query.TimeRange;
	var hasSmall = require.HasSmall;
	var addSizeQuery='{"bool": {"must_not": [{"term": {"ContactWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"ContactWorkingOnSmallProjects": "true"}}]}}';
	
	if (daysValues != undefined) {
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		
		var days = '';
		
		if (daysValues.includes('From') != true ){ 
			
			try{
				var daysRegex = /^([\d]+)([\w])$/g;
				var daysCountPattern = daysRegex.exec(daysValues);
				var daysCount = daysCountPattern[1];
				var daysPattern = daysCountPattern[2];
			}catch(err)
			{ throw ({Error : 'Parameter "TimeRange" value is Invalid.'}) }
			
			if (daysPattern.toLocaleLowerCase() == 'm'){
				daysCount = daysCount * 30;
			}
			else if (daysPattern.toLocaleLowerCase() == 'y') {
				daysCount = daysCount * 365;
			}
			else if (daysPattern.toLocaleLowerCase() == 'd') {
				daysCount = daysCount
			}
			else{
				throw ({Error : 'Parameter "TimeRange" value is Invalid.'})
			}
			
			var previousDate = exportedRequiredModules.moment().subtract(daysCount, 'days').format("YYYY-MM-DDT00:00:00");
			var projectHistories ='{ "nested": { "path": "OfficeHistories", "query": { "bool": { "filter": [ { "range": { "OfficeHistories.OfficeHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
			if(addSizeQuery != ''){
				var projectHistories =  projectHistories +','+addSizeQuery;
			}
			return projectHistories;
		}
		else{
			try{
				var fromToRegex = /From\s*\:\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*and\s*To\s*\:\s*([\d]{2}\-[\d]{2}\-[\d]{4})/g;

				var fromToDate = fromToRegex.exec(daysValues);
				
				if(fromToDate == null){
					throw ({Error : 'Parameter "TimeRange" value is Invalid. Date format should be \"DD-MM-YYYY\"'});
				}else{
					var currentDate = exportedRequiredModules.moment(fromToDate[2],'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59");
					var previousDate = exportedRequiredModules.moment(fromToDate[1],'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00");
					var projectHistories ='{ "nested": { "path": "OfficeHistories", "query": { "bool": { "filter": [ { "range": { "OfficeHistories.OfficeHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
					if(addSizeQuery != ''){
						var projectHistories =  projectHistories +','+addSizeQuery;
					}
					return projectHistories;
				}
			}catch(err)
			{ 
				throw ({Error : 'Parameter "TimeRange" value is Invalid. Date format should be \"DD-MM-YYYY\"'});
			}
		}
	}
	else{
		var currentDate = exportedRequiredModules.moment().format("YYYY-MM-DDT23:59:59");
		var previousDate = exportedRequiredModules.moment().subtract(1, 'days').format("YYYY-MM-DDT00:00:00");
		var projectHistories ='{ "nested": { "path": "OfficeHistories", "query": { "bool": { "filter": [ { "range": { "OfficeHistories.OfficeHistoryModified": { "gte": "'+previousDate+'", "lte": "'+currentDate+'" } } } ] } } } }';	
		if(addSizeQuery != ''){
			var projectHistories =  projectHistories +','+addSizeQuery;
		}
		return projectHistories;
	}
	
}