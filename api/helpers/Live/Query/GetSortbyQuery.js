'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetSortbyQuery.js) is used to form an elastic query for OrderBy parameter.
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: SortByCondition
Argument:exportedRequiredModules,require
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Sorting by parameter OrderBy. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.SortByCondition = function (exportedRequiredModules,require)
{
	var orderbyValues = require.query.OrderBy;
	
	if (orderbyValues == undefined) {
		var orderby = '"sort": [{"LatestEventDate": {"order": "desc"}}]';
		return orderby;
	}
	else{
		var possibleValues = ['LatestEventDate','Value','StartDate','EndDate','HeadingNotAnalysed','ProjectTown.not_analyzed','Client','LastModified'];
		
		orderbyValues = orderbyValues.replace('LastModified','LatestEventDate');
		if(orderbyValues.split(',')[0].trim() == "Distance"){
			var latValues = require.query.Lat;
			var longValues = require.query.Lon;
			if((latValues == undefined)  || (longValues == undefined)){
				throw({Error: 'Parameter "OrderBy" value Distance is allowed for radius endpoint'});
			}else{
				var radiusSort= '"sort": [{"_geo_distance": {"ProjectLocation": {"lat" : '+latValues+',"lon" : '+longValues+'},"order":"'+orderbyValues.split(',')[1].trim().toLowerCase()+'","unit":"mi","distance_type": "arc"}}]'
				return radiusSort;
			}
			
		}else if (possibleValues.includes(orderbyValues.split(',')[0].trim()) == false){
			throw({Error: 'Parameter "OrderBy" value is Invalid'});
		}else{
			try{
				var orderby = '"sort": [{"'+orderbyValues.split(',')[0].trim()+'": {"order": "'+orderbyValues.split(',')[1].trim().toLowerCase()+'"}}]';
				return orderby;
			}catch(err){
				throw({Error: 'Parameter "OrderBy" value is Invalid'});
			}
		}
		
	}
}