'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetRoleQuery.js) is used form a query for given Role .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryByRoles
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for role. 
	2. 'getSubscriptionQueryObj' returns location and sector based on subscription json.
	3. 'getRolesMetadata' returns metadata of roles.
	4. 'getRoleLocationsMetadata' returns metadata of roles location and towns.
	5. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByRoles = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var roleParents = requireQuery.query.RoleParents;
	var roles = requireQuery.query.Roles;
	var roleLocationRegions = requireQuery.query.RoleLocationRegions;
	var roleLocationCounties = requireQuery.query.RoleLocationCounties;
	var roleLocationTowns = requireQuery.query.RoleLocationTowns;
	var roleLocationPostcodes = requireQuery.query.RoleLocationPostcodes;
	
	var roleParentsList = [];
	var rolesList = [];
	var roleLocationRegionsList = [];
	var roleLocationCountiesList = [];
	var roleLocationTownsList = [];
	var roleLocationPostcodesList = [];
	
	if ((roleParents == undefined) && (roles == undefined)) {
		callback({Error:"Parameter RoleParents/Roles Values is Missing"}, null);
	}
	else{
		
		if (roleParents != undefined){
			roleParents.split(',').forEach(function(roleParent){
				roleParentsList.push(roleParent.toLowerCase());
			});  
		}
		
		if (roles != undefined){
			roles.split(',').forEach(function(role){
				rolesList.push(role.toLowerCase());
			});  
		}
		if (roleLocationRegions != undefined){
			roleLocationRegions.split(',').forEach(function(region){
				roleLocationRegionsList.push(region.toLowerCase());
			});  
		}
		if (roleLocationCounties != undefined){
			roleLocationCounties.split(',').forEach(function(county){
				roleLocationCountiesList.push(county.toLowerCase());
			});  
		}
		if (roleLocationTowns != undefined){
			roleLocationTowns.split(',').forEach(function(town){
				roleLocationTownsList.push(town.toLowerCase());
			});  
		}
		if (roleLocationPostcodes != undefined){
			roleLocationPostcodes.split(',').forEach(function(postcode){
				roleLocationPostcodesList.push(postcode);
			});  
		}
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			if(err){
				callback(err, null);
			}else{
				try{
					
					getRolesMetadata(exportedRequiredModules, roleParentsList, rolesList, function(err, roleParentDataList, roleDataList){
					
					if (err){
						callback(err,null);
					}
					else if((roleParentDataList.length == 0) && (roleDataList.length == 0)){
						callback({Error: 'Invalid parameter value given for RoleParents/Roles'},null);
					}
					else{
						getRoleLocationsMetadata(exportedRequiredModules, roleLocationRegionsList,roleLocationCountiesList,roleLocationTownsList,roleLocationPostcodesList, function(err, roleRegionList, roleCountyList, RoleTownList){
							// roleRegionList, roleCountyList, RoleTownList
							if(err){
								callback(err, null);
							}else{
								try{
									var rolesQuery = '';
									if (roleLocationPostcodesList.length > 0){
										roleLocationPostcodesList = roleLocationPostcodesList.map(function(x){ return x.toUpperCase()});
										// console.log("roleLocationRegionsList"+roleLocationRegionsList);
										var queryFieldRoleParent = '{"terms": %s}';
										var queryFieldRole = '{"terms": %s}';
										var queryFieldRoleRegion = '{"terms": %s}';
										var queryFieldRoleCounty = '{"terms": %s}';
										var queryFieldRoleTown = '{"terms": %s}';
										var queryFieldRolePostcode = '{"terms": %s}';
										var queryBuildRoleParent={};
										var queryBuildRole={};
										var queryBuildRoleRegion={};
										var queryBuildRoleCounty={};
										var queryBuildRoleTown={};
										var queryBuildRolePostcode={};
										
										queryBuildRoleParent["RoleList.RoleParent"]=roleParentDataList;
										queryBuildRole["RoleList.RoleParentRole"]=roleDataList;
										queryBuildRoleRegion["RoleList.LocationLevel1"]=roleRegionList;
										queryBuildRoleCounty["RoleList.LocationLevel2"]=roleCountyList;
										queryBuildRoleTown["RoleList.LocationLevel3"]=RoleTownList;
										queryBuildRolePostcode["RoleList.PostCodeDistrict"]=roleLocationPostcodesList;
										var roleQueryConcat='';
										var roleQueryLocationConcat='';
										if (roleParentDataList.length > 0) { roleQueryConcat = exportedRequiredModules.convertComma2Array(queryFieldRoleParent, JSON.stringify(queryBuildRoleParent));  }
										if (roleDataList.length > 0) { roleQueryConcat = roleQueryConcat + ', ' +exportedRequiredModules.convertComma2Array(queryFieldRole, JSON.stringify(queryBuildRole));  }
										
										if (roleRegionList.length > 0) { roleQueryLocationConcat = exportedRequiredModules.convertComma2Array(queryFieldRoleRegion, JSON.stringify(queryBuildRoleRegion));  }
										if (roleCountyList.length > 0) { roleQueryLocationConcat = roleQueryLocationConcat + ', ' +exportedRequiredModules.convertComma2Array(queryFieldRoleCounty, JSON.stringify(queryBuildRoleCounty));  }
										if (RoleTownList.length > 0) { roleQueryLocationConcat = roleQueryLocationConcat + ', ' +exportedRequiredModules.convertComma2Array(queryFieldRoleTown, JSON.stringify(queryBuildRoleTown));  }
										if (roleLocationPostcodesList.length > 0) { roleQueryLocationConcat = roleQueryLocationConcat + ', ' +exportedRequiredModules.convertComma2Array(queryFieldRolePostcode, JSON.stringify(queryBuildRolePostcode));  }
										
										roleQueryConcat = roleQueryConcat.replace(/^\s*undefined\s*\,/, "");
										roleQueryConcat = roleQueryConcat.replace(/^\s*\,/, "");
										roleQueryConcat = roleQueryConcat.replace(/\,\s*$/, "");

										roleQueryLocationConcat = roleQueryLocationConcat.replace(/^\s*undefined\s*\,/, "");
										roleQueryLocationConcat = roleQueryLocationConcat.replace(/^\s*\,/, "");
										roleQueryLocationConcat = roleQueryLocationConcat.replace(/\,\s*$/, "");
										
										var finalRoleQuery = '{ "bool": { "should": [ '+roleQueryConcat+' ] } }';
										var finalRoleLocationQuery = '{ "bool": { "should": [ '+roleQueryLocationConcat+' ] } }';
																
										var finalRoleQueryConcat =  finalRoleQuery;
										if (roleQueryLocationConcat != ''){ finalRoleQueryConcat = finalRoleQueryConcat +', '+finalRoleLocationQuery; }
										rolesQuery = '{"nested": { "path": "RoleList", "query": { "bool": { "filter": [  '+finalRoleQueryConcat+' ] } } } }';
									}else{
										if((roleRegionList.length == 0) && (roleCountyList.length == 0) && (RoleTownList.length == 0)){
											var queryFieldRoleParent = '{"terms": %s}';
											var queryFieldRole = '{"terms": %s}';
											var queryBuildRoleParent={};
											var queryBuildRole={};
											roleParentDataList = roleParentDataList.map(function(x){ return x.toLowerCase()});
											roleDataList = roleDataList.map(function(x){ return x.toLowerCase()});
											queryBuildRoleParent["RoleParents"]=roleParentDataList;
											queryBuildRole["ProjectRoles"]=roleDataList;
											var roleQueryConcat='';
											if (roleParentDataList.length > 0) { roleQueryConcat = exportedRequiredModules.convertComma2Array(queryFieldRoleParent, JSON.stringify(queryBuildRoleParent));  }
											if (roleDataList.length > 0) { roleQueryConcat = roleQueryConcat + ', ' +exportedRequiredModules.convertComma2Array(queryFieldRole, JSON.stringify(queryBuildRole));  }
											roleQueryConcat = roleQueryConcat.replace(/^\s*undefined\s*\,/, "");
											roleQueryConcat = roleQueryConcat.replace(/^\s*\,/, "");
											roleQueryConcat = roleQueryConcat.replace(/\,\s*$/, "");
											rolesQuery = '{ "bool": { "should": [ '+roleQueryConcat+' ] } }';
										}else{
											roleParentDataList = roleParentDataList.map(function(x){ return x.toLowerCase()});
											roleDataList = roleDataList.map(function(x){ return x.toLowerCase()});
											var queryFieldLevel1Parent = '{"terms": %s}';
											var queryFieldLevel1Role = '{"terms": %s}';
											var queryBuildLevel1Parent = {};
											var queryBuildLevel1Role = {};
											var queryFieldLevel2Parent = '{"terms": %s}';
											var queryFieldLevel2Role = '{"terms": %s}';
											var queryBuildLevel2Parent = {};
											var queryBuildLevel2Role = {};

											var queryFieldLevel3Parent = '{"terms": %s}';
											var queryFieldLevel3Role = '{"terms": %s}';
											var queryBuildLevel3Parent = {};
											var queryBuildLevel3Role = {};

											var roleLocationsLevel1WithRoleParents = [];
											var roleLocationsLevel2WithRoleParents = [];
											var roleLocationsLevel3WithRoleParents = [];
											var roleLocationsLevel1WithRole = [];
											var roleLocationsLevel2WithRole = [];
											var roleLocationsLevel3WithRole = [];
											roleRegionList.forEach(function(roleRegion){
												roleParentDataList.forEach(function(parentData){
													roleLocationsLevel1WithRoleParents.push(roleRegion+"#"+parentData);
												});
												roleDataList.forEach(function(roleData){
													roleLocationsLevel1WithRole.push(roleRegion+"#"+roleData);
												});
											})
											roleCountyList.forEach(function(roleCounty){
												roleParentDataList.forEach(function(parentData){
													roleLocationsLevel2WithRoleParents.push(roleCounty+"#"+parentData);
												});
												roleDataList.forEach(function(roleData){
													roleLocationsLevel2WithRole.push(roleCounty+"#"+roleData);
												});
											})
											RoleTownList.forEach(function(roleTown){
												roleParentDataList.forEach(function(parentData){
													roleLocationsLevel3WithRoleParents.push(roleTown+"#"+parentData);
												});
												roleDataList.forEach(function(roleData){
													roleLocationsLevel3WithRole.push(roleTown+"#"+roleData);
												});
											})
											// console.log("roleLocationsLevel1WithRoleParents"+roleLocationsLevel1WithRoleParents);
											queryBuildLevel1Parent["RoleLocationsLevel1WithRoleParents"] = roleLocationsLevel1WithRoleParents;
											queryBuildLevel1Role["RoleLocationsLevel1WithRole"] = roleLocationsLevel1WithRole;
											queryBuildLevel2Parent["RoleLocationsLevel2WithRoleParents"] = roleLocationsLevel2WithRoleParents;
											queryBuildLevel2Role["RoleLocationsLevel2WithRole"] = roleLocationsLevel2WithRole;
											queryBuildLevel3Parent["RoleLocationsLevel3WithRoleParents"] = roleLocationsLevel3WithRoleParents;
											queryBuildLevel3Role["RoleLocationsLevel3WithRole"] = roleLocationsLevel3WithRole;

											if (roleLocationsLevel1WithRoleParents.length > 0) { roleQueryLocationConcat = exportedRequiredModules.convertComma2Array(queryFieldLevel1Parent, JSON.stringify(queryBuildLevel1Parent));  }
											if (roleLocationsLevel2WithRoleParents.length > 0) { roleQueryLocationConcat = roleQueryLocationConcat + ', ' +exportedRequiredModules.convertComma2Array(queryFieldLevel2Parent, JSON.stringify(queryBuildLevel2Parent));  }
											if (roleLocationsLevel3WithRoleParents.length > 0) { roleQueryLocationConcat = roleQueryLocationConcat + ', ' +exportedRequiredModules.convertComma2Array(queryFieldLevel3Parent, JSON.stringify(queryBuildLevel3Parent));  }
											if (roleLocationsLevel1WithRole.length > 0) { roleQueryLocationConcat = roleQueryLocationConcat + ', ' +exportedRequiredModules.convertComma2Array(queryFieldLevel1Role, JSON.stringify(queryBuildLevel1Role));  }
											if (roleLocationsLevel2WithRole.length > 0) { roleQueryLocationConcat = roleQueryLocationConcat + ', ' +exportedRequiredModules.convertComma2Array(queryFieldLevel2Role, JSON.stringify(queryBuildLevel2Role));  }
											if (roleLocationsLevel3WithRole.length > 0) { roleQueryLocationConcat = roleQueryLocationConcat + ', ' +exportedRequiredModules.convertComma2Array(queryFieldLevel3Role, JSON.stringify(queryBuildLevel3Role));  }
											
											roleQueryLocationConcat = roleQueryLocationConcat.replace(/^\s*undefined\s*\,/, "");
											roleQueryLocationConcat = roleQueryLocationConcat.replace(/^\s*\,/, "");
											roleQueryLocationConcat = roleQueryLocationConcat.replace(/\,\s*$/, "");
											
											rolesQuery = '{ "bool": { "should": [ '+roleQueryLocationConcat+' ] } }';
										}
									}
									var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
									var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
									
									var queryConcat=projectHistories
									
									if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
									if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
									if (rolesQuery != ''){ queryConcat = queryConcat +', '+rolesQuery; }
									
									var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
									
									callback(null, FinalQuery);
								}catch (err){
									callback(err, null);
								}
							}
						});
					}
				});
				}catch (err){
					callback(err, null);
				}
			}
		});
	}
}

/******************************************************************************
Function: getRolesMetadata
Argument:exportedRequiredModules, roleParentsList, rolesList
Return:  get project roles.
Usage:
	1. Extracts project roles from metadata url.
*******************************************************************************/
function getRolesMetadata(exportedRequiredModules, roleParentsList, rolesList, callback){
	
	var roleParentDataList = [];
	var roleDataList = [];
	var checkRoleParentValue = [];
	var checkRoleValue = [];
	
	try{
	 	var urlProjectRole = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/projectroles';
		
		exportedRequiredModules.request(urlProjectRole, function (error, response,roleContent) {
			if( roleContent != undefined){
				var projectRole = JSON.parse(roleContent);
				projectRole._source.Roles.forEach(function(roleList) {
					if (roleParentsList.length > 0) {
						if (roleParentsList.includes(roleList.RoleGroupName.toLowerCase().replace('&','and')) == true){
							checkRoleParentValue.push(roleList.RoleGroupName);
							roleParentDataList.push(roleList.RoleGroupName);
						}
					}
					if (rolesList.length > 0) {
						roleList.Roles.forEach(function(role){
							if (rolesList.includes(role.toLowerCase().replace('&','and')) == true){
								checkRoleValue.push(role);
								roleDataList.push(roleList.RoleGroupName+"#"+role);
								if (roleParentDataList.includes(roleList.RoleGroupName)  == true){ 
									roleParentDataList.splice(roleParentDataList.indexOf(roleList.RoleGroupName),1);
								}
							}
							
						});
					}
					
				});
				checkRoleValue = checkRoleValue.map(function(x){ return x.toLowerCase().replace(', ',' and ').replace('&','and')});
				var missedRoleLevel2List = exportedRequiredModules.arrayDiff(rolesList, checkRoleValue);
				if(missedRoleLevel2List.length > 0){
					projectRole._source.Roles.forEach(function(roleList) {
						roleList.Roles.forEach(function(role){
							if (missedRoleLevel2List.includes(role.toLowerCase().replace('&','and')) == true){
								checkRoleValue.push(role);
								roleDataList.push(roleList.RoleGroupName+"#"+role);
								if (roleParentDataList.includes(roleList.RoleGroupName)  == true){  roleParentDataList.splice(roleParentDataList.indexOf(roleList.RoleGroupName),1);}
							}
						});
					});
				}
				if((checkRoleParentValue.length == roleParentsList.length) && (checkRoleValue.length == rolesList.length)){
					callback(null, roleParentDataList, roleDataList);
				}else{
					callback({Error: 'Invalid RoleParents/Roles value is given.'}, null);
				}
				
			}
			else{
				callback({Error: 'Role metadata URL response failed.'}, null);
			}
		});
	}catch(err){
		callback({Error: 'Role metadata URL response failed.'}, null);
	}
}

/******************************************************************************
Function: getRoleLocationsMetadata
Argument:exportedRequiredModules, roleLocationRegionsList,roleLocationCountiesList
,roleLocationTownsList,roleLocationPostcodesList
Return:  get project roles location and town.
Usage:
	1. Extracts project roles location and town from metadata url.
*******************************************************************************/
function getRoleLocationsMetadata(exportedRequiredModules, roleLocationRegionsList,roleLocationCountiesList,roleLocationTownsList,roleLocationPostcodesList, callback){
	try{
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
		
		var urlRequestLocations = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/locations';
		var urlRequestTowns = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/towns';
		var roleRegionList = [];
		var roleCountiesList = [];
		var roleTownList = [];
		var checkRoleRegionList = [];
		var checkRoleCountiesList = [];
		var checkRoleTownList = [];
		
		roleLocationRegionsList = roleLocationRegionsList.map(function(x){ return x.toLowerCase() })
		roleLocationCountiesList = roleLocationCountiesList.map(function(x){ return x.toLowerCase() })
		roleLocationTownsList = roleLocationTownsList.map(function(x){ return x.toLowerCase() })
		var sourceTowns=[];
		exportedRequiredModules.request(urlRequestLocations, function (error, response, locationContent) {
			exportedRequiredModules.request(urlRequestTowns, function (error, response, townContent) {	
				var locationList = JSON.parse(locationContent);
				var towns = JSON.parse(townContent);
				var errorMessage;
				
				locationList._source.Locations.forEach(function (locations){
					try{
						if (roleLocationRegionsList.includes(locations.Region.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							checkRoleRegionList.push(locations.Region.toLowerCase());
							roleRegionList.push(locations.Region.toLowerCase());
						}
						locations.Counties.forEach(function(countyValue){
							if (roleLocationCountiesList.includes(countyValue.toLowerCase().replace(', ',' and ').replace('&','and')) == true)
							{
								checkRoleCountiesList.push(countyValue);
								roleCountiesList.push(locations.Region.toLowerCase()+"#"+countyValue.toLowerCase());
								if (roleRegionList.includes(locations.Region.toLowerCase()) == true ){ roleRegionList.splice(roleRegionList.indexOf(locations.Region.toLowerCase()),1);}
							}
							towns._source.CountiesAndTowns.forEach(function (countiesAndTowns){
								if (countyValue.toLowerCase() == countiesAndTowns.County.toLowerCase()){
									countiesAndTowns.Towns.forEach(function (town){
										if (roleLocationTownsList.includes(town.toLowerCase().replace(', ',' and ').replace('&','and')) == true)
										{
											checkRoleTownList.push(town);
											roleTownList.push(locations.Region.toLowerCase()+"#"+countyValue.toLowerCase()+"#"+town.toLowerCase());
											if (roleRegionList.includes(locations.Region.toLowerCase()) == true ){ roleRegionList.splice(roleRegionList.indexOf(locations.Region.toLowerCase()),1);}
											if (roleCountiesList.includes(countyValue.toLowerCase()) == true ){ roleCountiesList.splice(roleCountiesList.indexOf(countyValue.toLowerCase()),1);}
										}
									});
								}
							});
						});	
					}
					catch(err){
						locations.Boroughs.forEach(function(countyValue){
							if (roleLocationCountiesList.includes(countyValue.toLowerCase().replace(', ',' and ').replace('&','and')) == true)
							{
								checkRoleCountiesList.push(countyValue);
								roleCountiesList.push(locations.Region.toLowerCase()+"#"+countyValue.toLowerCase());
								if (roleRegionList.includes(locations.Region.toLowerCase()) == true ){ roleRegionList.splice(roleRegionList.indexOf(locations.Region.toLowerCase()),1);}
							}
						});
					}
				});
				if((checkRoleRegionList.length == roleLocationRegionsList.length) && (checkRoleCountiesList.length == roleLocationCountiesList.length) && (checkRoleTownList.length == roleLocationTownsList.length)){
					callback(null, roleRegionList,roleCountiesList,roleTownList);
				}else if(checkRoleRegionList.length != roleLocationRegionsList.length){
					callback({Error : "Invalid RoleLocationRegions value is given"}, null, null, null);
				}else if(checkRoleCountiesList.length != roleLocationCountiesList.length){
					callback({Error : "Invalid RoleLocationCounties value is given"}, null, null, null);
				}
				else if(checkRoleTownList.length != roleLocationTownsList.length){
					callback({Error : "Invalid RoleLocationTowns value is given"}, null, null, null);
				}
				else{
					callback({Error : "Exception on Role Location Metadata"}, null, null, null);
				}
			});
		});
	}catch(err){
		// console.log("err"+err);
		callback({Error : "Exception on Role Location Metadata"}, null, null, null);
	}
}

/******************************************************************************
Function: roleOnlyOutputRevision
Argument:subscriptionJSON, requireQuery
Return: Output Response.
Usage:
	1. Returns a response for role only. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.roleOnlyOutputRevision = function (outputResponse, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	var roleParents = requireQuery.query.RoleParents;
	var roles = requireQuery.query.Roles;
	var roleLocationRegions = requireQuery.query.RoleLocationRegions;
	var roleLocationCounties = requireQuery.query.RoleLocationCounties;
	var roleLocationTowns = requireQuery.query.RoleLocationTowns;
	var roleLocationPostcodes = requireQuery.query.RoleLocationPostcodes;
	var blockedResultIndexNo = [];
	outputResponse.results.forEach(function(indexData, hitsIndexNo){
		var blockedRoleIndexNo = [];
		
		indexData.source.RoleList.forEach(function(roleList, listIndexNo){
			// if ((roleParents != undefined) && (roleList.RoleParent.toLowerCase().indexOf(roleParents.toLowerCase()) == -1)){
			if ((roleParents != undefined) && (roleList.RoleParent != null)){
				if(roleParents.toLowerCase().indexOf(roleList.RoleParent.toLowerCase()) == -1){blockedRoleIndexNo.push(listIndexNo);}
			}
			else if ((roles != undefined) && (roleList.Role != null)){
				if(roles.toLowerCase().indexOf(roleList.Role.toLowerCase()) == -1){blockedRoleIndexNo.push(listIndexNo);}
			}
			else if ((roleLocationRegions != undefined) && (roleList.LocationLevel1 != null)){
				if(roleLocationRegions.toLowerCase().indexOf(roleList.LocationLevel1.toLowerCase()) == -1){blockedRoleIndexNo.push(listIndexNo);}
			}
			else if ((roleLocationCounties != undefined) && (roleList.LocationLevel2 != null)){
				if(roleLocationCounties.toLowerCase().indexOf(roleList.LocationLevel2.toLowerCase()) == -1){blockedRoleIndexNo.push(listIndexNo);}
			}
			else if ((roleLocationTowns != undefined) && (roleList.LocationLevel3 != null)){
				if(roleLocationTowns.toLowerCase().indexOf(roleList.LocationLevel3.toLowerCase()) == -1){blockedRoleIndexNo.push(listIndexNo);}
			}
			else if ((roleLocationPostcodes != undefined) && (roleList.PostCodeDistrict != null)){
				if(roleLocationPostcodes.toLowerCase().indexOf(roleList.PostCodeDistrict.toLowerCase()) == -1){blockedRoleIndexNo.push(listIndexNo);}
			}
		});
		
		blockedRoleIndexNo.forEach(function(deleteIndex, index){
			outputResponse.results[hitsIndexNo].source.RoleList.splice((deleteIndex-index), 1);
		});
		if(outputResponse.results[hitsIndexNo].source.RoleList.length == 0){
			blockedResultIndexNo.push(hitsIndexNo);
		}
	});
	blockedResultIndexNo.forEach(function(deleteIndex, index){
		outputResponse.results.splice((deleteIndex-index), 1);
	});
	callback(null, outputResponse)
}