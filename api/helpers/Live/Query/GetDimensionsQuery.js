'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetDimensionsQuery.js) is used form a query for given Dimension.
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryByFromTo
Argument:fromToValues, dimensionField
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for ranges like From and To in Deimension. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
function getQueryByFromTo(fromToValues, dimensionField){
	
	if (fromToValues != undefined){
		
		var fromToDate =  /From\:([0-9\.\-]+)\s*\,\s*To\:([0-9\.\-]+)/ig.exec(fromToValues);
		var fromDate =  /From\:([0-9\.\-]+)\s*/g.exec(fromToValues);
		var toDate = /\s*To\:([0-9\.\-]+)/g.exec(fromToValues);
		
		if ((toDate == null) && (fromDate == null) && (fromToDate == null)){
			throw ({Error : "Invalid parameter value given for "+dimensionField+". It should be From:/To:"});
		}
		else if (((toDate == null) || (fromDate == null)) && (fromToDate != null)){
			throw ({Error : "Invalid parameter value given for "+dimensionField+". It should be From:/To:"});
		}
		else if (((toDate != null) || (fromDate != null)) && (fromToDate == null)){
			throw ({Error : "Invalid parameter value given for "+dimensionField+". It should be From:/To:"});
		}
		fromToValues.split(',').forEach(function(value, i){
			if ((i == 0) && (/(?:From|To)/g.exec(value) == null)){
				throw ({Error : "Invalid parameter value given for "+dimensionField+". It should be From:/To:"});
			}
			else if ((i == 1) && (/(?:From|To)/g.exec(value) == null)){
				throw ({Error : "Invalid parameter value given for "+dimensionField+". It should be From:/To:"});
			}
			else if (i > 1){
				throw ({Error : "Invalid parameter value given for "+dimensionField+". It should be From:/To:"});
			}
		});
		if (toDate != null){
			if (parseFloat(toDate[1]) < 0){
				throw ({Error : "Parameter "+dimensionField+" - To Value must be positive."});
			}
		}
		if(fromDate != null){
			if ((fromDate[1] != parseInt(fromDate[1])) && (dimensionField != "SiteArea")){
				throw ({Error : "Parameter "+dimensionField+" - From value must be integer."});
				// callback({Error : "From/To value must be integer."}, null);
			}
		}
		if(toDate != null){
			if ((toDate[1] != parseInt(toDate[1])) && (dimensionField != "SiteArea")){
				throw ({Error : "Parameter "+dimensionField+" - To value must be integer."});
				// callback({Error : "From/To value must be integer."}, null);
			}
		}
		if((fromDate != null) && (toDate != null)){
			if (parseFloat(fromDate[1]) > parseFloat(toDate[1]))
			{
				// callback({Error : "Your upper limit must be a higher value than your lower limit."}, null);
				throw ({Error : "Your upper limit must be a higher value than your lower limit in parameter \""+dimensionField+"\""});
			}
		}
		// else{
		var valuesQuery='';
		if((fromDate != null) && (toDate != null)){
			valuesQuery = '{ "range": { "'+dimensionField+'": { "gte": "'+fromDate[1]+'", "lte": "'+toDate[1]+'" } } }'
		}
		else if((fromDate != null) && (toDate == null)){
			valuesQuery = '{ "range": { "'+dimensionField+'": { "gte": "'+fromDate[1]+'" } } }'
		}
		else if((fromDate == null) && (toDate != null)){
			valuesQuery = '{ "range": { "'+dimensionField+'": { "lte": "'+toDate[1]+'" } } }'
		}
		return valuesQuery;
		
	}
	else{
		return "";
	}
	
}
/******************************************************************************
Function: getQueryByDimensions
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Deimensions like site area,floor area,storeys,underground storeys,length,blocks,parking spaces,underground parking spaces,height and units. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByDimensions = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var siteAreaValues = requireQuery.query.SiteArea;
	var floorAreaValues = requireQuery.query.FloorArea;
	var storeysValues = requireQuery.query.Storeys;
	var undergroundStoreysValues = requireQuery.query.UndergroundStoreys;
	var lengthValues = requireQuery.query.Length;
	var blocksValues = requireQuery.query.Blocks;
	var parkingSpacesValues = requireQuery.query.ParkingSpaces;
	var undergroundParkingSpacesValues = requireQuery.query.UndergroundParkingSpaces;
	var heightValues = requireQuery.query.Height;
	var unitsValues = requireQuery.query.Units;
	
	if ((siteAreaValues == undefined) && (floorAreaValues == undefined) && (storeysValues == undefined) && (undergroundStoreysValues == undefined) && (lengthValues == undefined)
	&& (blocksValues == undefined) && (parkingSpacesValues == undefined) && (undergroundParkingSpacesValues == undefined) && (heightValues == undefined)
	&& (unitsValues == undefined)) {
		callback({Error:"Parameter SiteArea/FloorArea/Storeys/UndergroundStoreys/Length/Blocks/ParkingSpaces/UndergroundParkingSpaces/Height/Units Values is Missing"}, null);
	}
	else{
	
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			
			if(err){
				callback(err, null);
			}
			else{
				try{
					
					var siteAreaQuery = getQueryByFromTo(siteAreaValues,"SiteArea");
					var floorAreaQuery = getQueryByFromTo(floorAreaValues,"FloorArea");
					
					var storeysQuery = getQueryByFromTo(storeysValues,"Storeys");
					var undergroundStoreysQuery = getQueryByFromTo(undergroundStoreysValues,"UndergroundStoreys");
					var lengthQuery = getQueryByFromTo(lengthValues,"Length");
					var blocksQuery = getQueryByFromTo(blocksValues,"Structures");
					var parkingSpacesQuery = getQueryByFromTo(parkingSpacesValues,"ParkingSpaces");
					var undergroundParkingSpacesQuery = getQueryByFromTo(undergroundParkingSpacesValues,"BasementParkingSpaces");
					var heightQuery = getQueryByFromTo(heightValues,"VerticalHeight");
					var unitsQuery = getQueryByFromTo(unitsValues,"Units");
					
					var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
					var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
					
					var queryConcat=projectHistories
					if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
					if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
					if (siteAreaQuery != '' ){ queryConcat = queryConcat +', '+siteAreaQuery; }
					if (floorAreaQuery != '' ){ queryConcat = queryConcat +', '+floorAreaQuery; }
					if (storeysQuery != '' ){ queryConcat = queryConcat +', '+storeysQuery; }
					if (undergroundStoreysQuery != '' ){ queryConcat = queryConcat +', '+undergroundStoreysQuery; }
					if (lengthQuery != '' ){ queryConcat = queryConcat +', '+lengthQuery; }
					if (blocksQuery != '' ){ queryConcat = queryConcat +', '+blocksQuery; }
					if (parkingSpacesQuery != '' ){ queryConcat = queryConcat +', '+parkingSpacesQuery; }
					if (undergroundParkingSpacesQuery != '' ){ queryConcat = queryConcat +', '+undergroundParkingSpacesQuery; }
					if (heightQuery != '' ){ queryConcat = queryConcat +', '+heightQuery; }
					if (unitsQuery != '' ){ queryConcat = queryConcat +', '+unitsQuery; }
					var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
					// console.log(FinalQuery);
					callback(null, FinalQuery);
				}catch (err){
					// console.log("err"+err);
					callback(err, null);
				}
			}
		});
	}
}

/******************************************************************************
Function: getQueryByUnits
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Unit Dimensions. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByUnits = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var fromValues = requireQuery.query.From;
	var toValues = requireQuery.query.To;
	
	if ((fromValues == undefined) && (toValues == undefined)) {
		callback({Error:"Parameter From/To Values is Missing"}, null);
	}
	else if ((parseFloat(fromValues) < 0) || (parseFloat(toValues) < 0)){
		callback({Error : "From/To Value must be positive."}, null);
	}
	else if(((fromValues != undefined) && (fromValues != parseInt(fromValues))) || ((toValues != undefined) && (toValues != parseInt(toValues)))){
		callback({Error : "From/To value must be integer."}, null);
	}
	else if (parseFloat(fromValues) > parseFloat(toValues))
	{
		callback({Error : "Your upper limit must be a higher value than your lower limit."}, null);
	}
	else{
	
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			try{
				if(err){
					callback(err, null);
				}
				else{
					var valuesQuery='';
					if ((fromValues != undefined) && (toValues != undefined)){
						valuesQuery = '{ "range": { "Units": { "gte": "'+fromValues+'", "lte": "'+toValues+'" } } }'
					}
					else if ((fromValues != undefined) && (toValues == undefined)){	
						valuesQuery = '{ "range": { "Units": { "gte": "'+fromValues+'" } } }'
					}
					else if ((fromValues == undefined) && (toValues != undefined)){	
						valuesQuery = '{ "range": { "Units": { "lte": "'+toValues+'" } } }'
					}
					
					try{
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						if (valuesQuery != '' ){ queryConcat = queryConcat +', '+valuesQuery; }
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);

					}catch (err){
						callback(err, null);
					}
				
				}
			}catch (err){
				callback(err, null);
			}
		});
	}
}
/******************************************************************************
Function: getQueryByVerticalHeight
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Vertical Height Dimensions. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByVerticalHeight = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var fromValues = requireQuery.query.From;
	var toValues = requireQuery.query.To;
	
	if ((fromValues == undefined) && (toValues == undefined)) {
		callback({Error:"Parameter From/To Values is Missing"}, null);
	}
	else if ((parseFloat(fromValues) < 0) || (parseFloat(toValues) < 0)){
		callback({Error : "From/To Value must be positive."}, null);
	}
	else if(((fromValues != undefined) && (fromValues != parseInt(fromValues))) || ((toValues != undefined) && (toValues != parseInt(toValues)))){
		callback({Error : "From/To value must be integer."}, null);
	}
	else if (parseFloat(fromValues) > parseFloat(toValues))
	{
		callback({Error : "Your upper limit must be a higher value than your lower limit."}, null);
	}
	else{
	
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			try{
				if(err){
					callback(err, null);
				}
				else{
					var valuesQuery='';
					if ((fromValues != undefined) && (toValues != undefined)){
						valuesQuery = '{ "range": { "VerticalHeight": { "gte": "'+fromValues+'", "lte": "'+toValues+'" } } }'
					}
					else if ((fromValues != undefined) && (toValues == undefined)){	
						valuesQuery = '{ "range": { "VerticalHeight": { "gte": "'+fromValues+'" } } }'
					}
					else if ((fromValues == undefined) && (toValues != undefined)){	
						valuesQuery = '{ "range": { "VerticalHeight": { "lte": "'+toValues+'" } } }'
					}
					
					try{
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						if (valuesQuery != '' ){ queryConcat = queryConcat +', '+valuesQuery; }
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);

					}catch (err){
						callback(err, null);
					}
				
				}
			}catch (err){
				callback(err, null);
			}
		});
	}
}
/******************************************************************************
Function: getQueryByBasementParkingSpaces
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Basement Parking Spaces Dimensions. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByBasementParkingSpaces = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var fromValues = requireQuery.query.From;
	var toValues = requireQuery.query.To;
	
	if ((fromValues == undefined) && (toValues == undefined)) {
		callback({Error:"Parameter From/To Values is Missing"}, null);
	}
	else if ((parseFloat(fromValues) < 0) || (parseFloat(toValues) < 0)){
		callback({Error : "From/To Value must be positive."}, null);
	}
	else if(((fromValues != undefined) && (fromValues != parseInt(fromValues))) || ((toValues != undefined) && (toValues != parseInt(toValues)))){
		callback({Error : "From/To value must be integer."}, null);
	}
	else if (parseFloat(fromValues) > parseFloat(toValues))
	{
		callback({Error : "Your upper limit must be a higher value than your lower limit."}, null);
	}
	else{
	
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			
			try{
				if(err){
					callback(err, null);
				}
				else{
					var valuesQuery='';
					if ((fromValues != undefined) && (toValues != undefined)){
						valuesQuery = '{ "range": { "BasementParkingSpaces": { "gte": "'+fromValues+'", "lte": "'+toValues+'" } } }'
					}
					else if ((fromValues != undefined) && (toValues == undefined)){	
						valuesQuery = '{ "range": { "BasementParkingSpaces": { "gte": "'+fromValues+'" } } }'
					}
					else if ((fromValues == undefined) && (toValues != undefined)){	
						valuesQuery = '{ "range": { "BasementParkingSpaces": { "lte": "'+toValues+'" } } }'
					}
					
					try{
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						if (valuesQuery != '' ){ queryConcat = queryConcat +', '+valuesQuery; }
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);

					}catch (err){
						callback(err, null);
					}
				
				}
			}catch (err){
				callback(err, null);
			}
		});
	}
}
/******************************************************************************
Function: getQueryByParkingSpaces
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Parking Spaces Dimensions. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByParkingSpaces = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var fromValues = requireQuery.query.From;
	var toValues = requireQuery.query.To;
	
	if ((fromValues == undefined) && (toValues == undefined)) {
		callback({Error:"Parameter From/To Values is Missing"}, null);
	}
	else if ((parseFloat(fromValues) < 0) || (parseFloat(toValues) < 0)){
		callback({Error : "From/To Value must be positive."}, null);
	}
	else if(((fromValues != undefined) && (fromValues != parseInt(fromValues))) || ((toValues != undefined) && (toValues != parseInt(toValues)))){
		callback({Error : "From/To value must be integer."}, null);
	}
	else if (parseFloat(fromValues) > parseFloat(toValues))
	{
		callback({Error : "Your upper limit must be a higher value than your lower limit."}, null);
	}
	else{
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			try{
				if(err){
					callback(err, null);
				}
				else{
					var valuesQuery='';
					if ((fromValues != undefined) && (toValues != undefined)){
						valuesQuery = '{ "range": { "ParkingSpaces": { "gte": "'+fromValues+'", "lte": "'+toValues+'" } } }'
					}
					else if ((fromValues != undefined) && (toValues == undefined)){	
						valuesQuery = '{ "range": { "ParkingSpaces": { "gte": "'+fromValues+'" } } }'
					}
					else if ((fromValues == undefined) && (toValues != undefined)){	
						valuesQuery = '{ "range": { "ParkingSpaces": { "lte": "'+toValues+'" } } }'
					}
					
					try{
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						if (valuesQuery != '' ){ queryConcat = queryConcat +', '+valuesQuery; }
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);

					}catch (err){
						callback(err, null);
					}
				
				}
			}catch (err){
				callback(err, null);
			}
		});
	}
}
/******************************************************************************
Function: getQueryByStructures
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Structures. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByStructures = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var fromValues = requireQuery.query.From;
	var toValues = requireQuery.query.To;
	
	if ((fromValues == undefined) && (toValues == undefined)) {
		callback({Error:"Parameter From/To Values is Missing"}, null);
	}
	else if ((parseFloat(fromValues) < 0) || (parseFloat(toValues) < 0)){
		callback({Error : "From/To Value must be positive."}, null);
	}
	else if(((fromValues != undefined) && (fromValues != parseInt(fromValues))) || ((toValues != undefined) && (toValues != parseInt(toValues)))){
		callback({Error : "From/To value must be integer."}, null);
	}
	else if (parseFloat(fromValues) > parseFloat(toValues))
	{
		callback({Error : "Your upper limit must be a higher value than your lower limit."}, null);
	}
	else{
	
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			try{
				if(err){
					callback(err, null);
				}
				else{
					var valuesQuery='';
					if ((fromValues != undefined) && (toValues != undefined)){
						valuesQuery = '{ "range": { "Structures": { "gte": "'+fromValues+'", "lte": "'+toValues+'" } } }'
					}
					else if ((fromValues != undefined) && (toValues == undefined)){	
						valuesQuery = '{ "range": { "Structures": { "gte": "'+fromValues+'" } } }'
					}
					else if ((fromValues == undefined) && (toValues != undefined)){	
						valuesQuery = '{ "range": { "Structures": { "lte": "'+toValues+'" } } }'
					}
					
					try{
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						if (valuesQuery != '' ){ queryConcat = queryConcat +', '+valuesQuery; }
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);

					}catch (err){
						callback(err, null);
					}
				
				}
			}catch (err){
				callback(err, null);
			}
		});
	}
}
/******************************************************************************
Function: getQueryByLength
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Length. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByLength = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var fromValues = requireQuery.query.From;
	var toValues = requireQuery.query.To;
	
	if ((fromValues == undefined) && (toValues == undefined)) {
		callback({Error:"Parameter From/To Values is Missing"}, null);
	}
	else if ((parseFloat(fromValues) < 0) || (parseFloat(toValues) < 0)){
		callback({Error : "From/To Value must be positive."}, null);
	}
	else if(((fromValues != undefined) && (fromValues != parseInt(fromValues))) || ((toValues != undefined) && (toValues != parseInt(toValues)))){
		callback({Error : "From/To value must be integer."}, null);
	}
	else if (parseFloat(fromValues) > parseFloat(toValues))
	{
		callback({Error : "Your upper limit must be a higher value than your lower limit."}, null);
	}
	else{
	
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			try{
				if(err){
					callback(err, null);
				}
				else{
					var valuesQuery='';
					if ((fromValues != undefined) && (toValues != undefined)){
						valuesQuery = '{ "range": { "Length": { "gte": "'+fromValues+'", "lte": "'+toValues+'" } } }'
					}
					else if ((fromValues != undefined) && (toValues == undefined)){	
						valuesQuery = '{ "range": { "Length": { "gte": "'+fromValues+'" } } }'
					}
					else if ((fromValues == undefined) && (toValues != undefined)){	
						valuesQuery = '{ "range": { "Length": { "lte": "'+toValues+'" } } }'
					}
					
					try{
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						if (valuesQuery != '' ){ queryConcat = queryConcat +', '+valuesQuery; }
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);

					}catch (err){
						callback(err, null);
					}
				
				}
			}catch (err){
				callback(err, null);
			}
		});
	}
}
/******************************************************************************
Function: getQueryByUndergroundStoreys
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for underground storeys. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByUndergroundStoreys = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var fromValues = requireQuery.query.From;
	var toValues = requireQuery.query.To;
	
	if ((fromValues == undefined) && (toValues == undefined)) {
		callback({Error:"Parameter From/To Values is Missing"}, null);
	}
	else if ((parseFloat(fromValues) < 0) || (parseFloat(toValues) < 0)){
		callback({Error : "From/To Value must be positive."}, null);
	}
	else if(((fromValues != undefined) && (fromValues != parseInt(fromValues))) || ((toValues != undefined) && (toValues != parseInt(toValues)))){
		callback({Error : "From/To value must be integer."}, null);
	}
	else if (parseFloat(fromValues) > parseFloat(toValues))
	{
		callback({Error : "Your upper limit must be a higher value than your lower limit."}, null);
	}
	else{

		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			try{
				if(err){
					callback(err, null);
				}
				else{
					var valuesQuery='';
					if ((fromValues != undefined) && (toValues != undefined)){
						valuesQuery = '{ "range": { "UndergroundStoreys": { "gte": "'+fromValues+'", "lte": "'+toValues+'" } } }'
					}
					else if ((fromValues != undefined) && (toValues == undefined)){	
						valuesQuery = '{ "range": { "UndergroundStoreys": { "gte": "'+fromValues+'" } } }'
					}
					else if ((fromValues == undefined) && (toValues != undefined)){	
						valuesQuery = '{ "range": { "UndergroundStoreys": { "lte": "'+toValues+'" } } }'
					}
					
					try{
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						if (valuesQuery != '' ){ queryConcat = queryConcat +', '+valuesQuery; }
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);

					}catch (err){
						callback(err, null);
					}
				
				}
			}catch (err){
				callback(err, null);
			}
		});
	}
}
/******************************************************************************
Function: getQueryByStoreys
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for storeys. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByStoreys = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var fromValues = requireQuery.query.From;
	var toValues = requireQuery.query.To;
	
	if ((fromValues == undefined) && (toValues == undefined)) {
		callback({Error:"Parameter From/To Values is Missing"}, null);
	}
	else if ((parseFloat(fromValues) < 0) || (parseFloat(toValues) < 0)){
		callback({Error : "From/To Value must be positive."}, null);
	}
	else if(((fromValues != undefined) && (fromValues != parseInt(fromValues))) || ((toValues != undefined) && (toValues != parseInt(toValues)))){
		callback({Error : "From/To value must be integer."}, null);
	}
	else if (parseFloat(fromValues) > parseFloat(toValues))
	{
		callback({Error : "Your upper limit must be a higher value than your lower limit."}, null);
	}
	else{
	
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			try{
				if(err){
					callback(err, null);
				}
				else{
					var valuesQuery='';
					if ((fromValues != undefined) && (toValues != undefined)){
						valuesQuery = '{ "range": { "Storeys": { "gte": "'+fromValues+'", "lte": "'+toValues+'" } } }'
					}
					else if ((fromValues != undefined) && (toValues == undefined)){	
						valuesQuery = '{ "range": { "Storeys": { "gte": "'+fromValues+'" } } }'
					}
					else if ((fromValues == undefined) && (toValues != undefined)){	
						valuesQuery = '{ "range": { "Storeys": { "lte": "'+toValues+'" } } }'
					}
					
					try{
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						if (valuesQuery != '' ){ queryConcat = queryConcat +', '+valuesQuery; }
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);

					}catch (err){
						callback(err, null);
					}
				
				}
			}catch (err){
				callback(err, null);
			}
		});
	}
}
/******************************************************************************
Function: getQueryByFloorArea
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for floor area. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByFloorArea = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var fromValues = requireQuery.query.From;
	var toValues = requireQuery.query.To;
	if ((fromValues == undefined) && (toValues == undefined)) {
		callback({Error:"Parameter From/To Values is Missing"}, null);
	}
	else if ((parseFloat(fromValues) < 0) || (parseFloat(toValues) < 0)){
		callback({Error : "From/To Value must be positive."}, null);
	}
	else if(((fromValues != undefined) && (fromValues != parseInt(fromValues))) || ((toValues != undefined) && (toValues != parseInt(toValues)))){
		callback({Error : "From/To value must be integer."}, null);
	}
	else if (parseFloat(fromValues) > parseFloat(toValues))
	{
		callback({Error : "Your upper limit must be a higher value than your lower limit."}, null);
	}
	else{
	
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			try{
				if(err){
					callback(err, null);
				}
				else{
					var valuesQuery='';
					if ((fromValues != undefined) && (toValues != undefined)){
						valuesQuery = '{ "range": { "FloorArea": { "gte": "'+fromValues+'", "lte": "'+toValues+'" } } }'
					}
					else if ((fromValues != undefined) && (toValues == undefined)){	
						valuesQuery = '{ "range": { "FloorArea": { "gte": "'+fromValues+'" } } }'
					}
					else if ((fromValues == undefined) && (toValues != undefined)){	
						valuesQuery = '{ "range": { "FloorArea": { "lte": "'+toValues+'" } } }'
					}
					
					try{
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						if (valuesQuery != '' ){ queryConcat = queryConcat +', '+valuesQuery; }
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);

					}catch (err){
						callback(err, null);
					}
				
				}
			}catch (err){
				callback(err, null);
			}
		});
	}
}

/******************************************************************************
Function: getQueryBySiteArea
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for site area. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryBySiteArea = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var fromValues = requireQuery.query.From;
	var toValues = requireQuery.query.To;
	
	var fromSiteValue = /^\s*([0-9\.]+)\s*$/g.exec(fromValues);
	var ToSiteValue = /^\s*([0-9\.]+)\s*$/g.exec(toValues);
	
	if ((fromValues == undefined) && (toValues == undefined)) {
		callback({Error:"Parameter From/To Values is Missing"}, null);
	}
	else if (parseFloat(fromValues) > parseFloat(toValues))
	{
		callback({Error : "Your upper limit must be a higher value than your lower limit."}, null);
	}
	else if(((fromValues != undefined) && (fromSiteValue == null)) || ((toValues != undefined) && (ToSiteValue == null))){
		callback({Error:"Parameter From/To Values is invalid"}, null);
	}
	else if ((parseFloat(fromValues) < 0) || (parseFloat(toValues) < 0)){
		callback({Error : "From/To Value must be positive."}, null);
	}
	else{
	
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			try{
				
				if(err){
					callback(err, null);
				}
				else{
					var valuesQuery='';
					if ((fromValues != undefined) && (toValues != undefined)){
						valuesQuery = '{ "range": { "SiteArea": { "gte": "'+fromValues+'", "lte": "'+toValues+'" } } }'
					}
					else if ((fromValues != undefined) && (toValues == undefined)){	
						valuesQuery = '{ "range": { "SiteArea": { "gte": "'+fromValues+'" } } }'
					}
					else if ((fromValues == undefined) && (toValues != undefined)){	
						valuesQuery = '{ "range": { "SiteArea": { "lte": "'+toValues+'" } } }'
					}
					
					try{
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						if (valuesQuery != '' ){ queryConcat = queryConcat +', '+valuesQuery; }
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);

					}catch (err){
						callback(err, null);
					}
				
				}
			}catch (err){
				callback(err, null);
			}
		});
	}
}

