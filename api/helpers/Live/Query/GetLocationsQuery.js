'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetLocationsQuery.js) is used for form a elastic query from subscription Json.
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryByRadius
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Radius,Latitude and Longitude of a location. 
	2. 'getSubscriptionData' returns location and sector based on subscription json.
	3. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByRadius = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var radiusValues = requireQuery.query.Radius;
	var latValues = requireQuery.query.Lat;
	var longValues = requireQuery.query.Lon;
	
	var unit;
	var sectorData = [];
	if (radiusValues != undefined){
		var unitRegex = /[\d]+((?:mi|km|m))$/g;
		unit = unitRegex.exec(radiusValues.trim());
	}
	
	if ((radiusValues == undefined) || (latValues == undefined) || (longValues == undefined)) {
		callback({Error:"Parameter is Missing, Required Parameters is Radius,Lat,Long"}, null);
	}
	else if(unit == null){
		callback({Error:"Radius unit must be mi or km."}, null);
	}
	else{
		for (let sectorLevel of jsonObject.Subscription.ProjectSectors) {
			var sector =  sectorLevel.Sector;
			
			if (sector != ""){
				
				sectorData.push(sector.toLowerCase());
			}
		}
		
		var queryBuildSector={};
		queryBuildSector.PrimaryAndSecondarySectorParents=sectorData;
		var queryFieldSector='{"terms": %s}';
		var PrimaryAndSecondarySectorParents = exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
		
		try{
			
			getSubscriptionData(subscriptionJSON, function(err, subscribedLocation, subscribedSector){
				if(err){
					callback(err, null);
				}else{
					var subscribedLocationQuery = '{"bool": {"should":'+subscribedLocation+'}}';
					var FinalQuery,radiusQuery;
					
					if (subscribedLocation != ''){
						radiusQuery = '{"bool": { "should": [ { "geo_distance": { "distance": "'+radiusValues+'", "ProjectLocation": { "lat": '+latValues+', "lon": '+longValues+' } } } ] } }, '+subscribedLocationQuery;
					}
					else{
						radiusQuery = '{"bool": { "should": [ { "geo_distance": { "distance": "'+radiusValues+'", "ProjectLocation": { "lat": '+latValues+', "lon": '+longValues+' } } } ] } }';
					}
					var sectorQuery = '{"bool": {"should": [ '+PrimaryAndSecondarySectorParents+' ]}}'
					
					try{
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						if (sectorData.length > 0){
							FinalQuery='{"query": {"bool": {"filter": ['+radiusQuery+','+sectorQuery+','+projectHistories+']}}, '+sortBy+'}';
							callback(null, FinalQuery);
						}else{
							FinalQuery='{"query": {"bool": {"filter": ['+radiusQuery+','+projectHistories+']}},'+sortBy+'}';
							callback(null, FinalQuery);
						}
						
					}catch (err){
						
						callback(err, null);
					}
				}
			});
		}
		catch(err){
			// console.log("err"+err);
			callback(err, null);
		}
	}
}
/******************************************************************************
Function: getQueryByPostcode
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for postcode district. 
	2. 'getLocationsTownsPostcodes' returns authorized postcode from metadata.
	3. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByPostcode = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var postcodeValues = requireQuery.query.PostCodeDistricts;
	
	var sectorData = getSectorData(jsonObject);
	
	if (postcodeValues == undefined) {
		callback({Error:"Parameter PostCodeDistricts is Missing"}, null);
	}
	else{
		
		exportedRequiredModules.getMetadataObj.getLocationsTownsPostcodes(jsonObject, "Postcode", function(err, authorizedPostcodes, locationContent, townContent){
			if(err){
				callback(err, null);
			}else{
				var postcodeLocation = [];
				postcodeValues.split(',').forEach(function (postcode){
					postcodeLocation.push(postcode.toLowerCase());
				});
				
				var notSubscribedPostcodes  = exportedRequiredModules.arrayDiff(postcodeLocation, authorizedPostcodes);
							
				if(notSubscribedPostcodes.length > 0)
				{
					callback({Error: 'The requested Postcode "'+notSubscribedPostcodes+'" is invalid/not authorized.'},null);
				}
				else{
					
					try{
						var postcodeList = postcodeLocation.map(function(x){ return x.toUpperCase()});
						var queryBuildPostcode={};
						var queryBuildSector={};
							
						queryBuildPostcode.ProjectPostCodeDistrict=postcodeList;
						queryBuildSector.PrimaryAndSecondarySectorParents=sectorData;

						var queryFieldPostcode='{"terms": %s}';
						var queryFieldSector='{"terms": %s}';

						var ProjectPostCodeDistrict = exportedRequiredModules.convertComma2Array(queryFieldPostcode, JSON.stringify(queryBuildPostcode)); 
						var PrimaryAndSecondarySectorParents = exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
						
						var FinalQuery ='';
						
						var postcodeQuery = '{"bool": {"should": [ '+ProjectPostCodeDistrict+' ]}}'
						var sectorQuery = '{"bool": {"should": [ '+PrimaryAndSecondarySectorParents+' ]}}'
						
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						if (sectorData.length > 0){
							FinalQuery='{"query": {"bool": {"filter": ['+postcodeQuery+','+sectorQuery+','+projectHistories+']}}, '+sortBy+'}';
							callback(null, FinalQuery);
						}else{
							FinalQuery='{"query": {"bool": {"filter": ['+postcodeQuery+','+projectHistories+']}},'+sortBy+'}';
							callback(null, FinalQuery);
						}
					}catch (err){
						callback(err, null);
					}
				}
			}
		});
	}
}

/******************************************************************************
Function: getQueryByTown
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for town. 
	2. 'getLocationsTownsPostcodes' returns authorized towns from metadata.
	3. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByTown = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	
	var jsonObject = JSON.parse(subscriptionJSON);
	var townValues = requireQuery.query.TownNames;
	
	var sectorData = getSectorData(jsonObject);
	
	if (townValues == undefined) {
		callback({Error:"Parameter TownNames is Missing"}, null);
	}
	else{
		
		// exportedRequiredModules.getMetadataObj.getLocationsTownsPostcodes(jsonObject, function(err, authorizedRegions,authorizedCounties,authorizedTowns,authorizedPostcodes, locationContent, townContent){
		exportedRequiredModules.getMetadataObj.getLocationsTownsPostcodes(jsonObject, "Town", function(err, authorizedTowns, locationContent, townContent){
			// console.log("locationContent"+locationContent);
			if(err){
				callback(err, null);
			}else{
				var locationLevel3 = [];
				townValues.split(',').forEach(function (town){
					locationLevel3.push(town.toLowerCase());
				});
				// console.log("authorizedTowns"+authorizedTowns);
				var notSubscribedTowns  = exportedRequiredModules.arrayDiff(locationLevel3, authorizedTowns);
				
				if(notSubscribedTowns.length > 0)
				{
					callback({Error: 'The requested Towns "'+notSubscribedTowns+'" is invalid/not authorized.'}, null);
				}
				else{
					
					var queryBuildLevel3={};
					var queryBuildSector={};
					// console.log("locationContent"+locationContent);
					exportedRequiredModules.getMetadataObj.townMetadataExtraction(locationLevel3, locationContent, townContent, function(err,locationList){
						if(err){
							callback(err, null);
						}else{
							try{
								queryBuildLevel3.ProjectLocationLevel3Facet=locationList;
								queryBuildSector.PrimaryAndSecondarySectorParents=sectorData;

								var queryFieldLevel3='{"terms": %s}';
								var queryFieldSector='{"terms": %s}';

								var ProjectLocationLevel3Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel3, JSON.stringify(queryBuildLevel3)); 
								var PrimaryAndSecondarySectorParents = exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
								
								var locationQuery = '';
								var FinalQuery ='';
								
								locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel3Facet+' ]}}'
								var sectorQuery = '{"bool": {"should": [ '+PrimaryAndSecondarySectorParents+' ]}}'
								
								var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
								var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
							
								if (sectorData.length > 0){
									FinalQuery='{"query": {"bool": {"filter": ['+locationQuery+','+sectorQuery+','+projectHistories+']}}, '+sortBy+'}';
									callback(null, FinalQuery);
								}else{
									FinalQuery='{"query": {"bool": {"filter": ['+locationQuery+','+projectHistories+']}},'+sortBy+'}';
									callback(null, FinalQuery);
								}
							}catch (err){
								callback(err, null);
							}
						}
					});
				}
			}
		});
	}
}
/******************************************************************************
Function: getQueryByCounty
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for county. 
	2. 'getLocationsTownsPostcodes' returns authorized counties from metadata.
	3. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByCounty = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var countyValues = requireQuery.query.CountyNames;
	
	var sectorData = getSectorData(jsonObject);
	
	if (countyValues == undefined) {
		callback({Error:"Parameter CountyNames is Missing"},null);
	}
	else{
		
		exportedRequiredModules.getMetadataObj.getLocationsTownsPostcodes(jsonObject, "County", function(err, authorizedCounties, locationContent, townContent){
			
			if(err){
				callback(err, null);
			}else{
				var locationLevel2 = [];
				countyValues.split(',').forEach(function (county){
					locationLevel2.push(county.toLowerCase());
				});
				
				var notSubscribedCounties  = exportedRequiredModules.arrayDiff(locationLevel2, authorizedCounties);
				
				if(notSubscribedCounties.length > 0)
				{
					callback({Error: 'The requested Counties "'+notSubscribedCounties+'" is invalid/not authorized.'}, null);
				}
				else{
					
					var queryBuildLevel2={};
					var queryBuildSector={};
					exportedRequiredModules.getMetadataObj.locationMetadataExtraction(locationLevel2, locationContent, function(err,locationList){
						if(err){
							callback(err, null);
						}else{
							try{
								queryBuildLevel2.ProjectLocationLevel2Facet=locationList;
								queryBuildSector.PrimaryAndSecondarySectorParents=sectorData;

								var queryFieldLevel2='{"terms": %s}';
								var queryFieldSector='{"terms": %s}';

								var ProjectLocationLevel2Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel2, JSON.stringify(queryBuildLevel2)); 
								var PrimaryAndSecondarySectorParents = exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
								
								var locationQuery = '';
								var FinalQuery ='';
								
								locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel2Facet+' ]}}'
								var sectorQuery = '{"bool": {"should": [ '+PrimaryAndSecondarySectorParents+' ]}}'
								
								
								var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
								var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
								if (sectorData.length > 0){
									FinalQuery='{"query": {"bool": {"filter": ['+locationQuery+','+sectorQuery+','+projectHistories+']}}, '+sortBy+'}';
									callback(null, FinalQuery);
								}else{
									FinalQuery='{"query": {"bool": {"filter": ['+locationQuery+','+projectHistories+']}},'+sortBy+'}';
									callback(null, FinalQuery);
								}
								
							}catch (err){
								callback(err, null);
							}
						}
					});
				}
			}
		});
	}
}
/******************************************************************************
Function: getQueryByRegions
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for region. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByRegions = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var regionValues = requireQuery.query.RegionNames;
	
	if (regionValues == undefined) {
		callback({Error:"Parameter RegionNames is Missing"}, null);
	}
	else{

		var queryBuildLevel1 = {};
		var queryBuildLevel2={};
		var queryBuildSector={};
		var requestedRegions = [];
		var locationLevel1 = [];
		var locationLevel2 = [];
		var authorizedRegion = [];
		var sectorData = getSectorData(jsonObject);
		var locationQuery,FinalQuery;
		regionValues = regionValues.replace('{','');
		regionValues = regionValues.replace('}','');
		regionValues.split(',').forEach(function (region){
			requestedRegions.push(region.toLowerCase());
		});
		
		for (let level1 of jsonObject.Subscription.ProjectRegions) {
			var region =  level1.Region;
			var county =  level1.Counties;
			if (requestedRegions.includes(region.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
				if (county != ""){
					exportedRequiredModules._.each(county, function ( instance){
						var concatRegion = region+"#"+instance
						locationLevel2.push(concatRegion.toLowerCase());
					});
				}
				else{
					locationLevel1.push(region.toLowerCase());
				}
			}
			authorizedRegion.push(region.toLowerCase().replace(', ',' and ').replace('&','and'));
		}
		
		var notSubscribedRegions  = exportedRequiredModules.arrayDiff(requestedRegions, authorizedRegion);
		
		if(notSubscribedRegions.length >  0)
		{
			callback({Error: 'The requested region "'+notSubscribedRegions+'" is invalid/not authorized.'}, null);
		}
		else{
			
			queryBuildLevel1.ProjectLocationLevel1Facet=locationLevel1;
			queryBuildLevel2.ProjectLocationLevel2Facet=locationLevel2;
			queryBuildSector.PrimaryAndSecondarySectorParents=sectorData;

			var queryFieldLevel1='{"terms": %s}';
			var queryFieldLevel2='{"terms": %s}';
			var queryFieldSector='{"terms": %s}';

			var ProjectLocationLevel1Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel1, JSON.stringify(queryBuildLevel1)); 
			var ProjectLocationLevel2Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel2, JSON.stringify(queryBuildLevel2)); 
			var PrimaryAndSecondarySectorParents = exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 

			if ((locationLevel2.length != 0) && (locationLevel1.length != 0)){
				locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel1Facet+','+ProjectLocationLevel2Facet+' ]}}'
			}
			else if ((locationLevel2.length != 0) && (locationLevel1.length == 0)){
				locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel2Facet+' ]}}'
			}
			else if (locationLevel1.length != 0){
				locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel1Facet+' ]}}'
			}

			var sectorQuery = '{"bool": {"should": [ '+PrimaryAndSecondarySectorParents+' ]}}'
			
			try{
				var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
				var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
			
				if (sectorData.length > 0){
					FinalQuery='{"query": {"bool": {"filter": ['+locationQuery+','+sectorQuery+','+projectHistories+']}}, '+sortBy+'}';
					callback(null, FinalQuery);
				}else{
					FinalQuery='{"query": {"bool": {"filter": ['+locationQuery+','+projectHistories+']}},'+sortBy+'}';
					callback(null, FinalQuery);
				}
				
			}catch (err){
				callback(err, null);
			}
		}
	}
}
/******************************************************************************
Function: getQueryByLocations
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for given location. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByLocations = function (subscriptionJSON, requireQuery, callback) {
	
	try{
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

		var jsonObject = JSON.parse(subscriptionJSON);
		var Pid = requireQuery.query.Pid;
		
		var locationLevel1 = [];
		var locationLevel2 = [];
		var sectorData = getSectorData(jsonObject);
		
		for (let level1 of jsonObject.Subscription.ProjectRegions) {
			var region =  level1.Region;
			var county =  level1.Counties;
			
			if (county != ""){
				
				exportedRequiredModules._.each(county, function ( instance){
					var concatRegion = region+"#"+instance
					locationLevel2.push(concatRegion.toLowerCase());
				});
			}
			else{
				locationLevel1.push(region.toLowerCase());
			}
		}
		
		var queryBuildLevel1={};
		var queryBuildLevel2={};
		var queryBuildSector={};

		queryBuildLevel1.ProjectLocationLevel1Facet=locationLevel1;
		queryBuildLevel2.ProjectLocationLevel2Facet=locationLevel2;
		queryBuildSector.PrimaryAndSecondarySectorParents=sectorData;

		var queryFieldLevel1='{"terms": %s}';
		var queryFieldLevel2='{"terms": %s}';
		var queryFieldSector='{"terms": %s}';

		var ProjectLocationLevel1Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel1, JSON.stringify(queryBuildLevel1)); 
		var ProjectLocationLevel2Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel2, JSON.stringify(queryBuildLevel2)); 
		var PrimaryAndSecondarySectorParents = exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 

		var locationQuery = '';
		var FinalQuery ='';
		
		if ((locationLevel2.length != 0) && (locationLevel1.length != 0)){
			locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel1Facet+','+ProjectLocationLevel2Facet+' ]}}'
		}
		else if ((locationLevel2.length != 0) && (locationLevel1.length == 0)){
			locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel2Facet+' ]}}'
		}
		else if (locationLevel1.length != 0){
			locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel1Facet+' ]}}'
		}

		var sectorQuery = '{"bool": {"should": [ '+PrimaryAndSecondarySectorParents+' ]}}'
		
		var projectHistories = exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
		
		var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
		if (Pid != undefined ){
			var pidQuery = '{"bool": {"must": [ {"terms": {"ProjectId":["'+Pid+'"]}} ]}}'
			if (sectorData.length > 0){
				FinalQuery='{"query": {"bool": {"filter": ['+pidQuery+','+locationQuery+','+sectorQuery+']}}, '+sortBy+'}';
				// console.log("FinalQuery",FinalQuery)
				callback(null, FinalQuery);
			}else{
				FinalQuery='{"query": {"bool": {"filter": ['+pidQuery+','+locationQuery+']}},'+sortBy+'}';
				// console.log("FinalQuery",FinalQuery)
				callback(null, FinalQuery);
			}
		}else{
			if (sectorData.length > 0){
				FinalQuery='{"query": {"bool": {"filter": ['+locationQuery+','+sectorQuery+','+projectHistories+']}}, '+sortBy+'}';
				callback(null, FinalQuery);
			}else{
				FinalQuery='{"query": {"bool": {"filter": ['+locationQuery+','+projectHistories+']}},'+sortBy+'}';
				callback(null, FinalQuery);
			}
		}
		
		
	}catch (err){
		console.log("err",err)
		if(err.includes("Error") == true) {
			callback(err, null);}
		else { 
			callback({Error : err}, null);
		}
	}
}
/******************************************************************************
Function: getSubscriptionData
Argument:subscription
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for given subscribed location and sector. 
*******************************************************************************/
function getSubscriptionData (subscription, callback) {
	try{
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
		var object = JSON.parse(subscription);
		
		var locationLevel1 = [];
		var locationLevel2 = [];
		var sectorData = [];
		
		for (let level1 of object.Subscription.ProjectRegions) {
			var region =  level1.Region;
			var county =  level1.Counties;
			if (county != ""){
				exportedRequiredModules._.each(county, function ( instance){
					var concatRegion = region+"#"+instance
					locationLevel2.push(concatRegion.toLowerCase());
				});
			}
			else{
				locationLevel1.push(region.toLowerCase());
			}
		}
		
		for (let sectorLevel of object.Subscription.ProjectSectors) {
			var sector =  sectorLevel.Sector;
			if (sector != ""){
				sectorData.push(sector.toLowerCase());
			}
			
		}
		
		var queryBuildLevel1={};
		var queryBuildLevel2={};
		var queryBuildSector={};

		queryBuildLevel1.ProjectLocationLevel1Facet=locationLevel1;
		queryBuildLevel2.ProjectLocationLevel2Facet=locationLevel2;
		queryBuildSector.PrimaryAndSecondarySectorParents=sectorData;

		var queryFieldLevel1='{"terms": %s}';
		var queryFieldLevel2='{"terms": %s}';
		var queryFieldSector='{"terms": %s}';

		var ProjectLocationLevel1Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel1, JSON.stringify(queryBuildLevel1)); 
		var ProjectLocationLevel2Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel2, JSON.stringify(queryBuildLevel2)); 
		var PrimaryAndSecondarySectorParents = exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
		var locationQuery,sectorQuery;
		if (locationLevel2.length > 0){
			locationQuery = '[ '+ProjectLocationLevel1Facet+','+ProjectLocationLevel2Facet+' ]';
		}
		else if (locationLevel1.length > 0){
			locationQuery = '[ '+ProjectLocationLevel1Facet+' ]';
		}
		if (sectorData.length > 0){
			sectorQuery = '[ '+PrimaryAndSecondarySectorParents+' ]';
		}	
		
		callback(null,locationQuery,sectorQuery);
	}catch(err){
		callback({Error: "Exception in Subscription JSON parser"}, null, null);
	}
}
/******************************************************************************
Function: getSectorData
Argument:jsonObject
Return: Array
Usage:
	1. Forms an array of subscribed sectors.
*******************************************************************************/
function getSectorData(jsonObject){
	var sectorData = [];
	for (let sectorLevel of jsonObject.Subscription.ProjectSectors) {
		var sector =  sectorLevel.Sector;
		if (sector != ""){
			sectorData.push(sector.toLowerCase());
		}
	}
	return sectorData;
}