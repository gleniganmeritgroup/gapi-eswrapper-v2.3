'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetCompanyContactQuery.js) is used form a query for given Company/Contact Ids .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getUniqueValues
Argument: Array
Return: Array
Usage:
    1. To get unique values from an array values
*******************************************************************************/
function getUniqueValues(arrayValues) {
    var uniqueValues = arrayValues.filter(function(elem, index, self) {
        return index === self.indexOf(elem);
    });
    return uniqueValues;
}

/******************************************************************************
Function: replaceNonFilterOption
Argument: query,type1,type2,type3
Return: query response
Usage:
    1. Replace braces,comma for query with no filter.
*******************************************************************************/
function replaceNonFilterOption(query,type1,type2,type3){
    query = query.replace(type1, ',');
    query = query.replace(type2, '{');
    query = query.replace(type3, '}');
    return query;
}

/******************************************************************************
Function: replaceUnauthorizedLocationValues
Argument: unauthorizedValues, beSpokeQuery
Return: query response
Usage:
    1. Replace unauthorized location in query.
*******************************************************************************/
function replaceUnauthorizedLocationValues(unauthorizedValues, beSpokeQuery){
  
    unauthorizedValues.forEach(function(values){
        var regexForRemoveValuesType1 = new RegExp('\\{\\s*\\"term\\"\\:\\s*\\{\\s*\\"[^\\"]*?LocationLevel[\\d]+Facet\\"\\:\\s*"[^\\"]*?'+values+'[^\\"]*?\\"\\s*\\}\\s*\\}\\s*\\,','ig');
        var regexForRemoveValuesType2 = new RegExp('\\,\\s*\\{\\s*\\"term\\"\\:\\s*\\{\\s*\\"[^\\"]*?LocationLevel[\\d]+Facet\\"\\:\\s*"[^\\"]*?'+values+'[^\\"]*?\\"\\s*\\}\\s*\\}\\s*\\]','ig');
        var regexForRemoveValuesType3 = new RegExp('\\[\\s*\\{\\s*\\"term\\"\\:\\s*\\{\\s*\\"[^\\"]*?LocationLevel[\\d]+Facet\\"\\:\\s*"[^\\"]*?'+values+'[^\\"]*?\\"\\s*\\}\\s*\\}\\s*\\]','ig');
        var regexForRemoveValuesInListType1 = new RegExp('\\"[^\\"]*?'+values+'[^\\"]*?\\"\\s*\\,','ig');
        var regexForRemoveValuesInListType2 = new RegExp('\\,\\s*\\"[^\\"]*?'+values+'[^\\"]*?\\"\\s*\\]','ig');
        var regexForRemoveValuesInListType3 = new RegExp('\\[\\s*\\"[^\\"]*?'+values+'[^\\"]*?\\"\\s*\\]','ig');
        var regexForRemoveEmptyListType1 = new RegExp('\\{\\s*\\"terms\\"\\:\\s*\\{\\s*\\"[^\\"]*?LocationLevel[\\d]+Facet\\"\\:\\s*\\[\\]\\s*}\\s*\\}\\,','ig');
        var regexForRemoveEmptyListType2 = new RegExp('\\,\\s*\\{\\s*\\"terms\\"\\:\\s*\\{\\s*\\"[^\\"]*?LocationLevel[\\d]+Facet\\"\\:\\s*\\[\\]\\s*\\}\\s*}\\s*\\]','ig');
        var regexForRemoveEmptyListType3 = new RegExp('\\[\\s*\\{\\s*\\"terms\\"\\:\\s*\\{\\s*\\"[^\\"]*?LocationLevel[\\d]+Facet\\"\\:\\s*\\[\\]\\s*\\}\\s*}\\s*\\]','ig');
        beSpokeQuery = replaceForJsonFormat(beSpokeQuery,regexForRemoveValuesType1,regexForRemoveValuesType2,regexForRemoveValuesType3);
        beSpokeQuery = replaceForJsonFormat(beSpokeQuery,regexForRemoveValuesInListType1,regexForRemoveValuesInListType2,regexForRemoveValuesInListType3);
        beSpokeQuery = replaceForJsonFormat(beSpokeQuery,regexForRemoveEmptyListType1,regexForRemoveEmptyListType2,regexForRemoveEmptyListType3);
    });
    
    return beSpokeQuery;
}

/******************************************************************************
Function: replaceForJsonFormat
Argument: query,type1,type2,type3
Return: query response
Usage:
    1. Replace square brackets for query with filter.
*******************************************************************************/
function replaceForJsonFormat(query,type1,type2,type3){
    query = query.replace(type1, '');
    query = query.replace(type2, ']');
    query = query.replace(type3, '[]');
    return query;
}

/******************************************************************************
Function: replaceEmptyBool
Argument: beSpokeQuery
Return: query response
Usage:
    1. Replace empty bool in query if filter is present.
*******************************************************************************/
function replaceEmptyBool(beSpokeQuery){

    var regexForRemoveEmptyListType1 = new RegExp('\\{\\"bool\\"\\s*\\:\\s*\\{\\"should\\"\\s*\\:\\s*\\[\\s*\\]\\}\\}\\s*\\,','ig');
    var regexForRemoveEmptyListType2 = new RegExp('\\,\\s*\\{\\"bool\\"\\s*\\:\\s*\\{\\"should\\"\\s*\\:\\s*\\[\\s*\\]\\}\\}\\s*\\]','ig');
    var regexForRemoveEmptyListType3 = new RegExp('\\[\\s*\\{\\"bool\\"\\s*\\:\\s*\\{\\"should\\"\\s*\\:\\s*\\[\\s*\\]\\}\\}\\s*\\]','ig');
    beSpokeQuery = replaceForJsonFormat(beSpokeQuery,regexForRemoveEmptyListType1,regexForRemoveEmptyListType2,regexForRemoveEmptyListType3);
    return beSpokeQuery;
}
/******************************************************************************
Function: replaceEmptyBoolForNonFilterOption
Argument: beSpokeQuery
Return: query response
Usage:
    1. Replace empty bool in query if no filter is present.
*******************************************************************************/
function replaceEmptyBoolForNonFilterOption(beSpokeQuery){

    var regexForRemoveEmptyListType1 = new RegExp('\\,\\s*\\"(?:should|must)\\"\\s*\\:\\s*\\[\\s*\\]\\s*\\,','ig');
    var regexForRemoveEmptyListType2 = new RegExp('\\{\\s*\\"(?:should|must)\\"\\s*\\:\\s*\\[\\s*\\]\\s*\\,','ig');
    var regexForRemoveEmptyListType3 = new RegExp('\\,\\s*\\"(?:should|must)\\"\\s*\\:\\s*\\[\\s*\\]\\s*\\}','ig');
    beSpokeQuery = replaceNonFilterOption(beSpokeQuery,regexForRemoveEmptyListType1,regexForRemoveEmptyListType2,regexForRemoveEmptyListType3);
    return beSpokeQuery;
}

/******************************************************************************
Function: getQueryByOfficeID
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Company by Id. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByOfficeID = function (requireQuery, callback) {
	// console.log("historyEventValues");
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	var IdValues = requireQuery.query.Ids;
	var IdValuesList = [];
	// console.log("historyEventValues"+historyEventValues);
	if (IdValues == undefined){
		callback({Error : "Parameter Ids is missing"}, null)
	}
	else{
		if (IdValues != undefined){
			IdValues.split(',').forEach(function(IdValues){
				IdValuesList.push(IdValues.toLowerCase());
			});  
		}
		var idStringRegex = /([A-Za-z]+)/g;
		var idStringValue = idStringRegex.exec(IdValuesList);
		// console.log("idStringValue"+idStringValue);
		if (idStringValue != null){
			callback({Error : "Parameter Ids should not contains string"}, null)
		}
		else if(IdValuesList.length > 100){
			callback({Error : "Parameter Ids should not greater than 100"}, null)
		}else{
		var queryBuildOfficeID={};
		try{
			queryBuildOfficeID.OfficeId=IdValuesList;
			
			var queryFieldOfficeID='{"terms": %s}';
			var OfficeId = exportedRequiredModules.convertComma2Array(queryFieldOfficeID, JSON.stringify(queryBuildOfficeID)); 
			
			// var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
			// var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
			
			// var queryConcat=projectHistories
			// if (OfficeId != ''){ queryConcat = queryConcat +', '+OfficeId; }
			var sortBy = SortByConditionCompany(exportedRequiredModules,requireQuery);
			var FinalQuery='{"query": {"bool": {"filter": ['+OfficeId+',{"bool": {"must_not": [{"term": {"OfficeWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"OfficeWorkingOnSmallProjects": "true"}},{"term": {"Publish": "false"}}]}}]}}, '+sortBy+'}';
			
			callback(null, FinalQuery);
		}catch (err){
			callback(err, null);
		}
	}
	}
}
/******************************************************************************
Function: getQueryByContactID
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching Contact by Id. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByContactID = function (requireQuery, callback) {
	// console.log("historyEventValues");
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	var IdValues = requireQuery.query.Ids;
	var IdValuesList = [];
	// console.log("historyEventValues"+historyEventValues);
	if (IdValues == undefined){
		callback({Error : "Parameter Ids is missing"})
	}
	else{
		if (IdValues != undefined){
			IdValues.split(',').forEach(function(IdValues){
				IdValuesList.push(IdValues.toLowerCase());
			});  
		}
		if(IdValuesList.length > 100){
			callback({Error : "Parameter Ids should not greater than 100"}, null)
		}else{
		var queryBuildContactID={};

		queryBuildContactID.ContactId=IdValuesList;
		try{
			var queryFieldContactID='{"terms": %s}';
			var ContactId = exportedRequiredModules.convertComma2Array(queryFieldContactID, JSON.stringify(queryBuildContactID)); 
			
			// var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
			// var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
			var sortBy = SortByConditionContact(exportedRequiredModules,requireQuery);
			// var queryConcat=projectHistories
			// if (OfficeId != ''){ queryConcat = queryConcat +', '+OfficeId; }
			
			var FinalQuery='{"query": {"bool": {"filter": ['+ContactId+',{"bool": {"must_not": [{"term": {"ContactWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"ContactWorkingOnSmallProjects": "true"}}]}}]}}, '+sortBy+'}';
			callback(null, FinalQuery);
		}catch (err){
			callback(err, null);
		}
	}
	}
}
/******************************************************************************
Function: companyQueryValidation
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for company search. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.companyQueryValidation = function (requireQuery, callback){
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	var radiusValues = requireQuery.query.Radius;
	var latValues = requireQuery.query.Lat;
	var longValues = requireQuery.query.Lon;
	
	var beSpokeQuery;
	try{
		if (typeof requireQuery.body === 'string' || requireQuery.body instanceof String){
			beSpokeQuery = JSON.parse(requireQuery.body);
		}else{
			beSpokeQuery = requireQuery.body;
		}
		
	}catch(error){
		beSpokeQuery = '';
	}
	
	var unit,radiusSearch;
	
	if (radiusValues != undefined){
		var unitRegex = /[\d]+((?:mi|km|m))$/g;
		unit = unitRegex.exec(radiusValues.trim());
	}
	
	if ((radiusValues == undefined) && (latValues == undefined) && (longValues == undefined)) {
        radiusSearch = false;
    }
    if(beSpokeQuery == ''){
		callback({Error:"Invalid query"}, null);
	}
	else if (((radiusValues == undefined) || (latValues == undefined) || (longValues == undefined)) && (radiusSearch != false)) {
		callback({Error:"Parameter is Missing, Required Parameters is Radius,Lat,Long"}, null);
	}
	else if((unit == null)  && (radiusSearch != false)){
		callback({Error:"Radius unit must be mi or km."}, null);
	}else{
		var sortBy = SortByConditionCompany(exportedRequiredModules,requireQuery);
        // console.log(sortBy)
        var sortByObj = JSON.parse("{"+sortBy+"}")
		if(beSpokeQuery.sort == undefined){
            beSpokeQuery.sort = sortByObj.sort;
        }
		if (radiusSearch != false){
			var beSpokeQuery = JSON.stringify(beSpokeQuery);
			var updatedBeSpokeQuery = removeLocation(beSpokeQuery)
			var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byCompanyTimeRange(exportedRequiredModules,requireQuery);
			
			var radiusQuery = '{"bool": { "should": [ { "geo_distance": { "distance": "'+radiusValues+'", "OfficeLocation": { "lat": '+latValues+', "lon": '+longValues+' } } } ] } }';	
			
			// console.log(updatedBeSpokeQuery);
			if(updatedBeSpokeQuery.includes('"filter"') == true){
				updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"bool":{"filter":[{', '{"bool":{"filter":['+radiusQuery+','+projectHistories+',{');
			}else if(updatedBeSpokeQuery.includes('{"query":{"bool":{"should":[') == true){
				updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"query":{"bool":{"should":[', '{"query":{"bool":{"must":['+radiusQuery+','+projectHistories+'],"should":[');
			}else if(updatedBeSpokeQuery.includes('{"query":{"bool":{"must":[') == true){
				updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"query":{"bool":{"must":[', '{"query":{"bool":{"should":['+radiusQuery+','+projectHistories+'],"must":[');
			}
			callback(null, updatedBeSpokeQuery);
		}else{
			var updatedBeSpokeQuery = JSON.stringify(beSpokeQuery);
			
			if(updatedBeSpokeQuery.includes('"filter"') == true){
				updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"bool":{"filter":[{', '{"bool":{"filter":[{"bool": {"must_not": [{"term": {"OfficeWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"OfficeWorkingOnSmallProjects": "true"}},{"term": {"Publish": "false"}}]}},{');
			}else if(updatedBeSpokeQuery.includes('{"query":{"bool":{"should":[') == true){
				
				updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"query":{"bool":{"should":[', '{"query":{"bool":{"must_not":[{"term": {"OfficeWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"OfficeWorkingOnSmallProjects": "true"}},{"term": {"Publish": "false"}}],"should":[');
			}else if(updatedBeSpokeQuery.includes('{"query":{"bool":{"must":[') == true){
				updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"query":{"bool":{"must":[', '{"query":{"bool":{"must_not":[{"term": {"OfficeWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"OfficeWorkingOnSmallProjects": "true"}},{"term": {"Publish": "false"}}],"must":[');
			}
			
			callback(null, updatedBeSpokeQuery);
		}
	}
}
/******************************************************************************
Function: contactQueryValidation
Argument:requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for contact search. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.contactQueryValidation = function (requireQuery, callback){
    
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	var radiusValues = requireQuery.query.Radius;
	var latValues = requireQuery.query.Lat;
	var longValues = requireQuery.query.Lon;
	
	var beSpokeQuery;
	try{
		if (typeof requireQuery.body === 'string' || requireQuery.body instanceof String){
			beSpokeQuery = JSON.parse(requireQuery.body);
		}else{
			beSpokeQuery = requireQuery.body;
		}
		
	}catch(error){
		beSpokeQuery = '';
	}
	var unit,radiusSearch;
	
	if (radiusValues != undefined){
		var unitRegex = /[\d]+((?:mi|km|m))$/g;
		unit = unitRegex.exec(radiusValues.trim());
	}
	
	if ((radiusValues == undefined) && (latValues == undefined) && (longValues == undefined)) {
        radiusSearch = false;
    }
    if(beSpokeQuery == ''){
		callback({Error:"Invalid query"}, null);
	}
	else if (((radiusValues == undefined) || (latValues == undefined) || (longValues == undefined)) && (radiusSearch != false)) {
		callback({Error:"Parameter is Missing, Required Parameters is Radius,Lat,Long"}, null);
	}
	else if((unit == null)  && (radiusSearch != false)){
		callback({Error:"Radius unit must be mi or km."}, null);
	}else{
		var sortBy = SortByConditionContact(exportedRequiredModules,requireQuery);
        // console.log(sortBy)
        var sortByObj = JSON.parse("{"+sortBy+"}")
		if(beSpokeQuery.sort == undefined){
            beSpokeQuery.sort = sortByObj.sort;
        }
		if (radiusSearch != false){
			var beSpokeQuery = JSON.stringify(beSpokeQuery);
			
			var updatedBeSpokeQuery = removeLocation(beSpokeQuery)
			var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byContactTimeRange(exportedRequiredModules,requireQuery);
			
			var radiusQuery = '{"bool": { "should": [ { "geo_distance": { "distance": "'+radiusValues+'", "ContactLocation": { "lat": '+latValues+', "lon": '+longValues+' } } } ] } }';	
				
			
			if(updatedBeSpokeQuery.includes('"filter"') == true){
				updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"bool":{"filter":[{', '{"bool":{"filter":['+radiusQuery+','+projectHistories+',{');
			}else if(updatedBeSpokeQuery.includes('{"query":{"bool":{"should":[') == true){
				
				updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"query":{"bool":{"should":[', '{"query":{"bool":{"must":['+radiusQuery+','+projectHistories+'],"should":[');
			}else if(updatedBeSpokeQuery.includes('{"query":{"bool":{"must":[') == true){
				updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"query":{"bool":{"must":[', '{"query":{"bool":{"should":['+radiusQuery+','+projectHistories+'],"must":[');
			}
			callback(null, updatedBeSpokeQuery);
		}else{
			var updatedBeSpokeQuery = JSON.stringify(beSpokeQuery);
			if(updatedBeSpokeQuery.includes('"filter"') == true){
				updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"bool":{"filter":[{', '{"bool":{"filter":[{"bool": {"must_not": [{"term": {"ContactWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"ContactWorkingOnSmallProjects": "true"}}]}},{');
			}else if(updatedBeSpokeQuery.includes('{"query":{"bool":{"should":[') == true){
				
				updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"query":{"bool":{"should":[', '{"query":{"bool":{"must_not":[{"term": {"ContactWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"ContactWorkingOnSmallProjects": "true"}}],"should":[');
			}else if(updatedBeSpokeQuery.includes('{"query":{"bool":{"must":[') == true){
				updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"query":{"bool":{"must":[', '{"query":{"bool":{"must_not":[{"term": {"ContactWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"ContactWorkingOnSmallProjects": "true"}}],"must":[');
			}
			// console.log(typeof beSpokeQuery);
			callback(null, updatedBeSpokeQuery);
		}
	}
}



/******************************************************************************
Function: getContactQueryByRadius
Argument: requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Radius,Latitude and Longitude of a location. 
	2. 'getSubscriptionData' returns location and sector based on subscription json.
	3. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getContactQueryByRadius = function (requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var radiusValues = requireQuery.query.Radius;
	var latValues = requireQuery.query.Lat;
	var longValues = requireQuery.query.Lon;
	// console.log("radiusValues",radiusValues)
	var unit;
	if (radiusValues != undefined){
		var unitRegex = /[\d]+((?:mi|km|m))$/g;
		unit = unitRegex.exec(radiusValues.trim());
	}
	
	if ((radiusValues == undefined) || (latValues == undefined) || (longValues == undefined)) {
		callback({Error:"Parameter is Missing, Required Parameters is Radius,Lat,Long"}, null);
	}
	else if(unit == null){
		callback({Error:"Radius unit must be mi or km or m."}, null);
	}
	else{
		
		try{
			var sortBy = SortByConditionContact(exportedRequiredModules,requireQuery);
			var radiusQuery = '{"bool": { "should": [ { "geo_distance": { "distance": "'+radiusValues+'", "ContactLocation": { "lat": '+latValues+', "lon": '+longValues+' } } } ] } },{"bool": {"must_not": [{"term": {"ContactWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"ContactWorkingOnSmallProjects": "true"}}]}}';
			var FinalQuery='{"query": {"bool": {"filter": ['+radiusQuery+']}}, '+sortBy+'}';
			callback(null, FinalQuery);
		
		}
		catch(err){
			callback(err, null);
		}
	}
}


/******************************************************************************
Function: getCompanyQueryByRadius
Argument: requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Radius,Latitude and Longitude of a location. 
	2. 'getSubscriptionData' returns location and sector based on subscription json.
	3. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getCompanyQueryByRadius = function (requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var radiusValues = requireQuery.query.Radius;
	var latValues = requireQuery.query.Lat;
	var longValues = requireQuery.query.Lon;
	// console.log("radiusValues",radiusValues)
	var unit;
	if (radiusValues != undefined){
		var unitRegex = /[\d]+((?:mi|km|m))$/g;
		unit = unitRegex.exec(radiusValues.trim());
	}
	
	if ((radiusValues == undefined) || (latValues == undefined) || (longValues == undefined)) {
		callback({Error:"Parameter is Missing, Required Parameters is Radius,Lat,Long"}, null);
	}
	else if(unit == null){
		callback({Error:"Radius unit must be mi or km or m."}, null);
	}
	else{
		
		try{
			var sortBy = SortByConditionCompany(exportedRequiredModules,requireQuery);
			var radiusQuery = '{"bool": { "should": [ { "geo_distance": { "distance": "'+radiusValues+'", "OfficeLocation": { "lat": '+latValues+', "lon": '+longValues+' } } } ] } },{"bool": {"must_not": [{"term": {"OfficeWorkingOnProjectRecordTypeProjects": "false"}},{"term": {"OfficeWorkingOnSmallProjects": "true"}},{"term": {"Publish": "false"}}]}}';
			var FinalQuery='{"query": {"bool": {"filter": ['+radiusQuery+']}}, '+sortBy+'}';
			callback(null, FinalQuery);
		
		}
		catch(err){
			callback(err, null);
		}
	}
}
/******************************************************************************
Function: removeLocation
Argument: beSpokeQuery
Return: query response
Usage:
    1. Replace location in query.
*******************************************************************************/
function removeLocation(beSpokeQuery){
	
	var locationRegex = /LocationLevel[\d]+Facet\"\s*\:\s*(?:\"([^\"]*?)\"|\[([^\]]*?)\])/ig;
	var locationLevel1 = [];
	var locationLevel2 = [];
	var locationLevel3 = [];
	var matchesLevel1;
	while (matchesLevel1 = locationRegex.exec(beSpokeQuery)) {
	
		if (matchesLevel1[1] != undefined){
			var locationsGroup = matchesLevel1[1].split("#");
			if (locationsGroup[0] != undefined){locationLevel1.push(locationsGroup[0]);}
			if (locationsGroup[1] != undefined){locationLevel2.push(locationsGroup[1]);}
			if (locationsGroup[2] != undefined){locationLevel3.push(locationsGroup[2]);}
		}
		else if (matchesLevel1[2] != undefined){
			var modifiedList = matchesLevel1[2].replace(/\"\s*\,\s*\"/g,'"ListSplit"');
			modifiedList.split('ListSplit').forEach(function(result){
				var locationsGroup =result.split("#");
				if (locationsGroup[0] != undefined){locationLevel1.push(locationsGroup[0].replace(/\"/g,''));}
				if (locationsGroup[1] != undefined){locationLevel2.push(locationsGroup[1].replace(/\"/g,''));}
				if (locationsGroup[2] != undefined){locationLevel3.push(locationsGroup[2].replace(/\"/g,''));}
			});
		}

	}
	
	locationLevel1 = getUniqueValues(locationLevel1);
	locationLevel2 = getUniqueValues(locationLevel2);
	locationLevel3 = getUniqueValues(locationLevel3);

	var updatedBeSpokeQuery='';
	updatedBeSpokeQuery = replaceUnauthorizedLocationValues(locationLevel1, beSpokeQuery);
	updatedBeSpokeQuery = replaceUnauthorizedLocationValues(locationLevel2, updatedBeSpokeQuery);
	updatedBeSpokeQuery = replaceUnauthorizedLocationValues(locationLevel3, updatedBeSpokeQuery);
	if(beSpokeQuery.includes('"filter"') == true){
		updatedBeSpokeQuery = replaceEmptyBool(updatedBeSpokeQuery);
	}
	else{
		updatedBeSpokeQuery = replaceEmptyBoolForNonFilterOption(updatedBeSpokeQuery);
	}
	// console.log("updatedBeSpokeQuery",updatedBeSpokeQuery)
	return updatedBeSpokeQuery;
}
/******************************************************************************
Function: SortByConditionCompany
Argument: exportedRequiredModules,require
Return: query response
Usage:
    1. Returns sort query for Company.
*******************************************************************************/
function SortByConditionCompany(exportedRequiredModules,require)
{
	var orderbyValues = require.query.OrderBy;
	
	if (orderbyValues == undefined) {
		var orderby = '"sort": [{"NameNotAnalysed": {"order": "asc"}}]';
		return orderby;
	}
	else{
		try{
			if(orderbyValues.split(',')[0].trim() == "distance"){
				throw({Error: 'Parameter "OrderBy" value is Invalid'});
			}else if(orderbyValues.split(',')[0].trim() == "Distance"){
				var latValues = require.query.Lat;
				var longValues = require.query.Lon;
				if((latValues == undefined)  || (longValues == undefined)){
					throw({Error: 'Parameter "OrderBy" value Distance is allowed for radius endpoint'});
				}else{
					var radiusSort= '"sort": [{"_geo_distance": {"OfficeLocation": {"lat" : '+latValues+',"lon" : '+longValues+'},"order":"'+orderbyValues.split(',')[1].trim().toLowerCase()+'","unit":"mi","distance_type": "arc"}}]'
					return radiusSort;
				}
			}else{
				var orderby = '"sort": [{"'+orderbyValues.split(',')[0].trim()+'": {"order": "'+orderbyValues.split(',')[1].trim().toLowerCase()+'"}}]';
				return orderby;
			}
		}catch(err){
			throw({Error: 'Parameter "OrderBy" value is Invalid'});
		}
	}
}
/******************************************************************************
Function: SortByConditionContact
Argument: exportedRequiredModules,require
Return: query response
Usage:
    1. Returns sort query for Contact.
*******************************************************************************/
function SortByConditionContact(exportedRequiredModules,require)
{
	var orderbyValues = require.query.OrderBy;
	
	if (orderbyValues == undefined) {
		var orderby = '"sort": [{"LastNameNotAnalysed": {"order": "asc"}}]';
		return orderby;
	}
	else{
		try{
			if(orderbyValues.split(',')[0].trim() == "distance"){
				throw({Error: 'Parameter "OrderBy" value is Invalid'});
			}else if(orderbyValues.split(',')[0].trim() == "Distance"){
				var latValues = require.query.Lat;
				var longValues = require.query.Lon;
				if((latValues == undefined)  || (longValues == undefined)){
					throw({Error: 'Parameter "OrderBy" value Distance is allowed for radius endpoint'});
				}else{
					var radiusSort= '"sort": [{"_geo_distance": {"ContactLocation": {"lat" : '+latValues+',"lon" : '+longValues+'},"order":"'+orderbyValues.split(',')[1].trim().toLowerCase()+'","unit":"mi","distance_type": "arc"}}]'
					return radiusSort;
				}
			}else{
				var orderby = '"sort": [{"'+orderbyValues.split(',')[0].trim()+'": {"order": "'+orderbyValues.split(',')[1].trim().toLowerCase()+'"}}]';
				return orderby;
			}
			
		}catch(err){
			throw({Error: 'Parameter "OrderBy" value is Invalid'});
		}
	}
}