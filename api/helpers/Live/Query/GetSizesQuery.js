'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetSizesQuery.js) is used form a query for given Sizes .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryBySizes
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Sizes. 
	2. 'getSubscriptionQueryObj' returns location and sector based on subscription json.
	3. 'getSizeMetadata' returns metadata of size.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryBySizes = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var sizeValues = requireQuery.query.Names;
	
	if ((sizeValues == undefined)) {
		callback({Error:"Parameter Size Values is Missing"},null);
	}
	else{
		
		var typeProjectSize = jsonObject.Subscription.HasSmall.toString();
		
		var sizeList = [];
		if (sizeValues != undefined){
				sizeValues.split(',').forEach(function (size){
					sizeList.push(size.toLowerCase());
			});}
		/*******************************************************************************************/
		/********************************************************************************************
		This Condition will validate the queried Size and Subscribed size
		********************************************************************************************/
		/*******************************************************************************************/
		
		if((typeProjectSize == false) && (sizeList.includes("small") > -1)){
			callback({Error: 'Your are not authorized for small projects'},null);
		}
		else
		{
			exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
				if(err){
					callback(err, null);
				}else{
						
					getSizeMetadata(exportedRequiredModules,sizeList, function(err, projectSizeType){
					
					if (err){
						callback({Error: 'Exception on Sizes Metadata'},null);
					}
					else if (projectSizeType.length == 0){
						callback({Error:"Parameter Size Values is Missing"},null);
					}
					else{
						try{
							var queryBuildProjectSize={};
							queryBuildProjectSize.ProjectSize=projectSizeType;
							var queryProjectSizeField='{"terms": %s}';
								
							var ProjectSize = '{"bool": {"should": [ '+exportedRequiredModules.convertComma2Array(queryProjectSizeField, JSON.stringify(queryBuildProjectSize))+' ]}}'; 
						
							var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
							var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
							
							var queryConcat=projectHistories
							if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
							if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
							
							if (ProjectSize != '' ){ queryConcat = queryConcat +', '+ProjectSize; }
							var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
							// console.log("FinalQuery"+FinalQuery);
							callback(null, FinalQuery);
						}catch (err){
							callback(err, null);
						}
						}
					});
					
				}
			});
		}
	}
}
/******************************************************************************
Function: getSizeMetadata
Argument:exportedRequiredModules, sizes
Return:  get sizes type.
Usage:
	1. Extracts sizes type from metadata url.
*******************************************************************************/
function getSizeMetadata(exportedRequiredModules, sizes, callback){
	var projectSizes=[];
	try{
	 	var urlRequestSizes= exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/projectsizetypes';
		
		exportedRequiredModules.request(urlRequestSizes, function (error, response,projectSizeContent) {
			if( projectSizeContent != undefined){
			var projSizeData = JSON.parse(projectSizeContent);
			projSizeData._source.ProjectSizeTypes.forEach(function(projectSizeTypeData) {
				if(sizes != undefined){
					if (sizes.includes(projectSizeTypeData.toLowerCase()) == true){
						projectSizes.push(projectSizeTypeData);
					}
				}
				});
				callback(null, projectSizes);
			}
			else{
				callback({Error: 'Sizes metadata URL response failed.'},null);
			}
		});
	}catch(err){
		callback({Error: 'Sizes metadata URL response failed.'},null);
	}
}