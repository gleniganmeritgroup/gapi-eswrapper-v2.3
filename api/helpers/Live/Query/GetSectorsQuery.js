'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetSectorsQuery.js) is used for form a elastic query from subscription Json.
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryByPrimaryAndSecondarySector
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Sectors. 
	2. 'getLocationSector' returns array of location and sector.
    3. 'getSectorsCatagories' returns authorized sector and category list from metadata url.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByPrimaryAndSecondarySector = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
    
    var sectorValues = requireQuery.query.Sectors;
    var categoryValues = requireQuery.query.Categories;
    var sectorValuesList = [];
    var categoryValuesList = [];
    if (sectorValues != undefined){
        sectorValues.split(',').forEach(function(sectorParams){
            sectorValuesList.push(sectorParams.toLowerCase());
        });  
    }
    if (categoryValues != undefined){
        categoryValues.split(',').forEach(function(categoryParams){
            categoryValuesList.push(categoryParams.toLowerCase());
        });  
    }  
    getLocationSector(subscriptionJSON, function(error,locationLevel1,locationLevel2,sectorData){

        if(error){
            callback(error, null);
        }
        else{
     
            exportedRequiredModules.getMetadataObj.getSectorsCatagories(sectorData, function(err, authorizedSectorList, authorizedCategoryList,sectorsContent){ 
                var authorizedSectorListTemp = authorizedSectorList.map(function(x){ return x.replace(', ',' and ').replace('&','and') })
                var authorizedCategoryListTemp = authorizedCategoryList.map(function(x){ return x.replace(', ',' and ').replace('&','and') })
                if (err){
                    callback({Error: "Sector Metadata extraction failed"}, null);
                }
                else if ((sectorValues != undefined) && (categoryValues != undefined) && (sectorValues.split(',').length > 1)){
                    callback({Error: "Multiple Sectors not allowed when the catagories passed"}, null);
                }
                else if (Array(exportedRequiredModules.arrayDiff(sectorValuesList,authorizedSectorListTemp))[0].length > 0){
                    callback({Error: 'The requested Sector - '+exportedRequiredModules.arrayDiff(sectorValuesList,authorizedSectorListTemp)+' is invalid/not authorized.'}, null);
                }
                else if (Array(exportedRequiredModules.arrayDiff(categoryValuesList,authorizedCategoryListTemp))[0].length > 0){
                    callback({Error: 'The requested Categories - '+exportedRequiredModules.arrayDiff(categoryValuesList,authorizedCategoryList)+' is invalid/not authorized.'}, null);
                }
                else{
                    try{
                        
                        var sectorQuery='';
                        var queryBuildSector={};
                        var queryFieldSector='{"terms": %s}';
                        
                        if ((sectorValues != undefined) && (categoryValues == undefined)){
                            queryBuildSector.PrimaryAndSecondarySectorParents=queryFormationUsingMetaContent(sectorValuesList, categoryValuesList, sectorsContent);
                            var PrimaryAndSecondarySectorParents= exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
                            sectorQuery = '{"bool": {"should": [ '+PrimaryAndSecondarySectorParents+' ]}}';

                        }
                        else if(categoryValues != undefined){
                            queryBuildSector.PrimaryAndSecondarySectors=queryFormationUsingMetaContent(sectorValuesList, categoryValuesList, sectorsContent);
                            var PrimaryAndSecondarySectors= exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
                            sectorQuery = '{"bool": {"should": [ '+PrimaryAndSecondarySectors+' ]}}';
                        }
                        else if((categoryValues == undefined) && (sectorValues == undefined) && (sectorData.length > 0)){
                            queryBuildSector.PrimaryAndSecondarySectorParents=sectorData;
                            var PrimaryAndSecondarySectorParents= exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
                            sectorQuery = '{"bool": {"should": [ '+PrimaryAndSecondarySectorParents+' ]}}';
                        }
                        
                        var queryBuildLevel1={};
                        var queryBuildLevel2={};

                        queryBuildLevel1.ProjectLocationLevel1Facet=locationLevel1;
                        queryBuildLevel2.ProjectLocationLevel2Facet=locationLevel2;

                        var queryFieldLevel1='{"terms": %s}';
                        var queryFieldLevel2='{"terms": %s}';

                        var ProjectLocationLevel1Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel1, JSON.stringify(queryBuildLevel1)); 
                        var ProjectLocationLevel2Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel2, JSON.stringify(queryBuildLevel2)); 

                        var locationQuery = '';
                        var FinalQuery ='';
                        if ((locationLevel1.length == 0) &&  (locationLevel2.length != 0)){
                            locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel2Facet+' ]}}'
                        }
                        else if ((locationLevel2.length != 0) && (locationLevel1.length != 0)){
                            locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel1Facet+','+ProjectLocationLevel2Facet+' ]}}'
                        }
                        else if (locationLevel1.length != 0){
                            locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel1Facet+' ]}}'
                        }
                    
                        var projectHistories = exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
                                
                        var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
                        
                        if (locationQuery == ''){
                            FinalQuery='{"query": {"bool": {"filter": ['+sectorQuery+','+projectHistories+']}}, '+sortBy+'}';
                            callback(null, FinalQuery);
                        }
                        else if (sectorQuery != ''){
                            FinalQuery='{"query": {"bool": {"filter": ['+locationQuery+','+sectorQuery+','+projectHistories+']}}, '+sortBy+'}';
                            callback(null, FinalQuery);
                        }else{
                            callback({Error: "Your are not subscribed any Sectors"},null);
                        }
                    } catch (err){
                        callback(err, null);
                    }
                }
            });
        }
    });
}

/******************************************************************************
Function: getQueryBySecondarySector
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Secondary Sectors. 
	2. 'getLocationSector' returns array of location and sector.
    3. 'getSectorsCatagories' returns authorized sector and category list from metadata url.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryBySecondarySector = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
    
    var sectorValues = requireQuery.query.Sectors;
    var categoryValues = requireQuery.query.Categories;
    var sectorValuesList = [];
    var categoryValuesList = [];
    if (sectorValues != undefined){
        sectorValues.split(',').forEach(function(sectorParams){
            sectorValuesList.push(sectorParams.toLowerCase());
        });  
    }
    if (categoryValues != undefined){
        categoryValues.split(',').forEach(function(categoryParams){
            categoryValuesList.push(categoryParams.toLowerCase());
        });  
    }  
    getLocationSector(subscriptionJSON, function(error,locationLevel1,locationLevel2,sectorData){

        if(error){
            callback(error, null);
        }
        else{
     
            exportedRequiredModules.getMetadataObj.getSectorsCatagories(sectorData, function(err, authorizedSectorList, authorizedCategoryList,sectorsContent){ 
                var authorizedSectorListTemp = authorizedSectorList.map(function(x){ return x.replace(', ',' and ').replace('&','and') })
                var authorizedCategoryListTemp = authorizedCategoryList.map(function(x){ return x.replace(', ',' and ').replace('&','and') })
                if (err){
                    callback({Error: "Sector Metadata extraction failed"}, null);
                }
                else if ((sectorValues != undefined) && (categoryValues != undefined) && (sectorValues.split(',').length > 1)){
                    callback({Error: "Multiple Sectors not allowed when the catagories passed"}, null);
                }
                else if (Array(exportedRequiredModules.arrayDiff(sectorValuesList,authorizedSectorListTemp))[0].length > 0){
                    callback({Error: 'The requested Sector - '+exportedRequiredModules.arrayDiff(sectorValuesList,authorizedSectorListTemp)+' is invalid/not authorized.'}, null);
                }
                else if (Array(exportedRequiredModules.arrayDiff(categoryValuesList,authorizedCategoryListTemp))[0].length > 0){
                    callback({Error: 'The requested Categories - '+exportedRequiredModules.arrayDiff(categoryValuesList,authorizedCategoryList)+' is invalid/not authorized.'}, null);
                }
                else{
                    try{
                        
                        var sectorQuery='';
                        var queryBuildSector={};
                        var queryFieldSector='{"terms": %s}';
                        
                        if ((sectorValues != undefined) && (categoryValues == undefined)){
                            queryBuildSector.SecondarySectorParents=queryFormationUsingMetaContent(sectorValuesList, categoryValuesList, sectorsContent);
                            var SecondarySectorParents= exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
                            sectorQuery = '{"bool": {"should": [ '+SecondarySectorParents+' ]}}';

                        }
                        else if(categoryValues != undefined){
                            queryBuildSector.SecondarySectors=queryFormationUsingMetaContent(sectorValuesList, categoryValuesList, sectorsContent);
                            var SecondarySectors= exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
                            sectorQuery = '{"bool": {"should": [ '+SecondarySectors+' ]}}';
                        }
                        else if((categoryValues == undefined) && (sectorValues == undefined) && (sectorData.length > 0)){
                            queryBuildSector.SecondarySectorParents=sectorData;
                            var SecondarySectorParents= exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
                            sectorQuery = '{"bool": {"should": [ '+SecondarySectorParents+' ]}}';
                        }
                        // console.log("sectorQuery"+sectorQuery);
                        var queryBuildLevel1={};
                        var queryBuildLevel2={};

                        queryBuildLevel1.ProjectLocationLevel1Facet=locationLevel1;
                        queryBuildLevel2.ProjectLocationLevel2Facet=locationLevel2;

                        var queryFieldLevel1='{"terms": %s}';
                        var queryFieldLevel2='{"terms": %s}';

                        var ProjectLocationLevel1Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel1, JSON.stringify(queryBuildLevel1)); 
                        var ProjectLocationLevel2Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel2, JSON.stringify(queryBuildLevel2)); 

                        var locationQuery = '';
                        var FinalQuery ='';
                        if ((locationLevel1.length == 0) &&  (locationLevel2.length != 0)){
                            locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel2Facet+' ]}}'
                        }
                        else if ((locationLevel2.length != 0) && (locationLevel1.length != 0)){
                            locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel1Facet+','+ProjectLocationLevel2Facet+' ]}}'
                        }
                        else if (locationLevel1.length != 0){
                            locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel1Facet+' ]}}'
                        }
                    
                        var projectHistories = exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
                                
                        var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
                        
                        if (locationQuery == ''){
                            FinalQuery='{"query": {"bool": {"filter": ['+sectorQuery+','+projectHistories+']}}, '+sortBy+'}';
                            callback(null, FinalQuery);
                        }
                        else if (sectorQuery != ''){
                            FinalQuery='{"query": {"bool": {"filter": ['+locationQuery+','+sectorQuery+','+projectHistories+']}}, '+sortBy+'}';
                            callback(null, FinalQuery);
                        }else{
                            callback({Error: "Your are not subscribed any Sectors"}, null);
                        }
                    } catch (err){
                        callback(err, null);
                    }
                }
            });
        }
    });
}

/******************************************************************************
Function: getQueryByPrimarySector
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Primary Sectors. 
	2. 'getLocationSector' returns array of location and sector.
    3. 'getSectorsCatagories' returns authorized sector and category list from metadata url.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByPrimarySector = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
    
    var sectorValues = requireQuery.query.Sectors;
    var categoryValues = requireQuery.query.Categories;
    var sectorValuesList = [];
    var categoryValuesList = [];
    
    if (sectorValues != undefined){
        sectorValues.split(',').forEach(function(sectorParams){
            sectorValuesList.push(sectorParams.toLowerCase());
        });  
    }
    if (categoryValues != undefined){
        categoryValues.split(',').forEach(function(categoryParams){
            categoryValuesList.push(categoryParams.toLowerCase());
        });  
    }  
    getLocationSector(subscriptionJSON, function(error,locationLevel1,locationLevel2,sectorData){

        if(error){
            callback(error, null);
        }
        else{
        // console.log("sectorData"+sectorData);
            exportedRequiredModules.getMetadataObj.getSectorsCatagories(sectorData, function(err, authorizedSectorList, authorizedCategoryList,sectorsContent){ 
                // console.log("sectorData"+authorizedSectorList);
                var authorizedSectorListTemp = authorizedSectorList.map(function(x){ return x.replace(', ',' and ').replace('&','and') })
                var authorizedCategoryListTemp = authorizedCategoryList.map(function(x){ return x.replace(', ',' and ').replace('&','and') })
                
                if (err){
                    callback({Error: "Sector Metadata extraction failed"}, null);
                }
                else if ((sectorValues != undefined) && (categoryValues != undefined) && (sectorValues.split(',').length > 1)){
                    callback({Error: "Multiple Sectors not allowed when the catagories passed"}, null);
                }
                else if (Array(exportedRequiredModules.arrayDiff(sectorValuesList,authorizedSectorListTemp))[0].length > 0){
                    callback({Error: 'The requested Sector - '+exportedRequiredModules.arrayDiff(sectorValuesList,authorizedSectorListTemp)+' is invalid/not authorized.'}, null);
                }
                else if (Array(exportedRequiredModules.arrayDiff(categoryValuesList,authorizedCategoryListTemp))[0].length > 0){
                    callback({Error: 'The requested Categories - '+exportedRequiredModules.arrayDiff(categoryValuesList,authorizedCategoryList)+' is invalid/not authorized.'}, null);
                }
                else{
                    try{
                        
                        var sectorQuery='';
                        var queryBuildSector={};
                        var queryFieldSector='{"terms": %s}';
                        
                        if ((sectorValues != undefined) && (categoryValues == undefined)){
                            queryBuildSector.PrimarySectorParents=queryFormationUsingMetaContent(sectorValuesList, categoryValuesList, sectorsContent,authorizedSectorListTemp);
                            var PrimarySectorParents= exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
                            sectorQuery = '{"bool": {"should": [ '+PrimarySectorParents+' ]}}';

                        }
                        else if(categoryValues != undefined){
                            queryBuildSector.PrimarySectors=queryFormationUsingMetaContent(sectorValuesList, categoryValuesList, sectorsContent,authorizedSectorListTemp);
                            var PrimarySectors= exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
                            sectorQuery = '{"bool": {"should": [ '+PrimarySectors+' ]}}';
                        }
                        else if((categoryValues == undefined) && (sectorValues == undefined) && (sectorData.length > 0)){
                            queryBuildSector.PrimarySectorParents=sectorData;
                            var PrimarySectorParents= exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
                            sectorQuery = '{"bool": {"should": [ '+PrimarySectorParents+' ]}}';
                        }
                        // console.log("sectorQuery"+sectorQuery);
                        var queryBuildLevel1={};
                        var queryBuildLevel2={};

                        queryBuildLevel1.ProjectLocationLevel1Facet=locationLevel1;
                        queryBuildLevel2.ProjectLocationLevel2Facet=locationLevel2;

                        var queryFieldLevel1='{"terms": %s}';
                        var queryFieldLevel2='{"terms": %s}';

                        var ProjectLocationLevel1Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel1, JSON.stringify(queryBuildLevel1)); 
                        var ProjectLocationLevel2Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel2, JSON.stringify(queryBuildLevel2)); 

                        var locationQuery = '';
                        var FinalQuery ='';
                        if ((locationLevel1.length == 0) &&  (locationLevel2.length != 0)){
                            locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel2Facet+' ]}}'
                        }
                        else if ((locationLevel2.length != 0) && (locationLevel1.length != 0)){
                            locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel1Facet+','+ProjectLocationLevel2Facet+' ]}}'
                        }
                        else if (locationLevel1.length != 0){
                            locationQuery = '{"bool": {"should": [ '+ProjectLocationLevel1Facet+' ]}}'
                        }
                    
                        var projectHistories = exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
                                
                        var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
                        
                        if (locationQuery == ''){
                            FinalQuery='{"query": {"bool": {"filter": ['+sectorQuery+','+projectHistories+']}}, '+sortBy+'}';
                            callback(null, FinalQuery);
                        }
                        else if (sectorQuery != ''){
                            FinalQuery='{"query": {"bool": {"filter": ['+locationQuery+','+sectorQuery+','+projectHistories+']}}, '+sortBy+'}';
                            callback(null, FinalQuery);
                        }else{
                            callback({Error: "Your are not subscribed any Sectors"},null);
                        }
                    } catch (err){
                        callback(err, null);
                    }
                }
            });
        }
    });
}
/******************************************************************************
Function: queryFormationUsingMetaContent
Argument:sectorValues,categoryValues,sectorsContent
Return: Array 
Usage:
	1. Returns query formation based on metacontent. 
*******************************************************************************/
function queryFormationUsingMetaContent(sectorValues,categoryValues,sectorsContent,authorizedSectorListContent){
    // console.log("sectorValues"+sectorValues);
    try{
        var sectorValueFormation = [];
        var sectorList = JSON.parse(sectorsContent);
        sectorList._source.Sectors.forEach(function(sectorGroup){
                if ((sectorValues.length > 0 ) && (categoryValues.length == 0) ){
                    if (sectorValues.includes(sectorGroup.SectorGroupName.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
                        sectorValueFormation.push(sectorGroup.SectorGroupName.toLowerCase())
                    }
                }
                else if ((categoryValues.length > 0) && (sectorValues.length > 0 )){
                    sectorGroup.SectorNames.forEach(function(sector){
                        if ( (sectorValues.includes(sectorGroup.SectorGroupName.toLowerCase().replace(', ',' and ').replace('&','and')) == true) && (categoryValues.includes(sector.toLowerCase().replace(', ',' and ').replace('&','and')) == true)){
                            sectorValueFormation.push(sectorGroup.SectorGroupName.toLowerCase()+"#"+sector.toLowerCase())
                        }
                    });
                }
                else if ((categoryValues.length > 0) && (sectorValues.length == 0)){
                sectorGroup.SectorNames.forEach(function(sector){
                    if (categoryValues.includes(sector.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
						     if(authorizedSectorListContent.includes(sectorGroup.SectorGroupName.toLowerCase()) == true){
                        sectorValueFormation.push(sectorGroup.SectorGroupName.toLowerCase()+"#"+sector.toLowerCase())
                    }}
                });
            }
        });
        return sectorValueFormation;
    }catch(err){
        return "";
    }
}
/******************************************************************************
Function: getLocationSector
Argument:subscriptionJSON
Return: Array of Region and Sector
Usage:
	1. Returns Region and Sector from subscription.
*******************************************************************************/
function getLocationSector(subscriptionJSON, callback){
    try{
        var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
        var jsonObject = JSON.parse(subscriptionJSON);
        var locationLevel1 = [];
        var locationLevel2 = [];
        var sectorData = [];
       
        for (let level1 of jsonObject.Subscription.ProjectRegions) {
            var region =  level1.Region;
            var county =  level1.Counties;
            
            if (county != ""){
                exportedRequiredModules._.each(county, function ( instance){
                    var concatRegion = region+"#"+instance;
                    locationLevel2.push(concatRegion.toLowerCase());
                });
            }
            else{
                locationLevel1.push(region.toLowerCase());
            }
        }
        
        for (let sectorLevel of jsonObject.Subscription.ProjectSectors) {
            var sector =  sectorLevel.Sector;
            if (sector != ""){
                sectorData.push(sector.toLowerCase());
            }	
        }
        callback(null, locationLevel1, locationLevel2, sectorData)
    }catch(err){
        // console.log("err"+err);
        callback({Error: "Exception on Subscription parsing"}, null)
    }
}