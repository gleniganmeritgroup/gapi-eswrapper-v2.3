'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetStatusQuery.js) is used form a query for given Status .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryByStatus
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for status. 
	2. 'getSubscriptionQueryObj' returns location and sector based on subscription json.
	3. 'getStatusMetadata' returns metadata of status type.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByStatus = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var statusNames = requireQuery.query.StatusNames;
	
	var statusList = [];
	
	if (statusNames != undefined){
		statusNames.split(',').forEach(function (status){
			statusList.push(status.toLowerCase());
	});}

	exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
		if(err){
			callback(err, null);
		}else{
				getStatusMetadata(exportedRequiredModules, statusList, function(err, statusDataList){
				
				if (err){
					callback(err, null);
				}else if (statusDataList.length != statusList.length ){
					callback({Error: 'Invalid StatusNames value is given.'}, null);
				}
				else if (statusDataList.length == 0 ){
					callback({Error: 'Invalid StatusNames value is given.'}, null);
				}
				else{
					try{
						var queryBuildStatus = {};
						
						var queryFieldStatus = '{"terms": %s}';
						
						queryBuildStatus.ProjectStatus = statusDataList;
						
						var statusQueryFormation = '';
					
						if (statusDataList.length > 0 ){ statusQueryFormation = '{"bool": {"should": [ '+exportedRequiredModules.convertComma2Array(queryFieldStatus, JSON.stringify(queryBuildStatus))+' ]}}'; }
						
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						if (statusQueryFormation != ''){ queryConcat = queryConcat +', '+statusQueryFormation; }
						
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);
					}catch (err){
						callback(err, null);
					}
				}
			});
			
		}
	});
}
/******************************************************************************
Function: getStatusMetadata
Argument:exportedRequiredModules, statusList
Return:  get status type.
Usage:
	1. Extracts status type from metadata url.
*******************************************************************************/
function getStatusMetadata(exportedRequiredModules, statusList, callback){
	
	var statusDataList = [];
	
	try{
	 	var urlStatusType = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/projectstatustypes';
		
		exportedRequiredModules.request(urlStatusType, function (error, response,statusContent) {
			if( statusContent != undefined){
				var statusData = JSON.parse(statusContent);
				statusData._source.ProjectStatusTypes.forEach(function(status) {
					if (statusList.length > 0) {
						if (statusList.includes(status.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							statusDataList.push(status);
						}
					}
					else{
						statusDataList.push(status);
					}
				});
				callback(null, statusDataList);
			}
			else{
				callback({Error: 'Status metadata URL response failed.'},null);
			}
		});
	}catch(err){
		callback({Error: 'Status metadata URL response failed.'},null);
	}
}
