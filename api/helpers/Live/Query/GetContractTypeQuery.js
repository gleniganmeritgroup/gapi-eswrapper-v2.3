'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetContractTypeQuery.js) is used form a query for given Contract Types .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryByContractType
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching contract type. 
	2. 'getSubscriptionQueryObj' returns location and sector based on subscription json.
	3. 'getContractTypeMetadata' returns metadata of contract type.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByContractType = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var typesValues = requireQuery.query.Types;
	
	var typesValuesList = [];
	
	if (typesValues != undefined){
		typesValues.split(',').forEach(function (contractType){
			typesValuesList.push(contractType.toLowerCase());
	});}
	
	exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(error, locationQuery,sectorQuery){
		if(error){
			callback(error, null);
		}
		else{
			try{	
				getContractTypeMetadata(exportedRequiredModules, typesValuesList, function(err, contractTypeDataList){
				// console.log("contractTypeDataList"+contractTypeDataList)
				// console.log("typesValuesList"+typesValuesList)
				if (err){
					callback(err, null);
				}
				else if ((contractTypeDataList.length != typesValuesList.length) && (typesValuesList.length > 0)){
					callback({Error: 'Invalid value given to ContractTypes'}, null);
				}else{
				
					var queryBuildContractType={};
					
					var queryFieldContractType='{"terms": %s}';
					
					queryBuildContractType.ContractType=contractTypeDataList;
					
					var contractTypeQueryFormation, contractTypeQueryConcat;
				
					if (contractTypeDataList.length > 0 ){ contractTypeQueryConcat = exportedRequiredModules.convertComma2Array(queryFieldContractType, JSON.stringify(queryBuildContractType)); }
					
					contractTypeQueryConcat = contractTypeQueryConcat.replace(/^\s*undefined\s*\,/, "");
					contractTypeQueryConcat = contractTypeQueryConcat.replace(/^\s*\,/, "");
					contractTypeQueryConcat = contractTypeQueryConcat.replace(/\,\s*$/, "");
					contractTypeQueryFormation= '{"bool": {"should": [ '+contractTypeQueryConcat+' ]}}'; 

					try{
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						
						if (contractTypeQueryFormation != '' ){ queryConcat = queryConcat +', '+contractTypeQueryFormation; }
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);
					}catch (err){
						callback(err, null);
					}
				}
			});
			}catch (err){
				callback(err, null);
			}
		}
	});
}
/******************************************************************************
Function: getContractTypeMetadata
Argument:exportedRequiredModules, typesValuesList
Return: get contract type.
Usage:
	1. Extracts contract type from metadata
*******************************************************************************/
function getContractTypeMetadata(exportedRequiredModules, typesValuesList, callback){
	
	var contractTypeDataList = [];
	
	try{
	 	var urlRequestContractType = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/projectcontracttypes';
		
		exportedRequiredModules.request(urlRequestContractType, function (error, response,contractTypeContent) {
			if( contractTypeContent != undefined){
				var contractTypeData = JSON.parse(contractTypeContent);
				contractTypeData._source.ProjectContractTypes.forEach(function(contractType) {
					if (typesValuesList.length > 0) {
						if (typesValuesList.includes(contractType.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							contractTypeDataList.push(contractType);
						}
					}
					else{
						contractTypeDataList.push(contractType);
					}
				});
				
				callback(null, contractTypeDataList);
			}
			else{
				callback({Error: 'Contract Type metadata URL response failed.'}, null);
			}
		});
	}catch(err){
		// console.log("err")
		throw({Error: 'Contract Type metadata URL response failed.'});
	}
}