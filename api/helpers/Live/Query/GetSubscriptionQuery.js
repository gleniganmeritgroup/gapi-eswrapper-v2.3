'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetSubscriptionQuery.js) is used for form a elastic query from subscription Json.
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQuery
Argument:subscription
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for location and sector. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQuery = function (subscription, callback) {
	try{
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
		var object = JSON.parse(subscription);
		
		var locationLevel1 = [];
		var locationLevel2 = [];
		var sectorData = [];
		
		for (let level1 of object.Subscription.ProjectRegions) {
			var region =  level1.Region;
			var county =  level1.Counties;
			if (county != ""){
				exportedRequiredModules._.each(county, function ( instance){
					var concatRegion = region+"#"+instance
					locationLevel2.push(concatRegion.toLowerCase());
				});
			}
			else{
				locationLevel1.push(region.toLowerCase());
			}
		}
		
		for (let sectorLevel of object.Subscription.ProjectSectors) {
			var sector =  sectorLevel.Sector;
			
			
			if (sector != ""){
				
				sectorData.push(sector.toLowerCase());
			}
			
		}
		
		var queryBuildLevel1={};
		var queryBuildLevel2={};
		var queryBuildSector={};

		queryBuildLevel1.ProjectLocationLevel1Facet=locationLevel1;
		queryBuildLevel2.ProjectLocationLevel2Facet=locationLevel2;
		queryBuildSector.PrimaryAndSecondarySectorParents=sectorData;

		var queryFieldLevel1='{"terms": %s}';
		var queryFieldLevel2='{"terms": %s}';
		var queryFieldSector='{"terms": %s}';

		var ProjectLocationLevel1Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel1, JSON.stringify(queryBuildLevel1)); 
		var ProjectLocationLevel2Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel2, JSON.stringify(queryBuildLevel2)); 
		var PrimaryAndSecondarySectorParents = exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
		var locationQuery,sectorQuery;
		if (locationLevel2.length > 0){
			locationQuery = '{"bool": {"should":['+ProjectLocationLevel1Facet+','+ProjectLocationLevel2Facet+' ] }}';
		}
		else if (locationLevel1.length > 0){
			locationQuery = '{"bool": {"should":['+ProjectLocationLevel1Facet+' ] }}';
		}
		if (sectorData.length > 0){
			sectorQuery = '{"bool": {"should":['+PrimaryAndSecondarySectorParents+' ] }}';
		}	
		callback(null,locationQuery,sectorQuery);
	}catch(err){
		callback({Error: err},null,null);
	}
}