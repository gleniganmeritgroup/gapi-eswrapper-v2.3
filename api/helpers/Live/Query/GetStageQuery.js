'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetStageQuery.js) is used form a query for given stages .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryByStagesBoth
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for planning stages and contract stages. 
	2. 'getSubscriptionQueryObj' returns location and sector based on subscription json.
	3. 'getStagesMetadataForBoth' returns metadata of planning and contract stages for both Level1 and Level2.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByStagesBoth = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var PlanningStagesValues = requireQuery.query.PlanningStages;
	var PlanningStagesLevel2Values = requireQuery.query.PlanningStagesLevel2;
	var ContractStagesValues = requireQuery.query.ContractStages;
	var ContractStagesLevel2Values = requireQuery.query.ContractStagesLevel2;
	
	if ((PlanningStagesValues == undefined) && (PlanningStagesLevel2Values == undefined)) {
		callback({Error:"Parameter PlanningStages/PlanningStagesLevel2 is Missing"},null);
	}
	else{
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			if(err){
				callback(err, null);
			}else{
				var planningStagesList = [];
				var planningLevel2List = [];
				var contractStagesList = [];
				var contractLevel2List = [];
				
				if (PlanningStagesValues != undefined){
					PlanningStagesValues.split(',').forEach(function (stage){
						planningStagesList.push(stage.toLowerCase());
				});}
				if (ContractStagesValues != undefined){
					ContractStagesValues.split(',').forEach(function (stage){
						contractStagesList.push(stage.toLowerCase());
				});}
				if (PlanningStagesLevel2Values != undefined){
					PlanningStagesLevel2Values.split(',').forEach(function (level2){
						planningLevel2List.push(level2.toLowerCase());
				});}
				if (ContractStagesLevel2Values != undefined){
					ContractStagesLevel2Values.split(',').forEach(function (level2){
						contractLevel2List.push(level2.toLowerCase());
				});}
				getStagesMetadataForBoth(exportedRequiredModules,planningStagesList, contractStagesList, planningLevel2List, contractLevel2List, function(err, planningStageLevel1List, planningStageLevel2List, contractStageLevel1List,contractStageLevel2List){
					
					if (err){
						callback(err,null);
					}
					else{
						
						try{
							var queryBuildPlanningLevel1={};
							var queryBuildPlanningLevel2={};
							var queryBuildContractLevel1={};
							var queryBuildContractLevel2={};
							
							queryBuildPlanningLevel1.PlanningStageParent=planningStageLevel1List;
							queryBuildPlanningLevel2.PlanningStage=planningStageLevel2List;
							queryBuildContractLevel1.ContractStageParent=contractStageLevel1List;
							queryBuildContractLevel2.ContractStage=contractStageLevel2List;
							
							var queryFieldPlanningLevel1='{"terms": %s}';
							var queryFieldPlanningLevel2='{"terms": %s}';
							var queryFieldContractLevel1='{"terms": %s}';
							var queryFieldContractLevel2='{"terms": %s}';
							var stageQueryFormation = '';
							var stageQueryConcat = '';
							
							if (planningStageLevel1List.length > 0 ){ stageQueryConcat = exportedRequiredModules.convertComma2Array(queryFieldPlanningLevel1, JSON.stringify(queryBuildPlanningLevel1)); }
							if (planningStageLevel2List.length > 0 ){ stageQueryConcat = stageQueryConcat +', '+exportedRequiredModules.convertComma2Array(queryFieldPlanningLevel2, JSON.stringify(queryBuildPlanningLevel2));}
							if (contractStageLevel1List.length > 0 ){ stageQueryConcat = stageQueryConcat +', '+exportedRequiredModules.convertComma2Array(queryFieldContractLevel1, JSON.stringify(queryBuildContractLevel1)); }
							if (contractStageLevel2List.length > 0 ){ stageQueryConcat = stageQueryConcat +', '+exportedRequiredModules.convertComma2Array(queryFieldContractLevel2, JSON.stringify(queryBuildContractLevel2)); }
							stageQueryConcat = stageQueryConcat.replace(/^\s*undefined\s*\,/, "");
							stageQueryConcat = stageQueryConcat.replace(/^\s*\,/, "");
							stageQueryConcat = stageQueryConcat.replace(/\,\s*$/, "");
							stageQueryFormation= '{"bool": {"must": [ '+stageQueryConcat+' ]}}'; 
							
							var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
							var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
							
							var queryConcat=projectHistories
							if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
							if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
							if (stageQueryFormation != ''){ queryConcat = queryConcat +', '+stageQueryFormation; }
							var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
							// console.log("FinalQuery"+FinalQuery);
							callback(null, FinalQuery);
						}catch (err){
							callback(err, null);
						}
					}
				});
			}
		});
	}
}
/******************************************************************************
Function: getQueryByStages
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for stages. 
	2. 'getSubscriptionQueryObj' returns location and sector based on subscription json.
	3. 'getStagesMetadata' returns metadata of planning and contract stages.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByStages = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var StagesValues = requireQuery.query.Stages;
	var Level2Values = requireQuery.query.Level2;
	
	if ((StagesValues == undefined) && (Level2Values == undefined)) {
		callback({Error:"Parameter Stages/Level2 is Missing"},null);
	}
	else{
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			if(err){
				callback(err, null);
			}else{
				var stagesList = [];
				var level2List = [];
				
				if (StagesValues != undefined){
					StagesValues.split(',').forEach(function (stage){
						stagesList.push(stage.toLowerCase());
				});}
				if (Level2Values != undefined){
					Level2Values.split(',').forEach(function (level2){
						level2List.push(level2.toLowerCase());
				});}
				
				getStagesMetadata(exportedRequiredModules,stagesList, level2List, function(err, planningStageLevel1List, planningStageLevel2List, contractStageLevel1List,contractStageLevel2List){
					
					if (err){
						callback(err,null);
					}
					else{
						
						try{	
							var queryBuildPlanningLevel1={};
							var queryBuildPlanningLevel2={};
							var queryBuildContractLevel1={};
							var queryBuildContractLevel2={};
							
							queryBuildPlanningLevel1.PlanningStageParent=planningStageLevel1List;
							queryBuildPlanningLevel2.PlanningStage=planningStageLevel2List;
							queryBuildContractLevel1.ContractStageParent=contractStageLevel1List;
							queryBuildContractLevel2.ContractStage=contractStageLevel2List;
							
							var queryFieldPlanningLevel1='{"terms": %s}';
							var queryFieldPlanningLevel2='{"terms": %s}';
							var queryFieldContractLevel1='{"terms": %s}';
							var queryFieldContractLevel2='{"terms": %s}';
							var stageQueryFormation = '';
							var stageQueryConcat = '';
							
							if (planningStageLevel1List.length > 0 ){ stageQueryConcat = exportedRequiredModules.convertComma2Array(queryFieldPlanningLevel1, JSON.stringify(queryBuildPlanningLevel1)); }
							if (planningStageLevel2List.length > 0 ){ stageQueryConcat = stageQueryConcat +', '+exportedRequiredModules.convertComma2Array(queryFieldPlanningLevel2, JSON.stringify(queryBuildPlanningLevel2));}
							if (contractStageLevel1List.length > 0 ){ stageQueryConcat = stageQueryConcat +', '+exportedRequiredModules.convertComma2Array(queryFieldContractLevel1, JSON.stringify(queryBuildContractLevel1)); }
							if (contractStageLevel2List.length > 0 ){ stageQueryConcat = stageQueryConcat +', '+exportedRequiredModules.convertComma2Array(queryFieldContractLevel2, JSON.stringify(queryBuildContractLevel2)); }
							stageQueryConcat = stageQueryConcat.replace(/^\s*undefined\s*\,/, "");
							stageQueryConcat = stageQueryConcat.replace(/^\s*\,/, "");
							stageQueryConcat = stageQueryConcat.replace(/\,\s*$/, "");
							stageQueryFormation= '{"bool": {"should": [ '+stageQueryConcat+' ]}}'; 
							
							
							var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
							var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
							
							var queryConcat=projectHistories
							if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
							if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
							if (stageQueryFormation != ''){ queryConcat = queryConcat +', '+stageQueryFormation; }
							var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
							// console.log("FinalQuery"+FinalQuery);
							callback(null, FinalQuery);
						}catch (err){
							callback(err, null);
						}
					}
				});
			}
		});
	}
}
/******************************************************************************
Function: getStagesMetadata
Argument:exportedRequiredModules, stages, level2
Return:  get planning and contract stages.
Usage:
	1. Extracts planning and contract stages from metadata url.
*******************************************************************************/
function getStagesMetadata (exportedRequiredModules, stages, level2, callback){
	
	var planningStageLevel1List = [];
	var planningStageLevel2List = [];
	var contractStageLevel1List = [];
	var contractStageLevel2List = [];
	var checkStagesValue = [];
	var checkLevel2Value = [];
	try{
		var urlRequestPlanningStage = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/planningstages';
		var urlRequestContractStage = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/contractstages';
		
		exportedRequiredModules.request(urlRequestPlanningStage, function (error, response, planningStageContent) {
			var planningStageList = JSON.parse(planningStageContent);
			planningStageList._source.PlanningStages.forEach(function(planningStage) {
				
				if (stages.length > 0){
					if (stages.includes(planningStage.PlanningStageGroupName.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
						checkStagesValue.push(planningStage.PlanningStageGroupName);
						planningStageLevel1List.push(planningStage.PlanningStageGroupName)
					}
				}
				planningStage.PlanningStageNames.forEach(function(planningStageLevel2){
					if (level2.length > 0){
						if (level2.includes(planningStageLevel2.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							checkLevel2Value.push(planningStageLevel2);
							planningStageLevel2List.push(planningStage.PlanningStageGroupName+"#"+planningStageLevel2)
							if (planningStageLevel1List.includes(planningStage.PlanningStageGroupName) == true){ planningStageLevel1List.splice(planningStageLevel1List.indexOf(planningStage.PlanningStageGroupName),1); }
						}
					}
				})
			});

			checkLevel2Value = checkLevel2Value.map(function(x){ return x.toLowerCase().replace(', ',' and ').replace('&','and')});
			var missedLevel2List = exportedRequiredModules.arrayDiff(level2, checkLevel2Value);
			if (missedLevel2List.length > 0){
				planningStageList._source.PlanningStages.forEach(function(planningStage) {
					planningStage.PlanningStageNames.forEach(function(planningStageLevel2){
						if (missedLevel2List.includes(planningStageLevel2.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							checkLevel2Value.push(planningStageLevel2);
							planningStageLevel2List.push(planningStage.PlanningStageGroupName+"#"+planningStageLevel2)
							if (planningStageLevel1List.includes(planningStage.PlanningStageGroupName) == true){ planningStageLevel1List.splice(planningStageLevel1List.indexOf(planningStage.PlanningStageGroupName),1); }
						}
					
					})
				});
			}		
			exportedRequiredModules.request(urlRequestContractStage, function (error, response, contractStageContent) {
				var contractStageList = JSON.parse(contractStageContent);
				contractStageList._source.ContractStages.forEach(function(contractStage) {
					if (stages.length > 0){
						if (stages.includes(contractStage.ContractStageGroupName.toLowerCase()) == true){
							checkStagesValue.push(contractStage.ContractStageGroupName);
							contractStageLevel1List.push(contractStage.ContractStageGroupName)
						}
					}
					contractStage.ContractStageNames.forEach(function(contractStageLevel2){
						if (level2.length > 0){
							if (level2.includes(contractStageLevel2.toLowerCase()) == true){
								checkLevel2Value.push(contractStageLevel2);
								contractStageLevel2List.push(contractStage.ContractStageGroupName+"#"+contractStageLevel2)
								if (contractStageLevel1List.includes(contractStage.ContractStageGroupName) == true){ contractStageLevel1List.splice(contractStageLevel1List.indexOf(contractStage.ContractStageGroupName),1); }
							}
						}
					})
				});
				checkLevel2Value = checkLevel2Value.map(function(x){ return x.toLowerCase().replace(', ',' and ').replace('&','and')});
				var missedContractLevel2List = exportedRequiredModules.arrayDiff(level2, checkLevel2Value);
				if (missedContractLevel2List.length > 0){
				contractStageList._source.ContractStages.forEach(function(contractStage) {
					contractStage.ContractStageNames.forEach(function(contractStageLevel2){
							if (missedContractLevel2List.includes(contractStageLevel2.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
								checkLevel2Value.push(contractStageLevel2);
								contractStageLevel2List.push(contractStage.ContractStageGroupName+"#"+contractStageLevel2)
								if (contractStageLevel1List.includes(contractStage.ContractStageGroupName) == true){ contractStageLevel1List.splice(contractStageLevel1List.indexOf(contractStage.ContractStageGroupName),1); }
							}
							
						})
					
					});
				}
				if ((checkStagesValue.length == stages.length) && (checkLevel2Value.length == level2.length)){
					callback(null, planningStageLevel1List, planningStageLevel2List, contractStageLevel1List,contractStageLevel2List);
				}else{
					callback({Error: 'Invalid Stages/Level2 value given.'}, null);
				}
			});
		});
		
	}catch(err){
		callback({Error: 'Stages metadata URL response failed.'}, null);
	}
}
/******************************************************************************
Function: getStagesMetadataForBoth
Argument:exportedRequiredModules, planningStagesList, contractStagesList,
 planningLevel2List, contractLevel2List
Return:  get planning and contract stages for both Level1 and Level2.
Usage:
	1. Extracts planning and contract stages for both Level1 and Level2 from metadata url.
*******************************************************************************/
function getStagesMetadataForBoth (exportedRequiredModules, planningStagesList, contractStagesList, planningLevel2List, contractLevel2List, callback){
	
	var planningStageLevel1List = [];
	var planningStageLevel2List = [];
	var contractStageLevel1List = [];
	var contractStageLevel2List = [];
	var checkPlanningStagesLevel1Value = [];
	var checkPlanningStagesLevel2Value = [];
	var checkContractStagesLevel1Value = [];
	var checkContractStagesLevel2Value = [];
	
	try{
		var urlRequestPlanningStage = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/planningstages';
		var urlRequestContractStage = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/contractstages';
	
		exportedRequiredModules.request(urlRequestPlanningStage, function (error, response, planningStageContent) {
			var planningStageList = JSON.parse(planningStageContent);
			planningStageList._source.PlanningStages.forEach(function(planningStage) {
				if (planningStagesList.length > 0){
					if (planningStagesList.includes(planningStage.PlanningStageGroupName.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
						checkPlanningStagesLevel1Value.push(planningStage.PlanningStageGroupName);
						planningStageLevel1List.push(planningStage.PlanningStageGroupName)
					}
				}
				if (planningLevel2List.length > 0){
					planningStage.PlanningStageNames.forEach(function(planningStageLevel2){
						if (planningLevel2List.includes(planningStageLevel2.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							checkPlanningStagesLevel2Value.push(planningStageLevel2);
							planningStageLevel2List.push(planningStage.PlanningStageGroupName+"#"+planningStageLevel2)
							if (planningStageLevel1List.includes(planningStage.PlanningStageGroupName) == true){ planningStageLevel1List.splice(planningStageLevel1List.indexOf(planningStage.PlanningStageGroupName),1); }
						}
					
					})
				}
			});
			checkPlanningStagesLevel2Value = checkPlanningStagesLevel2Value.map(function(x){ return x.toLowerCase().replace(', ',' and ').replace('&','and')});
			var missedLevel2List = exportedRequiredModules.arrayDiff(planningLevel2List, checkPlanningStagesLevel2Value);
			if (missedLevel2List.length > 0){
				planningStageList._source.PlanningStages.forEach(function(planningStage) {
					planningStage.PlanningStageNames.forEach(function(planningStageLevel2){
						if (missedLevel2List.includes(planningStageLevel2.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							checkPlanningStagesLevel2Value.push(planningStageLevel2);
							planningStageLevel2List.push(planningStage.PlanningStageGroupName+"#"+planningStageLevel2)
							if (planningStageLevel1List.includes(planningStage.PlanningStageGroupName) == true){ planningStageLevel1List.splice(planningStageLevel1List.indexOf(planningStage.PlanningStageGroupName),1); }
						}
					
					})
				});
			}			

			exportedRequiredModules.request(urlRequestContractStage, function (error, response, contractStageContent) {
				var contractStageList = JSON.parse(contractStageContent);
				contractStageList._source.ContractStages.forEach(function(contractStage) {
					if (contractStagesList.length > 0){
						if (contractStagesList.includes(contractStage.ContractStageGroupName.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							checkContractStagesLevel1Value.push(contractStage.ContractStageGroupName);
							contractStageLevel1List.push(contractStage.ContractStageGroupName)
						}
					}
					if (contractLevel2List.length > 0){
						contractStage.ContractStageNames.forEach(function(contractStageLevel2){
							if (contractLevel2List.includes(contractStageLevel2.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
								checkContractStagesLevel2Value.push(contractStageLevel2);
								contractStageLevel2List.push(contractStage.ContractStageGroupName+"#"+contractStageLevel2)
								if (contractStageLevel1List.includes(contractStage.ContractStageGroupName) == true){ contractStageLevel1List.splice(contractStageLevel1List.indexOf(contractStage.ContractStageGroupName),1); }
							}
							
						})
					}
				});
				checkContractStagesLevel2Value = checkContractStagesLevel2Value.map(function(x){ return x.toLowerCase().replace(', ',' and ').replace('&','and')});
				var missedContractLevel2List = exportedRequiredModules.arrayDiff(contractLevel2List, checkContractStagesLevel2Value);
				if (missedContractLevel2List.length > 0){
				contractStageList._source.ContractStages.forEach(function(contractStage) {
					contractStage.ContractStageNames.forEach(function(contractStageLevel2){
							if (missedContractLevel2List.includes(contractStageLevel2.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
								checkContractStagesLevel2Value.push(contractStageLevel2);
								contractStageLevel2List.push(contractStage.ContractStageGroupName+"#"+contractStageLevel2)
								if (contractStageLevel1List.includes(contractStage.ContractStageGroupName) == true){ contractStageLevel1List.splice(contractStageLevel1List.indexOf(contractStage.ContractStageGroupName),1); }
							}
							
						})
					
					});
				}
				if((checkPlanningStagesLevel1Value.length == planningStagesList.length) && (checkPlanningStagesLevel2Value.length == planningLevel2List.length) && (checkContractStagesLevel1Value.length == contractStagesList.length) && (checkContractStagesLevel2Value.length == contractLevel2List.length)){
					callback(null, planningStageLevel1List, planningStageLevel2List, contractStageLevel1List,contractStageLevel2List);
				}
				else if((checkPlanningStagesLevel1Value.length != planningStagesList.length) || (checkPlanningStagesLevel2Value.length != planningLevel2List.length)){
					callback({Error: 'Invalid PlanningStages/PlanningStagesLevel2 value is given'}, null);
				}
				else if((checkContractStagesLevel1Value.length != contractStagesList.length) || (checkContractStagesLevel2Value.length != contractLevel2List.length)){
					// ContractStages/ContractStagesLevel2
					callback({Error: 'Invalid ContractStages/ContractStagesLevel2 value is given'}, null);
				}
			});
		});
		
	}catch(err){
		callback({Error: 'Stages metadata URL response failed.'}, null);
	}
}