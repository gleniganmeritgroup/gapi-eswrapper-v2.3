'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetProjectDateQuery.js) is used form a query for given Project Date .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryByProjectDate
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for project date. 
	2. 'getSubscriptionQueryObj' returns location and sector based on subscription json.
	3. 'getDateOptionsMetadata' returns metadata of project date options.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByProjectDate = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var startDateFrom = requireQuery.query.StartDateFrom;
	var startDateTo = requireQuery.query.StartDateTo;
	var endDateFrom = requireQuery.query.EndDateFrom;
	var endDateTo = requireQuery.query.EndDateTo;
	var startDatePeriod = requireQuery.query.StartDatePeriod;
	var endDatePeriod = requireQuery.query.EndDatePeriod;
	
	var startDatePeriodList = [];
	var endDatePeriodList = [];
	if (endDatePeriod != undefined){
		endDatePeriod.split(',').forEach(function(endDate){
			endDatePeriodList.push(endDate.toLowerCase());
		});  
	}
	
	if (startDatePeriod != undefined){
		startDatePeriod.split(',').forEach(function(startDate){
			startDatePeriodList.push(startDate.toLowerCase());
		});  
	}
	
	if ((startDateFrom == undefined) && (startDateTo == undefined) && (endDateFrom == undefined) && (endDateTo == undefined) && (startDatePeriod == undefined) && (endDatePeriod == undefined)) {
		callback({Error:"Parameter StartDateFrom/StartDateTo/EndDateFrom/EndDateTo/StartDatePeriod/EndDatePeriod Values is Missing"},null);
	}
	else if((startDateFrom != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(startDateFrom) == null)){
		callback({Error:"Parameter StartDateFrom value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
	else if((startDateTo != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(startDateTo) == null)){
		callback({Error:"Parameter startDateTo value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
	else if((endDateFrom != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(endDateFrom) == null)){
		callback({Error:"Parameter endDateFrom value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
	else if((endDateTo != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(endDateTo) == null)){
		callback({Error:"Parameter endDateTo value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
	else if ((startDateFrom == undefined)  && (startDatePeriodList.length > 1)){
		callback({Error:"Parameter StartDatePeriod should not accept multiple values"},null);
	}
	else if ((endDatePeriod == undefined)  && (endDatePeriodList.length > 1)){
		callback({Error:"Parameter EndDatePeriod should not accept multiple values"},null);
	}
	else if((startDateTo != undefined) && (startDateFrom != undefined) && (exportedRequiredModules.moment(startDateFrom,'DD-MM-YYYY') > exportedRequiredModules.moment(startDateTo,'DD-MM-YYYY'))){
		callback({Error:"StartDateFrom date should not greater than StartDateTo date."},null);
	}
	else if((endDateFrom != undefined) && (endDateTo != undefined) && (exportedRequiredModules.moment(endDateFrom,'DD-MM-YYYY') > exportedRequiredModules.moment(endDateTo,'DD-MM-YYYY'))){
		callback({Error:"EndDateFrom date should not greater than EndDateTo date."},null);
	}
	else{
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			if(err){
				callback(err, null);
			}else{
					
				getDateOptionsMetadata(exportedRequiredModules, startDatePeriodList, endDatePeriodList, function(err, startDatePeriodData, endDataPeriodData){
				
				if (err){
					callback({Error: 'Exception on Date Options Metadata'}, null);
				}
				else if (((endDatePeriod != undefined) && (endDataPeriodData.length == 0)) || ((startDatePeriodData.length == 0) && (startDatePeriod != undefined))){
					callback({Error: 'Invalid Start/End Date Period Given'}, null);
				}
				else{
					try{	
						var startPeriodQuery = '';
						var endPeriodQuery = '';
						var startDateQuery = '';
						var endDateQuery = '';
						
						if (startDatePeriodData.length > 0) {startPeriodQuery = '{"range":{"StartDate":'+startDatePeriodData[0]+'}}';}
						if (endDataPeriodData.length > 0) {endPeriodQuery = '{"range":{"EndDate":'+endDataPeriodData[0]+'}}';}
						
						if ((startDateFrom != undefined) && (startDateTo != undefined)){
							var gteStartDate = exportedRequiredModules.moment(startDateFrom,'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00")+'.000Z';
							var lteStartDate = exportedRequiredModules.moment(startDateTo,'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59")+'.000Z';
							startDateQuery = '{"range":{"StartDate":{"gte":"'+gteStartDate+'","lte":"'+lteStartDate+'"}}}';
						}
						else if ((startDateFrom != undefined) && (startDateTo == undefined)){
							var gteStartDate = exportedRequiredModules.moment(startDateFrom,'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00")+'.000Z';
							
							startDateQuery = '{"range":{"StartDate":{"gte":"'+gteStartDate+'"}}}';
						}
						else if ((startDateFrom == undefined) && (startDateTo != undefined)){
							var lteStartDate = exportedRequiredModules.moment(startDateTo,'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59")+'.000Z';
							
							startDateQuery = '{"range":{"StartDate":{"lte":"'+lteStartDate+'"}}}';
						}
						
						if ((endDateFrom != undefined) && (endDateTo != undefined)){
							var gteEndDate = exportedRequiredModules.moment(endDateFrom,'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00")+'.000Z';
							var lteEndDate = exportedRequiredModules.moment(endDateTo,'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59")+'.000Z';
							endDateQuery = '{"range":{"EndDate":{"gte":"'+gteEndDate+'","lte":"'+lteEndDate+'"}}}';
						}
						else if ((endDateFrom != undefined) && (endDateTo == undefined)){
							var gteEndDate = exportedRequiredModules.moment(endDateFrom,'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00")+'.000Z';
							endDateQuery = '{"range":{"EndDate":{"gte":"'+gteEndDate+'"}}}';
						}
						else if ((endDateFrom == undefined) && (endDateTo != undefined)){
							var lteEndDate = exportedRequiredModules.moment(endDateTo,'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59")+'.000Z';
							endDateQuery = '{"range":{"EndDate":{"lte":"'+lteEndDate+'"}}}';
						}
						var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
						var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
						
						var queryConcat=projectHistories
						
						if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
						if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
						if (startPeriodQuery != ''){ queryConcat = queryConcat +', '+startPeriodQuery; }
						if (endPeriodQuery != ''){ queryConcat = queryConcat +', '+endPeriodQuery; }
						if (startDateQuery != ''){ queryConcat = queryConcat +', '+startDateQuery; }
						if (endDateQuery != ''){ queryConcat = queryConcat +', '+endDateQuery; }
						
						var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
						// console.log("FinalQuery"+FinalQuery);
						callback(null, FinalQuery);
					}catch (err){
						callback(err, null);
					}
					}
				});
				
			}
		});
	}
}
/******************************************************************************
Function: getDateOptionsMetadata
Argument:exportedRequiredModules, startDatePeriodList, endDatePeriodList
Return:  get options for project date.
Usage:
	1. Extracts project date options from metadata url.
*******************************************************************************/
function getDateOptionsMetadata(exportedRequiredModules, startDatePeriodList, endDatePeriodList, callback){
	
	var startDateList = [];
	var endDateList = [];
	// var data = {'Tomorrow' : '{ "gte" : "now+1d/d", "lte" : "now+1d/d"}', 'In the next week' : '{ "gte" : "now+1d/d", "lte" : "now+1w/d"}', 'In the next 2 weeks' : '{ "gte" : "now+1d/d", "lte" : "now+2w/d"}', 'In the next month' : '{ "gte" : "now+1d/d", "lte" : "now+M/d"}', 'In the next 3 months' : '{ "gte" : "now+1d/d", "lte" : "now+3M/d"}', 'In the next 6 months' : '{ "gte" : "now+1d/d", "lte" : "now+6M/d"}', 'In the next year' : '{ "gte" : "now+1d/d", "lte" : "now+1y/d"}', 'Yesterday' : '{ "gte" : "now-1d/d", "lte" : "now-1d/d"}', 'In the last week' : '{ "gte" : "now-1w/d", "lte" : "now-1d/d"}', 'In the last 2 weeks' : '{ "gte" : "now-2w/d", "lte" : "now-1d/d"}', 'In the last month' : '{ "gte" : "now-M/d", "lte" : "now-1d/d"}', 'In the last 3 months' : '{ "gte" : "now-3M/d", "lte" : "now-1d/d"}', 'In the last 6 months' : '{ "gte" : "now-6M/d", "lte" : "now-1d/d"}', 'In the last year' : '{ "gte" : "now-1y/d", "lte" : "now-1d/d"}'};
	var datePeriod = exportedRequiredModules.datePeriodOptionsObj.Options.OptionsJSON;
	startDatePeriodList = startDatePeriodList.map(function(x){ return x.toLowerCase()});
	endDatePeriodList = endDatePeriodList.map(function(x){ return x.toLowerCase()});
	try{
	 	var urlDateOptions = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/projectdateoptions';
		
		exportedRequiredModules.request(urlDateOptions, function (error, response,dateOptionContent) {
			if( dateOptionContent != undefined){
				var dateOption = JSON.parse(dateOptionContent);
				dateOption._source.ProjectDateOptions.forEach(function(option) {
					if (startDatePeriodList.length > 0) {
						if (startDatePeriodList.includes(option.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							startDateList.push(datePeriod[option]);
						}
					}
					if (endDatePeriodList.length > 0) {
						if (endDatePeriodList.includes(option.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							endDateList.push(datePeriod[option]);
						}
					}
				});
				
				callback(null, startDateList, endDateList);
			}
			else{
				callback({Error: 'Date Option metadata URL response failed.'}, null);
			}
		});
	}catch(err){
		callback({Error: 'Date Option metadata URL response failed.'}, null);
	}
}