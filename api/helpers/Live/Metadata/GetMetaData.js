'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetMetaData.js) is used to collect all regions, counties, towns and
postcode, which is come under the each subscription.
********************************************************************************************/
/*******************************************************************************************/


/******************************************************************************
Function: townMetadataExtraction
Argument:townList, locationContent, townContent
Return: list of towns from metadata
Usage:
	1. Extracts town list from metadata
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.townMetadataExtraction = function (townList, locationContent, townContent, callback) {
	try {
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

		townList = townList.map(function (x) { return x.toLowerCase() })

		var sourceTowns = [];
		// console.log("locationList"+locationList);
		var locationList = JSON.parse(locationContent);
		var towns = JSON.parse(townContent);
		var errorMessage;
		locationList._source.Locations.forEach(function (locations) {
			try {
				locations.Counties.forEach(function (countyValue) {
					towns._source.CountiesAndTowns.forEach(function (countiesAndTowns) {
						if (countyValue.toLowerCase() == countiesAndTowns.County.toLowerCase()) {
							countiesAndTowns.Towns.forEach(function (town) {
								if (townList.includes(town.toLowerCase().replace(', ', ' and ').replace('&', 'and')) == true) {
									sourceTowns.push(locations.Region.toLowerCase() + "#" + countyValue.toLowerCase() + "#" + town.toLowerCase());
								}
							});
						}
					});
				});
			}
			catch (err) {
				errorMessage = err;
			}
		});
		callback(null, sourceTowns);
	} catch (err) {
		//	console.log("err" + err);
		callback({ Error: "Exception on Town Metadata extraction" }, null);
	}
}

/******************************************************************************
Function: locationMetadataExtraction
Argument:CountiesList, locationContent
Return: list of counties from metadata
Usage:
	1. Extracts county list from metadata
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.locationMetadataExtraction = function (CountiesList, locationContent, callback) {
	try {
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

		CountiesList = CountiesList.map(function (x) { return x.toLowerCase() })

		var sourceCounties = [];
		var locationList = JSON.parse(locationContent);
		locationList._source.Locations.forEach(function (locations) {
			try {
				locations.Counties.forEach(function (countyValue) {

					if (CountiesList.includes(countyValue.toLowerCase().replace(', ', ' and ').replace('&', 'and')) == true) {
						sourceCounties.push(locations.Region.toLowerCase() + "#" + countyValue.toLowerCase());
					}
				});
			}
			catch (err) {
				locations.Boroughs.forEach(function (countyValue) {

					if (CountiesList.includes(countyValue.toLowerCase().replace(', ', ' and ').replace('&', 'and')) == true) {
						sourceCounties.push(locations.Region.toLowerCase() + "#" + countyValue.toLowerCase());
					}
				});
			}
		});
		callback(null, sourceCounties);
	} catch (err) {
		callback({ Error: "Exception on Location Metadata extraction" }, null);
	}
}

/******************************************************************************
Function: metadataExtraction
Argument:exportedRequiredModules,subscriptionJSONObj,urlRequestLocations,urlRequestTowns,urlRequestPostcode,returnValue
Return: list of region,counties,town,postcode from metadata
Usage:
	1. Extracts region,counties,town,postcode list from metadata
*******************************************************************************/
function metadataExtraction(exportedRequiredModules, subscriptionJSONObj, urlRequestLocations, urlRequestTowns, urlRequestPostcode, returnValue, callback) {
	var subscriberRegionList = [];
	var subscriberCountyList = [];
	var subscriberTownList = [];
	var subscriberPostcodeList = [];
	var locationSource, townSource;
	exportedRequiredModules.request(urlRequestLocations, function (error, response, locationContent) {

		subscriptionJSONObj.Subscription.ProjectRegions.forEach(function (locationLevel1) {
			var region = locationLevel1.Region;
			var countyList = locationLevel1.Counties;
			var locationList = JSON.parse(locationContent);
			locationSource = locationContent;
			locationList._source.Locations.forEach(function (locations) {
				if (region.toLowerCase() == locations.Region.toLowerCase()) {
					subscriberRegionList.push(locations.Region.toLowerCase().replace(', ', ' and ').replace('&', 'and'));
					try {
						if (countyList != "") {

							countyList = countyList.map(function (x) { return x.toLowerCase() })
							locations.Counties.forEach(function (countyData) {

								if ((countyList.includes(countyData.toLowerCase()) == true)) {
									subscriberCountyList.push(countyData.toLowerCase().replace(', ', ' and ').replace('&', 'and'));
								}
							});
						}
						else {
							locations.Counties.forEach(function (county) {
								subscriberCountyList.push(county.toLowerCase().replace(', ', ' and ').replace('&', 'and'));
							});
						}
					} catch (err) {

						if (countyList != "") {
							countyList = countyList.map(function (x) { return x.toLowerCase() })
							locations.Boroughs.forEach(function (borough) {

								if ((countyList.includes(borough.toLowerCase()) == true)) {
									subscriberCountyList.push(borough.toLowerCase().replace(', ', ' and ').replace('&', 'and'));
								}
							});
						}
						else {
							locations.Boroughs.forEach(function (borough) {
								subscriberCountyList.push(borough.toLowerCase().replace(', ', ' and ').replace('&', 'and'));
							});
						}
					}
				}
			});
		});
		// console.log("urlRequestTowns"+urlRequestTowns);
		exportedRequiredModules.request(urlRequestTowns, function (error, response, townContent) {
			townSource = townContent;
			var towns = JSON.parse(townContent);
			towns._source.CountiesAndTowns.forEach(function (countiesAndTowns) {
				if (subscriberCountyList.includes(countiesAndTowns.County.toLowerCase().replace(', ', ' and ').replace('&', 'and')) == true) {
					countiesAndTowns.Towns.forEach(function (town) {
						subscriberTownList.push(town.toLowerCase().replace(', ', ' and ').replace('&', 'and'));
					});
				}
			});
			exportedRequiredModules.request(urlRequestPostcode, function (error, response, postcodeContent) {
				var postcodeSource = JSON.parse(postcodeContent);
				postcodeSource.hits.hits.forEach(function (postcodeData) {
					// console.log(subscriberCountyList);
					if (subscriberCountyList.includes(postcodeData._source.County.toLowerCase()) == true) {
						subscriberPostcodeList.push(postcodeData._source.PostcodeDistrict.toLowerCase());
					}
				});
				if (returnValue == "Town") { callback(null, subscriberTownList, locationSource, townSource); }
				else if (returnValue == "Region") { callback(null, subscriberRegionList, locationSource, townSource); }
				else if (returnValue == "County") { callback(null, subscriberCountyList, locationSource, townSource); }
				else if (returnValue == "Postcode") { callback(null, subscriberPostcodeList, locationSource, townSource); }
				else if (returnValue == "search") { callback(null, subscriberRegionList, subscriberCountyList, subscriberTownList, subscriberPostcodeList); }

			});
		});
	});
}

/******************************************************************************
Function: toGetPostcodeDistrictSize
Argument:exportedRequiredModules,urlRequestPostcode
Return: get postcode content
Usage:
	1. Extracts postcode list from metadata
*******************************************************************************/
function toGetPostcodeDistrictSize(exportedRequiredModules, urlRequestPostcode, callback) {
	exportedRequiredModules.request(urlRequestPostcode, function (error, response, postcodeContent) {
		var postcodeContent = JSON.parse(postcodeContent);
		callback(null, postcodeContent.hits.total);
	});
}
/******************************************************************************
Function: getLocationsTownsPostcodes
Argument:subscriptionJSONObj, returnValue
Return: get location,town,postcode content
Usage:
	1. Get Authorized location,town,postcode list from metadata.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getLocationsTownsPostcodes = function (subscriptionJSONObj, returnValue, callback) {
	try {
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

		var urlRequestLocations = exportedRequiredModules.elasticIndexHost + "/" + exportedRequiredModules.elasticIndexName + '/metadata/locations';
		var urlRequestTowns = exportedRequiredModules.elasticIndexHost + "/" + exportedRequiredModules.elasticIndexName + '/metadata/towns';
		var urlRequestPostcode = exportedRequiredModules.elasticIndexHost + "/" + exportedRequiredModules.elasticIndexName + '/postcode_district/_search';
		toGetPostcodeDistrictSize(exportedRequiredModules, urlRequestPostcode, function (err, result) {
			urlRequestPostcode = urlRequestPostcode + "?size=" + result
			metadataExtraction(exportedRequiredModules, subscriptionJSONObj, urlRequestLocations, urlRequestTowns, urlRequestPostcode, returnValue, function (err, subscriberRegionList, subscriberCountyList, subscriberTownList, subscriberPostcodeList) {
				callback(null, subscriberRegionList, subscriberCountyList, subscriberTownList, subscriberPostcodeList);
			});
		});
	} catch (err) {
		callback({ Error: "Exception on Location Metadata" }, null);
	}
}
/******************************************************************************
Function: getSectorsCatagories
Argument:subscriptionSectors
Return: get sectors,categories content
Usage:
	1. Get Authorized sectors,categories list from metadata.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getSectorsCatagories = function (subscriptionSectors, callback) {
	try {
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
		var authorizedSectorList = [];
		var authorizedSubSectorList = [];

		var subscriptionSubSectors = []; /* Subscription having subSector/category this will come as second argument */

		subscriptionSectors = subscriptionSectors.map(function (x) { return x.toLowerCase() })
		subscriptionSubSectors = subscriptionSubSectors.map(function (x) { return x.toLowerCase() })

		var urlRequestSectors = exportedRequiredModules.elasticIndexHost + "/" + exportedRequiredModules.elasticIndexName + '/metadata/sectors';

		exportedRequiredModules.request(urlRequestSectors, function (error, response, sectorsContent) {

			var sectorList = JSON.parse(sectorsContent);
			sectorList._source.Sectors.forEach(function (sectorGroup) {
				if (subscriptionSectors.includes(sectorGroup.SectorGroupName.toLowerCase()) == true) {
					authorizedSectorList.push(sectorGroup.SectorGroupName.toLowerCase());

					sectorGroup.SectorNames.forEach(function (sector) {
						if ((subscriptionSubSectors.includes(sectorGroup.SectorGroupName.toLowerCase()) == true) || (subscriptionSubSectors.length == 0)) {
							authorizedSubSectorList.push(sector.toLowerCase());
						}
					});

				}
			});
			callback(null, authorizedSectorList, authorizedSubSectorList, sectorsContent);
		});
	} catch (err) {
		callback({ Error: "Exception on Sector Metadata" }, null);
	}
}

/******************************************************************************
Function: getMetadataSource
Argument:request
Return: get metadata source
Usage:
	1. Get metadata source from metadata url.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getMetadataSource = function (request, metadataID, size, callback) {
	try {
		var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
		//	var metadataID = request.swagger.params['id'].value
		//	var size = request.query.Size;
		// console.log("size"+size);
		var url;

		if (metadataID == 'postcode_district') {
			url = exportedRequiredModules.elasticIndexHost + "/" + exportedRequiredModules.elasticIndexName + '/postcode_district/_search';
		}
		else if (metadataID == '_search') {
			url = exportedRequiredModules.elasticIndexHost + "/" + exportedRequiredModules.elasticIndexName + '/metadata/_search';
		}
		else {
			url = exportedRequiredModules.elasticIndexHost + "/" + exportedRequiredModules.elasticIndexName + '/metadata/' + metadataID;
		}
		if ((size != parseInt(size)) && (size != undefined)) {
			callback({ Error: "Parameter size is invalid" }, null);
		}
		else if (size != undefined) {
			url = url + "?size=" + size
		}
		exportedRequiredModules.request(url, function (error, response, metaSource) {
			metaSource = JSON.parse(metaSource);
			var sourceData = JSON.stringify(metaSource);
			sourceData = sourceData.replace(/\"_index\":\s*\"[^\"]*?\"\,/ig, '');
			sourceData = sourceData.replace(/\"_type\":\s*\"[^\"]*?\"\,/ig, '');
			sourceData = sourceData.replace(/\"_version\":\s*(?:\"[^\"]*?\"\,|[\d]+\,)/ig, '');
			sourceData = sourceData.replace(/\"_score\":\s*(?:\"[^\"]*?\"|null)\,/ig, '');
			sourceData = sourceData.replace(/_id/g, 'id');
			sourceData = sourceData.replace(/_source/g, 'source');

			callback(null, JSON.parse(sourceData));

		});

	} catch (err) {
		callback({ Error: "Exception on Metadata source extraction" }, null);
	}
}
/******************************************************************************
Function: getMetadataSearchCompare
Argument:SubscritionJSON,ResultJSON
Return: get metadata source based on subscription
Usage:
	1. Get metadata source from metadata url.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getMetadataSearchCompare = function (request, SubscritionJSON, ResultJSON, callback) {

	var Towns = request.query.Town;
	

	var SubscritionJSONObj = JSON.parse(SubscritionJSON);
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	ResultJSON.hits.hits.forEach(function (indexData, hitsIndexNo) {
		//	console.log(indexData.id);

		if (indexData.id == 'locations') {
			exportedRequiredModules.getMetadataObj.getMetadataLocationCompare(request, indexData, SubscritionJSONObj, function (error, locationRes) {
				//				ResultJSON.hits.hits[hitsIndexNo].source.Locations = locationRes;



				ResultJSON = JSON.stringify(ResultJSON);
			});


		}

	});

	var ResultJsonobj = JSON.parse(ResultJSON);
	ResultJsonobj.hits.hits.forEach(function (indexData, hitsIndexNo) {
		if (indexData.id == 'sectors') {

			exportedRequiredModules.getMetadataObj.getMetadataSectorCompare(indexData, SubscritionJSONObj, function (error, sectorResult) {

				//	ResultJsonobj.hits.hits[hitsIndexNo].source.Sectors = sectorResult;
				ResultJSON = JSON.stringify(ResultJsonobj);

			});

		}

	});


	exportedRequiredModules.getMetadataObj.getMetadatasSubscribedCounty(ResultJSON, function (error, SubcscibeddcountyList) {
		var TempTownRes = [];
		var ResultsJsonobj = JSON.parse(ResultJSON);
		ResultsJsonobj.hits.hits.forEach(function (indexData, hitsIndexNo) {
			if (indexData.id == 'towns') {
				exportedRequiredModules.getMetadataObj.getMetadataTownCompare(indexData, SubcscibeddcountyList, function (error, TownResult) {
					//	console.log(TownResult);
					if (Towns != undefined) {
						TownResult.forEach(function (innerindexCountyAndTownData, innerhitsIndexNo) {
							innerindexCountyAndTownData.Towns.forEach(function (innerindexTownData, innerhitsIndexNo) {

								if (((innerindexTownData).indexOf(Towns)) > -1) {
									//		console.log(innerindexTownData);
									TempTownRes.push(innerindexTownData);
								}

							});
							innerindexCountyAndTownData.Towns = TempTownRes;
							TempTownRes = [];

						});
					}
					ResultsJsonobj.hits.hits[hitsIndexNo].source.CountiesAndTowns = TownResult;
					ResultJSON = JSON.stringify(ResultsJsonobj);

				});


			}
		});

	});
	ResultJSON = JSON.parse(ResultJSON);
	var metaDataId = 'postcode_district';
	var requestedSize=request.query.PostcodeSize;
	if(requestedSize !=undefined){
		var size = requestedSize;
	}
	// else{
	// var size = parseInt(exportedRequiredModules.credentialsObj.ElasticIndex.metadata_size);
	// }

	exportedRequiredModules.getMetadataObj.getMetadataPostcodes(request, metaDataId, size, SubscritionJSON, function (error, metadataPostSource) {
		var metadataPostSourceResult = {}
		metadataPostSourceResult["Metadata"] = ResultJSON.hits.hits
		metadataPostSourceResult["Postcode_district"] = metadataPostSource.hits.hits

		//console.log(JSON.stringify(metadataPostSourceResult));
		// console.log(JSON.stringify(ResultJSON));
		//	ResultJSON.push(metadataSource);
		// console.log("ResultJSON ",typeof(ResultJSON));
		//	console.log(JSON.stringify(ResultJSON));

		callback(null, metadataPostSourceResult);
	});
	//	console.log(JSON.stringify(ResultJSON));


}

/******************************************************************************
Function: getMetadataLocationCompare
Argument:indexData,SubscritionJSONObj
Return: get metadata source based on subscribed location
Usage:
	1. Get metadata source from metadata url and compare locations with subscription.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getMetadataLocationCompare = function (request, indexData, SubscritionJSONObj, callback) {
	var locationRes = [];
	var locationResTemp = [];
	var Region = request.query.Region;
	var County = request.query.County;
	var Boroughs = request.query.Boroughs;

	indexData.source.Locations.forEach(function (innerindexData, innerhitsIndexNo) {
		SubscritionJSONObj.Subscription.ProjectRegions.forEach(function (SubscribedRegionData, SubscribedRegionIndexNo) {
			if (innerindexData.Region == SubscribedRegionData.Region) {

				if (SubscribedRegionData.Counties.length > 0) {

					if (innerindexData.Region != 'London') {
						innerindexData.Counties = SubscribedRegionData.Counties
					}
					else {
						innerindexData.Boroughs = SubscribedRegionData.Counties;
					}

					// delete ResultJSON.hits.hits[hitsIndexNo].source.Locations[innerhitsIndexNo];
				}
				else {
					if (innerindexData.Region != 'London') {
						innerindexData.Counties = innerindexData.Counties
					}
					else {
						innerindexData.Boroughs = innerindexData.Boroughs
					}
				}
				locationRes.push(innerindexData);
			}
		});
	});
	var TempRegion = [];
	if (Region != undefined) {
		locationRes.forEach(function (locationResults, locationResultsIndexNo) {

			if (((locationResults.Region).indexOf(Region)) > -1) {
				TempRegion.push(locationResults);
			}
		});
		if (TempRegion.length > 0) {
			locationRes = TempRegion;
		}
		else {
			locationRes = [];
		}
	}

	if (County != undefined) {
		var TempCounty = [];
		locationRes.forEach(function (locationResults, locationResultsIndexNo) {
			//	console.log(locationResults);

			if (locationResults.Region != 'London') {

				locationResults.Counties.forEach(function (locationCountyResults, locationTownResultsIndexNo) {

					//console.log(((locationCountyResults).indexOf(County)));
					if (((locationCountyResults).indexOf(County)) > -1) {
						//		console.log(locationCountyResults);

						TempCounty.push(locationCountyResults);

					}

				});
				//	console.log(TempCounty, " TempCounty");

				if (TempCounty.length > 0) {
					locationResults.Counties = TempCounty;
					TempCounty = [];
				} else {

					locationResults.Counties = [];
				}


			}
		});
	}
	if (Boroughs != undefined) {
		var TempCounty = [];
		locationRes.forEach(function (locationResults, locationResultsIndexNo) {
			//console.log(locationResults);
			if (locationResults.Region == 'London') {
				locationResults.Boroughs.forEach(function (locationCountyResults, locationTownResultsIndexNo) {

					//	console.log(((locationCountyResults).indexOf(County)));
					if (((locationCountyResults).indexOf(Boroughs)) > -1) {
						//		console.log(locationCountyResults);

						TempCounty.push(locationCountyResults);
					}


				});
				//console.log(TempCounty, " TempCounty");

				if (TempCounty.length > 0) {
					locationResults.Boroughs = TempCounty;
					TempCounty = [];
				} else {

					locationResults.Boroughs = [];
				}


			}
		});
	}




	indexData.source.Locations = locationRes;
	callback(null, indexData);

}

/******************************************************************************
Function: getMetadataSectorCompare
Argument:indexData,SubscritionJSONObj
Return: get metadata source based on subscribed sectors
Usage:
	1. Get metadata source from metadata url and compare sectors with subscription.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getMetadataSectorCompare = function (indexData, SubscritionJSONObj, callback) {
	var sectorResult = [];
	indexData.source.Sectors.forEach(function (innerindexSectorData, innerhitsIndexNo) {

		SubscritionJSONObj.Subscription.ProjectSectors.forEach(function (SubscribedSectorData, SubscribedSectorIndexNo) {


			if (innerindexSectorData.SectorGroupName == SubscribedSectorData.Sector) {

				sectorResult.push(innerindexSectorData);
			}


		});

	});
	indexData.source.Sectors = sectorResult;
	callback(null, indexData);
}
/******************************************************************************
Function: getMetadatasSubscribedCounty
Argument:ResultJSON
Return: get metadata source based on subscribed county
Usage:
	1. Get metadata source from metadata url and compare county with subscription.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getMetadatasSubscribedCounty = function (ResultJSON, callback) {

	var SubcscibeddcountyList = [];
	var ResultsJsonobj = JSON.parse(ResultJSON);
	ResultsJsonobj.hits.hits.forEach(function (indexData, hitsIndexNo) {
		if (indexData.id == 'locations') {
			indexData.source.Locations.forEach(function (innerindexData, innerhitsIndexNo) {

				if (innerindexData.Region != 'London') {
					innerindexData.Counties.forEach(function (countyRes, indexNoCounty) {
						SubcscibeddcountyList.push(countyRes);
						//	console.log(countyRes);

					});
				}
				else {
					innerindexData.Boroughs.forEach(function (countyRes, indexNoCounty) {
						SubcscibeddcountyList.push(countyRes);
						//	console.log(countyRes);

					});

				}
			});
		}



	});
	callback(null, SubcscibeddcountyList);
}
/******************************************************************************
Function: getMetadataTownCompare
Argument:indexData,SubcscibeddcountyList
Return: get metadata source based on subscribed towns
Usage:
	1. Get metadata source from metadata url and compare towns with subscription.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getMetadataTownCompare = function (indexData, SubcscibeddcountyList, callback) {
	var TownResult = [];
	indexData.source.CountiesAndTowns.forEach(function (innerindexCountyAndTownData, innerhitsIndexNo) {
		SubcscibeddcountyList.forEach(function (SubscribedCounty, innerhitsSubsIndexNo) {
			if (innerindexCountyAndTownData.County == SubscribedCounty) {
				TownResult.push(innerindexCountyAndTownData);
			}
		});
	});
	callback(null, TownResult);
}


/******************************************************************************
Function: getSubscribedCountyWithSalesForce
Argument:size,subscriptionJSON
Return: get metadata source based on subscription data
Usage:
	1. Get metadata source from metadata url.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getSubscribedCountyWithSalesForce = function (request, size, subscriptionJSON, callback) {

	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	var Towns = request.query.Town;
	var metadataIdSearch = '_search';
	var indexDataResult = '';
	var TempResult = [];

	exportedRequiredModules.getMetadataObj.getMetadataSource(request, metadataIdSearch, size, function (error, metadataSource) {
	//	console.log(metadataSource);
		if (error) {
			callback({ "Error": "ërror" }, null)
		} else {
			exportedRequiredModules.getMetadataObj.getMetadataSearchCompare(request, subscriptionJSON, metadataSource, function (error, metadataResult) {
				//	console.log("In",metadataResult );
				if (error) {
					callback({ "Error": "ërror" }, null)
				} else {
				
					metadataResult.Metadata.forEach(function (indexData, hitsIndexNo, arrayLen) {
					//console.log("In1", indexData);
						if (indexData.id == 'towns') {

							indexDataResult = indexData;
						}

						if (hitsIndexNo == ((arrayLen.length) - 1)) {
							if (Towns != undefined) {
								//console.log("In", Towns);
								indexDataResult.source.CountiesAndTowns.forEach(function (ResulTownsElements, hitsIndexNo) {
									ResulTownsElements.Towns.forEach(function (ResulTownElements, hitsInnerIndexNo) {

										
										if (((ResulTownElements).indexOf(Towns)) > -1) {
											TempResult.push(ResulTownElements);
										}

									});
										
									ResulTownsElements.Towns = TempResult;
									TempResult = [];

								});


							}

							callback(null, indexDataResult);
						}

					});

				}
			});

		}

	});


}

/******************************************************************************
Function: getMetadataPostcodes
Argument:metaDataId,size,subscriptionJSON
Return: get metadata source based on subscribed postcodes
Usage:
	1. Get metadata source from metadata url and compare postcodes with subscription.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getMetadataPostcodes = function (request, metaDataId, size, subscriptionJSON, callback) {
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
	var SubscritionJSONObj = JSON.parse(subscriptionJSON);
	var postcodes = request.query.Postcodes;
	var TempResult = [];
	var ResultPostCodes = [];
	exportedRequiredModules.getMetadataObj.getMetadataSource(request, metaDataId, size, function (error, metadataSource) {
		if (error) {
			callback({ "Error": "ërror" }, null)
		} else {
			var totalResults = parseInt(metadataSource.hits.total);
			exportedRequiredModules.getMetadataObj.getMetadataSource(request, metaDataId, totalResults, function (error, metadataAllSource) {
				if (error) {
					callback({ "Error": "ërror" }, null)
				} else {
					SubscritionJSONObj.Subscription.ProjectRegions.forEach(function (SubscribedLocationsData, SubscribedLocationIndexNo) {
						if ((SubscribedLocationsData.Counties.length) > 0) {
							SubscribedLocationsData.Counties.forEach(function (SubscribedCountyData, SubscribedLocationIndexNo) {
								metadataAllSource.hits.hits.forEach(function (postcodesArray, hitsindexNo) {
									if (SubscribedCountyData == postcodesArray.source.County) {
										ResultPostCodes.push(postcodesArray);
										//		console.log(postcodesArray);
									}
								});

							});
						}
						else {
							metadataAllSource.hits.hits.forEach(function (postcodesArray, hitsindexNo) {
								if (SubscribedLocationsData.Region == postcodesArray.source.Region) {
									ResultPostCodes.push(postcodesArray);
									//console.log(postcodesArray);
								}
							});
						}
					});

					if (postcodes != undefined) {
						ResultPostCodes.forEach(function (ResultPostcodeElements, hitsindexNo) {

							if (((ResultPostcodeElements.source.PostcodeDistrict).indexOf(postcodes)) > -1) {
								TempResult.push(ResultPostcodeElements);
							}
						});
						ResultPostCodes = TempResult;
					}
					exportedRequiredModules.getMetadataObj.getMetadataFinalResults(ResultPostCodes, size, function (error, FinalResult) {
						metadataAllSource.hits.hits = FinalResult;
					//	console.log(typeof (metadataAllSource));
						callback(null, metadataAllSource);
					});
				}
			});
		}
	});
}

/******************************************************************************
Function: getMetadataFinalResults
Argument:ResultPostCodes,size
Return: get metadata source
Usage:
	1. Get final metadata source from metadata url after comparing with subscription.
	2. This Function is exported as it will be visible to rest of program when
	   called from helper function.
*******************************************************************************/
exports.getMetadataFinalResults = function (ResultPostCodes, size, callback) {
	var FinalResult = [];

	if (parseInt(ResultPostCodes.length) > size) {
		for (var resultcount = 0; resultcount < size; resultcount++) {
			FinalResult.push(ResultPostCodes[resultcount]);
		}
	}
	else {
		FinalResult = ResultPostCodes;
	}
	callback(null, FinalResult);
}