'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetSubscriptionFromMongoDB.js) is used get a subscription Json from MongoDB.
********************************************************************************************/
/*******************************************************************************************/
var yamlConfig = require('js-yaml');
var fs = require('fs');
let SalesforceConnection = require("node-salesforce-connection");
let moment = require('moment');

/******************************************************************************
Function: getSubscriptionJSON
Argument:keyValue
Return: return json 
Usage:
	1. Returns a Subscription JSON from Mongodb.
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/

/******************************************************************************
Function: handler
Argument:query
Usage:
	1. Function handles request by passing query to salesforce connection and returning the result.
*******************************************************************************/
async function handler (userID)  {
    
    let sfConn = new SalesforceConnection();
    var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
    
    var credentialsObj = yamlConfig.safeLoad(fs.readFileSync('./config/Credentials.yml', 'utf8'));
    
    let username = credentialsObj.Salesforce.username;
    let password = credentialsObj.Salesforce.password;
    let token = credentialsObj.Salesforce.token;
    let hostname = credentialsObj.Salesforce.host;
    let apiVersion = credentialsObj.Salesforce.version;

    let queryPath = "/services/data/v"+apiVersion+"/query/?q=";

    if(exportedRequiredModules.SFConnection == ""){
        // console.log("connection empty")
        await sfConn.soapLogin({
            hostname: hostname,
            apiVersion: apiVersion,
            username: username,
            password: password+token,
        });
        exportedRequiredModules.SFConnection=sfConn;
        var query = credentialsObj.Salesforce.query.replace('userID', "'"+userID+"'");
        let recentAccounts = await sfConn.rest(queryPath + encodeURIComponent(query));
        return recentAccounts
    }else{
        // console.log("connection not empty")
        try{
            var query = credentialsObj.Salesforce.query.replace('userID', "'"+userID+"'");
            let recentAccounts = await exportedRequiredModules.SFConnection.rest(queryPath + encodeURIComponent(query));
            return recentAccounts
        }catch(error){
            // console.log("connection expired")
            await sfConn.soapLogin({
                hostname: hostname,
                apiVersion: apiVersion,
                username: username,
                password: password+token,
            });
            exportedRequiredModules.SFConnection=sfConn;
            var query = credentialsObj.Salesforce.query.replace('userID', "'"+userID+"'");
            let recentAccounts = await sfConn.rest(queryPath + encodeURIComponent(query));
            return recentAccounts
        }
        
    }
     
}

/******************************************************************************
Function: subscription
Argument:query
Usage:
	1. Returns records based on the query from salesforce.
*******************************************************************************/
function subscription(userID, callback){
    //console.log(userID);
    var initializePromise =  handler(userID);

    initializePromise.then(function(result) {
        var userDetails = result;
      //  console.log(userDetails);
        callback(null,userDetails);
    }, function(err) {
        callback(err,null);
    })
}

// function getSubscriptionJSON (userID, callback) {
exports.getSubscriptionJSON= function (userID, callback) {
    subscription(userID, function(error,response){
        if(error){
            callback(error,null);
        }else{
        var subscriptionList = []
        for (let account of response.records) {
            
            var subscriptionJSON = {}
            subscriptionJSON.Id = account.AccountId
            subscriptionJSON.Subscription_JSON__c = account.Contact.Account.Subscription_JSON__c
            subscriptionJSON.GP_NoteSharingEnabled__c = account.Contact.Account.GP_NoteSharingEnabled__c
            
            subscriptionList.push(subscriptionJSON);
        }   
        if(subscriptionList.length > 0){
            callback(null,subscriptionList);
        }else{
            callback({"Error" : "Invalid Account"},null);
        }
        
        }
    });
    
}
// var userID='0056E000002Dplh'
// getSubscriptionJSON(userID, function (error, subscriptionList) {
//     console.log("subscriptionList",subscriptionList[0].Id)

// })