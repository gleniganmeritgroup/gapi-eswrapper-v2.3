'use strict';

/********************************************************************************************
 ********************************************************************************************
 * This is unit test cases for Glenigan API Application - Live endpoints (unitTestCase_Live.js) 
 ********************************************************************************************
 ********************************************************************************************
 */

/* Requiring the npm modules for unit testing */
var expect  = require('chai').expect;
var request = require('request');
var mongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var elasticsearch = require('elasticsearch');
var yamlConfig = require('js-yaml');
var iniParserModule = require('parse-ini');
var moment = require('moment');
var fs = require('fs');
var async = require('async');
var salesforceConnection = require("node-salesforce-connection");
var credentialsObject = yamlConfig.safeLoad(fs.readFileSync('./config/Credentials.yml', 'utf8'));

var subscriptionJSON = '{"SubscriptionExists": true,"Subscription": {"SubscriptionStartDate": "2014-09-30","SubscriptionEndDate": "2019-02-15","ProjectSectors": [{"Sector": "Private Housing"},{"Sector": "Industrial"},{"Sector": "Offices"},{"Sector": "Retail"},{"Sector": "Hotel & Leisure"},{"Sector": "Education"},{"Sector": "Community & Amenity"},{"Sector": "Health"},{"Sector": "Utilities"},{"Sector": "Infrastructure"}],"ProjectRegions": [{"Region": "London","Counties": []},{"Region": "East Midlands","Counties": ["Lincolnshire"]},{"Region": "Yorkshire and the Humber","Counties": ["Humberside"]}],"InsightSectors": [],"InsightRegions": [],"HasSmall": false,"HasReporting": false,"HasAPI": true,"HasInsight": false,"HasOpportunities": false,"HasCompanyProfiles": false}}';

var inputDataForExceptions = [
    //radius
    {"controller":"getLocationsQueryObj","helper":"getQueryByRadius","query":{"Radius":"5mm","Lat":"50.7235471","Lon":"-1.8664622"}, "error": "Invalid radius unit is given"},
    //postcode
    {"controller":"getLocationsQueryObj","helper":"getQueryByPostcode","query":{"PostCodeDistricts":"DN38112"}, "error": "Invalid PostCodeDistricts value is given"},
    //town
   {"controller":"getLocationsQueryObj","helper":"getQueryByTown","query":{"TownNames":"Barnetby11"}, "error": "Invalid TownNames value is given"},
    //county
    {"controller":"getLocationsQueryObj","helper":"getQueryByCounty","query":{"CountyNames":"Lincolnshire11"}, "error": "Invalid CountyNames value is given"},
    //region
    {"controller":"getLocationsQueryObj","helper":"getQueryByRegions","query":{"RegionNames":"East Midlands11"}, "error": "Invalid RegionNames value is given"},
    // Status
    {"controller":"getStatusQueryObj","helper":"getQueryByStatus","query":{"StatusNames":"Cancelled1"}, "error": "Invalid StatusNames value is given"},
    // Size
    {"controller":"getSizesQueryObj","helper":"getQueryBySizes","query":{"Names":"Large1"}, "error": "Invalid Names value is given"},
    // Stages
    {"controller":"getStageQueryObj","helper":"getQueryByStages","query":{"Stages":"Early Planning1","Level2":"Outline Grant after Appeal"}, "error": "Invalid Stages value is given"},
    {"controller":"getStageQueryObj","helper":"getQueryByStagesBoth","query":{"PlanningStages":"Early Planning","PlanningStageLevel2":"Outline Grant after Appeal1","ContractStages":"Pre-Tender1"}, "error": "Invalid PlanningStageLevel2 value is given"},
    // Primary Sector
    {"controller":"getSectorsQueryObj","helper":"getQueryByPrimarySector","query":{"Sectors":"Health","Categories":"Colleges1"}, "error": "Invalid Categories value is given"},
    // Secondary Sector
    {"controller":"getSectorsQueryObj","helper":"getQueryBySecondarySector","query":{"Sectors":"Health1","Categories":"Colleges"}, "error": "Invalid Sectors value is given"},
    // Primary Or Secondary Sector
    {"controller":"getSectorsQueryObj","helper":"getQueryByPrimaryAndSecondarySector","query":{"Sectors":"Health1","Categories":"Colleges"}, "error": "Invalid Sectors value is given"},
    // Values
    {"controller":"getValuesQueryObj","helper":"getQueryByValues","query":{"From":"2.5ma","To":"5m"}, "error": "Invalid From value unit is given"},
    // Material
    {"controller":"getMaterialsQueryObj","helper":"getQueryByMaterials","query":{"MaterialParents":"lift1","Materials":"car"}, "error": "Invalid MaterialParents value is given"},
    // Contract Type
    {"controller":"getContractTypeQueryObj","helper":"getQueryByContractType","query":{"Types":"Self-Build1"}, "error": "Invalid Types value is given"},
    // Development Type
    {"controller":"getDevelopmentTypeQueryObj","helper":"getQueryByDevelopmentType","query":{"Types":"New1"}, "error": "Invalid Types value is given"},
    // Contract Development Type
    {"controller":"getContractDevTypeQueryObj","helper":"getQueryByContractDevType","query":{"ContractTypes":"Land Sale1, Self-Build1","DevelopmentTypes":"Extension, New"}, "error": "Invalid ContractTypes value is given"},
    // Planning Application
        //PlanningAuthority
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByPlanningAuthority","query":{"CouncilNames":"Aberdeen1"}, "error": "Invalid CouncilNames value is given"},
        //planningType
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByPlanningType","query":{"PlanningNames":"Pre-Planning1"}, "error": "Invalid PlanningNames value is given"},
        //applicationType
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByApplicationType","query":{"ApplicationNames":"Retention1,Building notices1"}, "error": "Invalid ApplicationNames value is given"},
        //planningApplication
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByPlanningApplication","query":{"CouncilNames": undefined,"PlanningNames":"Pre-Planning,Detailed Planning","ApplicationNames":"Retention,Building notices"}, "error": "parameter CouncilNames is missing"},
        //applicationDate
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByApplicationDate","query":{"From":"04-1-2018","To":"02-03-2018"}, "error": "Invalid From value is given"},
        //permissionDate
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByPermissionDate","query":{"From":"04-1-2018","To":"02-03-2018"}, "error": "Invalid From value is given"},
        //refusedDate
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByRefusedDate","query":{"From":"04-01-2018","To":"02-3-2018"}, "error": "Invalid To value is given"},
        //withdrawnDate
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByWithdrawnDate","query":{"From":"04-01-2018","To":"02-3-2018"}, "error": "Invalid To value is given"},
    // Project Dates
    {"controller":"getProjectDateQueryObj","helper":"getQueryByProjectDate","query":{"StartDateFrom":"04-01-2018","StartDateTo":"02-2-2018"}, "error": "Invalid StartDateTo value is given"},
    // Dimension
        //dimensionSiteArea
        {"controller":"getDimensionsQueryObj","helper":"getQueryBySiteArea","query":{"From":"2","To":"3.5"}, "error": "Invalid To value is given. It should be integer"},
        //dimensionFloorArea
        {"controller":"getDimensionsQueryObj","helper":"getQueryByFloorArea","query":{"From":"2","To":"3.5"}, "error": "Invalid To value is given. It should be integer"},
        //dimensionUndergroundStoreys
        {"controller":"getDimensionsQueryObj","helper":"getQueryByUndergroundStoreys","query":{"From":"2","To":"3.5"}, "error": "Invalid To value is given. It should be integer"},
        //dimensionLength
        {"controller":"getDimensionsQueryObj","helper":"getQueryByLength","query":{"From":"2","To":"3.5"}, "error": "Invalid To value is given. It should be integer"},
        //dimensionStructures
        {"controller":"getDimensionsQueryObj","helper":"getQueryByStructures","query":{"From":"2","To":"3.5"}, "error": "Invalid To value is given. It should be integer"},
        //dimensionParkingSpaces
        {"controller":"getDimensionsQueryObj","helper":"getQueryByParkingSpaces","query":{"From":"2","To":"3.5"}, "error": "Invalid To value is given. It should be integer"},
        //dimensionUnderGroundParking
        {"controller":"getDimensionsQueryObj","helper":"getQueryByBasementParkingSpaces","query":{"From":"2","To":"3.5"}, "error": "Invalid To value is given. It should be integer"},
        //dimensionVerticalHeight
        {"controller":"getDimensionsQueryObj","helper":"getQueryByVerticalHeight","query":{"From":"2","To":"3.5"}, "error": "Invalid To value is given. It should be integer"},
        //dimensionUnits
        {"controller":"getDimensionsQueryObj","helper":"getQueryByUnits","query":{"From":"2","To":"3.5"}, "error": "Invalid To value is given. It should be integer"},
        //dimension
        {"controller":"getDimensionsQueryObj","helper":"getQueryByDimensions","query":{"SiteArea":"From:2,To:3","FloorArea":"From:2,To:3","Storeys":"From:2,To:3","UndergroundStoreys":"From:2,To:3","Length":"From:2,To:3","Blocks":"From:2,To:3","ParkingSpaces":"From:2,To:3","UndergroundParkingSpaces":"From:2,To:3","Height":"From:2.5,To:3","Units":"From:2,To:3"}, "error": "Invalid Height value is given. It should be integer"},
    //Role
    {"controller":"getRoleQueryObj","helper":"getQueryByRoles","query":{"RoleLocationRegions":"East Midlands,London","Roles":"Client1,Fit Out Contractor1"}, "error": "Invalid Roles value is given"},
];

var inputDataForQueryParsing = [
    //radius
    {"controller":"getLocationsQueryObj","helper":"getQueryByRadius","query":{"Radius":"5mi","Lat":"50.7235471","Lon":"-1.8664622"}},
    //postcode
    {"controller":"getLocationsQueryObj","helper":"getQueryByPostcode","query":{"PostCodeDistricts":"DN38"}},
    //town
   {"controller":"getLocationsQueryObj","helper":"getQueryByTown","query":{"TownNames":"Barnetby"}},
    //county
    {"controller":"getLocationsQueryObj","helper":"getQueryByCounty","query":{"CountyNames":"Lincolnshire"}},
    //region
    {"controller":"getLocationsQueryObj","helper":"getQueryByRegions","query":{"RegionNames":"East Midlands"}},
    // Status
    {"controller":"getStatusQueryObj","helper":"getQueryByStatus","query":{"StatusNames":"Cancelled"}},
    // Size
    {"controller":"getSizesQueryObj","helper":"getQueryBySizes","query":{"Names":"Large"}},
    // Stages
    {"controller":"getStageQueryObj","helper":"getQueryByStages","query":{"Stages":"Early Planning","Level2":"Outline Grant after Appeal"}},
    {"controller":"getStageQueryObj","helper":"getQueryByStagesBoth","query":{"PlanningStages":"Early Planning","PlanningStageLevel2":"Outline Grant after Appeal","ContractStages":"Pre-Tender"}},
    // Primary Sector
    {"controller":"getSectorsQueryObj","helper":"getQueryByPrimarySector","query":{"Sectors":"Health","Categories":"Colleges"}},
    // Secondary Sector
    {"controller":"getSectorsQueryObj","helper":"getQueryBySecondarySector","query":{"Sectors":"Health","Categories":"Colleges"}},
    // Primary Or Secondary Sector
    {"controller":"getSectorsQueryObj","helper":"getQueryByPrimaryAndSecondarySector","query":{"Sectors":"Health","Categories":"Colleges"}},
    // Values
    {"controller":"getValuesQueryObj","helper":"getQueryByValues","query":{"From":"2.5m","To":"5m"}},
    // Material
    {"controller":"getMaterialsQueryObj","helper":"getQueryByMaterials","query":{"MaterialParents":"lift","Materials":"car"}},
    // Contract Type
    {"controller":"getContractTypeQueryObj","helper":"getQueryByContractType","query":{"Types":"Land Sale"}},
    // Development Type
    {"controller":"getDevelopmentTypeQueryObj","helper":"getQueryByDevelopmentType","query":{"Types":"Extension"}},
    // Contract Development Type
    {"controller":"getContractDevTypeQueryObj","helper":"getQueryByContractDevType","query":{"ContractTypes":"Land Sale","DevelopmentTypes":"Extension"}},
    // Planning Application
        //PlanningAuthority
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByPlanningAuthority","query":{"CouncilNames":"Aberdeen,Angus"}},
        //planningType
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByPlanningType","query":{"PlanningNames":"Pre-Planning,Detailed Planning"}},
        //applicationType
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByApplicationType","query":{"ApplicationNames":"Retentions,Building notices"}},
        //planningApplication
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByPlanningApplication","query":{"CouncilNames":"Aberdeen,Angus","PlanningNames":"Pre-Planning,Detailed Planning","ApplicationNames":"Retentions,Building notices"}},
        //applicationId
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByApplicationId","query":{"Ids":"07/18/0083/F, 17/01100/FUL"}},
        //applicationDate
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByApplicationDate","query":{"From":"04-01-2018","To":"02-03-2018"}},
        //permissionDate
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByPermissionDate","query":{"From":"04-01-2018","To":"02-03-2018"}},
        //refusedDate
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByRefusedDate","query":{"From":"04-01-2018","To":"02-03-2018"}},
        //withdrawnDate
        {"controller":"getPlanningApplicationQueryObj","helper":"getQueryByWithdrawnDate","query":{"From":"04-01-2018","To":"02-03-2018"}},
    // Project Dates
    {"controller":"getProjectDateQueryObj","helper":"getQueryByProjectDate","query":{"StartDateFrom":"04-01-2018","StartDateTo":"02-02-2018"}},
    // Dimension
        //dimensionSiteArea
        {"controller":"getDimensionsQueryObj","helper":"getQueryBySiteArea","query":{"From":"2","To":"3"}},
        //dimensionFloorArea
        {"controller":"getDimensionsQueryObj","helper":"getQueryByFloorArea","query":{"From":"2","To":"3"}},
        //dimensionUndergroundStoreys
        {"controller":"getDimensionsQueryObj","helper":"getQueryByUndergroundStoreys","query":{"From":"2","To":"3"}},
        //dimensionLength
        {"controller":"getDimensionsQueryObj","helper":"getQueryByLength","query":{"From":"2","To":"3"}},
        //dimensionStructures
        {"controller":"getDimensionsQueryObj","helper":"getQueryByStructures","query":{"From":"2","To":"3"}},
        //dimensionParkingSpaces
        {"controller":"getDimensionsQueryObj","helper":"getQueryByParkingSpaces","query":{"From":"2","To":"3"}},
        //dimensionUnderGroundParking
        {"controller":"getDimensionsQueryObj","helper":"getQueryByBasementParkingSpaces","query":{"From":"2","To":"3"}},
        //dimensionVerticalHeight
        {"controller":"getDimensionsQueryObj","helper":"getQueryByVerticalHeight","query":{"From":"2","To":"3"}},
        //dimensionUnits
        {"controller":"getDimensionsQueryObj","helper":"getQueryByUnits","query":{"From":"2","To":"3"}},
        //dimension
        {"controller":"getDimensionsQueryObj","helper":"getQueryByDimensions","query":{"SiteArea":"From:2,To:3","FloorArea":"From:2,To:3","Storeys":"From:2,To:3","UndergroundStoreys":"From:2,To:3","Length":"From:2,To:3","Blocks":"From:2,To:3","ParkingSpaces":"From:2,To:3","UndergroundParkingSpaces":"From:2,To:3","Height":"From:2,To:3","Units":"From:2,To:3"}},
    //Role
    {"controller":"getRoleQueryObj","helper":"getQueryByRoles","query":{"RoleLocationRegions":"East Midlands,London","Roles":"Client,Fit Out Contractor"}},
    // Company / Office
    {"controller":"getCompanyContactQueryObj","helper":"getQueryByOfficeID","query":{"Ids":"2138801"}},
    //Contact
    {"controller":"getCompanyContactQueryObj","helper":"getQueryByContactID","query":{"Ids":"2509220"}}
];
/* Endpoints List*/
var endpointsArray = [
    "/glenigan/project","/glenigan/project/sector/primaryOnly","/glenigan/project/sector/secondaryOnly","/glenigan/project/sector/primaryOrSecondary","/glenigan/project/Stage","/glenigan/project/stages/bothStage","/glenigan/project/size","/glenigan/project/value","/glenigan/project/material","/glenigan/project/contractType","/glenigan/project/developmentType","/glenigan/project/contractDevelopmentType","/glenigan/project/status","/glenigan/project/developmentType","/glenigan/project/contractDevelopmentType","/glenigan/project/planningApplication/withdrawnDate","/glenigan/project/planningApplication/planningAuthority","/glenigan/project/planningApplication/applicationDate","/glenigan/project/planningApplication/applicationType","/glenigan/project/planningApplication/planningAuthority-planningType-applicationType","/glenigan/project/planningApplication/applicationId","/glenigan/project/planningApplication/applicationDate","/glenigan/project/planningApplication/permissionDate","/glenigan/project/planningApplication/refusedDate","/glenigan/project/projectDate","/glenigan/project/dimension/siteArea","/glenigan/project/dimension/floorArea","/glenigan/project/dimension/underGroundStoreys","/glenigan/project/dimension/storeys","/glenigan/project/dimension/length","/glenigan/project/dimension/block","/glenigan/project/dimension/parkingSpaces","/glenigan/project/dimension/underGroundParking","/glenigan/project/dimension/verticalHeight","/glenigan/project/dimension/unit","/glenigan/project/dimension","/glenigan/project/role","/glenigan/project/newProject","/glenigan/project/updatedProject","/glenigan/project/location","/glenigan/project/region","/glenigan/project/county","/glenigan/project/town","/glenigan/project/postCode","/glenigan/project/radius"
];

/* Getting the port*/

var port = ''
try{
    var configObject = iniParserModule.parse('./config/LocalConfig.ini');
    port = process.env.PORT || configObject.Local.port;
}catch(error){
    port = process.env.PORT || 80;
}

/**
 * After completing all test cases exit the script (unitTestCase_Live.js) 
 */
after(function(done){
    done();
    process.exit(0);
});


/**
 * Glenigan API unit test cases
 */
describe('Glenigan API Unit Test Cases', function() {

    /*******************************************************************************************
     * Unit test case for Checking server  (app.js)
     *******************************************************************************************/
     
    describe ('Checking the port', function() {
        this.timeout(5000);
        it('port '+port+' is working fine', function(done){
            try{
                var appTest = require('./app.js');
                done();
            }catch (e){}
        });
    });
    /*******************************************************************************************
     * Unit test case for Checking configuration of application (config.js)
    *******************************************************************************************/
     
    describe ('Check the configuration file', function() {
        this.timeout(5000);
        it('Modules are installed correctly', function(done){
            try{
                var configTest = require('./config.js');
                done();
            }catch (e){}
        });
    });

    /*******************************************************************************************
     *  MongoDB Connection unit test case
     *******************************************************************************************/
     
    describe ('Check the MongoDB Connection of application', function() {
        this.timeout(5000);
        it('MongoDB Connection is working', function(done){
            /** Connection URL */
            var url = "mongodb://"+credentialsObject.MongoDB.host+":"+credentialsObject.MongoDB.port+"/";
            mongoClient.connect(url, function(error, db) {
                /**
                 * Checking the MongoDB Connection
                 */
                assert.equal(null, error);
                done(); 
                db.close();
            });            
        });
    });

    /*******************************************************************************************
     * Elasticsearch connection unit test case
     *******************************************************************************************/
     
    describe ('Check the Elasticsearch Connection of application', function() {
        this.timeout(5000);
        it('Elasticsearch Connection is working', function(done){
             var exportedRequiredModules = require('./config.js').requiredModulesExport;
             var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
                host: exportedRequiredModules.elasticIndexHost
             });
            elasticServerClient.ping({
                /**
                 * Ping  usually has a 3000ms timeout
                 */
                requestTimeout: 30000
                }, 
            function (error) {
                /**
                 * Checking the Elasticsearch Connection
                 */
                assert.equal(null, error);
                done();
            });
        });
    });

    /*******************************************************************************************
     *                SalesforceConnection Connection unit test case
     *******************************************************************************************/

    describe ('Check the Salesforce Connection of application', function() {
        this.timeout(5000);
        it('SalesforceConnection is working', function(done){
            (async () => {
                let sfConn = new salesforceConnection();
                let username = credentialsObject.Salesforce.username;
                let password = credentialsObject.Salesforce.password;
                let token = credentialsObject.Salesforce.token;
                let hostname = credentialsObject.Salesforce.host;
                let apiVersion = credentialsObject.Salesforce.version;

                let queryPath = "/services/data/v"+apiVersion+"/query/?q=";

                await sfConn.soapLogin({
                    hostname: hostname,
                    apiVersion: apiVersion,
                    username: username,
                    password: password+token
                });
                var lastHour = moment().subtract(10000, 'hours').format("YYYY-MM-DDTHH:00:00")+"Z";
                var query = eval(credentialsObject.Salesforce.query);

                let recentAccounts = await sfConn.rest(queryPath + encodeURIComponent(query));
                done();
            })().catch(ex => console.error(ex.stack));
        });
    });

    
    /*******************************************************************************************
     * Checking the Meta data end-points
     *******************************************************************************************/

    /* "/glenigan/metadata/_search" Checking the end-point */
    describe ('"/glenigan/metadata/_search" Checking the end-point', function() {
        this.timeout(10000);
        it('"/glenigan/metadata/_search" should return 200', function(done){
            var exportedRequiredModules = require('./config.js').requiredModulesExport;
            var host = exportedRequiredModules.elasticIndexHost;
            request(host+'/glenigan/metadata/_search', function(error, response, body) {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
    });

    /* "/glenigan/postcode_district/_search */
    describe ('"/glenigan/postcode_district/_search" Checking the end-point', function() {
        this.timeout(10000);
        it('"/glenigan/postcode_district/_search" should return 200', function(done){
            var exportedRequiredModules = require('./config.js').requiredModulesExport;
            var host = exportedRequiredModules.elasticIndexHost;
            request(host+'/glenigan/postcode_district/_search', function(error, response, body) {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
    });

   
    /*******************************************************************************************
    *  Checking the end-points of the application
    * ******************************************************************************************/

    describe ('Checking that all the Endpoints are defined', function() {
        var exportedRequiredModules = require('./Config.js').requiredModulesExport;
        function endpoints(object,callback){
            describe ('"'+object+'" Checking the end-point', function() {
                this.timeout(10000);
                it('"'+object+'" - endpoint should return a message that Parameter (key) is required', function(done){
                    request('http://localhost:'+port+object, function(error, response, body) {
                        var data = JSON.parse(body); expect("REQUIRED").to.equal(data.code);
                        done();
                        callback(null,1);
                    });
                });
            });
        }
        async.mapSeries(endpointsArray, endpoints, function(error, result) {
        // console.log(result);
        });
    });

    describe ('Checking query formation', function() {
        var exportedRequiredModules = require('./Config.js').requiredModulesExport;
        function helperQueryParsing(object,callback){
            describe ('Checking the helper function - '+object.controller, function() {
                this.timeout(20000);
                it('Should given a valid query from '+object.helper+' function', function(done){
                    if(object.controller !="getCompanyContactQueryObj")
                        exportedRequiredModules[object.controller][object.helper](subscriptionJSON,object, function(error, queryValue){
                           assert.equal(null, error);
                            expect((typeof(queryValue) == 'string')?JSON.parse(queryValue):queryValue).to.be.an('object');
                            done();
                            callback(null,1);
                        });
                    else{
                        exportedRequiredModules[object.controller][object.helper](object, function(error, queryValue){
                            assert.equal(null, error);
                            expect((typeof(queryValue) == 'string')?JSON.parse(queryValue):queryValue).to.be.an('object');
                            done();
                            callback(null,1);
                        });
                    }
                });
            });
        }
        async.mapSeries(inputDataForQueryParsing, helperQueryParsing, function(error, result) {
        //console.log(result);
        });
    });
    describe ('Checking exception handling on helper functions', function() {
        var exportedRequiredModules = require('./Config.js').requiredModulesExport;
        function helperCheckExceptions(object,callback){
            // console.log(object);
            describe ('Checking exception on helper function - '+object.controller, function() {
                this.timeout(20000);
                it('Should throw an exception - '+object.error, function(done){
                    if(object.controller !="getCompanyContactQueryObj")
                        exportedRequiredModules[object.controller][object.helper](subscriptionJSON,object, function(error, queryValue){
                            assert.equal(null, queryValue);
                            done();
                            callback(null,1);
                        });
                    else{
                        exportedRequiredModules[object.controller][object.helper](object, function(error, queryValue){
                            assert.equal(null, queryValue);
                            done();
                            callback(null,1);
                        });
                    }
                });
            });
        }
        async.mapSeries(inputDataForExceptions, helperCheckExceptions, function(error, result) {
        //console.log(result);
        });
    });

    describe ('Checking exception handling for invalid parameter', function() {
        var exportedRequiredModules = require('./Config.js').requiredModulesExport;
        var testCase1 = {url : '/glenigan/project/radius', query : { TimeRange: '',key: '03f0994b-2cef-451b-938b-b84cda2ee2e4',Radius: '10mi',Lat: '51.5',Lon: '-0.13' } };
        describe ('Checking with empty parameter', function() {
            this.timeout(10000);
            it('Should throw an exception - Parameter value should not be empty', function(done){
                exportedRequiredModules.paramsValidationAndGetSubscriptionObj(testCase1, function(error, value){
                    assert.equal(null, value);
                    done();
                });
            });
        });
        var testCase2 = {url : '/glenigan/project/location', query : { TimeRange: '',key: '03f0994b-2cef-451b-938b-b84cda2ee2e4',Radius: '10mi',Lat: '51.5',Lon: '-0.13' } };
        describe ('Checking with invalid parameter', function() {
            this.timeout(10000);
            it('Should throw an exception - Invalid parameter - Radius,Lat,Lon', function(done){
                exportedRequiredModules.paramsValidationAndGetSubscriptionObj(testCase2, function(error, value){
                    assert.equal(null, value);
                    done();
                });
            });
        });
        var testCase3 = {url : '/glenigan/project/radius', query : { TimeRange: ['2y',''],key: '03f0994b-2cef-451b-938b-b84cda2ee2e4',Radius: '10mi',Lat: '51.5',Lon: '-0.13' } };
        describe ('Checking with duplicate parameter', function() {
            this.timeout(10000);
            it('Should throw an exception - Duplicate parameter not allowed', function(done){
                exportedRequiredModules.paramsValidationAndGetSubscriptionObj(testCase3, function(error, value){
                    assert.equal(null, value);
                    done();
                });
            });
        });
        var testCase4 = {url : '/glenigan/project/radius', query : { TimeRange: '{2y}',key: '03f0994b-2cef-451b-938b-b84cda2ee2e4',Radius: '10mi',Lat: '51.5',Lon: '-0.13' } };
        describe ('Checking with Curly brackets in the parameter', function() {
            this.timeout(10000);
            it('Should throw an exception - Curly brackets are not allowed in the Parameters', function(done){
                exportedRequiredModules.paramsValidationAndGetSubscriptionObj(testCase4, function(error, value){
                   assert.equal(null, value);
                    done();
                });
            });
        });
        var testCase4 = {url : '/glenigan/project/radius', query : { TimeRange: '2y', key: undefined,Radius: '10mi',Lat: '51.5',Lon: '-0.13' } };
        describe ('Checking with missing key', function() {
            this.timeout(10000);
            it('Should throw an exception - Key Missing', function(done){
                exportedRequiredModules.paramsValidationAndGetSubscriptionObj(testCase4, function(error, value){
                   assert.equal(null, value);
                    done();
                });
            });
        });
    });
});