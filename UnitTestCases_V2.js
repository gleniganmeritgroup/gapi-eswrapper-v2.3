'use strict';

/********************************************************************************************
 ********************************************************************************************
 * This is unit test cases for Glenigan API Application - Live endpoints (unitTestCase_Live.js) 
 ********************************************************************************************
 ********************************************************************************************
 */

/* Requiring the npm modules for unit testing */
var expect  = require('chai').expect;
var request = require('request');
var mongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var elasticsearch = require('elasticsearch');
var yamlConfig = require('js-yaml');
var iniParserModule = require('parse-ini');
var moment = require('moment');
var fs = require('fs');
var async = require('async');
var salesforceConnection = require("node-salesforce-connection");
var credentialsObject = yamlConfig.safeLoad(fs.readFileSync('./config/Credentials.yml', 'utf8'));

var subscriptionJSON = '{"SubscriptionExists": true,"Subscription": {"SubscriptionStartDate": "2014-09-30","SubscriptionEndDate": "2019-02-15","ProjectSectors": [{"Sector": "Private Housing"},{"Sector": "Industrial"},{"Sector": "Offices"},{"Sector": "Retail"},{"Sector": "Hotel & Leisure"},{"Sector": "Education"},{"Sector": "Community & Amenity"},{"Sector": "Health"},{"Sector": "Utilities"},{"Sector": "Infrastructure"}],"ProjectRegions": [{"Region": "London","Counties": []},{"Region": "East Midlands","Counties": ["Lincolnshire"]},{"Region": "Yorkshire and the Humber","Counties": ["Humberside"]}],"InsightSectors": [],"InsightRegions": [],"HasSmall": false,"HasReporting": false,"HasAPI": true,"HasInsight": false,"HasOpportunities": false,"HasCompanyProfiles": false}}';


var inputDataForQueryParsing = [
    //My Day createEvent
    {"controller":"myDayTaskObj","helper":"createEvent","query":{"UserId":"0056E000002DplhQAC"}, "AccountId" : "12345677", "body" : '{"MyDayStartTime": "2025-09-11T18:00:00.000Z", "Duration" : "1h" ,"MyDayType": "Office", "MyDayTypeId" : "0812902912312"}'},
    
    //My Day updateEvent
    {"controller":"myDayTaskObj","helper":"updateEvent","query":{"UserId":"0056E000002DplhQAC"}, "AccountId" : "12345677", "body" : '{"MyDayStartTime": "2025-09-11T18:00:00.000Z", "Duration" : "1h" ,"MyDayType": "Office", "MyDayTypeId" : "0812902912312"}'},

    //My Day Get Count
    {"controller":"myDayTaskObj","helper":"getCount","query":{"UserId":"0056E000002DplhQAC","Date":"14-09-2018"}},
    
    //My Day Get upcomingEvent
    {"controller":"getMyDayEventObj","helper":"upcomingEvent","query":{"UserId": "0056E000002DplhQAC", "Type": "Project"}},
    
    //My Day Get getMydayAllEventsQuery
    {"controller":"getMyDayEventObj","helper":"getMydayAllEventsQuery","query":{"UserId": "0056E000002DplhQAC","Date":"14-09-2018", "Type": "Project"}},
    
    //getContactNotesQueryObj
    {"controller":"getContactNotesQueryObj","helper":"getQueryByContactNotes","query":{"UserId": "0056E000002DplhQAC"}},
    
    //getContactNotesQueryObj
    {"controller":"getContactNotesQueryObj","helper":"postQueryByContactNotes","query":{"UserId": "0056E000002DplhQAC"},"body" : '{"ContactId" : "1003662","CompanyId": "0016E00000QjCwzQAF","UserId": "0056E000002DplhQAC","NoteText": "RECEPTION PASSED at JOHN WHITE, ADV THAT HE WAS DUE TO GO ON ANNUAL LEAVE AND WOULD LIKE THE LIT EMAILED FOR WHEN HE GOT BACK, EMAIL SENT TO JOHN.WHITE@APPCONSTRUCTION.CO.UK"}', "MultipleArgument" : "true"},
    
    //getContactTagsLinkQueryObj
    {"controller":"getContactTagsLinkQueryObj","helper":"getQueryByContactTagsLink","query":{"UserId": "0056E000002DplhQAC", "ContactTagId" : "test"}},
    
    //GetContactTagsQuery
    {"controller":"getContactTagsQueryObj","helper":"getQueryByContactTags","query":{"UserId": "0056E000002DplhQAC"}},
    
    //postQueryByContactTags
    {"controller":"getContactTagsQueryObj","helper":"postQueryByContactTags","query":{"UserId": "0056E000002DplhQAC"},"body" : '{"UserId":"0056E000002Dplh","TagType":"Contact","TagName":"MeritContact","TagCompanyId":"1003662","TagColour":"#69b3e8"}', "MultipleArgument" : "true"},
    
    // postQueryByContactTags
    {"controller":"getOfficeFollowQueryObj","helper":"postQueryByOfficeFollow","query":{"UserId": "0056E000002DplhQAC"},"body" : '{"UserId":"0056E000002DplhQAC","OfficeId" : "25025219","Relationship": "Other","NotificationType":"office-update-any"}', "MultipleArgument" : "true"},
    
    //getQueryByOfficeFollow
    {"controller":"getOfficeFollowQueryObj","helper":"getQueryByOfficeFollow","query":{"UserId": "0056E000002DplhQAC"}},
    
    //postQueryByOfficeNotes
    {"controller":"getOfficeNotesQueryObj","helper":"postQueryByOfficeNotes","query":{"UserId": "0056E000002DplhQAC"},"body" : '{"OfficeId" : "25025219","CompanyId": "0016E00000QjCwzQAF","UserId": "0056E000002DplhQAC","NoteText": "OK " }', "MultipleArgument" : "true"},
    
    //getQueryByOfficeNotes
    {"controller":"getOfficeNotesQueryObj","helper":"getQueryByOfficeNotes","query":{"UserId": "0056E000002DplhQAC"}},
    
    //postQueryByOfficeTagsLink
    {"controller":"getOfficeTagsLinkQueryObj","helper":"postQueryByOfficeTagsLink","query":{"UserId": "0056E000002DplhQAC"},"body" : '{"TaggedUserId":"0056E000002DplhQAC","TagId":"TestTagId1","OfficeId":"25025219"}', "MultipleArgument" : "true,true"},
    
    //getQueryByOfficeTagsLink
    {"controller":"getOfficeTagsLinkQueryObj","helper":"getQueryByOfficeTagsLink","query":{"UserId": "0056E000002DplhQAC"}},
    
    //postQueryByOfficeTags
    {"controller":"getOfficeTagsQueryObj","helper":"postQueryByOfficeTags","query":{"UserId": "0056E000002DplhQAC","TagId" : "TestTagId1"},"body" : '{"UserID":"0056E000002DplhQAC","TagType":"Personal","TagName":"TestSiva","TagCompanyId":"0016E00000QjCwzQAF","TagColour":"#69b3e8"}', "MultipleArgument" : "true"},
    
    //getQueryByofficeTags
    {"controller":"getOfficeTagsQueryObj","helper":"getQueryByofficeTags","query":{"UserId": "0056E000002DplhQAC"}},
    
    //postQueryByProjectFollow
    {"controller":"getProjectFollowQueryObj","helper":"postQueryByProjectFollow","query":{"UserId": "0056E000002DplhQAC","TagId" : "TestTagId1"},"body" : '{"UserID":"0056E000002DplhQAC","TagType":"Personal","TagName":"TestSiva","TagCompanyId":"0016E00000QjCwzQAF","TagColour":"#69b3e8"}', "MultipleArgument" : "true"},
    
    //getQueryByProjectFollows
    {"controller":"getProjectFollowQueryObj","helper":"getQueryByProjectFollows","query":{"UserId": "0056E000002DplhQAC"}},
    
    //postQueryByProjectNotes
    {"controller":"getProjectNotesQueryObj","helper":"postQueryByProjectNotes","query":{"UserId": "0056E000002DplhQAC","TagId" : "TestTagId1"},"body" : '{"UserID":"0056E000002DplhQAC","TagType":"Personal","TagName":"TestSiva","TagCompanyId":"0016E00000QjCwzQAF","TagColour":"#69b3e8"}', "MultipleArgument" : "true"},
    
    //getQueryByProjectNotes
    {"controller":"getProjectNotesQueryObj","helper":"getQueryByProjectNotes","query":{"UserId": "0056E000002DplhQAC"}}
    
];
/* Endpoints List*/
var endpointsArray = [ "/glenigan/myday/count", "/glenigan/myday/upcomingevent",  "/glenigan/myday/allevents", "/glenigan/project/executesearch", "/glenigan/project/savedsearch","/glenigan/project/taglink", "/glenigan/company/taglink", "/glenigan/project/follow", "/glenigan/company/follow", "/glenigan/project/notes", "/glenigan/company/notes", "/glenigan/company/tags/delete", "/glenigan/project/tags/delete", "/glenigan/metadata/_search", "/glenigan/metadata/locations", "/glenigan/metadata/sectors", "/glenigan/metadata/towns", "/glenigan/metadata/postcode_district","/glenigan/contact/taglink/delete", "/glenigan/contact/notes/delete", "/glenigan/project/notes/delete", "/glenigan/project/follow/delete", "/glenigan/company/taglink/delete", "/glenigan/company/follow/delete", "/glenigan/company/notes/delete?NoteId=TestNoteId01", "/glenigan/project/taglink/delete" ];
var postEndpointsArray = [ "/glenigan/myday/update", "/glenigan/myday/create", "/glenigan/project/enquiry", "/glenigan/contact/taglink/createorupdate", "/glenigan/contact/notes/createorupdate", "/glenigan/contact/tags/createorupdate", "/glenigan/project/notes/createorupdate","/glenigan/company/tags/createorupdate", "/glenigan/company/taglink/createorupdate", "/glenigan/company/follow/createorupdate", "/glenigan/company/notes/createorupdate?NoteId=TestNoteId01", "/glenigan/project/tags/createorupdate?TagId=TestTagId1", "/glenigan/project/taglink/createorupdate", "/glenigan/project/follow/createorupdate" ];

/* Getting the port*/

var port = ''
try{
    var configObject = iniParserModule.parse('./config/LocalConfig.ini');
    port = process.env.PORT || configObject.Local.port;
}catch(error){
    port = process.env.PORT || 80;
}

/**
 * After completing all test cases exit the script (unitTestCase_Live.js) 
 */
after(function(done){
    done();
    process.exit(0);
});


/**
 * Glenigan API unit test cases
 */
describe('Glenigan API V2 Unit Test Cases', function() {

    
    /*******************************************************************************************
     * Elasticsearch connection unit test case
     *******************************************************************************************/
     
    describe ('Check the Elasticsearch Connection of application', function() {
        this.timeout(5000);
        it('Elasticsearch Connection is working', function(done){
             var exportedRequiredModules = require('./config.js').requiredModulesExport;
             var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
                host: exportedRequiredModules.elasticIndexHost
             });
            elasticServerClient.ping({
                /**
                 * Ping  usually has a 3000ms timeout
                 */
                requestTimeout: 30000
                }, 
            function (error) {
                /**
                 * Checking the Elasticsearch Connection
                 */
                assert.equal(null, error);
                done();
            });
        });
    });

    /*******************************************************************************************
     *                SalesforceConnection Connection unit test case
     *******************************************************************************************/

    describe ('Check the Salesforce Connection of application', function() {
        this.timeout(5000);
        it('SalesforceConnection is working', function(done){
            (async () => {
                let sfConn = new salesforceConnection();
                let username = credentialsObject.Salesforce.username;
                let password = credentialsObject.Salesforce.password;
                let token = credentialsObject.Salesforce.token;
                let hostname = credentialsObject.Salesforce.host;
                let apiVersion = credentialsObject.Salesforce.version;

                let queryPath = "/services/data/v"+apiVersion+"/query/?q=";

                await sfConn.soapLogin({
                    hostname: hostname,
                    apiVersion: apiVersion,
                    username: username,
                    password: password+token
                });
                var lastHour = moment().subtract(10000, 'hours').format("YYYY-MM-DDTHH:00:00")+"Z";
                // var query = eval(credentialsObject.Salesforce.query);
                var query = credentialsObject.Salesforce.query.replace('userID', "'0056E000002DplhQAC'");
                let recentAccounts = await sfConn.rest(queryPath + encodeURIComponent(query));
                done();
            })().catch(ex => console.error(ex.stack));
        });
    });

   
    /*******************************************************************************************
    *  Checking the end-points of the application
    * ******************************************************************************************/

    describe ('Checking that all the Endpoints are defined', function() {
        var exportedRequiredModules = require('./Config.js').requiredModulesExport;
        function endpoints(object,callback){
            describe ('"'+object+'" Checking the end-point', function() {
                this.timeout(10000);
                it('"'+object+'" - endpoint should return a message that Parameter (UserId) is required', function(done){
                    request('http://localhost:'+port+object, function(error, response, body) {
                        // console.log(body)
                        var data = JSON.parse(body); expect("REQUIRED").to.equal(data.code);
                        done();
                        callback(null,1);
                    });
                });
            });
        }
        async.mapSeries(endpointsArray, endpoints, function(error, result) {
        // console.log(result);
        });
    });

    describe ('Checking that all the Post Endpoints are defined', function() {
        var exportedRequiredModules = require('./Config.js').requiredModulesExport;
        function postEndpoints(object,callback){
            describe ('"'+object+'" Checking the Post end-point', function() {
                this.timeout(10000);
                it('"'+object+'" - endpoint should return a message that Parameter (UserId) is required', function(done){
                    
                    request({
                        url: 'http://localhost:'+port+object, method: "POST", 
                        headers: {'Content-Type': 'application/json'}, 
                        qs: {'key1': 'xxx', 'key2': 'yyy'}
                      }, function(error, response, body){
                        var data = JSON.parse(body); expect("REQUIRED").to.equal(data.code);
                        done();
                        callback(null,1);
                      });
                });
            });
        }
        async.mapSeries(postEndpointsArray, postEndpoints, function(error, result) {
        // console.log(result);
        });
    });

    describe ('Checking query formation', function() {
        var exportedRequiredModules = require('./Config.js').requiredModulesExport;
        function helperQueryParsing(object,callback){
            describe ('Checking the helper function - '+object.controller, function() {
                this.timeout(20000);
                it('Should given a valid query from '+object.helper+' function', function(done){
                    
                    if(object.MultipleArgument == undefined)
                        exportedRequiredModules[object.controller][object.helper](object, function(error, queryValue){
                           assert.equal(null, error);
                            expect((typeof(queryValue) == 'string')?JSON.parse(queryValue):queryValue).to.be.an('object');
                            done();
                            callback(null,1);
                        });
                    else{
                        var splitedArgument = object.MultipleArgument.split(',')
                        if(splitedArgument.length == 1){
                            exportedRequiredModules[object.controller][object.helper](object,splitedArgument[0], function(error, queryValue){
                                assert.equal(null, error);
                                expect((typeof(queryValue) == 'string')?JSON.parse(queryValue):queryValue).to.be.an('object');
                                done();
                                callback(null,1);
                            });
                        }else if(splitedArgument.length == 2){
                            exportedRequiredModules[object.controller][object.helper](object,splitedArgument[0],splitedArgument[1], function(error, queryValue){
                                assert.equal(null, error);
                                expect((typeof(queryValue) == 'string')?JSON.parse(queryValue):queryValue).to.be.an('object');
                                done();
                                callback(null,1);
                            });
                        }
                        
                    }
                });
            });
        }
        async.mapSeries(inputDataForQueryParsing, helperQueryParsing, function(error, result) {
        //console.log(result);
        });
    });
});