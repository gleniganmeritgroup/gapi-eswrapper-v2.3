# GAPI- ESWrapper #


This componet is used to relicating the subscriptions from salesforce db and also tobb creating and managing the subscriptions in the Gravitee.

### Prerequisites

* node JS version 8+ - [https://nodejs.org/en/download/] 

### Installation
Checkout the Repository or downlod the repoistoy from the source controller to the right path.

### Running

Before running the component please check the credentials of MongoDB, Garvitee and Salesforrce in credentials.yml in the config folder.
The model config file as below
````
ElasticIndex:
    https:
        host: hostname of ES
        port: port of ES
    index: Live index name
    he_index: HE index name
    historic_index: historic indices name
    type: project
    contact_type: contact
    company_type: company
    username: Uname
    password: Pass
    size: Sixe of the Resuslts

MongoDB:
    host: localhost
    port: 27017

SalesforceSubscription:
    mongoDBName: SalesforceSubscription
    collectionName: Salesforce Subscription Details

Gravitee:
    mongoDBName: gravitee
    applicationsCollectionName: applications
    planCollectionName: plans
    keyCollectionName: keys

Salesforce:
    host: test.salesforce.com
    username: uname
    password: pass
    token: APItoken 
    version: "39.0"
    query: '"select Id,Name,X4C_Account_Status__c,Subscription_End_Date__c, Suspension_Date__c,Subscription_JSON__c,LastModifiedDate from Account where Suspension_Date__c = NULL and LastModifiedDate > "+lastHour'

QueryLogs:
    Enable: true

InHouseCredential:
    parameter: gleniganKey
    value: key value
````

Running the application,

```` 
Node:

$ npm install
$ cd gapi-eswrapper
$ node app.js

Windows scheduler:

Start/ Stop the windows scheduler Component.

````

### Functionalities
 * Accesing the ES through the EP
 * Swagger documentations

 
## Built With

* node JS

## Versioning
Version 1.0

## Authors
* **Muthubabu H ** - *Developer* - [Merit Software Services](http://Meritgroup.co.uk)
* **Sivamaniyan Murugesan ** - *Developer* - [Merit Software Services](http://Meritgroup.co.uk)
* **RadhaKrishnan V ** - *Developer* - [Merit Software Services](http://Meritgroup.co.uk)
* **Smith Samson V J ** - *Reviewer* - [Merit Software Services](http://Meritgroup.co.uk)