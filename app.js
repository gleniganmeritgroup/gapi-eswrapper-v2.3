'use strict';
/*******************************************************************************************/
/********************************************************************************************
This file (app.js) has Express node module which will create web server for entire application
and Swagger is a API documentation and it's used to define an endpoints and parameters.

********************************************************************************************/
/*******************************************************************************************/

var SwaggerExpress = require('swagger-express-mw');
// var app = require('express')();
// var express = require('express');
var iniParserModule = require('parse-ini');
var cmd = require('node-cmd');
var exportedRequiredModules = require('./config.js').requiredModulesExport;
var copyFile = require('quickly-copy-file');
var jsforce = require('jsforce');
var app = exportedRequiredModules.appObj;
// 24 * 60 * 60 * 1000 => 24 hours
app.use(exportedRequiredModules.sessionObj({ secret: "secret", domain: 'localhost' , cookie: { maxAge: 24 * 60 * 60 * 1000 },saveUninitialized: true, resave: false}));
const {
  exec
} = require('child_process');
// var configObj = exportedRequiredModules.iniParserModuleObj.parse('./config/LocalConfig.ini');
var configObj = exportedRequiredModules.credentialsObj;

var port = ''
var localMachine = ''
var basePath = ''
  try {
    /* Gets the port in which the server is supposed to listen from "LocalConfig.ini" file */
    
    port = process.env.PORT || configObj.SSO.port;
    localMachine = configObj.SSO.machine;
    basePath = configObj.SSO.basepath;
    exportedRequiredModules.loggerObj.info("Port Running in " + port);
    console.log("Port Running in " + port);
  } catch (err) {
    /* If any error occurs , server will listen to the port 80  */
    port = process.env.PORT || 80;
    localMachine = "localhost"
    exportedRequiredModules.loggerObj.info("Port Running in " + port);
    console.log("Port Running in " + port);
  }
var config = {
  appRoot: __dirname
};
var salesforceDetails = exportedRequiredModules.credentialsObj.Salesforce;
var instanceUrl = "https://"+salesforceDetails.host

const oauth2 = new jsforce.OAuth2({
  loginUrl: instanceUrl,
  clientId: salesforceDetails.consumerKey,
  clientSecret: salesforceDetails.consumerSecret,
  redirectUri: salesforceDetails.callbackUrl
});


app.get("/"+basePath+"/auth/login", function(req, res) {
  // Redirect to Salesforce login/authorization page
  exportedRequiredModules.sessObj=req.session;
  if(req.query.UserId != undefined){
    var sessionExpireDate = exportedRequiredModules.moment(exportedRequiredModules.sessObj.cookie._expires,'YYYY-MM-DDTHH:mm:ss.SSS[Z]').toISOString();
    exportedRequiredModules.MongoClient.connect(exportedRequiredModules.mongoDBConnectionString, function(err, db) {
      if(err){
        res.send({"Error" : "MongoDB connection Failed"});
      }else{
        var appTokenDetails = exportedRequiredModules.credentialsObj.AppToken;
        var dbo = db.db(appTokenDetails.DBName);
        // console.log(req.query.UserId)
        dbo.collection(appTokenDetails.collectionName).find({"UserId": req.query.UserId}).toArray(function (err, result) {
          // console.log(result)
          if (result.length > 0 ){
            var expireDate = exportedRequiredModules.moment(result[0].expireDateTime,'YYYY-MM-DDTHH:mm:ss.SSS[Z]').toISOString();
            var currentDate = exportedRequiredModules.moment().toISOString();
            if(expireDate < currentDate){
              dbo.collection(appTokenDetails.collectionName).deleteOne({"UserId": req.query.UserId}, function(err, results) {
                    db.close();
                    var replaceURL = oauth2.getAuthorizationUrl();
                    replaceURL = replaceURL.replace(instanceUrl, salesforceDetails.G2Website);
                    res.redirect(replaceURL);
                });
            }else{
              exportedRequiredModules.getSubscriptionObj.getSubscriptionJSON(req.query.UserId, function (error, subscriptionList) {
                            
                if (error) {
                      dbo.collection(appTokenDetails.collectionName).deleteOne({"UserId": req.query.UserId}, function(err, results) {
                        db.close();
                        res.status(404);
                        res.setHeader('Content-Type', 'application/json');
                        res.send(JSON.stringify(error, null, 3));
                    });
                }else{
                  dbo.collection(appTokenDetails.collectionName).updateOne({UserId : req.query.UserId}, { $set : {expireDateTime: sessionExpireDate,UpdatedDate : exportedRequiredModules.moment().toISOString(),"TokenStatus" : "Old"}}, {upsert:true});
                  db.close();
                  res.send({"UserId" : req.query.UserId, "UserToken" : result[0].Token, "expireDateTime" : sessionExpireDate, "Status" : "Old"});
                }
              });
              
            }
            
          }else{
            var replaceURL = oauth2.getAuthorizationUrl();
            replaceURL = replaceURL.replace(instanceUrl, salesforceDetails.G2Website);
            res.redirect(replaceURL);
          }
          
        })
      }
    })
  }else{
    var replaceURL = oauth2.getAuthorizationUrl();
    replaceURL = replaceURL.replace(instanceUrl, salesforceDetails.G2Website);
    res.redirect(replaceURL);
  }
});

app.get('/'+basePath+'/token', function(req, res) {
  exportedRequiredModules.sessObj=req.session;
  const conn = new jsforce.Connection({oauth2: oauth2});
      const code = req.query.code;
      // console.log("req",req);
      conn.authorize(req.query.code, function(err, userInfo) {
        // console.log("userInfo",userInfo)
        if (err) { return console.error("This error is in the auth callback: " + err); }
        
        if(exportedRequiredModules.sessObj[userInfo.id] == undefined){
          exportedRequiredModules.sessObj[userInfo.id] = code
        }
        var expireDate = exportedRequiredModules.moment(exportedRequiredModules.sessObj.cookie._expires,'YYYY-MM-DDTHH:mm:ss.SSS[Z]').toISOString();
        exportedRequiredModules.MongoClient.connect(exportedRequiredModules.mongoDBConnectionString, function(err, db) {
          if(err){
            res.send({"Error" : "MongoDB connection Failed"});
          }else{
            var appTokenDetails = exportedRequiredModules.credentialsObj.AppToken;
            var dbo = db.db(appTokenDetails.DBName);
            dbo.collection(appTokenDetails.collectionName).find({"UserId": userInfo.id}).toArray(function (err, result) {
              if (result.length > 0 ){
                var dbExpireDate = exportedRequiredModules.moment(result[0].expireDateTime,'YYYY-MM-DDTHH:mm:ss.SSS[Z]').toISOString();
                // var currentDate1 = exportedRequiredModules.moment();
                var currentDate = exportedRequiredModules.moment().toISOString();
                if(dbExpireDate < currentDate){
                  dbo.collection(appTokenDetails.collectionName).deleteOne({"UserId": userInfo.id}, function(err, deletedResults) {
                    dbo.collection(appTokenDetails.collectionName).insertOne({"UserId":  userInfo.id,
                      "Token":	exportedRequiredModules.sessObj[userInfo.id],
                      "expireDateTime": expireDate,
                      "TokenStatus": "New",
                      "CreatedDate": exportedRequiredModules.moment().toISOString(),
                      "UpdatedDate": exportedRequiredModules.moment().toISOString()
                      });
                    exportedRequiredModules.sessObj["Status"] = "New";
                    db.close();
                    if(localMachine.includes('http') == true){
                      res.redirect(localMachine+'/'+basePath+'/valid?Id=' + userInfo.id+'&expireDateTime='+expireDate);
                    }else{
                      res.redirect('http://'+localMachine+':'+port+'/'+basePath+'/valid?Id=' + userInfo.id+'&expireDateTime='+expireDate);
                    }
                  });
                }else{
                  exportedRequiredModules.sessObj[userInfo.id] = result[0].Token;
                  exportedRequiredModules.sessObj["Status"] = "Old";
                  dbo.collection(appTokenDetails.collectionName).updateOne({UserId : userInfo.id}, { $set : {UpdatedDate : exportedRequiredModules.moment().toISOString(),expireDateTime : expireDate, TokenStatus : "Old"}}, {upsert:true});
                  db.close();
                  if(localMachine.includes('http') == true){
                    res.redirect(localMachine+'/'+basePath+'/valid?Id=' + userInfo.id+'&expireDateTime='+expireDate);
                  }else{
                    res.redirect('http://'+localMachine+':'+port+'/'+basePath+'/valid?Id=' + userInfo.id+'&expireDateTime='+expireDate);
                  }
                }
              }else{
                
                dbo.collection(appTokenDetails.collectionName).insertOne({"UserId":  userInfo.id,
                      "Token":	exportedRequiredModules.sessObj[userInfo.id],
                      "expireDateTime": expireDate,
                      "TokenStatus": "New",
                      "CreatedDate": exportedRequiredModules.moment().toISOString(),
                      "UpdatedDate": exportedRequiredModules.moment().toISOString()
                      });
                exportedRequiredModules.sessObj["Status"] = "New";
                db.close();
                if(localMachine.includes('http') == true){
                  res.redirect(localMachine+'/'+basePath+'/valid?Id=' + userInfo.id+'&expireDateTime='+expireDate);
                }else{
                  res.redirect('http://'+localMachine+':'+port+'/'+basePath+'/valid?Id=' + userInfo.id+'&expireDateTime='+expireDate);
                }
              }
              
            })
            
          }
        })
  });

});

app.get("/"+basePath+"/valid", function(req, res) {
  // console.log(req.headers)
  exportedRequiredModules.sessObj=req.session;
  exportedRequiredModules.MongoClient.connect(exportedRequiredModules.mongoDBConnectionString, function(err, db) {
    var appTokenDetails = exportedRequiredModules.credentialsObj.AppToken;
    var dbo = db.db(appTokenDetails.DBName);
    if(err){
      // console.log('Failed - MongoDB connection Failed');
      res.send({"Error" : "MongoDB connection Failed"});
    }else{
      exportedRequiredModules.getSubscriptionObj.getSubscriptionJSON(req.query.Id, function (error, subscriptionList) {
                            
        if (error) {
              dbo.collection(appTokenDetails.collectionName).deleteOne({"UserId": req.query.Id}, function(err, results) {
                db.close();
                res.status(404);
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify(error, null, 3));
            });
        }else{
          dbo.collection(appTokenDetails.collectionName).find({"UserId": req.query.Id}).toArray(function (err, result) {
            db.close();
            if (result.length > 0 ){
              res.send({"UserId" : result[0].UserId, "UserToken" : result[0].Token, "expireDateTime" : result[0].expireDateTime, "Status" : result[0].TokenStatus});
            }else{
              res.send("Token is not generated - "+req.query.Id)
            }
            
          })
        }
      });
    }
  })
});
module.exports = app;
var sourcePath = exportedRequiredModules.credentialsObj.ESWrapperPath.path;

/******************************************************************************
SwaggerExpress is integrated with Swagger and Express Server
*******************************************************************************/
SwaggerExpress.create(config, function (err, swaggerExpress) {
  if (err) {
    throw err;
  }
  process.on('uncaughtException', function (err) {
    exportedRequiredModules.loggerObj.error(err.stack);
  });
  // install middleware
  swaggerExpress.register(app);
  
  /* call the listen method to start the service */
  app.listen(port);

});